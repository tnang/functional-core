 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.compression;

import java.net.URL;
import jema.common.types.CV_WebResource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class CompressTextTest {
    
    public CompressTextTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getResource method, of class CompressText.
     */
    @Test
    public void testGetResource() {
        try{
            System.out.println("* Compression JUnit4Test: CompressTextTest : testGetResource()");
            CompressText instance = new CompressText();
            instance.in = new CV_WebResource(new URL("http://somewhere"));
            URL expResult = new URL("http://somewhere");
            URL result = instance.getResource();
            assertEquals(expResult, result);
        }catch(Exception e){
           fail(e.getMessage());
        }
    }

    /**
     * Test of getExtension method, of class CompressText.
     */
    @Test
    public void testGetExtension() {
        System.out.println("* Compression JUnit4Test: CompressTextTest : testGetExtension()");
        CompressText instance = new CompressText();
        String expResult = ".txt";
        String result = instance.getExtension();
        assertEquals(expResult, result);
    }
    
}
