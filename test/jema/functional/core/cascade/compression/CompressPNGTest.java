/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.compression;

import java.net.URL;
import jema.common.types.CV_WebResource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class CompressPNGTest {
    
    public CompressPNGTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getResource method, of class CompressPNG.
     */
    @Test
    public void testGetResource() {
        try{
            System.out.println("* Compression JUnit4Test: CompressPNGTest : testGetResource()");
            CompressPNG instance = new CompressPNG();
            instance.in = new CV_WebResource(new URL("http://somewhere"));
            URL expResult = new URL("http://somewhere");
            URL result = instance.getResource();
            assertEquals(expResult, result);
        }catch(Exception e){
           fail(e.getMessage());
        }
    }

    /**
     * Test of getExtension method, of class CompressPNG.
     */
    @Test
    public void testGetExtension() {
        System.out.println("* Compression JUnit4Test: CompressPNGTest : testGetExtension()");
        CompressPNG instance = new CompressPNG();
        String expResult = ".png";
        String result = instance.getExtension();
        assertEquals(expResult, result);
    }
    
}
