 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import jema.common.types.CV_Boolean;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import jema.common.types.CV_Integer;

/**
 *
 * @author CASCADE
 */
public class IntegerInRangeTest {
    
    public IntegerInRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isIntegerInRange( method, of class IntegerInRange.
     */
    @Test
    public void testIsIntegerInRange() {
        System.out.println("* Conditionals JUnit4Test: IntegerInRangeTest : testIsIntegerInRange(()");
        CV_Integer low = new CV_Integer(0);
        CV_Integer high = new CV_Integer(10);
        CV_Integer n =  new CV_Integer(5);
        IntegerInRange instance = new IntegerInRange();
        boolean expResult = true;
        boolean result = instance.isIntegerInRange(low, high, n);
        assertEquals(expResult, result);
        
        low = new CV_Integer(5);
        high = new CV_Integer(5);
        n = new CV_Integer(5);
        instance = new IntegerInRange();
        instance.isOpenInterval = new CV_Boolean(false);
        expResult = true;
        result = instance.isIntegerInRange(low, high, n);
        assertEquals(expResult, result);
        
        low = new CV_Integer(5);
        high = new CV_Integer(5);
        n = new CV_Integer(5);
        instance = new IntegerInRange();
        instance.isOpenInterval = new CV_Boolean(true);
        expResult = false;
        result = instance.isIntegerInRange(low, high, n);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of run method, of class IntegerInRange.
     */
    @Test
    public void testRun() {
        try{       
            System.out.println("* Conditionals JUnit4Test: IntegerInRangeTest : testRun()");
            IntegerInRange instance = new IntegerInRange();
            instance.integerToCheck = new CV_Integer(5);
            instance.lowerBoundInteger = new CV_Integer(0);
            instance.upperBoundInteger = new CV_Integer(10);
            boolean result = instance.call();
            assertTrue("Default run - integer within bounds",result);

            instance = new IntegerInRange();
            instance.integerToCheck = new CV_Integer(5);
            instance.lowerBoundInteger = new CV_Integer(5);
            instance.upperBoundInteger = new CV_Integer(5);
            instance.isOpenInterval = new CV_Boolean(false);
            result = instance.call();
            assertTrue("This is true when isOpenInterval is false and all values are the same.",result);

            instance = new IntegerInRange();
            instance.integerToCheck = new CV_Integer(5);
            instance.lowerBoundInteger = new CV_Integer(5);
            instance.upperBoundInteger= new CV_Integer(5);
            instance.isOpenInterval = new CV_Boolean(true);
            result = instance.call();
            assertFalse("This is false when isOpenInterval is true and all values are the same.",result);
        
        }catch(Exception e){
            fail(e.getMessage());
        } 
    }
   
     /**
     * Test of intervalContains method, of class IntegerInRange for a expected exception.
     * In this case I expect to see an exception because the low value is higher than the high value.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Conditionals JUnit4Test: IntegerInRangeTest : testExpectedException()");
        CV_Integer low = new CV_Integer(5);
        CV_Integer high = new CV_Integer(4);
        CV_Integer n = new CV_Integer(0);
        IntegerInRange instance = new IntegerInRange();
        boolean result = instance.isIntegerInRange(low, high, n);
    }
    
}
//UNCLASSIFIED