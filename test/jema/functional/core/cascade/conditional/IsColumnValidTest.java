package jema.functional.core.cascade.conditional;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

/**
 * Unit test cases for IsColumnValid functional.
 * @author CASCADE
 *
 */
public class IsColumnValidTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(IsColumnValidTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private static final String CSV_FILE = "germany_points.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * IsColumnValid test - true
	 */
	@Test
	public void testIsColumnValidTrue()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			CV_Table srcTable = ctx.createTableFromCSV(
				this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);                

			// Expected results
			TableHeader tableHeader = srcTable.getHeader();
			ColumnHeader colHeader = tableHeader.getHeader(0);

			// Initialize functional
			IsColumnValid func = new IsColumnValid();
			func.srcTable = srcTable;
			func.columnName = CV_String.valueOf(colHeader.getName());
			func.columnType = CV_String.valueOf(colHeader.getType().name());
			func.ctx = ctx;

			// Run functional
			Boolean result = func.call();
			
			if(srcTable != null) {srcTable.close();}
			assertTrue("Valid column name and type", result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * IsColumnValid test - false
	 */
	@Test
	public void testIsColumnValidFalse()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			CV_Table srcTable = ctx.createTableFromCSV(
				this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);                

			// Expected results
			TableHeader tableHeader = srcTable.getHeader();
			ColumnHeader colHeader = tableHeader.getHeader(0);

			// Initialize functional
			IsColumnValid func = new IsColumnValid();
			func.srcTable = srcTable;
			func.columnName = CV_String.valueOf(colHeader.getName());
			func.columnType = CV_String.valueOf(ParameterType.table.name());
			func.ctx = ctx;

			// Run functional
			Boolean result = func.call();
			
			if(srcTable != null) {srcTable.close();}
			assertTrue("Mismatched column name and type", !result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);                

			// Expected results
			TableHeader tableHeader = srcTable.getHeader();
			ColumnHeader colHeader = tableHeader.getHeader(0);

			try
			{
				IsColumnValid func = new IsColumnValid();
				func.srcTable = null;
				func.columnName = CV_String.valueOf(colHeader.getName());
				func.columnType = CV_String.valueOf(colHeader.getType().name());
				func.ctx = ctx;
				func.call();
				assertTrue("Source Table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				IsColumnValid func = new IsColumnValid();
				func.srcTable = srcTable;
				func.columnName = CV_String.valueOf(colHeader.getName());
				func.columnType = null;
				func.ctx = ctx;
				func.call();
				assertTrue("Column type is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				IsColumnValid func = new IsColumnValid();
				func.srcTable = srcTable;
				func.columnName = CV_String.valueOf(colHeader.getName());
				func.columnType = CV_String.valueOf("BadType");
				func.ctx = ctx;
				func.call();
				assertTrue("Column type is invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
