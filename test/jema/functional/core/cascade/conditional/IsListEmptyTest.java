/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 22, 2015 7:18:10 PM
 */
package jema.functional.core.cascade.conditional;

import java.util.Arrays;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class IsListEmptyTest {

    /**
     * Test of call method, of class IsListEmpty.
     */
    @Test
    public void testCall() {
        System.out.println("* Conditionals JUnit4Test: IsListEmptyTest : testCall(()");

        //Test list of strings
        IsListEmpty instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String("test"), new CV_String("test"), new CV_Integer(12), new CV_Length(23.9), null);
        Boolean result = instance.call();
        assertFalse("List is not empty", result);

        //Test empty list
        instance = new IsListEmpty();
        instance.inputList = Arrays.asList();
        result = instance.call();
        assertTrue("List is empty", result);
    }

    /**
     * Test of call method, of class IsListEmpty.
     */
    @Test
    public void testCall_ignoreEmpties() {
        System.out.println("* Conditionals JUnit4Test: IsListEmptyTest : testCall_ignoreEmpties()");

        //Test list of null values and ignoreEmpties = false
        IsListEmpty instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String(null), null);
        instance.ignoreEmpties = new CV_Boolean(false);
        Boolean result = instance.call();
        assertFalse("List is not empty", result);

        //Test list of null values and ignoreEmpties = true
        instance.ignoreEmpties = new CV_Boolean(true);
        result = instance.call();
        assertTrue("List is empty", result);

        //Test list of empty strings and ignoreEmpties = false
        instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String(""), new CV_String(""));
        instance.ignoreEmpties = new CV_Boolean(false);
        result = instance.call();
        assertFalse("List is not empty", result);

        //Test list of empty strings and ignoreEmpties = true
        instance.ignoreEmpties = new CV_Boolean(true);
        result = instance.call();
        assertTrue("List is empty", result);

        //Test list containing null and string and ignoreEmpties = true
        instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String(null), new CV_String("a"));
        instance.ignoreEmpties = new CV_Boolean(true);
        result = instance.call();
        assertFalse("List is not empty", result);

        //Test list containing empty string and null and ignoreEmpties = true
        instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String(""), null);
        instance.ignoreEmpties = new CV_Boolean(true);
        result = instance.call();
        assertTrue("List is empty", result);

        //Test list containing no empty or null values and ignoreEmpties = true
        instance = new IsListEmpty();
        instance.inputList = Arrays.asList(new CV_String("test"), new CV_String("test"), new CV_Integer(12), new CV_Length(23.9));
        instance.ignoreEmpties = new CV_Boolean(true);
        result = instance.call();
        assertFalse("List is not empty", result);
    }
}
