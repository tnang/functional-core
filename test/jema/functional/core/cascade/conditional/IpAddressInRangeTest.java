/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jema.functional.core.cascade.conditional;

import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class IpAddressInRangeTest {
    
    public IpAddressInRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class IpAddressInRange.
     */
    @Test
    public void testRun() {
        System.out.println("* Conditionals JUnit4Test: IpAddressInRange : testRun(()");
        IpAddressInRange instance;
        
        System.out.println("\ttesting ipv4 inputs in range");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("127.0.0.1");
        instance.inputCidr = new CV_String("127.0.0.0/24");        
        instance.run();
        assertTrue(instance.outputRangeCheck.value());
        assertEquals("IP: ipv4 CIDR: ipv4", instance.outputValidator.value());
        assertEquals("127.0.0.0 - 127.0.0.255", instance.outputRange.value());
        
        System.out.println("\ttesting ipv4 input out of range");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("74.125.30.103");
        instance.inputCidr = new CV_String("127.0.0.0/24");        
        instance.run();
        assertFalse(instance.outputRangeCheck.value());
        assertEquals("IP: ipv4 CIDR: ipv4", instance.outputValidator.value());
        assertEquals("127.0.0.0 - 127.0.0.255", instance.outputRange.value());
        
        System.out.println("\ttesting ipv6 input in range #1");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("BB06:1234:4321:2121:0202:B3FF:FE1E:8329");
        instance.inputCidr = new CV_String("BB06:1234:4321:2121::/64");        
        instance.run();
        assertTrue(instance.outputRangeCheck.value());
        assertEquals("IP: ipv6 CIDR: ipv6", instance.outputValidator.value());
        assertEquals("bb06:1234:4321:2121:0:0:0:0 - bb06:1234:4321:2121:ffff:ffff:ffff:ffff", instance.outputRange.value());
    
        System.out.println("\ttesting ipv6 input in range #2");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("BB06:1234::FE1E:8329");
        instance.inputCidr = new CV_String("BB06:1234:4321:2121::/24");        
        instance.run();
        assertTrue(instance.outputRangeCheck.value());
        assertEquals("IP: ipv6 CIDR: ipv6", instance.outputValidator.value());
        assertEquals("bb06:1200:0:0:0:0:0:0 - bb06:12ff:ffff:ffff:ffff:ffff:ffff:ffff", instance.outputRange.value());
        
        System.out.println("\ttesting ipv6 input out of range #1");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("BB06:18ff::FE1E:8329");
        instance.inputCidr = new CV_String("BB06:1234:4321:2121::/24");        
        instance.run();
        assertFalse(instance.outputRangeCheck.value());
        assertEquals("IP: ipv6 CIDR: ipv6", instance.outputValidator.value());
        assertEquals("bb06:1200:0:0:0:0:0:0 - bb06:12ff:ffff:ffff:ffff:ffff:ffff:ffff", instance.outputRange.value());
        
        System.out.println("\ttesting ipv6 input out of range #2");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("BB06:18ff:4321:1212:0202:5555:FE1E:8329");
        instance.inputCidr = new CV_String("BB06:1234:4321:2121::/24");        
        instance.run();
        assertFalse(instance.outputRangeCheck.value());
        assertEquals("IP: ipv6 CIDR: ipv6", instance.outputValidator.value());
        assertEquals("bb06:1200:0:0:0:0:0:0 - bb06:12ff:ffff:ffff:ffff:ffff:ffff:ffff", instance.outputRange.value());
        
        System.out.println("\ttesting ipv6 ipv4 mis-match");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("BB06:18ff:4321:1212:0202:5555:FE1E:8329");
        instance.inputCidr = new CV_String("192.168.0.0/28");        
        instance.run();

        //assertFalse(instance.outputRangeCheck.value());
        //assertEquals("IP: ipv6 CIDR: ipv4", instance.outputValidator.value());
        //assertEquals("192.168.0.0 - 192.168.0.15", instance.outputRange.value());
        
        System.out.println("\ttesting ipv4 ipv6 mis-match");
        instance = new IpAddressInRange();
        instance.inputAddress = new CV_String("74.125.30.103");
        instance.inputCidr = new CV_String("FFFF::/28");        
        instance.run();
        //assertFalse(instance.outputRangeCheck.value());
        assertEquals("IP: ipv4 CIDR: ipv6", instance.outputValidator.value());
        assertEquals("ffff:0:0:0:0:0:0:0 - ffff:f:ffff:ffff:ffff:ffff:ffff:ffff", instance.outputRange.value());
    }
    
}
