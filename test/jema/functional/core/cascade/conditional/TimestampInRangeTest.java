 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import jema.common.types.CV_Timestamp;

/**
 *
 * @author CASCADE
 */
public class TimestampInRangeTest {
    
    public TimestampInRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isTimestampInRange() method, of class TimestampInRange.
     */
    @Test
    public void testIsTimestampInRange() {
        System.out.println("* Conditionals JUnit4Test: TimestampInRangeTest : testIsTimestampInRange(()");
        //expected result is true because n value is between low and high 
        CV_Timestamp low = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        CV_Timestamp high = new CV_Timestamp("2014-09-26T16:09:44.328Z");
        CV_Timestamp n = new CV_Timestamp("2014-09-26T16:09:43.328Z");
        TimestampInRange instance = new TimestampInRange();
        boolean expResult = true;
        boolean result = instance.isTimestampInRange(low, high, n);
        assertEquals(expResult, result);
        
        //expected result is false because all values are the same
        low = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        high = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        n = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        instance = new TimestampInRange();
        expResult = false;
        result = instance.isTimestampInRange(low, high, n);
        assertEquals(expResult, result);
        

       //expected result is false because n value is outside range
        low = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        high = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        n = new CV_Timestamp("2014-09-26T16:09:45.328Z");
        instance = new TimestampInRange();
        expResult = false;
        result = instance.isTimestampInRange(low, high, n);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of run method, of class TimestampInRange.
     */
    @Test
    public void testRun() {
        try{       
            System.out.println("* Conditionals JUnit4Test: TimestampInRangeTest : testRun()");
            TimestampInRange instance = new TimestampInRange();
            instance.timestampToCheck = new CV_Timestamp("2014-09-26T16:09:43.328Z");
            instance.lowerBoundTimestamp =  new CV_Timestamp("2014-09-26T16:09:42.328Z");
            instance.upperBoundTimestamp =  new CV_Timestamp("2014-09-26T16:09:44.328Z");
            boolean result = instance.call();
            assertTrue("Default run - timestamp within bounds",result);

        
        }catch(Exception e){
            fail(e.getMessage());
        } 
    }
   
     /**
     * Test of isTimestampInRange method, of class TimestampInRange for a expected exception.
     * In this case I expect to see an exception because the low value is higher than the high value.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Conditionals JUnit4Test: TimestampInRangeTest : testExpectedException()");
        CV_Timestamp low = new CV_Timestamp("2014-09-26T16:09:44.328Z");
        CV_Timestamp high = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        CV_Timestamp n = new CV_Timestamp("2014-09-26T16:09:42.328Z");
        TimestampInRange instance = new TimestampInRange();
        boolean result = instance.isTimestampInRange(low, high, n);
    }
    
}
//UNCLASSIFIED