 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author CASCADE
 */
public class DecimalInRangeTest {
    
    public DecimalInRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isDecimalInRange method, of class DecimalInRange.
     */
    @Test
    public void testIsDecimalInRange() {
        System.out.println("* Conditionals JUnit4Test: DecimalInRangeTest : testIsDecimalInRange()");
        CV_Decimal low = new CV_Decimal(5.4);
        CV_Decimal high = new CV_Decimal(5.6);
        CV_Decimal n = new CV_Decimal(5.5);
        DecimalInRange instance = new DecimalInRange();
        boolean expResult = true;
        boolean result = instance.isDecimalInRange(low, high, n);
        assertEquals(expResult, result);
        
        
        low = new CV_Decimal(5.0);
        high = new CV_Decimal(5.0);
        n = new CV_Decimal(5.0);
        instance = new DecimalInRange();
        instance.isOpenInterval = new CV_Boolean(false);
        expResult = true;
        result = instance.isDecimalInRange(low, high, n);
        assertEquals(expResult, result);
        
        low = new CV_Decimal(5.0);
        high = new CV_Decimal(5.0);
        n = new CV_Decimal(5.0);
        instance = new DecimalInRange();
        instance.isOpenInterval = new CV_Boolean(true);
        expResult = false;
        result = instance.isDecimalInRange(low, high, n);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of call method, of class DecimalInRange.
     */
    @Test
    public void testCall() {
        try{
            System.out.println("* Conditionals JUnit4Test: DecimalInRangeTest : testRun()");
            DecimalInRange instance = new DecimalInRange();
            instance.decimalToCheck = new CV_Decimal(5.0);
            instance.lowerBoundDecimal = new CV_Decimal(0.5);
            instance.upperBoundDecimal= new CV_Decimal(10.1);
            Boolean result = instance.call();
            assertTrue("Default run - decimal within bounds",result);
            instance = new DecimalInRange();
            
            instance.decimalToCheck = new CV_Decimal(5.0);
            instance.lowerBoundDecimal = new CV_Decimal(5.0);
            instance.upperBoundDecimal= new CV_Decimal(5.0);
            instance.isOpenInterval = new CV_Boolean(false);
            result = instance.call();
            assertTrue("This is true when isOpenInterval is false and all values are the same.",result);

            instance = new DecimalInRange();
            instance.decimalToCheck = new CV_Decimal(5.0);
            instance.lowerBoundDecimal = new CV_Decimal(5.0);
            instance.upperBoundDecimal= new CV_Decimal(5.0);
            instance.isOpenInterval = new CV_Boolean(true);
            result = instance.call();
            assertFalse("This is false when isOpenInterval is true and all values are the same.",result);
        }catch(Exception e){
            fail(e.getMessage());
        }
    }
   
     /**
     * Test of intervalContains method, of class DecimalInRange for a expected exception.
     * In this case I expect to see an exception because the low value is higher than the high value.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Conditionals JUnit4Test: DecimalInRangeTest : testExpectedException()");
        CV_Decimal low = new CV_Decimal(10.1);
        CV_Decimal high = new CV_Decimal(8.5);
        CV_Decimal n = new CV_Decimal(9.0);
        DecimalInRange instance = new DecimalInRange();
        boolean result = instance.isDecimalInRange(low, high, n);
    }
    
}
//UNCLASSIFIED
