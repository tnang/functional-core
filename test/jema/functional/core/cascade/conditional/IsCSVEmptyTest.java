package jema.functional.core.cascade.conditional;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_WebResource;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

/**
 * Unit test cases for IsCSVEmpty functional.
 * @author CASCADE
 *
 */
public class IsCSVEmptyTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(IsCSVEmptyTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private static final String CSV_FILE = "Employee.csv";

	/** Empty CSV */
	private static final String CSV_FILE_EMPTY = "EmployeeEmpty.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * IsCSVEmpty test - true
	 */
	@Test
	public void testIsCSVEmptyTrue()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			URL url = this.getClass().getResource("/data/" + CSV_FILE_EMPTY);
			CV_WebResource webResource = new CV_WebResource(url);

			// Initialize functional
			IsCSVEmpty func = new IsCSVEmpty();
			func.in = webResource;
			func.ctx = ctx;

			// Run functional
			Boolean isEmpty = func.call();			
			assertTrue("CSV File is empty", isEmpty);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * IsCSVEmpty test - false
	 */
	@Test
	public void testIsCSVEmptyFalse()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			URL url = this.getClass().getResource("/data/" + CSV_FILE);
			CV_WebResource webResource = new CV_WebResource(url);

			// Initialize functional
			IsCSVEmpty func = new IsCSVEmpty();
			func.in = webResource;
			func.ctx = ctx;

			// Run functional
			Boolean isEmpty = func.call();			
			assertTrue("CSV File is not empty", !isEmpty);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				IsCSVEmpty func = new IsCSVEmpty();
				func.in = null;
				func.ctx = ctx;
				func.call();
				assertTrue("CSV input URL is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CV_WebResource webResourceBad = new CV_WebResource("BadWR");
				IsCSVEmpty func = new IsCSVEmpty();
				func.in = webResourceBad;
				func.ctx = ctx;
				func.call();
				assertTrue("CSV input URL is invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
