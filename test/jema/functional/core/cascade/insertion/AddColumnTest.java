package jema.functional.core.cascade.insertion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Integer;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class AddColumnTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(AddColumnTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private final static String CSV_FILE = "Department.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * AddColumn test
	 */
	@Test
	public void testAddColumn()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  

			CV_String columnName = CV_String.valueOf("NewPoint");
			CV_String columnType =  CV_String.valueOf(ParameterType.point.name());
			CV_Integer columnIndex = CV_Integer.valueOf(1);
			CV_Point defaultValue = new CV_Point("POINT(5.5 10.10)");

			// Initialize functional
			AddColumn func = new AddColumn();
			func.srcTable = srcTable;
			func.columnName = columnName;
			func.columnType = columnType;
			func.columnIndex = columnIndex;
			func.defaultValue = defaultValue;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * AddColumn test
	 */
	@Test
	public void testAddColumnIndex()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  

			CV_String columnName = CV_String.valueOf("NewColumn");
			CV_String columnType =  CV_String.valueOf(ParameterType.string.name());
			CV_Integer columnIndex = null;
			CV_String defaultValue = CV_String.valueOf("DefaultValue");

			// Initialize functional
			AddColumn func = new AddColumn();
			func.srcTable = srcTable;
			func.columnName = columnName;
			func.columnType = columnType;
			func.columnIndex = columnIndex;
			func.defaultValue = defaultValue;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * AddColumn test
	 */
	@Test
	public void testAddColumnDefaultValue()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  

			CV_String columnName = CV_String.valueOf("NewColumn");
			CV_String columnType =  CV_String.valueOf(ParameterType.string.name());
			CV_Integer columnIndex = CV_Integer.valueOf(1);
			CV_String defaultValue = null;

			// Initialize functional
			AddColumn func = new AddColumn();
			func.srcTable = srcTable;
			func.columnName = columnName;
			func.columnType = columnType;
			func.columnIndex = columnIndex;
			func.defaultValue = defaultValue;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  

			CV_String columnName = CV_String.valueOf("NewColumn");
			CV_String columnType =  CV_String.valueOf(ParameterType.string.name());
			CV_Integer columnIndex = CV_Integer.valueOf(1);
			CV_String defaultValue = CV_String.valueOf("DefaultValue");

			try
			{
				AddColumn func = new AddColumn();
				func.srcTable = null;
				func.columnName = columnName;
				func.columnType = columnType;
				func.columnIndex = columnIndex;
				func.defaultValue = defaultValue;
				func.ctx = ctx;
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumn func = new AddColumn();
				func.srcTable = srcTable;
				func.columnName = null;
				func.columnType = columnType;
				func.columnIndex = columnIndex;
				func.defaultValue = defaultValue;
				func.ctx = ctx;
				func.run();
				assertTrue("Column name is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumn func = new AddColumn();
				func.srcTable = srcTable;
				func.columnName = columnName;
				func.columnType = null;
				func.columnIndex = columnIndex;
				func.defaultValue = defaultValue;
				func.ctx = ctx;
				func.run();
				assertTrue("Column type is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumn func = new AddColumn();
				func.srcTable = srcTable;
				func.columnName = columnName;
				CV_String badColumnType = CV_String.valueOf("BadColumnType");
				func.columnType = badColumnType;
				func.columnIndex = columnIndex;
				func.defaultValue = defaultValue;
				func.ctx = ctx;
				func.run();
				assertTrue("Column type is invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumn func = new AddColumn();
				func.srcTable = srcTable;
				func.columnName = columnName;
				func.columnType = columnType;
				CV_Integer badColumnIndex = CV_Integer.valueOf(999);
				func.columnIndex = badColumnIndex;
				func.defaultValue = defaultValue;
				func.ctx = ctx;
				func.run();
				assertTrue("Column index is out of range" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
