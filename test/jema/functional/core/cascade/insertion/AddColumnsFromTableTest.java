package jema.functional.core.cascade.insertion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class AddColumnsFromTableTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(AddColumnsFromTableTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private final static String CSV_FILE = "Department.csv";

	/** Donor CSV */
	private final static String CSV_FILE_DONOR = "Employee.csv";

	/** Donor CSV with WKT parameter type */
	private static final String CSV_FILE_GERMANY = "germany_points.csv";
	
	/** Donor coloumn prefix */
	private final static String PREFIX = "DC";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * AddColumnsFromTable test
	 */
	@Test
	public void testAddColumnsFromTable()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  
			CV_Table srcDonorTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_DONOR), null); 

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			TableHeader header = srcDonorTable.getHeader();
			for(ColumnHeader colHeader : header.asList())
			{
				columnNameList.add(CV_String.valueOf(colHeader.getName()));
			}

			// Initialize functional
			AddColumnsFromTable func = new AddColumnsFromTable();
			func.srcTable = srcTable;
			func.srcDonorTable = srcDonorTable;
			func.columnNameList = columnNameList;
			func.donorColumnPrefix = CV_String.valueOf(PREFIX);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(srcDonorTable != null) {destTable.close();}
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * AddColumnsFromTable test
	 */
	@Test
	public void testAddColumnsFromTableWKT()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  
			CV_Table srcDonorTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null); 

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			TableHeader header = srcDonorTable.getHeader();
			for(ColumnHeader colHeader : header.asList())
			{
				if(colHeader.getType().equals(ParameterType.point))
				{
					columnNameList.add(CV_String.valueOf(colHeader.getName()));
				}
			}

			// Initialize functional
			AddColumnsFromTable func = new AddColumnsFromTable();
			func.srcTable = srcTable;
			func.srcDonorTable = srcDonorTable;
			func.columnNameList = columnNameList;
			func.donorColumnPrefix = CV_String.valueOf(PREFIX);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(srcDonorTable != null) {destTable.close();}
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * AddColumnsFromTable test - no prefix
	 */
	@Test
	public void testAddColumnsFromTableNoPrefix()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  
			CV_Table srcDonorTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_DONOR), null); 

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			TableHeader header = srcDonorTable.getHeader();
			for(ColumnHeader colHeader : header.asList())
			{
				columnNameList.add(CV_String.valueOf(colHeader.getName()));
			}

			// Initialize functional
			AddColumnsFromTable func = new AddColumnsFromTable();
			func.srcTable = srcTable;
			func.srcDonorTable = srcDonorTable;
			func.columnNameList = columnNameList;
			func.donorColumnPrefix = null;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			if(srcDonorTable != null) {destTable.close();}
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);  
			CV_Table srcDonorTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_DONOR), null); 

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			TableHeader header = srcDonorTable.getHeader();
			for(ColumnHeader colHeader : header.asList())
			{
				columnNameList.add(CV_String.valueOf(colHeader.getName()));
			}

			try
			{
				AddColumnsFromTable func = new AddColumnsFromTable();
				func.srcTable = null;
				func.srcDonorTable = srcDonorTable;
				func.columnNameList = columnNameList;
				func.donorColumnPrefix = CV_String.valueOf(PREFIX);
				func.ctx = ctx;
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumnsFromTable func = new AddColumnsFromTable();
				func.srcTable = srcTable;
				func.srcDonorTable = null;
				func.columnNameList = columnNameList;
				func.donorColumnPrefix = CV_String.valueOf(PREFIX);
				func.ctx = ctx;
				func.run();
				assertTrue("Donor table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumnsFromTable func = new AddColumnsFromTable();
				func.srcTable = srcTable;
				func.srcDonorTable = srcDonorTable;
				func.columnNameList = null;
				func.donorColumnPrefix = CV_String.valueOf(PREFIX);
				func.ctx = ctx;
				func.run();
				assertTrue("Column name list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				AddColumnsFromTable func = new AddColumnsFromTable();
				func.srcTable = srcTable;
				func.srcDonorTable = srcDonorTable;
				List<CV_String> columnNameListBad = new ArrayList<CV_String>();
				columnNameListBad.add(CV_String.valueOf("BadName"));
				func.columnNameList = columnNameListBad;
				func.donorColumnPrefix = CV_String.valueOf(PREFIX);
				func.ctx = ctx;
				func.run();
				assertTrue("Stop index and Length are set" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
