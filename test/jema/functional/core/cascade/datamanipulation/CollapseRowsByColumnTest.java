/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.filter.DeduplicateRowsTest;
import jema.functional.core.cascade.filter.FilterTableOddEvenTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author warriner
 */
public class CollapseRowsByColumnTest {
    
        
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    public CollapseRowsByColumnTest() {
    }

    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row6 = {new CV_String("row6"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable = testTable.toReadable();
            
            

        } catch (Exception ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class CollapseRowsByColumn.
     * This test ensures a collapsed column and a input deliminator.
     */
    @Test
    public void testRun() throws TableException {
        System.out.println("* Datamanipulation JUnit4Test: CollapseRowsByColumn :  testRun()");
        CollapseRowsByColumn instance = new CollapseRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.deliminator = new CV_String("|");
        instance.columnName = new CV_String("test");
        instance.run();

        int rowcount_result = 0;
        rowcount_result = instance.outputTable.size();

        int expected_rowcount_result = 3;

        assertEquals(expected_rowcount_result, rowcount_result);

        final CV_Table table = instance.outputTable.toReadable();
        table.stream().forEach((List<CV_Super> current_row) -> {

            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd|even");

                } else if (table.getCurrentRowNum() == 1) {

                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd|even|row6");

                } else if (table.getCurrentRowNum() == 2) {

                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd");

                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

    }
    
    /**
     * Test of run method, of class CollapseRowsByColumn.
     * This test ensure a default deliminator of a space is used if a deliminator is not provided.
     */
    @Test
    public void testRun2() throws TableException {
        System.out.println("* Datamanipulation JUnit4Test: CollapseRowsByColumn :  testRun2()");
        CollapseRowsByColumn instance = new CollapseRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        //instance.deliminator = new CV_String("|");
        instance.columnName = new CV_String("test");
        instance.run();

        int rowcount_result = 0;
        rowcount_result = instance.outputTable.size();

        int expected_rowcount_result = 3;

        assertEquals(expected_rowcount_result, rowcount_result);

        final CV_Table table = instance.outputTable.toReadable();
        table.stream().forEach((List<CV_Super> current_row) -> {

            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd even");

                } else if (table.getCurrentRowNum() == 1) {

                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd even row6");

                } else if (table.getCurrentRowNum() == 2) {

                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd");

                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

    }
    
    /**
     * Test of run method, of class CollapseRowsByColumn.
     * This test ensures a empty table as input will not affect the functional
     */
    @Test
    public void testRun3() throws TableException, Exception {
        System.out.println("* Datamanipulation JUnit4Test: CollapseRowsByColumn :  testRun3()");

        test_ctx = new ExecutionContextImpl();
        testTable = test_ctx.createTable("Test2");
        testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
        testTable = testTable.toReadable();

        CollapseRowsByColumn instance = new CollapseRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnName = new CV_String("test");
        instance.run();
        
        final CV_Table table = instance.outputTable.toReadable();
        
        int rowcount_result = 0;
        rowcount_result = table.size();

        int expected_rowcount_result = 0;

        assertEquals(expected_rowcount_result, rowcount_result);

    }
    

    /**
     * Test of testExpectedException method, of class CollapseRowsByColumn for a expected
     * exception stemmed from a misspelled Column Name.
     *
     */
    @Test(expected = RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Datamanipulation JUnit4Test: CollapseRowsByColumn :  testExpectedException()");
        CollapseRowsByColumn instance = new CollapseRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.deliminator = new CV_String("|");
        instance.columnName = new CV_String("badColumnName");
        instance.run();

    }
    
    
}
