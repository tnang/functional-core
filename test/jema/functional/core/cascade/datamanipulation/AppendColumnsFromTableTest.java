/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 8, 2015 9:10:46 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class AppendColumnsFromTableTest {

    ExecutionContext test_ctx;
    CV_Table empTable, deptTable;
    String method = "* DataManipulation JUnit4Test: AppendColumnsFromTable : ";

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            empTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Employee.csv"), null).toReadable();
            deptTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Department.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(AppendColumnsFromTableTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class AppendColumnsFromTable.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_columnExists() throws Exception {
        System.out.println(method + "testRun_columnExists()");

        AppendColumnsFromTable instance = new AppendColumnsFromTable();
        instance.ctx = test_ctx;
        instance.targetTable = empTable;
        instance.appendTable = deptTable;
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(3);
                    assertEquals("Charles", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(AppendColumnsFromTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class AppendColumnsFromTable.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_selectColumns() throws Exception {
        System.out.println(method + "testRun_selectColumns()");

        AppendColumnsFromTable instance = new AppendColumnsFromTable();
        instance.ctx = test_ctx;
        instance.targetTable = deptTable;
        instance.appendTable = empTable;
        CV_String[] columnNames = {new CV_String("name"), new CV_String("employeeid")};
        instance.appendColumnNames = new ArrayList<>(Arrays.asList(columnNames));
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Production", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals("George", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(3);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("George", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(AppendColumnsFromTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class AppendColumnsFromTable.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_selectColumns_exists() throws Exception {
        System.out.println(method + "testRun_selectColumns_exists()");

        AppendColumnsFromTable instance = new AppendColumnsFromTable();
        instance.ctx = test_ctx;
        instance.targetTable = deptTable;
        instance.appendTable = empTable;
        CV_String[] columnNames = {new CV_String("deptName"), new CV_String("name")};
        instance.appendColumnNames = new ArrayList<>(Arrays.asList(columnNames));
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Production", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals("George", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("George", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(AppendColumnsFromTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class AppendColumnsFromTable.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_selectColumns_singleColExists_() throws Exception {
        System.out.println(method + "testRun_selectColumns_singleColExists_()");

        AppendColumnsFromTable instance = new AppendColumnsFromTable();
        instance.ctx = test_ctx;
        instance.targetTable = deptTable;
        instance.appendTable = empTable;
        CV_String[] columnNames = {new CV_String("deptName")};
        instance.appendColumnNames = new ArrayList<>(Arrays.asList(columnNames));
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Production", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals("George", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals("Harriet", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(AppendColumnsFromTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
