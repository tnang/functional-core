/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 26, 2015
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.core.cascade.conversion.TableToJsonTest;
import jema.common.types.CV_String;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class RoundDecimalsTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    public RoundDecimalsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            testTable = test_ctx.createTable("Test");
            // random table with some MGRS in column MGRS
            testTable.setHeader(new TableHeader("MGRS{string},Decimals{string},Test{string}"));
            CV_Super[] row1 = {new CV_String("18SUH6789043210"), new CV_String("1.005151"), new CV_String("4QFJ12345678")};
            CV_Super[] row2 = {new CV_String("18SUH6789043210"), new CV_String("51.54327752"), new CV_String("19TDJ3858897366")};
            CV_Super[] row3 = {new CV_String("36PVC35503645"), new CV_String("45234.52425"), new CV_String("19TDJ 38588 97366")};
            CV_Super[] row4 = {new CV_String("36PVC35503645"), new CV_String("52"), new CV_String("16SGL01GF4554")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable = testTable.toReadable();
        } catch (Exception ex) {
            Logger.getLogger(TableToJsonTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method for Table, of class RoundDecimalsTest.
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRunTable() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testRun().Table");
        RoundDecimals instance = new RoundDecimals();
        instance.inputTable = testTable;
        //System.out.println(testTable.readRow().get(1).toString());
        instance.ctx = test_ctx;
        instance.input_roundingMethod = new CV_String("halfUp");
        instance.inputColumnName = new CV_String("Decimals");
        instance.inputRoundingPlace = new CV_Integer(5);
        instance.run();
        CV_Table outputTable = instance.outputTable.toReadable();
        assertTrue(outputTable.readRow().get(3).toString().equals("1.00515"));
        assertTrue(outputTable.readRow().get(3).toString().equals("51.54328"));
        assertTrue(outputTable.readRow().get(3).toString().equals("45234.52425"));
        assertTrue(outputTable.readRow().get(3).toString().equals("52.0"));     
    }
    
    
    /**
     * Test of run method for List, of class RoundDecimalsTest.
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRunList() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testRun().List");
        RoundDecimals instance = new RoundDecimals();
        CV_String[] inputList = {
            new CV_String("1.00515"), 
            new CV_String("51.54328"), 
            new CV_String("45234.52425"), 
            new CV_String("52.0")
        };
        List<CV_String> test = Arrays.asList(inputList);
        instance.inputList = test;
        instance.ctx = test_ctx;
        instance.input_roundingMethod = new CV_String("halfUp");
        instance.inputRoundingPlace = new CV_Integer(5);
        instance.run();
        List<CV_Decimal> outputList = instance.outputList;
        
        assertTrue(outputList.get(0).toString().equals("1.00515"));
        assertTrue(outputList.get(1).toString().equals("51.54328"));
        assertTrue(outputList.get(2).toString().equals("45234.52425"));
        assertTrue(outputList.get(3).toString().equals("52.0"));
    }
    
    @Test
    public void fixStringHalfUp() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().halfUp");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "halfUp");
        String testOutput2 = instance.fixString(5.5, 0, "halfUp");
        String testOutput3 = instance.fixString(5.1, 0, "halfUp");
        String testOutput4 = instance.fixString(-5.1, 0, "halfUp");
        String testOutput5 = instance.fixString(-5.5, 0, "halfUp");
        String testOutput6 = instance.fixString(-5.6, 0, "halfUp");
        
        assertTrue(testOutput1.equals("6"));
        assertTrue(testOutput2.equals("6"));
        assertTrue(testOutput3.equals("5"));
        assertTrue(testOutput4.equals("-5"));
        assertTrue(testOutput5.equals("-6"));
        assertTrue(testOutput6.equals("-6"));
    }
    
    @Test
    public void fixStringHalfDown() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().halfDown");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "halfDown");
        String testOutput2 = instance.fixString(5.5, 0, "halfDown");
        String testOutput3 = instance.fixString(5.1, 0, "halfDown");
        String testOutput4 = instance.fixString(-5.1, 0, "halfDown");
        String testOutput5 = instance.fixString(-5.5, 0, "halfDown");
        String testOutput6 = instance.fixString(-5.6, 0, "halfDown");
        
        assertTrue(testOutput1.equals("6"));
        assertTrue(testOutput2.equals("5"));
        assertTrue(testOutput3.equals("5"));
        assertTrue(testOutput4.equals("-5"));
        assertTrue(testOutput5.equals("-5"));
        assertTrue(testOutput6.equals("-6"));
    }
    
    @Test
    public void fixStringUp() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().up");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "up");
        String testOutput2 = instance.fixString(5.5, 0, "up");
        String testOutput3 = instance.fixString(5.1, 0, "up");
        String testOutput4 = instance.fixString(-5.1, 0, "up");
        String testOutput5 = instance.fixString(-5.5, 0, "up");
        String testOutput6 = instance.fixString(-5.6, 0, "up");
        
        assertTrue(testOutput1.equals("6"));
        assertTrue(testOutput2.equals("6"));
        assertTrue(testOutput3.equals("6"));
        assertTrue(testOutput4.equals("-6"));
        assertTrue(testOutput5.equals("-6"));
        assertTrue(testOutput6.equals("-6"));
    }
    
    @Test
    public void fixStringDown() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().down");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "down");
        String testOutput2 = instance.fixString(5.5, 0, "down");
        String testOutput3 = instance.fixString(5.1, 0, "down");
        String testOutput4 = instance.fixString(-5.1, 0, "down");
        String testOutput5 = instance.fixString(-5.5, 0, "down");
        String testOutput6 = instance.fixString(-5.6, 0, "down");
        
        assertTrue(testOutput1.equals("5"));
        assertTrue(testOutput2.equals("5"));
        assertTrue(testOutput3.equals("5"));
        assertTrue(testOutput4.equals("-5"));
        assertTrue(testOutput5.equals("-5"));
        assertTrue(testOutput6.equals("-5"));
    }
    
    @Test
    public void fixStringCeiling() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().ceiling");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "ceiling");
        String testOutput2 = instance.fixString(5.5, 0, "ceiling");
        String testOutput3 = instance.fixString(5.1, 0, "ceiling");
        String testOutput4 = instance.fixString(-5.1, 0, "ceiling");
        String testOutput5 = instance.fixString(-5.5, 0, "ceiling");
        String testOutput6 = instance.fixString(-5.6, 0, "ceiling");
        
        assertTrue(testOutput1.equals("6"));
        assertTrue(testOutput2.equals("6"));
        assertTrue(testOutput3.equals("6"));
        assertTrue(testOutput4.equals("-5"));
        assertTrue(testOutput5.equals("-5"));
        assertTrue(testOutput6.equals("-5"));
    }
    
    @Test
    public void fixStringFloor() throws TableException {
        System.out.println("* Conversion JUnit4Test: RoundDecimalColumnTest : testFixString().floor");
        RoundDecimals instance = new RoundDecimals();
        instance.ctx = test_ctx;
        
        String testOutput1 = instance.fixString(5.6, 0, "floor");
        String testOutput2 = instance.fixString(5.5, 0, "floor");
        String testOutput3 = instance.fixString(5.1, 0, "floor");
        String testOutput4 = instance.fixString(-5.1, 0, "floor");
        String testOutput5 = instance.fixString(-5.5, 0, "floor");
        String testOutput6 = instance.fixString(-5.6, 0, "floor");
        
        assertTrue(testOutput1.equals("5"));
        assertTrue(testOutput2.equals("5"));
        assertTrue(testOutput3.equals("5"));
        assertTrue(testOutput4.equals("-6"));
        assertTrue(testOutput5.equals("-6"));
        assertTrue(testOutput6.equals("-6"));
    }
}
    
    