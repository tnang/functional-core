package jema.functional.core.cascade.datamanipulation;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class GenerateTestDataTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(GenerateTestDataTest.class.getName());

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();
	
	/** Execution context */
	private static ExecutionContext ctx = null;
	
	/** Number of generated rows */
	private static Long NUM_ROWS = 100l;
	
	/** Percentage of sample data containing null or blanks */
	private static Long PCT_NULL_ROWS = 10l;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * Generate test data test
	 */
	@Test
	public void testGenerateData()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			for(ParameterType parameterType : ParameterType.values())
			{
				List<CV_String> columnNames = new ArrayList<CV_String>();
				List<CV_String> columnDataTypes = new ArrayList<CV_String>();
				
				switch(parameterType)
				{
				case bool:
				case box: 
				case circle: 
				case decimal: 
				case duration: 
				case ellipse: 
				case geometryCollection: 
				case integer: 
				case length: 
				case lineString: 
				case multiLineString: 
				case multiPoint: 
				case multiPolygon: 
				case point: 
				case polygon: 
				case string: 
				case temporalRange: 
				case timestamp: 
					break;

				default:
					continue;
				}
				
				columnNames.add(CV_String.valueOf("COL_" + parameterType.name().toUpperCase()));
				columnDataTypes.add(CV_String.valueOf(parameterType.name()));
				
				// Initialize functional
				GenerateTestData func = new GenerateTestData();
				func.columnNames = columnNames;
				func.columnDataTypes = columnDataTypes;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(NUM_ROWS);
				func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
				func.ctx = ctx;
	
				// Run functional
				func.run();
				CV_Table destTable = func.destTable;
				
				cvDataGenerator.printData(destTable, Level.FINE, methodName);
				boolean result = (destTable != null);
	
				assertTrue(result);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Generate test data test
	 */
	@Test
	public void testGenerateDataList()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			List<CV_String> columnNames = new ArrayList<CV_String>();
			List<CV_String> columnDataTypes = new ArrayList<CV_String>();
			
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
				case bool:
				case box: 
				case circle: 
				case decimal: 
				case duration: 
				case ellipse: 
				case geometryCollection: 
				case integer: 
				case length: 
				case lineString: 
				case multiLineString: 
				case multiPoint: 
				case multiPolygon: 
				case point: 
				case polygon: 
				case string: 
				case temporalRange: 
				case timestamp: 
					break;

				default:
					continue;
				}
				
				columnNames.add(CV_String.valueOf("COL_" + parameterType.name().toUpperCase()));
				columnDataTypes.add(CV_String.valueOf(parameterType.name()));
			}
				
			// Initialize functional
			GenerateTestData func = new GenerateTestData();
			func.columnNames = columnNames;
			func.columnDataTypes = columnDataTypes;
			func.regexFormat = null;
			func.numberOfRows = CV_Integer.valueOf(NUM_ROWS);
			func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			cvDataGenerator.printData(destTable, Level.FINE, methodName);
			boolean result = (destTable != null);

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			List<CV_String> columnNames = new ArrayList<CV_String>();
			List<CV_String> columnDataTypes = new ArrayList<CV_String>();
			
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
				case bool:
				case box: 
				case circle: 
				case decimal: 
				case duration: 
				case ellipse: 
				case geometryCollection: 
				case integer: 
				case length: 
				case lineString: 
				case multiLineString: 
				case multiPoint: 
				case multiPolygon: 
				case point: 
				case polygon: 
				case string: 
				case temporalRange: 
				case timestamp: 
					break;

				default:
					continue;
				}
				
				columnNames.add(CV_String.valueOf("COL_" + parameterType.name().toUpperCase()));
				columnDataTypes.add(CV_String.valueOf(parameterType.name()));
			}
			
			try
			{
				GenerateTestData func = new GenerateTestData();
				func.columnNames = null;
				func.columnDataTypes = columnDataTypes;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(NUM_ROWS);
				func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
				func.ctx = ctx;
				func.run();
				assertTrue("Column name is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GenerateTestData func = new GenerateTestData();
				func.columnNames = columnNames;
				func.columnDataTypes = null;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(NUM_ROWS);
				func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
				func.ctx = ctx;
				func.run();
				assertTrue("Column data type is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GenerateTestData func = new GenerateTestData();
				func.columnNames = columnNames;
				func.columnDataTypes = columnDataTypes;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(0);
				func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
				func.ctx = ctx;
				func.run();
				assertTrue("Number of rows is zero" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GenerateTestData func = new GenerateTestData();
				func.columnNames = null;
				List<CV_String> columnDataTypesBad = new ArrayList<CV_String>(columnDataTypes);
				columnDataTypesBad.add(CV_String.valueOf("BadDataType"));
				func.columnDataTypes = columnDataTypesBad;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(-1);
				func.pctBlankValues = CV_Integer.valueOf(110);
				func.ctx = ctx;
				func.run();
				assertTrue("All arguments are invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GenerateTestData func = new GenerateTestData();
				List<CV_String> columnNamesBad = new ArrayList<CV_String>(columnNames);
				columnNamesBad.add(CV_String.valueOf("ExtraColName"));
				func.columnNames = columnNamesBad;
				func.columnDataTypes = columnDataTypes;
				func.regexFormat = null;
				func.numberOfRows = CV_Integer.valueOf(NUM_ROWS);
				func.pctBlankValues = CV_Integer.valueOf(PCT_NULL_ROWS);
				func.ctx = ctx;
				func.run();
				assertTrue("Mismatched column counts" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
