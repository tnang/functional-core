/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 15, 2015 8:55:50 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class ExpandRowsByColumnTest {

    ExecutionContext test_ctx;
    CV_Table testTable;
    String method = "* DataManipulation JUnit4Test: ExpandRowsByColumn : ";

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            testTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/expandRows.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class ExpandRowsByColumn.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_wrongType() {
        System.out.println(method + "testRun_wrongType()");

        ExpandRowsByColumn instance = new ExpandRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnName = new CV_String("employeeID");
        instance.run();
    }

    /**
     * Test of run method, of class ExpandRowsByColumn.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnNotFound() {
        System.out.println(method + "testRun_columnNotFound()");

        ExpandRowsByColumn instance = new ExpandRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnName = new CV_String("BACON");
        instance.run();
    }

    /**
     * Test of run method, of class ExpandRowsByColumn.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun() throws TableException {
        System.out.println(method + "testRun()");

        ExpandRowsByColumn instance = new ExpandRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnName = new CV_String("deptName");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry!Smith", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry!Smith", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("HR", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Sales", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 5) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ExpandRowsByColumnTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ExpandRowsByColumn.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_separator() throws TableException {
        System.out.println(method + "testRun_separator()");

        ExpandRowsByColumn instance = new ExpandRowsByColumn();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnName = new CV_String("name");
        instance.separator = new CV_String("!");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance:HR", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Smith", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance:HR", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Sales:", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertNull(thiscolumnrowvalue);
                    thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Stowe", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ExpandRowsByColumnTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
