package jema.functional.core.cascade.datamanipulation;

import static org.junit.Assert.*;

import java.util.List;

import jema.common.types.CV_String;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.conversion.StringToKML;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ScanStringTest {

	/**
	 * Input String that will be processed
	 */
	String testInputString = "The/quick/brown/fox";
	
	/**
	 * Input Regex string that will be used to process input string
	 * This RegEx matches / delimited text
	 */
	String testRegEx = "\\w+\\s*(?:(?:;(\\s*\\w+\\s*)?)+)?";
	
	/**
	 * How many matches should we have
	 */
	public int expectedMatches = 4;
	
	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testCtx = new ExecutionContextImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testScanString() {
		String message = "* DataManipulation.Strings.ScanString JUnit4Test: ";
		
		System.out.println(message+"ScanString : testScanString()");

		
		ScanString instance = new ScanString();
		instance.input_string = new CV_String(testInputString);
		instance.input_regex = new CV_String(testRegEx);
		instance.ctx = testCtx;
		instance.run();
		
		List<CV_String> results = instance.output;
		// Check number of regex changes
		assertEquals(message+"RegEx Pattern find Expected Number of Results",results.size(),expectedMatches);

	}

}
