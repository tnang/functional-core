/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Sep 24, 2015 12:43:14 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class JoinTablesTest {

    ExecutionContext test_ctx;
    CV_Table empTable, empTable2, deptTable, leftTable, rightTable, fullTable, innerTable, innerTable2;
    String method = "* DataManipulation JUnit4Test: JoinTables : ";

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            empTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Employee.csv"), null).toReadable();
            empTable2 = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Employee_2.csv"), null).toReadable();
            deptTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Department.csv"), null).toReadable();
            fullTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/fullResult.csv"), null).toReadable();
            leftTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/leftResult.csv"), null).toReadable();
            rightTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/rightResult.csv"), null).toReadable();
            innerTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/innerResult.csv"), null).toReadable();
            innerTable2 = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/innerResult_2.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(JoinTablesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class JoinTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_left() throws Exception {
        System.out.println(method + "testRun_left()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(false);
        instance.joinType = new CV_String("LEFT OUTER JOIN");
        instance.run();

        CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), leftTable);
        assertTrue(result);
    }

    /**
     * Test of run method, of class JoinTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_right() throws Exception {
        System.out.println(method + "testRun_right()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(true);
        instance.joinType = new CV_String("RIGHT OUTER JOIN");
        instance.run();

        //TODO: use compareTable once bug is fixed
        //CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        //boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), rightTable);
        //assertTrue(result);
        //dataGen.printData(instance.result, Level.INFO, null);
        //dataGen.printData(rightTable, Level.INFO, null);
        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals("Sales", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertNull(thiscolumnrowvalue);
                    thiscolumnrowvalue = current_row.get(4);
                    assertEquals("Charles", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class JoinTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_full() throws Exception {
        System.out.println(method + "testRun_full()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(false);
        instance.joinType = new CV_String("FULL OUTER JOIN");
        instance.run();

        //TODO: use compareTable once bug is fixed
        //CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        //boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), fullTable);
        //assertTrue(result);
        //dataGen.printData(instance.result, Level.INFO, null);
        //dataGen.printData(fullTable, Level.INFO, null);
        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals("Harriet", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class JoinTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_inner() throws Exception {
        System.out.println(method + "testRun_inner()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(false);
        instance.joinType = new CV_String("INNER JOIN");
        instance.run();

        CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), innerTable);
        assertTrue(result);
    }

    /**
     * Test of run method, of class JoinTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_inner_dupColumn() throws Exception {
        System.out.println(method + "testRun_inner_dupColumn()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = empTable2;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(true);
        instance.joinType = new CV_String("INNER JOIN");
        instance.run();

        CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), innerTable2);
        assertTrue(result);
    }

    /**
     * Test of run method, of class JoinTables.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_baseColumnNotFound() {
        System.out.println(method + "testRun_right()");

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("BACON");
        instance.joinKey = new CV_String("deptName");
        instance.includeField2 = new CV_Boolean(true);
        instance.joinType = new CV_String("RIGHT OUTER JOIN");
        instance.run();
    }

    /**
     * Test of run method, of class JoinTables.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_joinColumnNotFound() {
        System.out.println(method + "testRun_right()");

        JoinTables instance = new JoinTables();
        instance.ctx = test_ctx;
        instance.baseTable = empTable;
        instance.joinTable = deptTable;
        instance.baseKey = new CV_String("deptName");
        instance.joinKey = new CV_String("BACON");
        instance.includeField2 = new CV_Boolean(true);
        instance.joinType = new CV_String("RIGHT OUTER JOIN");
        instance.run();
    }
}
