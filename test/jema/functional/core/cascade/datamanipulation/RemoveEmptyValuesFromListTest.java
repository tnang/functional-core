/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 30, 2015 9:53:33 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.Arrays;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class RemoveEmptyValuesFromListTest {

    /**
     * Test of run method, of class RemoveEmptyValuesFromList.
     */
    @Test
    public void testRun() {
        System.out.println("* Data Manipulation JUnit4Test: RemoveEmptyValuesFromList : testRun()");

        //Test list of values
        RemoveEmptyValuesFromList instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(new CV_String("test"), new CV_String("test"), new CV_Integer(12), new CV_Length(23.9));
        instance.run();
        assertEquals(instance.inputList, instance.result);

        //Test empty list
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList();
        instance.run();
        assertEquals(Arrays.asList(), instance.result);

        //Test list with null values
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(null, new CV_String(null));
        instance.run();
        assertEquals(Arrays.asList(), instance.result);

        //Test list with null values and strings
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(new CV_String(null), new CV_String("test"), new CV_Integer(12), null);
        instance.run();
        assertEquals(Arrays.asList(new CV_String("test"), new CV_Integer(12)), instance.result);

        //Test list with empty strings
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(new CV_String(""), new CV_String(""));
        instance.run();
        assertEquals(Arrays.asList(), instance.result);

        //Test list with empty strings and values
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(new CV_String("test"), new CV_String(""), new CV_Length(23.9), new CV_String(""));
        instance.run();
        assertEquals(Arrays.asList(new CV_String("test"), new CV_Length("23.9")), instance.result);

        //Test list with null values, empty strings and values
        instance = new RemoveEmptyValuesFromList();
        instance.inputList = Arrays.asList(new CV_String(null), new CV_String("test"), new CV_String(""), new CV_Length(23.9), null, new CV_String(""));
        instance.run();
        assertEquals(Arrays.asList(new CV_String("test"), new CV_Length(23.9)), instance.result);
    }
}
