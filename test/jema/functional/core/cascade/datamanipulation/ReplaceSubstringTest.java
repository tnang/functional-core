/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 27, 2015
 */
package jema.functional.core.cascade.datamanipulation;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class ReplaceSubstringTest {
    
    public ReplaceSubstringTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ReplaceSubstring.
     */
    @Test
    public void testRun() {
        System.out.println("* Data Manipulation.Strings JUnit4Test: ReplaceSubstring : testRun(()");
        ReplaceSubstring instance = new ReplaceSubstring();
        
        System.out.println("\tTesting matching - true - no match");
        instance.input_caseSensitive = new CV_Boolean(true);
        instance.input_string = new CV_String("hi my name is McBonnie and i like burgers");
        instance.input_subString = new CV_String("kittenmittens");
        instance.input_replacementString = new CV_String("BurgerMan");
        instance.run();
        assertTrue(instance.output.value().equals("hi my name is McBonnie and i like burgers"));
        
        System.out.println("\tTesting matching - true - no match #2");
        instance.input_caseSensitive = new CV_Boolean(true);
        instance.input_string = new CV_String("hi my name is McBonnie and i like burgers");
        instance.input_subString = new CV_String("mcbonnie");
        instance.input_replacementString = new CV_String("BurgerMan");
        instance.run();
        assertTrue(instance.output.value().equals("hi my name is McBonnie and i like burgers"));
        
        System.out.println("\tTesting matching - true - match");
        instance.input_caseSensitive = new CV_Boolean(true);
        instance.input_string = new CV_String("hi my name is McBonnie and i like burgers");
        instance.input_subString = new CV_String("McBonnie");
        instance.input_replacementString = new CV_String("BurgerMan");
        instance.run();
        assertTrue(instance.output.value().equals("hi my name is BurgerMan and i like burgers"));
        
        System.out.println("\tTesting matching - false - match");
        instance.input_caseSensitive = new CV_Boolean(false);
        instance.input_string = new CV_String("hi my name is McBonnie and i like burgers");
        instance.input_subString = new CV_String("mcbonnie");
        instance.input_replacementString = new CV_String("BurgerMan");
        instance.run();
        assertTrue(instance.output.value().equals("hi my name is BurgerMan and i like burgers"));
        
        System.out.println("\tTesting matching - false - no match");
        instance.input_caseSensitive = new CV_Boolean(false);
        instance.input_string = new CV_String("hi my name is McBonnie and i like burgers");
        instance.input_subString = new CV_String("noMatch");
        instance.input_replacementString = new CV_String("BurgerMan");
        instance.run();
        assertTrue(instance.output.value().equals("hi my name is McBonnie and i like burgers"));
 
    }
    
}
