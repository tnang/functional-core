 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.filter.DeduplicateRowsTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 * Note that you must set the VM options within the IDE to the following for this test to run.
 * -Djema.common.util.MongoClientFactory.mongoServers=localhost:45321
 *  Additionally, a MongoDB instance must be available on port 45321...which is accomplished with the FDK JSH being running and available.
 */
public class ReOrderColumnsTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
        CV_Table testTable2;
    public ReOrderColumnsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {   
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal},testInteger{integer},testTimestamp1{timestamp},testTimestamp2{timestamp},testTimestamp3{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111), new CV_Decimal(1.211111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111), new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000), new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row7 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row8 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(ReOrderColumnsTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ReOrderColumns.
     * @throws jema.common.types.table.TableException
     */
 @Test
    public void testRun() throws TableException, Exception {
        System.out.println("* DataManipulation JUnit4Test: ReorderColumnsTest : testRun()");

        ReOrderColumns instance = new ReOrderColumns();
        instance.ctx = test_ctx;
        instance.input = Arrays.asList(new CV_String("testcolumn1"), new CV_String("test"));
        instance.inputTable = testTable;
        instance.run();
        
        
        final CV_Table dest_table = instance.outputTable.toReadable();

        CommonVocabDataGenerator dg = new CommonVocabDataGenerator();
        //dg.printData(dest_table, Level.INFO, null);
        //Create the testdata which represents the desired result.
        List<List<CV_Super>> testData = new ArrayList<>();
        List<CV_Super> rowSrc = new ArrayList<>();
        CV_Table new_table = testTable.toReadable();
        while (new_table.readRow(rowSrc)) {
            List<CV_Super> rowDest = new ArrayList<>();
            //here we are flipping the columns 
            rowDest.add(rowSrc.get(1));
            rowDest.add(rowSrc.get(0));
            //add the newly flipped columns as row into testdata
            testData.add(rowDest);
        }

        boolean result =dg.compareTable(Thread.currentThread().getStackTrace()[1].getMethodName(), dest_table.getUri(), dest_table.toReadable(), testData);
        assertTrue(result);
        
       //here is the other way to check the values in the columns
       CV_Table dest_table2 = dest_table.toReadable();
       List<CV_Super> current_row =new ArrayList<>();
       while(dest_table2.readRow(current_row)){
            try {

                if (((int)dest_table2.getCurrentRowNum())%2==0) {
                    CV_Super thiscolumnrowvalue2 = current_row.get(1);
                    assertEquals(thiscolumnrowvalue2.toString(), "even");
                }else{
                    CV_Super thiscolumnrowvalue2 = current_row.get(1);
                    assertEquals(thiscolumnrowvalue2.toString(), "odd");
                }
                  
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
 
    /**
     * Test of run2 method, of class ReOrderColumns.
     * @throws jema.common.types.table.TableException
     */
  @Test  
    public void testRun2()throws TableException, Exception  {
        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();
        System.out.println("* DataManinpulation JUnit4Test: ReorderColumns :" + methodname);

        CommonVocabDataGenerator dg = new CommonVocabDataGenerator();

        List<ColumnHeader> sourceColumns = testTable.getHeader().asList();
        //testColumns reverse the first two column of the sourceColumn....
        List<ColumnHeader> testColumns = Arrays.asList(sourceColumns.get(1),sourceColumns.get(0));
       //this gets a string version of the testColumns headers to input into the functional.
        List<CV_String> input_list = new ArrayList<>();
        for (ColumnHeader h : testColumns) {
            input_list.add(CV_String.valueOf(h.getName()));
        }
        //testData is essentially a CV_Table of the test data.
        List<List<CV_Super>> testData = new ArrayList<>();
        testTable.stream().forEach((List<CV_Super> current_row) -> {
            try {

                List<CV_Super> row = testTable.readRow();
                //this creates a test row based on a map of the input row, header info , testColumns
                //NOTE THAT addTestColumns should be renamed to createTestRow  -- John Lewis said he may do that in the future.
                List<CV_Super> testrow = dg.addTestColumns(row, testTable.getHeader(), testColumns);
                testData.add(testrow);

            } catch (TableException ex) {
                Logger.getLogger(ReOrderColumnsTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        ReOrderColumns instance = new ReOrderColumns();
        instance.ctx = test_ctx;
        instance.input = input_list;
        instance.inputTable = testTable;
        instance.run();
        
        //compare the destination table with that of the test data to ensure all is correct.
        boolean result = dg.compareTable(methodname, instance.outputTable.getUri(),instance.outputTable.toReadable(), testData);
        assertTrue(result);
        //dg.printData(instance.outputTable, Level.INFO, null);


    }
    
    /**
     * Test of run3 method, of class ReOrderColumns.
     * This test specifically addresses isApppending = true
     * @throws jema.common.types.table.TableException
     */
  @Test  
    public void testRun3()throws TableException, Exception  {
        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();
        System.out.println("* DataManinpulation JUnit4Test: ReorderColumns :" + methodname);

        CommonVocabDataGenerator dg = new CommonVocabDataGenerator();

        List<ColumnHeader> sourceColumns = testTable.getHeader().asList();
        //testColumns reorders the columns 
        List<ColumnHeader> testColumns = Arrays.asList(sourceColumns.get(6),sourceColumns.get(0),sourceColumns.get(1));
        //destColumns resembles testColumns with the
        List<ColumnHeader> destColumns = Arrays.asList(sourceColumns.get(6),sourceColumns.get(0),sourceColumns.get(1),sourceColumns.get(2),sourceColumns.get(3),sourceColumns.get(4),sourceColumns.get(5));
        
       //this gets a string version of the testColumns headers to input into the functional.
        List<CV_String> input_list = new ArrayList<>();
        for (ColumnHeader h : testColumns) {
            input_list.add(CV_String.valueOf(h.getName()));
        }
        //testData is essentially a CV_Table of the test data.
        List<List<CV_Super>> testData = new ArrayList<>();
        testTable.stream().forEach((List<CV_Super> current_row) -> {
            try {

                //this creates a test row based on a map of the input row, header info , testColumns
                //NOTE THAT addTestColumns should be renamed to createTestRow  -- John Lewis said he may do that in the future.
                List<CV_Super> testrow = dg.addTestColumns(current_row, testTable.getHeader(), destColumns);
                testData.add(testrow);

            } catch (Exception ex) {
                Logger.getLogger(ReOrderColumnsTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        ReOrderColumns instance = new ReOrderColumns();
        instance.ctx = test_ctx;
        instance.input = input_list;
        instance.inputTable = testTable;
        instance.isAppending = new CV_Boolean("true");
        instance.run();
        
        //compare the destination table with that of the test data to ensure all is correct.
        boolean result = dg.compareTable(methodname, instance.outputTable.getUri(),instance.outputTable.toReadable(), testData);
        assertTrue(result);
       // dg.printData(instance.outputTable, Level.INFO, null);
    }
    
    /**
     * Test of run4 method, of class ReOrderColumns.
     * This test specifically a bad column name doesn't affect a good column name
     * @throws jema.common.types.table.TableException
     */
  @Test  
    public void testRun4()throws TableException  {
        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();
        System.out.println("* DataManinpulation JUnit4Test: ReorderColumns :" + methodname);


        ReOrderColumns instance = new ReOrderColumns();
        instance.ctx = test_ctx;
        instance.input = Arrays.asList(new CV_String("testcolumn2"),new CV_String("bad Column2"));
        instance.inputTable = testTable;
        instance.isAppending = new CV_Boolean("false");
        instance.run();
        
        //compare the destination table with that of the test data to ensure all is correct.
        List<ColumnHeader> list =instance.outputTable.getHeader().asList();
       
        assertTrue( list.size()==1);
        //CommonVocabDataGenerator dg = new CommonVocabDataGenerator();
        //dg.printData(instance.outputTable, Level.INFO, null);
    }
    
    
    
    
}
