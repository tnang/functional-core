 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.datamanipulation;


import java.util.Arrays;
import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class DeduplicateListTest {
    
    public DeduplicateListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DeduplicateList.
     */
    @Test
    public void testRun() {
        System.out.println("* Filter JUnit4Test: DeduplicateList : testRun()");
        DeduplicateList instance = new DeduplicateList();
        
        //test for duplicate values of different CV_String types
        instance.input = Arrays.asList(new CV_String("test"),new CV_String("test"),new CV_String("Test"),new CV_String("Test"));
        instance.run();
        int result = instance.result.size();
        int expResult = instance.input.size()/2;
        assertEquals(expResult, result);
        
        
        //test decimal string value in a list
        instance = new DeduplicateList();
        instance.input =Arrays.asList(new CV_String("10.444"),new CV_String("10.444"));
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size()/2;
        assertEquals(expResult, result);

        
        /** Currently unable to use a CV_Super as the list type...will check with JEMA team to see if that will change in the future
        //test decimal value in a list
        instance = new DeduplicateList();
        instance.input =Arrays.asList(new CV_Decimal(10.444),new CV_Decimal(10.444));
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size()/2;
        assertEquals(expResult, result);
        
        //test integer value in a list

        instance = new DeduplicateList();
        instance.input =Arrays.asList(new CV_Integer(10),new CV_Integer(10));
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size()/2;
        assertEquals(expResult, result);
         

        
        //test boolean types
        instance = new DeduplicateList();
        instance.input  =Arrays.asList(new CV_Boolean(false),new CV_Boolean(false));
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size()/2;
        assertEquals(expResult, result); 
        
        //test different types
        instance = new DeduplicateList();
        instance.input  =Arrays.asList(new CV_Boolean(false),new CV_Boolean(false),new CV_Integer(10),new CV_Integer(10),new CV_Decimal(10.444),new CV_Decimal(10.444),new CV_String("Test"),new CV_String("Test"));
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size()/2;
        assertEquals(expResult, result);  

        //test empty list
        instance = new DeduplicateList();
        instance.input = new ArrayList();
        instance.run();
        result = instance.result.size();
        expResult = instance.input.size();
        assertEquals(expResult, result);  
        **/

    }
}
//UNCLASSIFIED

