package jema.functional.core.cascade.datamanipulation;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Integer;
import jema.common.types.CV_Table;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class CopyTableRowsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(CopyTableRowsTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** Source CSV */
	private final static String CSV_FILE_GERMANY = "germany_points.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * CopyTableRows test - default case
	 */
	@Test
	public void testCopyTableRowsDefault()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize functional
			CopyTableRows func = new CopyTableRows();
			func.srcTable = srcTable;
			func.startIndex = null;
			func.stopIndex = null;
			func.length = null;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			cvDataGenerator.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * CopyTableRows test - default case
	 */
	@Test
	public void testCopyTableRowsStart()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize functional
			CopyTableRows func = new CopyTableRows();
			func.srcTable = srcTable;
			func.startIndex = CV_Integer.valueOf(1);
			func.stopIndex = null;
			func.length = null;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			cvDataGenerator.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * CopyTableRows test - default case
	 */
	@Test
	public void testCopyTableRowsStop()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize functional
			CopyTableRows func = new CopyTableRows();
			func.srcTable = srcTable;
			func.startIndex = null;
			func.stopIndex = CV_Integer.valueOf(20);
			func.length = null;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			cvDataGenerator.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * CopyTableRows test - default case
	 */
	@Test
	public void testCopyTableRowsLength()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize functional
			CopyTableRows func = new CopyTableRows();
			func.srcTable = srcTable;
			func.startIndex = null;
			func.stopIndex = null;
			func.length = CV_Integer.valueOf(20);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			cvDataGenerator.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * CopyTableRows test - default case
	 */
	@Test
	public void testCopyTableRowsStartStop()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize functional
			CopyTableRows func = new CopyTableRows();
			func.srcTable = srcTable;
			func.startIndex = CV_Integer.valueOf(10);
			func.stopIndex = CV_Integer.valueOf(20);
			func.length = null;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			cvDataGenerator.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			try
			{
				CopyTableRows func = new CopyTableRows();
				func.srcTable = null;
				func.startIndex = null;
				func.stopIndex = null;
				func.length = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CopyTableRows func = new CopyTableRows();
				func.srcTable = srcTable;
				func.startIndex = CV_Integer.valueOf(20);
				func.stopIndex = CV_Integer.valueOf(10);
				func.length = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Start index after stop index" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CopyTableRows func = new CopyTableRows();
				func.srcTable = srcTable;
				func.startIndex = CV_Integer.valueOf(-1);
				func.stopIndex = CV_Integer.valueOf(-2);
				func.length = CV_Integer.valueOf(-2);
				func.ctx = ctx;
				func.run();
				assertTrue("Start index after stop index" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CopyTableRows func = new CopyTableRows();
				func.srcTable = srcTable;
				func.startIndex = CV_Integer.valueOf(1);
				func.stopIndex = CV_Integer.valueOf(2);
				func.length = CV_Integer.valueOf(20);
				func.ctx = ctx;
				func.run();
				assertTrue("Stop index and Length are set" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
