/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 27, 2015
 */
package jema.functional.core.cascade.datamanipulation;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class ExtractSubstringTest {
    
    public ExtractSubstringTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ExtractSubstring.
     */
    @Test
    public void testRun() {
        System.out.println("* Data Manipulation.Strings JUnit4Test: ExtractSubstring : testRun(()");
        ExtractSubstring instance = new ExtractSubstring();
        
        System.out.println("\tTesting matching from start");
        instance.input_string = new CV_String("test_string_for-fun");
        instance.input_startIndex = new CV_Integer(1);
        instance.input_stopIndex = new CV_Integer(4);
        String testMatch = "test";
        instance.run();
        assertEquals(instance.output.value(), testMatch);
        
        System.out.println("\tTesting matching from middle");
        instance.input_string = new CV_String("test_string_for-fun");
        instance.input_startIndex = new CV_Integer(2);
        instance.input_stopIndex = new CV_Integer(5);
        testMatch = "est_";
        instance.run();
        assertEquals(instance.output.value(), testMatch);
        
        System.out.println("\tTesting 0 index");
        instance.input_string = new CV_String("test_string_for-fun");
        instance.input_startIndex = new CV_Integer(0);
        instance.input_stopIndex = new CV_Integer(5);
        boolean thrown = false;
        try {
            instance.run();
        } catch (IllegalStateException e) {
            if (e.getMessage().equals("Index starts at 1, not 0!")) {
                thrown = true;
            }
            
        }
        assertTrue(thrown);
        
        System.out.println("\tTesting max > min error");
        instance.input_string = new CV_String("test_string_for-fun");
        instance.input_startIndex = new CV_Integer(6);
        instance.input_stopIndex = new CV_Integer(5);
        thrown = false;
        try {
            instance.run();
        } catch (IllegalStateException e) {
            if (e.getMessage().equals("Stop Index must be larger than Start Index!")) {
                thrown = true;
            }
            
        }
        assertTrue(thrown);
        
        System.out.println("\tTesting max > min error");
        instance.input_string = new CV_String("test_string_for-fun");
        instance.input_startIndex = new CV_Integer(2147483646);
        instance.input_stopIndex = new CV_Integer(2147483647);
        thrown = false;
        try {
            instance.run();
        } catch (IllegalStateException e) {
            if (e.getMessage().equals("Index is too large!")) {
                thrown = true;
            }
            
        }
        assertTrue(thrown);
        
        
        
    }
    
}
