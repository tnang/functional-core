/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Aug 31, 2015 10:31:10 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

/**
 * @author CASCADE
 */
public class ConvertDistanceInTableTest {

    ExecutionContext test_ctx;
    CV_Table testTable;

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            testTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Distance.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class ConvertDistanceInTable.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_MeterToKM() throws TableException {
        System.out.println("* Conversion JUnit4Test: ConvertDistanceInTable : testRun_MeterToKM()");

        ConvertDistanceInTable instance = new ConvertDistanceInTable();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDist = new CV_String("Distance");
        //Test instance.columnNameDistOut Default = "Distance Conversion"
        instance.unitIn = new CV_String("METER");
        instance.unitOut = new CV_String("KM");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(0.032, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(12.34567, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(0.001, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(0.0, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ConvertDistanceInTable.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_NMToMile_columnExists() throws TableException {
        System.out.println("* Conversion JUnit4Test: ConvertDistanceInTable : testRun_NMToMile_columnExists()");

        ConvertDistanceInTable instance = new ConvertDistanceInTable();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDist = new CV_String("Distance");
        instance.columnNameDistOut = new CV_String("Distance Output");
        instance.unitIn = new CV_String("NM");
        instance.unitOut = new CV_String("MILE");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(36.82494233675336, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(14207.143308080807, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(1.1507794480235425, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(0.0, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConvertDistanceInTableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ConvertDistanceInTable.
     *
     */
    @Test(expected = RuntimeException.class)
    public void testRun_NMToMile_columnExists_wrongType() {
        System.out.println("* Conversion JUnit4Test: ConvertDistanceInTable : testRun_NMToMile_columnExists_wrongType()");

        ConvertDistanceInTable instance = new ConvertDistanceInTable();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDist = new CV_String("Distance");
        instance.columnNameDistOut = new CV_String("Distance Out");
        instance.unitIn = new CV_String("NM");
        instance.unitOut = new CV_String("MILE");
        instance.run();
    }

    /**
     * Test of run method, of class ConvertDistanceInTable.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnNotFound() {
        System.out.println("* Conversion JUnit4Test: ConvertDistanceInTable : testRun_columnNotFound()");

        //Test column name not in table
        ConvertDistanceInTable instance = new ConvertDistanceInTable();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDist = new CV_String("BACON");
        instance.columnNameDistOut = new CV_String("Distance Out");
        instance.unitIn = new CV_String("METER");
        instance.unitOut = new CV_String("FOOT");
        instance.run();
    }
}
