package jema.functional.core.cascade.datamanipulation;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class GeoClusterRowsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(GeoClusterRowsTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private final static String CSV_FILE_GERMANY = "germany_points.csv";

	/** Maximum separation distance */
	private final static Double MAX_SEPARATION = 50.0;

	/** Minimum clusters */
	private final static Long MIN_GEOS = 5L;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * Geocluster test
	 */
	//@Test
	public void testGeoCluster()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize input parameters
			TableHeader tableHeader = srcTable.getHeader();
			ColumnHeader latColHeader = tableHeader.getHeader("loc_lat");
			ColumnHeader lonColHeader = tableHeader.getHeader("loc_lon");

			// Initialize functional
			GeoClusterRows func = new GeoClusterRows();
			func.srcTable = srcTable;
			func.latitudeColumnName = CV_String.valueOf(latColHeader.getName());
			func.longitudeColumnName = CV_String.valueOf(lonColHeader.getName());
			func.discriminatorColumnName = null;
			func.maxSeparation = CV_Decimal.valueOf(MAX_SEPARATION);
			func.minGeos = CV_Integer.valueOf(MIN_GEOS);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destGeosTable = func.destGeosTable;
			CV_Table destClusterTable = func.destClusterTable;

			boolean result = (destGeosTable != null) && (destClusterTable != null);

			assertTrue(result);

			if(destGeosTable != null) {destGeosTable.close();}
			if(destClusterTable != null) {destClusterTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);

			// Initialize input parameters
			TableHeader tableHeader = srcTable.getHeader();
			ColumnHeader latColHeader = tableHeader.getHeader("loc_lat");
			ColumnHeader lonColHeader = tableHeader.getHeader("loc_lon");

			try
			{
				GeoClusterRows func = new GeoClusterRows();
				func.srcTable = null;
				func.latitudeColumnName = CV_String.valueOf(latColHeader.getName());
				func.longitudeColumnName = CV_String.valueOf(lonColHeader.getName());
				func.discriminatorColumnName = null;
				func.maxSeparation = CV_Decimal.valueOf(MAX_SEPARATION);
				func.minGeos = CV_Integer.valueOf(MIN_GEOS);
				func.ctx = ctx;
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GeoClusterRows func = new GeoClusterRows();
				func.srcTable = srcTable;
				func.latitudeColumnName = null;
				func.longitudeColumnName = CV_String.valueOf(lonColHeader.getName());
				func.discriminatorColumnName = null;
				func.maxSeparation = CV_Decimal.valueOf(MAX_SEPARATION);
				func.minGeos = CV_Integer.valueOf(MIN_GEOS);
				func.ctx = ctx;
				func.run();
				assertTrue("Latitude column is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GeoClusterRows func = new GeoClusterRows();
				func.srcTable = srcTable;
				func.latitudeColumnName = CV_String.valueOf(latColHeader.getName());
				func.longitudeColumnName = null;
				func.discriminatorColumnName = null;
				func.maxSeparation = CV_Decimal.valueOf(MAX_SEPARATION);
				func.minGeos = CV_Integer.valueOf(MIN_GEOS);
				func.ctx = ctx;
				func.run();
				assertTrue("Longitude column is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				GeoClusterRows func = new GeoClusterRows();
				func.srcTable = srcTable;
				func.latitudeColumnName = CV_String.valueOf("latitudeColumnNameBad");
				func.longitudeColumnName = CV_String.valueOf("longitudeColumnNameBad");
				func.discriminatorColumnName = null;
				func.maxSeparation = CV_Decimal.valueOf(-1.0);
				func.minGeos = CV_Integer.valueOf(0);
				func.ctx = ctx;
				func.run();
				assertTrue("All arguments are invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
