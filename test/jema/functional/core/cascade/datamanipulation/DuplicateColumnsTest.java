package jema.functional.core.cascade.datamanipulation;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class DuplicateColumnsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(DuplicateColumnsTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** Source CSV */
	private final static String CSV_FILE = "Employee.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * DuplicateColumns test
	 */
	@Test
	public void testDuplicateColumns()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);

			// Setup parameters
			List<CV_String> origColumnHeaders = new ArrayList<CV_String>();
			List<CV_String> duplicateColumnHeaders = new ArrayList<CV_String>();

			TableHeader tableHeader = srcTable.getHeader();
			for(int iCol = 0; iCol < 1; iCol++)
			{
				ColumnHeader colHeader = tableHeader.getHeader(iCol);
				origColumnHeaders.add(CV_String.valueOf(colHeader.getName()));
				duplicateColumnHeaders.add(CV_String.valueOf("DUP_"+colHeader.getName()));
			}

			// Initialize functional
			DuplicateColumns func = new DuplicateColumns();
			func.srcTable = srcTable;
			func.origColumnHeaders = origColumnHeaders;
			func.duplicateColumnHeaders = duplicateColumnHeaders;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * DuplicateColumns test
	 */
	@Test
	public void testDuplicateColumnsAll()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);

			// Setup parameters
			List<CV_String> origColumnHeaders = new ArrayList<CV_String>();
			List<CV_String> duplicateColumnHeaders = new ArrayList<CV_String>();

			TableHeader tableHeader = srcTable.getHeader();
			for(int iCol = 0; iCol < tableHeader.size(); iCol++)
			{
				ColumnHeader colHeader = tableHeader.getHeader(iCol);
				origColumnHeaders.add(CV_String.valueOf(colHeader.getName()));
				duplicateColumnHeaders.add(CV_String.valueOf("DUP_"+colHeader.getName()));
			}

			// Initialize functional
			DuplicateColumns func = new DuplicateColumns();
			func.srcTable = srcTable;
			func.origColumnHeaders = origColumnHeaders;
			func.duplicateColumnHeaders = duplicateColumnHeaders;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			TableUtils.printData(destTable, Level.FINE, methodName);

			boolean result = (destTable != null);
			assertTrue(result);

			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			CV_Table srcTable = ctx.createTableFromCSV(
					this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);

			// Setup parameters
			List<CV_String> origColumnHeaders = new ArrayList<CV_String>();
			List<CV_String> duplicateColumnHeaders = new ArrayList<CV_String>();

			TableHeader tableHeader = srcTable.getHeader();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				origColumnHeaders.add(CV_String.valueOf(colHeader.getName()));
				duplicateColumnHeaders.add(CV_String.valueOf("DUP_"+colHeader.getName()));
			}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = null;
				func.origColumnHeaders = origColumnHeaders;
				func.duplicateColumnHeaders = duplicateColumnHeaders;
				func.ctx = ctx;
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = srcTable;
				func.origColumnHeaders = null;
				func.duplicateColumnHeaders = duplicateColumnHeaders;
				func.ctx = ctx;
				func.run();
				assertTrue("Original column headers list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = srcTable;
				func.origColumnHeaders = origColumnHeaders;
				func.duplicateColumnHeaders = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Duplicate column headers list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = srcTable;
				func.origColumnHeaders = origColumnHeaders;
				List<CV_String> duplicateColumnHeadersBad = new ArrayList<CV_String>(duplicateColumnHeaders);
				duplicateColumnHeadersBad.add(CV_String.valueOf("AdditionalColName"));
				func.duplicateColumnHeaders = duplicateColumnHeadersBad;
				func.ctx = ctx;
				func.run();
				assertTrue("List size mismatch" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = srcTable;
				List<CV_String> origColumnHeadersBad = new ArrayList<CV_String>(duplicateColumnHeaders);
				origColumnHeadersBad.add(CV_String.valueOf("BadColName"));
				func.origColumnHeaders = origColumnHeadersBad;
				func.duplicateColumnHeaders = duplicateColumnHeaders;
				func.ctx = ctx;
				func.run();
				assertTrue("Bad column name" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				DuplicateColumns func = new DuplicateColumns();
				func.srcTable = srcTable;
				func.origColumnHeaders = origColumnHeaders;
				List<CV_String> duplicateColumnHeadersBad = new ArrayList<CV_String>(origColumnHeaders);
				func.duplicateColumnHeaders = duplicateColumnHeadersBad;
				func.ctx = ctx;
				func.run();
				assertTrue("Duplicate column name" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
