/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 22, 2015 8:26:27 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class CombineJSONTest {

    /**
     * ExecutionContext
     */
    public ExecutionContext testCtx;

    /**
     * Message string to be printed out for every test
     */
    String message = "* DataManipulation JUnit4Test: CombineJSON : ";

    String arrayLastNameExpected = "[{\"firstname\":\"Jane\",\"phones\":{\"mobile\":\"222-555-9999\",\"home\":\"111-555-9999\"},\"office\":\"3x111\",\"lastname\":\"Miller\"},{\"firstname\":\"Joe\",\"phones\":{\"mobile\":\"222-555-1111\",\"home\":\"111-555-1111\"},\"office\":\"3x999\",\"lastname\":\"Dirt\"},{\"firstname\":\"Bob\",\"phones\":{\"mobile\":\"222-555-9999\",\"home\":\"111-555-9999\"},\"office\":\"3x555\",\"lastname\":\"Smith\"},{\"firstname\":\"Jill\",\"phones\":{\"mobile\":\"222-555-8888\",\"home\":\"111-555-8888\"},\"office\":\"3x555\",\"lastname\":\"Davis\"}]";
    String arrayOfficeExpected = "[{\"firstname\":\"Joe\",\"phones\":{\"mobile\":\"222-555-1111\",\"home\":\"111-555-1111\"},\"office\":\"3x999\",\"lastname\":\"Dirt\"},{\"firstname\":[\"Bob\",\"Jill\"],\"phones\":[{\"mobile\":\"222-555-9999\",\"home\":\"111-555-9999\"},{\"mobile\":\"222-555-8888\",\"home\":\"111-555-8888\"}],\"office\":\"3x555\",\"lastname\":[\"Smith\",\"Davis\"]},{\"firstname\":\"Jane\",\"phones\":{\"mobile\":\"222-555-9999\",\"home\":\"111-555-9999\"},\"office\":\"3x111\",\"lastname\":\"Miller\"}]";
    String arrayPhonesExpected = "[{\"firstname\":\"Joe\",\"phones\":{\"mobile\":\"222-555-1111\",\"home\":\"111-555-1111\"},\"office\":\"3x999\",\"lastname\":\"Dirt\"},{\"firstname\":\"Jill\",\"phones\":{\"mobile\":\"222-555-8888\",\"home\":\"111-555-8888\"},\"office\":\"3x555\",\"lastname\":\"Davis\"},{\"firstname\":[\"Bob\",\"Jane\"],\"phones\":{\"mobile\":\"222-555-9999\",\"home\":\"111-555-9999\"},\"office\":[\"3x555\",\"3x111\"],\"lastname\":[\"Smith\",\"Miller\"]}]";

    String objOfficeExpected = "[{\"firstname\":[\"Sally\",\"Margie\"],\"phones\":[{\"mobile\":\"222-555-0000\",\"home\":\"111-555-0000\"},{\"mobile\":\"222-555-6666\",\"home\":\"111-555-6666\"}],\"office\":\"3x000\",\"lastname\":[\"Dirt\",\"Smith\"]}]";

    /**
     * Functional Resources
     */
    List<CV_WebResource> inputArrays;

    /**
     * Functional Resources
     */
    List<CV_WebResource> inputObjects;

    @Before
    public void setUp() throws Exception {

        CV_WebResource wr1 = new CV_WebResource(this.getClass().getResource("/data/CombineJSON_1.json").toURI());
        CV_WebResource wr2 = new CV_WebResource(this.getClass().getResource("/data/CombineJSON_2.json").toURI());
        CV_WebResource wr3 = new CV_WebResource(this.getClass().getResource("/data/CombineJSON_3.json").toURI());
        CV_WebResource wr4 = new CV_WebResource(this.getClass().getResource("/data/CombineJSON_4.json").toURI());

        inputArrays = new ArrayList<>();
        inputArrays.add(wr1);
        inputArrays.add(wr2);
        inputArrays.add(wr3);

        inputObjects = new ArrayList<>();
        inputObjects.add(wr3);
        inputObjects.add(wr4);

        testCtx = new ExecutionContextImpl();
    }

    /**
     * Test of run method, of class CombineJSON.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testRun_array_lastname() throws IOException {
        System.out.println(message + "testRun_array_lastname()");

        CombineJSON instance = new CombineJSON();
        instance.ctx = testCtx;
        instance.files = inputArrays;
        instance.combineKey = new CV_String("lastname");
        instance.run();

        CV_WebResource result = instance.output;
        String resultContents = IOUtils.toString(result.getURL().openStream(), "UTF-8");

        Assert.assertEquals(arrayLastNameExpected, resultContents);
    }

    /**
     * Test of run method, of class CombineJSON.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testRun_array_office() throws IOException {
        System.out.println(message + "testRun_array_office()");

        CombineJSON instance = new CombineJSON();
        instance.ctx = testCtx;
        instance.files = inputArrays;
        instance.combineKey = new CV_String("office");
        instance.run();

        CV_WebResource result = instance.output;
        String resultContents = IOUtils.toString(result.getURL().openStream(), "UTF-8");

        Assert.assertEquals(arrayOfficeExpected, resultContents);
    }

    /**
     * Test of run method, of class CombineJSON.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testRun_array_phones() throws IOException {
        System.out.println(message + "testRun_array_phones()");

        CombineJSON instance = new CombineJSON();
        instance.ctx = testCtx;
        instance.files = inputArrays;
        instance.combineKey = new CV_String("phones");
        instance.run();

        CV_WebResource result = instance.output;
        String resultContents = IOUtils.toString(result.getURL().openStream(), "UTF-8");

        Assert.assertEquals(arrayPhonesExpected, resultContents);
    }

    /**
     * Test of run method, of class CombineJSON.
     *
     */
    @Test(expected = RuntimeException.class)
    public void testRun_array_mobile() {
        System.out.println(message + "testRun_array_mobile()");

        CombineJSON instance = new CombineJSON();
        instance.ctx = testCtx;
        instance.files = inputArrays;
        instance.combineKey = new CV_String("mobile");
        instance.run();
    }

    /**
     * Test of run method, of class CombineJSON.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testRun_obj_office() throws IOException {
        System.out.println(message + "testRun_obj_office()");

        CombineJSON instance = new CombineJSON();
        instance.ctx = testCtx;
        instance.files = inputObjects;
        instance.combineKey = new CV_String("office");
        instance.run();

        CV_WebResource result = instance.output;
        String resultContents = IOUtils.toString(result.getURL().openStream(), "UTF-8");

        Assert.assertEquals(objOfficeExpected, resultContents);
    }
}
