/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 8, 2015 9:25:00 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class ConcatenateHeterogeneousTablesTest {

    ExecutionContext test_ctx;
    CV_Table empTable, empTable2, deptTable, concatEmpEmp2IgnoreTable;
    String method = "* DataManipulation JUnit4Test: ConcatenateHeterogeneousTables : ";

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            empTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Employee.csv"), null).toReadable();
            empTable2 = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Employee_2.csv"), null).toReadable();
            deptTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/Department.csv"), null).toReadable();
            concatEmpEmp2IgnoreTable = test_ctx.createTableFromCSV(this.getClass().getResourceAsStream("/data/concatEmpEmp2Ignore.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(JoinTablesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class ConcatenateHeterogeneousTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_oneTable() throws Exception {
        System.out.println(method + "testRun_oneTable()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<CV_Table> tables = new ArrayList<>();
        tables.add(empTable);

        ConcatenateHeterogeneousTables instance = new ConcatenateHeterogeneousTables();
        instance.ctx = test_ctx;
        instance.inputTables = new ArrayList<>();
        instance.inputTables.addAll(tables);
        instance.run();

        CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), empTable.toReadable());
        assertTrue(result);
    }

    /**
     * Test of run method, of class ConcatenateHeterogeneousTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun() throws Exception {
        System.out.println(method + "testRun()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<CV_Table> tables = new ArrayList<>();
        tables.add(empTable);
        tables.add(deptTable);

        ConcatenateHeterogeneousTables instance = new ConcatenateHeterogeneousTables();
        instance.ctx = test_ctx;
        instance.inputTables = new ArrayList<>();
        instance.inputTables.addAll(tables);
        instance.run();

        //TODO: use compareTable once bug is fixed
        //CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        //boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), concatEmpDeptTable);
        //assertTrue(result);
        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                    thiscolumnrowvalue = current_row.get(3);
                    assertEquals("Harriet", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConcatenateHeterogeneousTablesTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ConcatenateHeterogeneousTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_diffCase() throws Exception {
        System.out.println(method + "testRun_diffCase()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<CV_Table> tables = new ArrayList<>();
        tables.add(empTable2);
        tables.add(deptTable);

        ConcatenateHeterogeneousTables instance = new ConcatenateHeterogeneousTables();
        instance.ctx = test_ctx;
        instance.inputTables = new ArrayList<>();
        instance.inputTables.addAll(tables);
        instance.run();

        //TODO: use compareTable once bug is fixed
        //CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        //boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), concatEmp2DeptTable);
        //assertTrue(result);
        //dataGen.printData(instance.result, Level.INFO, null);
        //dataGen.printData(concatEmp2DeptTable, Level.INFO, null);
        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Fred", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals("7654", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Sales", thiscolumnrowvalue.value());
                    thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 5) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                    thiscolumnrowvalue = current_row.get(3);
                    assertEquals("Harriet", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConcatenateHeterogeneousTablesTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ConcatenateHeterogeneousTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_diffType() throws Exception {
        System.out.println(method + "testRun_diffType()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<CV_Table> tables = new ArrayList<>();
        tables.add(empTable);
        tables.add(empTable2);

        ConcatenateHeterogeneousTables instance = new ConcatenateHeterogeneousTables();
        instance.ctx = test_ctx;
        instance.inputTables = new ArrayList<>();
        instance.inputTables.addAll(tables);
        instance.run();

        //TODO: use compareTable once bug is fixed
        //CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        //boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), concatEmp2DeptTable);
        //assertTrue(result);
        //dataGen.printData(instance.result, Level.INFO, null);
        //dataGen.printData(concatEmpEmp2Table, Level.INFO, null);
        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals("Harry", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals((long) 2241, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(2);
                    assertEquals("Finance", thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                    thiscolumnrowvalue = current_row.get(3);
                    assertEquals("9876", thiscolumnrowvalue.value());
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(ConcatenateHeterogeneousTablesTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class ConcatenateHeterogeneousTables.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRun_ignoreType() throws Exception {
        System.out.println(method + "testRun_ignoreType()");

        String methodname = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<CV_Table> tables = new ArrayList<>();
        tables.add(empTable);
        tables.add(empTable2);

        ConcatenateHeterogeneousTables instance = new ConcatenateHeterogeneousTables();
        instance.ctx = test_ctx;
        instance.inputTables = new ArrayList<>();
        instance.inputTables.addAll(tables);
        instance.ignoreType = new CV_Boolean(true);
        instance.run();

        CommonVocabDataGenerator dataGen = new CommonVocabDataGenerator();
        boolean result = dataGen.compareTable(methodname, instance.result.getUri(), instance.result.toReadable(), concatEmpEmp2IgnoreTable.toReadable());
        assertTrue(result);
    }
}
