/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class FilterTableRowByIntegerRangeTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    CV_Table testTable2;
    public FilterTableRowByIntegerRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {   
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testInteger{integer}"));
            CV_Super[] row1 = {new CV_String("true"), new CV_Integer(1)};
            CV_Super[] row2 = {new CV_String("true"), new CV_Integer(2)};
            CV_Super[] row3 = {new CV_String("true"), new CV_Integer(3)};
            CV_Super[] row4 = {new CV_String("true"), new CV_Integer(4)};
            CV_Super[] row5 = {new CV_String("false"), new CV_Integer(5)};
            CV_Super[] row6 = {new CV_String("false"), new CV_Integer(6)};
            CV_Super[] row7 = {new CV_String("false"), new CV_Integer(7)};
            CV_Super[] row8 = {new CV_String("false"), new CV_Integer(8)};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(FilterTableRowByIntegerRangeTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class FilterTableRowByIntegerRangeTest.
     * 
     * @throws jema.common.types.table.TableException
     */
 @Test
    public void testRun() throws TableException  {
        System.out.println("* Filter JUnit4Test: FilterTableRowByIntegerRangeTest : testRun()");

        FilterTableRowByIntegerRange instance = new FilterTableRowByIntegerRange();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.maxValue = new CV_Integer(4);
        instance.minValue = new CV_Integer(1);
        instance.columnName = new CV_String("testInteger");
        instance.run();

        CV_Table dest_table = instance.outputTable.toReadable();
        
        assertEquals(dest_table.size(), 4);
        
        List<CV_Super> current_row = new ArrayList<>();
        while (dest_table.readRow(current_row)) {

            CV_Super thiscolumnrowvalue = current_row.get(0);
            assertEquals(thiscolumnrowvalue.toString(), "true");

        }
        //CommonVocabDataGenerator dg = new CommonVocabDataGenerator();
        //dg.printData(instance.outputTable, Level.INFO, null);
    }
    
    
    /**
     * Test of testExpectedException method, of class FilterTableRowByIntegerRangeTest for a expected
     * exception. In this case I expect to see an exception because the column name is invalid.
     */
    @Test(expected = RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Filter JUnit4Test: FilterTableRowByIntegerRangeTest : testExpectedException()");
        FilterTableRowByIntegerRange instance = new FilterTableRowByIntegerRange();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.maxValue = new CV_Integer(4);
        instance.minValue = new CV_Integer(1);
        instance.columnName = new CV_String("INVALID COLUMN");
        instance.run();
    }
    
    
    
    /**
     * Test of testExpectedException method, of class
     * FilterTableRowByIntegerRangeTest for a expected exception. In this case
     * I expect to see an exception because the Timestamp value is invalid.
     */
    @Test(expected = RuntimeException.class)
    public void testExpectedException2() {
        System.out.println("* Filter JUnit4Test: FilterTableRowByIntegerRangeTest : testExpectedException2()");
        FilterTableRowByIntegerRange instance = new FilterTableRowByIntegerRange();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.maxValue = new CV_Integer("BAD DATA");
        instance.minValue = new CV_Integer(1);
        instance.columnName = new CV_String("testTimestamp");
        instance.run();
    }

    
}