 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 
 * @author CASCADE
 */
public class DeduplicateRowsTest {

    
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    public DeduplicateRowsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal},testInteger{integer},testTimestamp1{timestamp},testTimestamp2{timestamp},testTimestamp3{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111),new CV_Decimal(1.211111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111),new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000),new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111),new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-05T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(1.15110001),new CV_Decimal(1.111111), new CV_Integer(3),new CV_Timestamp("2015-01-05T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row7 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111),new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-05T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row8 = {new CV_String("even"), new CV_Decimal(1.15110001),new CV_Decimal(1.111111), new CV_Integer(3),new CV_Timestamp("2015-01-05T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z"),new CV_Timestamp("2015-01-01T16:09:43.328Z")};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable = testTable.toReadable();
            
            

        } catch (Exception ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DeduplicateRows.
     * This is a time range test similar to JEMA1.0 help documentation where order of rows is important.
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
   @Test  
    public void testRun() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun()");

        CV_Table run2Table = test_ctx.createTable("Test For Run2");
        run2Table.setHeader(new TableHeader("testTimestamp2{timestamp},testTimestamp3{timestamp}"));
        CV_Super[] row1 = {new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
        CV_Super[] row2 = {new CV_Timestamp("2015-01-02T16:09:43.328Z"), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
        CV_Super[] row3 = {new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        

        run2Table = run2Table.toReadable();

        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:4d");
        instance.run();
        int rowcount_result = 0;
        try {
          //  System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 2;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
       

       final CV_Table table = instance.outputTable.toReadable();
       table.stream().forEach((List<CV_Super> current_row) -> {

             try {
                 if (table.getCurrentRowNum() == 0) {
                     CV_Super thiscolumnrowvalue1 = current_row.get(0);
                     assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T16:09:43.328Z");
                     
                 } else if (table.getCurrentRowNum() == 1) {
                     
                     CV_Super thiscolumnrowvalue1 = current_row.get(0);
                     assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-05T16:09:43.328Z");
                     
                 }
                 table.readRow();
             } catch (TableException ex) {
                 Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
             }

       });

        
        
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp3:4d");
        instance.run();
        rowcount_result = 0;
      
        try {
           
            rowcount_result = instance.outputTable.size();
            
            CV_Table table2 =instance.outputTable.toReadable();
            table2.stream().forEach((List<CV_Super> current_row) -> {
            
                CV_Super thiscolumnrowvalue = current_row.get(1);
                assertEquals(thiscolumnrowvalue.value().toString(), "2015-01-02T16:09:43.328Z");
                
            });
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expected_rowcount_result = 1;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
     
    }
    
    /**
     * Test 2 of run method, of class DeduplicateRows.
     * This test tests for consistent results despite order of columns user entry of the oder of columns
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
    @Test
    public void testRun2() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun2()");

        CV_Table run2Table = test_ctx.createTable("Test For Run2");
        run2Table.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal}"));
        CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111)};
        CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111), new CV_Decimal(1.211111)};
        CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111), new CV_Decimal(1.111111)};
        CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000), new CV_Decimal(1.111111)};
        CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111)};
        CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111)};
        //rows 7 and 8 are exact duplicates of row 6
        CV_Super[] row7 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111)};
        CV_Super[] row8 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111)};
       
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row4)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row5)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row6)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row7)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row8)));
        run2Table = run2Table.toReadable();

        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:4,testcolumn2:4");
        instance.run();
        int result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expResult = 6;
       
        assertEquals(expResult, result);
        run2Table = run2Table.toReadable();
        //order of columns should not change the expected result
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn2:4,testcolumn1:4");
        instance.run();
        result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expResult = 6;
       
        assertEquals(expResult, result);
        
        
        try {

            CV_Table table =instance.outputTable.toReadable();
            table.stream().forEach((List<CV_Super> current_row) -> {
            
                CV_Super thiscolumnrowvalue = current_row.get(0);
                try {
                   
                    if(table.getCurrentRowNum()%2==0){
  
                        assertEquals(thiscolumnrowvalue.value().toString(), "odd");
      
                    }else{
                        assertEquals(thiscolumnrowvalue.value().toString(), "even");
                    }
                    table.readRow();
                } catch (TableException ex) {
                    Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
                }                
            });
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
     
    }
    

    /**
     * Test 3 of run method, of class DeduplicateRows.
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
    @Test
    public void testRun3() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun3()");


        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("test");
        instance.run();
        int result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expResult = 2;
       
        assertEquals(expResult, result);
      
        testTable = testTable.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("test,testInteger");
        instance.run();
        result = 0;
        try {
           // System.out.println(instance.outputTable.size());
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expResult = 6;
       
        assertEquals(expResult, result);
        
        CV_Table table =instance.outputTable.toReadable();
   
        table.stream().forEach((List<CV_Super> current_row) -> {

            try {

                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                     CV_Super thiscolumnrowvalue2 = current_row.get(2);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "odd");
                    assertEquals(thiscolumnrowvalue2.value().toString(), "1.111111");
                }else if(table.getCurrentRowNum() == 1) {
                     CV_Super thiscolumnrowvalue1 = current_row.get(0);
                     CV_Super thiscolumnrowvalue2 = current_row.get(2);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "even");
                    assertEquals(thiscolumnrowvalue2.value().toString(), "1.211111");
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
     
    }
    
    
    /**
     * Test 4 of run method, of class DeduplicateRows.
     * This test tests for decimal precision
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
    @Test
    public void testRun4() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun4()");

        CV_Table run2Table = test_ctx.createTable("Test For Run4");
        run2Table.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal}"));
        CV_Super[] row1 = {new CV_String("Unique"), new CV_Decimal(1.1111), new CV_Decimal(1.10001111)};
        CV_Super[] row2 = {new CV_String("Duplic"), new CV_Decimal(1.1111), new CV_Decimal(1.10001111)};
        
        CV_Super[] row3 ={new CV_String("Unique"), new CV_Decimal(1.1111), new CV_Decimal(1.12001111)};
        CV_Super[] row4 ={new CV_String("Duplic"), new CV_Decimal(1.1111), new CV_Decimal(1.12001111)};
        
        CV_Super[] row5 = {new CV_String("Unique"), new CV_Decimal(1.1111), new CV_Decimal(1.12301111)};
        CV_Super[] row6 = {new CV_String("Duplic"), new CV_Decimal(1.1111), new CV_Decimal(1.12301111)};
        
        CV_Super[] row7 ={new CV_String("Unique"), new CV_Decimal(1.1111), new CV_Decimal(1.12341111)};
        CV_Super[] row8 ={new CV_String("Duplic"), new CV_Decimal(1.1111), new CV_Decimal(1.12341111)};
        
        CV_Super[] row9 ={new CV_String("Unique"), new CV_Decimal(1.1111), new CV_Decimal(1.12345111)};
        CV_Super[] row10 ={new CV_String("Duplic"), new CV_Decimal(1.1111), new CV_Decimal(1.12345111)};

       
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row4)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row5)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row6)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row7)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row8)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row9)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row10)));

        run2Table = run2Table.toReadable();
        //precision set to 1
        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:1,testcolumn2:1");
        instance.run();
        int result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expResult = 1;
       
        assertEquals(expResult, result);
        
        CV_Table table =instance.outputTable.toReadable();
   
        table.stream().forEach((List<CV_Super> current_row) -> {

            try {

                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue1 = current_row.get(2);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "1.10001111");

                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        
       // System.out.println("_____________________________TEST PRECSISION2");
        run2Table = run2Table.toReadable();
        //increase precsion to 2
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:2,testcolumn2:2");
        instance.run();
        result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expResult = 2;
       
        assertEquals(expResult, result);
        
        //increase precsion to 3
        //System.out.println("_____________________________TEST PRECSISION3");
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:3,testcolumn2:3");
        instance.run();
        result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expResult = 3;
       
        assertEquals(expResult, result);
        
        //increase precsion to 4
        //System.out.println("_____________________________TEST PRECSISION4");
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:4,testcolumn2:4");
        instance.run();
        result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Expect 5 if HALF_UP rounding was used but we are using FLOOR rounding.
        expResult =4;
       
        assertEquals(expResult, result);
        
        //set rounding to true at precsion to 4
        //System.out.println("_____________________________TEST PRECSISION4");
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:4,testcolumn2:4");
        instance.isRounded = new CV_Boolean(true);
        instance.run();
        result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Expect 5 since we are using HALF_UP rounding.
        expResult =5;
       
        assertEquals(expResult, result);
    }
      /**
     * Test of run method, of class DeduplicateRows.
     * This is a time range test of different levels of time precision
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
    @Test  
    public void testRun5() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun5()");

        CV_Table run2Table = test_ctx.createTable("Test For Run2");
        run2Table.setHeader(new TableHeader("testTimestamp2{timestamp}"));
        //first three row are all within 3 days of another
        CV_Super[] row1 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row2 = {new CV_Timestamp("2015-01-02T16:09:43.328Z")};
        CV_Super[] row3 = {new CV_Timestamp("2015-01-03T16:09:43.328Z")};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));       
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        //these three row are all within 3 hours of another
        CV_Super[] row4 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row5 = {new CV_Timestamp("2015-01-01T17:09:43.328Z")};
        CV_Super[] row6 = {new CV_Timestamp("2015-01-01T18:09:43.328Z")};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row4)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row5)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row6)));
        //these three row are all within 3 minutes of another
        CV_Super[] row7 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row8 = {new CV_Timestamp("2015-01-01T16:10:43.328Z")};
        CV_Super[] row9 = {new CV_Timestamp("2015-01-01T16:11:43.328Z")};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row7)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row8)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row9)));
        //these three row are all within 3 seconds of another
        CV_Super[] row10 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row11 = {new CV_Timestamp("2015-01-01T16:09:44.328Z")};
        CV_Super[] row12 = {new CV_Timestamp("2015-01-01T16:09:45.328Z")};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row10)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row11)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row12)));
        

        run2Table = run2Table.toReadable();

        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:3d");
        instance.run();
        int rowcount_result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 1;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
        CV_Table table =instance.outputTable.toReadable();
   
        table.stream().forEach((List<CV_Super> current_row) -> {

            try {

                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue1 = current_row.get(0);
                    assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T16:09:43.328Z");

                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:3h");
        instance.run();
        rowcount_result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expected_rowcount_result = 3;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:3m");
        instance.run();
        rowcount_result = 0;
        try {
            //System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expected_rowcount_result = 5;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
        run2Table = run2Table.toReadable();
        instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:3s");
        instance.run();
        rowcount_result = 0;
        try {
           // System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        expected_rowcount_result = 7;
       
        assertEquals(expected_rowcount_result, rowcount_result);
     
    }
    
    
    /**
     * Test of run method, of class DeduplicateRows.
     * This is a time range test across two columns
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
    @Test
    public void testRun6() throws TableException, Exception {
    System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun6()");

    CV_Table run2Table = test_ctx.createTable("Test For Run2");
    run2Table.setHeader(new TableHeader("testTimestamp2{timestamp},testTimestamp3{timestamp}"));
    //the first two rows in the second column are within 5 minutes of eachother.
    CV_Super[] row1 = {new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
    CV_Super[] row2 = {new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:10:43.328Z")};
    CV_Super[] row3 = {new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:50:43.328Z")};
    run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
    run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
    run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));

    run2Table = run2Table.toReadable();

    DeduplicateRows instance = new DeduplicateRows();
    instance.inputTable = run2Table;
    instance.ctx = test_ctx;
    instance.input_columns = new CV_String("testTimestamp2:1d,testTimestamp3:5m");
    instance.run();
    int rowcount_result = 0;
    try {
       // System.out.println(instance.outputTable.size());
        rowcount_result = instance.outputTable.size();
    } catch (TableException ex) {
        Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int expected_rowcount_result = 2;

    assertEquals(expected_rowcount_result, rowcount_result);
    
    CV_Table table =instance.outputTable.toReadable();
   
    table.stream().forEach((List<CV_Super> current_row) -> {

       
        try {

            if (table.getCurrentRowNum() == 0) {
                CV_Super thiscolumnrowvalue1 = current_row.get(0);
                CV_Super thiscolumnrowvalue2 = current_row.get(1);
                assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T16:09:43.328Z");
                assertEquals(thiscolumnrowvalue2.value().toString(), "2015-01-01T16:09:43.328Z");

            } else if(table.getCurrentRowNum() == 1){
                
                CV_Super thiscolumnrowvalue1 = current_row.get(0);
                CV_Super thiscolumnrowvalue2 = current_row.get(1);
                assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T16:09:43.328Z");
                assertEquals(thiscolumnrowvalue2.value().toString(), "2015-01-01T16:50:43.328Z");
                
            }
            table.readRow();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    });
        
    }
    
     /**
     * Test of run method, of class DeduplicateRows.
     * This is a time range test  where order of rows is important.
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
     */
   @Test  
    public void testRun7() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun7()");

        CV_Table run2Table = test_ctx.createTable("Test For Run2");
        run2Table.setHeader(new TableHeader("testTimestamp2{timestamp}"));
        CV_Super[] row1 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};//first row
        CV_Super[] row2 = {new CV_Timestamp("2015-01-01T16:09:53.328Z")};//add 10 seconds
        CV_Super[] row3 = {new CV_Timestamp("2015-01-01T16:19:43.328Z")};//add 10 minutes
        CV_Super[] row4 = {new CV_Timestamp("2015-01-01T23:09:43.328Z")};//add 7 hours
        CV_Super[] row5 = {new CV_Timestamp("2015-01-01T16:09:43.328Z")};//this is the same time as the first row.
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row4)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row5)));
        

        run2Table = run2Table.toReadable();

        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testTimestamp2:2h");
        instance.run();
        int rowcount_result = 0;
        try {
           // System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 2;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
        
            CV_Table table =instance.outputTable.toReadable();
   
    table.stream().forEach((List<CV_Super> current_row) -> {

       
        try {

            if (table.getCurrentRowNum() == 0) {
                CV_Super thiscolumnrowvalue1 = current_row.get(0);
                assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T16:09:43.328Z");


            } else if(table.getCurrentRowNum() == 1){
                
                CV_Super thiscolumnrowvalue1 = current_row.get(0);
                assertEquals(thiscolumnrowvalue1.value().toString(), "2015-01-01T23:09:43.328Z");

                
            }
            table.readRow();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    });
        

    }    
    
    
    /**
     * Test of run method, of class DeduplicateRows.
     * This is a test of null values in the columns of the table.
     * The test should run without exception.
     * @throws jema.common.types.table.TableException
     * @throws java.lang.Exception
    */
    @Test
    public void testRun8() throws TableException, Exception {
         System.out.println("* Filter JUnit4Test: DeduplicateRows : testRun8()");

        CV_Table run2Table = test_ctx.createTable("Test For Run2");
        run2Table.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal},testTimestamp1{timestamp}"));
        CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111), new CV_Decimal(1.211111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111), new CV_Decimal(1.111111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000), new CV_Decimal(1.111111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111),new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        //rows 7is null for testing purposes
        CV_Super[] row7 = {null, null, null, null};
        CV_Super[] row8 = {new CV_String("NOTNULL"), null, null, null};
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row1)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row2)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row3)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row4)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row5)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row6)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row7)));
        run2Table.appendRow(new ArrayList<>(Arrays.asList(row8)));
        run2Table = run2Table.toReadable();

        DeduplicateRows instance = new DeduplicateRows();
        instance.inputTable = run2Table;
        instance.ctx = test_ctx;
        instance.input_columns = new CV_String("testcolumn1:4,testTimestamp1:4");
        instance.run();
            
        int expected_rowcount_result = 8;
        
         int rowcount_result = 0;
        try {
           // System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        assertEquals(expected_rowcount_result, rowcount_result);
        
     
    }

    /**
     * Test of the TimestampCompare method, of class DeduplicateRows.
     * This is a time range test that compares if a time existing within a timeframe range.
     */
    @Test
    public void testTimestampCompare(){
        System.out.println("* Filter JUnit4Test: DeduplicateRows : testTimestampCompare()");
        CV_Timestamp time1 = new CV_Timestamp("2015-01-01T16:09:43.328Z");
        CV_Timestamp time2 = new CV_Timestamp("2015-01-01T16:09:53.328Z");
        boolean result=   DeduplicateRows.TimestampCompare(time1, time2, 60);
        assertTrue(result);
    }
    
    /**
     * Test of run method, of class DeduplicateRows for a expected exception.
     * In this case I expect to see an exception if the column decimal precision value is not associated with a column.
     */
    @Test (expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Filter JUnit4Test: DeduplicateRows :  testExpectedException()");
        DeduplicateRows instance = new DeduplicateRows();
        instance.input_columns=new CV_String("testcolumn1:adsf");//column precision is missing
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.run();

    }
    
    /**
     * Test of run method, of class DeduplicateRows for a expected exception. In
     * this case I expect to see an exception if the column cannot be found in the table.
     */
    @Test (expected=RuntimeException.class)
    public void testExpectedException2() {
        System.out.println("* Filter JUnit4Test: DeduplicateRows :  testExpectedException()");
        DeduplicateRows instance = new DeduplicateRows();
        instance.input_columns = new CV_String("doesnt_exist:4");//column precision is missing
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.run();

    }
    
    
    
}
