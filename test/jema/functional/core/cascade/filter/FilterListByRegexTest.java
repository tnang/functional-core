package jema.functional.core.cascade.filter;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.functional.api.ExecutionContextImpl;


public class FilterListByRegexTest 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(FilterListByRegexTest.class.getName());

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.getMessage()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.getMessage()));
		}    	
	}

	/**
	 * Simple REGEX test
	 */
	@Test
	public void testSimpleRegex1()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("/[12]/"));
			regexList.add(CV_String.valueOf("/[5]/"));
			
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// Validate results
			boolean result = this.compareResults(methodName, expectedResultList, filteredList);
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple REGEX test
	 */
	@Test
	public void testSimpleRegex2()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(!((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("[0-9&&[^125]]"));
			
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// Validate results
			boolean result = this.compareResults(methodName, expectedResultList, filteredList);
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid REGEX test
	 */
	@Test
	public void testWordRegex()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				CV_String str = CV_String.valueOf(String.format("Simple input data string %d", iStr));
				expectedResultList.add(str);
				data.add(str);
			}
			data.add(CV_String.valueOf("An excluded string"));
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("^((?!excluded).)*$"));
			
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// Validate results
			boolean result = this.compareResults(methodName, expectedResultList, filteredList);
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple REGEX test - no data match
	 */
	@Test
	public void testRegexNoMatch()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			data.add(CV_String.valueOf(String.format("(A) NO DIGITS")));
			data.add(CV_String.valueOf(String.format("(B) NO DIGITS")));
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("/[12]/"));
			regexList.add(CV_String.valueOf("/[5]/"));
			
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// Validate results
			boolean result = this.compareResults(methodName, expectedResultList, filteredList);
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple REGEX test - no data match
	 */
	@Test
	public void testRegexInterfaceNoMatch()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			data.add(CV_String.valueOf(String.format("(A) NO DIGITS")));
			data.add(CV_String.valueOf(String.format("(B) NO DIGITS")));
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("/[12]/"));
			regexList.add(CV_String.valueOf("/[5]/"));
			
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// List to String
			CV_String delimiter = new CV_String("");
			CV_String str = this.listToString(filteredList, delimiter);
			LOGGER.log(Level.FINE, String.format("%s: str=%s", methodName, str));
			
			// Validate results
			boolean result = str != null;						
			assertTrue("Result should be an empty string", result);
				
			// Count list element
			CV_Integer count = this.countListElement(filteredList);
			LOGGER.log(Level.FINE, String.format("%s: count=%d", methodName, count.value()));
			
			// Validate results
			result = count != null;						
			assertTrue("Result count be zero", result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple REGEX test - no data match
	 */
	@Test
	public void testRegexInterfaceMatch()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(!((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("[0-9&&[^125]]"));
						
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// List to String
			CV_String delimiter = new CV_String("");
			CV_String str = this.listToString(filteredList, delimiter);
			LOGGER.log(Level.FINE, String.format("%s: str=%s", methodName, str));
			
			// Validate results
			boolean result = str != null;						
			assertTrue("Result should be an empty string", result);
				
			// Count list element
			CV_Integer count = this.countListElement(filteredList);
			LOGGER.log(Level.FINE, String.format("%s: count=%d", methodName, count.value()));
			
			// Validate results
			result = count != null;						
			assertTrue("Result count be zero", result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple REGEX test - no data match
	 */
	@Test
	public void testRegexInterfaceNull()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(!((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			data.add(null);
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("[0-9&&[^125]]"));
						
			// Initialize functional
			FilterListByRegex func = new FilterListByRegex();
			func.inputList = data;
			func.regexList = regexList;
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			List<CV_String> filteredList = func.filteredList;
			
			// List to String
			CV_String delimiter = new CV_String("");
			CV_String str = this.listToString(filteredList, delimiter);
			LOGGER.log(Level.FINE, String.format("%s: str=%s", methodName, str));
			
			// Validate results
			boolean result = str != null;						
			assertTrue("Result should be an empty string", result);
				
			// Count list element
			CV_Integer count = this.countListElement(filteredList);
			LOGGER.log(Level.FINE, String.format("%s: count=%d", methodName, count.value()));
			
			// Validate results
			result = count != null;						
			assertTrue("Result count be zero", result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(!((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			regexList.add(CV_String.valueOf("[0-9&&[^125]]"));
			
			try
			{
				FilterListByRegex func = new FilterListByRegex();
				func.inputList = null;
				func.regexList = regexList;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Input list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.getMessage());assertTrue(methodName, true);}
			
			try
			{
				FilterListByRegex func = new FilterListByRegex();
				func.inputList = data;
				func.regexList = null;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("REGEX list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.getMessage());assertTrue(methodName, true);}
			
			try
			{
				FilterListByRegex func = new FilterListByRegex();
				func.inputList = data;
				func.regexList = regexList;
				func.ctx = null;
				func.run();
				assertTrue("Context expression is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.getMessage());assertTrue(methodName, true);}
			
			try
			{
				FilterListByRegex func = new FilterListByRegex();
				func.inputList = null;
				func.regexList = null;
				func.ctx = null;
				func.run();
				assertTrue("All arguments are null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.getMessage());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRegex()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// InputExpected Test results
			List<CV_String> data = new ArrayList<CV_String>();
			List<CV_String> expectedResultList = new ArrayList<CV_String>();
			for(int iStr = 0; iStr < 10; iStr++)
			{
				String str = String.format("Simple input data string %d", iStr);
				if(!((iStr == 1) || (iStr == 2) || (iStr == 5)))
				{
					expectedResultList.add(CV_String.valueOf(str));
				}
				data.add(CV_String.valueOf(str));
			}
			
			// Regular expression list
			List<CV_String> regexList = new ArrayList<CV_String>();
			
			// Missing right brackets
			regexList.add(CV_String.valueOf("[0-9&&[^125"));
			
			try
			{
				FilterListByRegex func = new FilterListByRegex();
				func.inputList = data;
				func.regexList = regexList;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Regex - Missing right brackets" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.getMessage());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param tableJava Destination table containing JAVA computed data
	 * @param tableSQL Destination table containing SQL computed data
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	public boolean compareResults(
		String methodName,
		List<CV_String> expectedResultList,
		List<CV_String> actualResultList) throws Exception
	{
		boolean result = (expectedResultList != null) && (actualResultList != null);

		try
		{
			if(result)
			{
				int actualListSize = actualResultList.size();
				int expectedListSize = expectedResultList.size();
				result = (actualListSize == expectedListSize);

				for(int iExpected = 0; iExpected < expectedListSize && result; iExpected++)
				{
					int iActual = iExpected;
					CV_String expectedResult = expectedResultList.get(iExpected);
					CV_String actualResult = expectedResultList.get(iActual);
					if(expectedResult == null && actualResult == null) {result = true;}
					else if(expectedResult == null || actualResult == null) {result = false;}
					else {result = expectedResult.equals(actualResult);}			
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s - List compare failed: %s", methodName, e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}

		return result;
	}
	
	/**
	 * Jema-2 ListToString functional.
	 * @param input Input list of strings.
	 * @param delimiter Delimiter
	 * @return String joined from list of strings.
	 */
	protected CV_String listToString(
			List<CV_String> input,
			CV_String delimiter)
	{
		CV_String output = null;
        List<String> stringInput = 
        	input.stream().filter(Objects::nonNull).map(e -> e.value()).collect(Collectors.toList());
        output = new CV_String(String.join(delimiter.value(), stringInput));
        return output;

	}
	
	/**
	 * Jema-2 CountListElement functional.
	 * @param list Input list of strings.
	 * @return String joined from list of strings.
	 */
	protected CV_Integer countListElement(
			List<? extends CV_Super<?>> list)
	{
		CV_Integer count = null;
		count = new CV_Integer(String.valueOf(list.size()));
        return count;

	}

}
