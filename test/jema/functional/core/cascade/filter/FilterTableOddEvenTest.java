 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Note that this unit test has a dependency on the mongo-java-driver.jar.
 * @author CASCADE
 */
public class FilterTableOddEvenTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    
    public FilterTableOddEvenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable = testTable.toReadable();
            
            

        } catch (Exception ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class FilterTableOddEven for a expected exception.
     * In this case I expect to see an exception if no table is provided.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Filter JUnit4Test: FilterTableOddEven :  testExpectedException()");
        FilterTableOddEven instance = new FilterTableOddEven();
        instance.even =new CV_Boolean(true);
        instance.ctx = test_ctx;
        instance.run();

    }
   /**
     * Test of run method, of class FilterTableOddEven.
     * In these cases, I expect to see either 2 or 3 rows returned depending on if odd or even is selected.
     */
    @Test
    public void testRun() {
        System.out.println("* Filter JUnit4Test: FilterTableOddEven : testRun()");
        FilterTableOddEven instance = new FilterTableOddEven();
        instance.inputTable= testTable;
        //Here we are looking for a count of even rows which should be 2
        instance.even =new CV_Boolean(true);
        instance.ctx = test_ctx;
        instance.run();

        int expResult = 2;
        int result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
        
        
        instance = new FilterTableOddEven();
        instance.inputTable= testTable;
        //Here we are looking for a count of odd rows which should be 3
        instance.even =new CV_Boolean(false);
        instance.ctx = test_ctx;
        instance.run();

        expResult = 3;
        result = 0;
        try {
            result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
        
    }

    /**
     * Test of run method, of class FilterTableOddEven.
     * This test looks at the data in the output table to confirm result set.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun2() throws TableException {
        System.out.println("* Filter JUnit4Test: FilterTableOddEven : testRun()2");

        FilterTableOddEven instance = new FilterTableOddEven();
        instance.inputTable = testTable;
        instance.even = new CV_Boolean(true);
        instance.ctx = test_ctx;
        instance.run();

        final CV_Table dest_table = instance.outputTable.toReadable();

        List<CV_Super> current_row = new ArrayList<>();
        while (dest_table.readRow(current_row)) {

            CV_Super thiscolumnrowvalue = current_row.get(0);
            assertEquals(thiscolumnrowvalue.toString(), "even");

        }
        
        
        
        instance = new FilterTableOddEven();
        instance.inputTable = testTable;
        instance.even = new CV_Boolean(false);
        instance.ctx = test_ctx;
        instance.run();

        final CV_Table dest_table2 = instance.outputTable.toReadable();

        current_row = new ArrayList<>();
        while (dest_table2.readRow(current_row)) {

            CV_Super thiscolumnrowvalue = current_row.get(0);
            assertEquals(thiscolumnrowvalue.toString(), "odd");

        }
        
    }
    
}
//UNCLASSIFIED