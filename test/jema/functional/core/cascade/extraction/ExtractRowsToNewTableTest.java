 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.filter.FilterTableOddEvenTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class ExtractRowsToNewTableTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    
    public ExtractRowsToNewTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(7.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row7 = {new CV_String("odd"), new CV_Decimal(8.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row8 = {new CV_String("even"), new CV_Decimal(9.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row9 = {new CV_String("odd"), new CV_Decimal(16.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            CV_Super[] row10 = {new CV_String("even"), new CV_Decimal(17.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row9)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row10)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(FilterTableOddEvenTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ExtractRowsToNewTable.
     * This test uses the stopIndex value.
     */
    @Test
    public void testRun() throws TableException {
        System.out.println("* Extraction JUnit4Test: ExtractRowsToNewTableTest : testRun()");
        ExtractRowsToNewTable instance = new ExtractRowsToNewTable();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.startIndex = new CV_Integer(1);
        instance.stopIndex = new CV_Integer(2);
        instance.run();

        CV_Table dest_table = instance.outputTable.toReadable();
        assertEquals(dest_table.size(), 2);

        dest_table.stream().forEach(row -> {
            try {

                int row_number = dest_table.getCurrentRowNum();
                List<CV_Super> current_row = dest_table.readRow();
                if (row_number == 0) {

                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals(thiscolumnrowvalue.toString(), "odd");

                } else if (row_number == 1) {

                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals(thiscolumnrowvalue.toString(), "even");

                }

            } catch (TableException ex) {
                fail("Exception" + ex.getMessage());
            }

        });
    }
    
    
    /**
     * Test of run method, of class ExtractRowsToNewTable.
     * This test uses the length value.
     */
    @Test
    public void testRun2()throws TableException  {
        System.out.println("* Extraction JUnit4Test: ExtractRowsToNewTableTest : testRun2()");
        ExtractRowsToNewTable instance = new ExtractRowsToNewTable();
        instance.ctx =test_ctx;
        instance.inputTable = testTable;
        instance.startIndex = new CV_Integer(2);
        instance.length = new CV_Integer(5);
        instance.run();
        
        
        CV_Table dest_table = instance.outputTable.toReadable();
        assertEquals(dest_table.size(), 5);

        dest_table.stream().forEach(row -> {
            try {

                int row_number = dest_table.getCurrentRowNum();
                List<CV_Super> current_row = dest_table.readRow();
                if (row_number == 0) {

                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals(thiscolumnrowvalue.toString(), "even");

                } else if (row_number == 1) {

                    CV_Super thiscolumnrowvalue = current_row.get(0);
                    assertEquals(thiscolumnrowvalue.toString(), "odd");

                }else if (row_number == 3) {

                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(thiscolumnrowvalue.toString(), "6.0");

                }

            } catch (TableException ex) {
                fail("Exception" + ex.getMessage());
            }

        });
    }
    
    
    /**
     * Test of testExpectedException method, of class
     * ExtractRowsToNewTable for a expected exception. In this case
     * I expect to see an exception because the start index is greater than the stop index.
     */
    @Test(expected = RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Extraction JUnit4Test: ExtractRowsToNewTableTest : testExpectedException()");
        ExtractRowsToNewTable instance = new ExtractRowsToNewTable();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.startIndex = new CV_Integer(200);
        instance.stopIndex = new CV_Integer(30);
        instance.run();
    }
   
    
        /**
     * Test of testExpectedException method, of class
     * ExtractRowsToNewTable for a expected exception. In this case
     * I expect to see an exception because neither a stop-index or length was provided as input.
     */
    @Test(expected = RuntimeException.class)
    public void testExpectedException2() {
        System.out.println("* Extraction JUnit4Test: ExtractRowsToNewTableTest : testExpectedException2()");
        ExtractRowsToNewTable instance = new ExtractRowsToNewTable();
        instance.ctx = test_ctx;
        instance.inputTable = testTable;
        instance.startIndex = new CV_Integer(200);
        instance.run();
    }
    
}
//Unclassified
