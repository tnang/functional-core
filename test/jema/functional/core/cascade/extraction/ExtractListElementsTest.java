 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.extraction;

import java.util.Arrays;
import java.util.List;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class ExtractListElementsTest {
    
    List<CV_String> testlist;
    
    public ExtractListElementsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        testlist =  Arrays.asList(new CV_String("1"),new CV_String("2"),new CV_String("3"),new CV_String("4"),new CV_String("5"),new CV_String("6"),new CV_String("7"),new CV_String("8"),new CV_String("9"),new CV_String("10"));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ExtractListElements.
     */
    @Test
    public void testRun() {
        System.out.println("* Filter JUnit4Test: ExtractListElements : testRun()");
        ExtractListElements instance = new ExtractListElements();
        instance.input = testlist;
        instance.startIndex = new CV_Integer(1);
        instance.stopIndex = new CV_Integer(5); 
        instance.run();
        int result = instance.result.size();
        int expResult = 5;
        assertEquals(expResult, result);
        List<CV_String> compare_to_result =  Arrays.asList(new CV_String("1"),new CV_String("2"),new CV_String("3"),new CV_String("4"),new CV_String("5"));
        assertTrue(compare_to_result.equals(instance.result));
        
        instance = new ExtractListElements();
        instance.input = testlist;
        instance.startIndex = new CV_Integer(1);
        instance.length = new CV_Integer(3);
        instance.run();
        result = instance.result.size();
        expResult = 3;
        assertEquals(expResult, result);
        compare_to_result =  Arrays.asList(new CV_String("1"),new CV_String("2"),new CV_String("3"));
        assertTrue(compare_to_result.equals(instance.result));
        
        
        instance = new ExtractListElements();
        instance.input = testlist;
        instance.stopIndex = new CV_Integer(10);
        instance.length = new CV_Integer(3);
        instance.run();
        result = instance.result.size();
        expResult = 3;
        assertEquals(expResult, result);
        compare_to_result =  Arrays.asList(new CV_String("8"),new CV_String("9"),new CV_String("10"));
        assertTrue(compare_to_result.equals(instance.result));
       
    }
    
    /**
     *Test of run method, of class ExtractListElements for ExpectedExceptions.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Filter JUnit4Test: ExtractListElements : testExpectedException()");
        ExtractListElements instance = new ExtractListElements();
        instance.input = testlist;
        instance.startIndex = new CV_Integer(6);
        instance.stopIndex = new CV_Integer(5); 
        instance.run();
    }
    
        /**
     *Test of run method, of class ExtractListElements for ExpectedExceptions.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException2() {
        System.out.println("* Filter JUnit4Test: ExtractListElements : testExpectedException2()");
        ExtractListElements instance = new ExtractListElements();
        instance.input = testlist;
        instance.startIndex = new CV_Integer(-1);
        instance.stopIndex = new CV_Integer(5); 
        instance.run();
    }
    
    
}
