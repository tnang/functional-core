/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 13, 2015 9:32:07 AM
 */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class ExtractColumnAsStringTest {

    ExecutionContext test_ctx;
    CV_Table testTable;

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("Odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("evEn"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(6.0), null, new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(ExtractColumnAsStringTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class ExtractColumnAsString.
     */
    @Test
    public void testRun() {
        System.out.println("* Manipulation.Table JUnit4Test: ExtractColumnAsString : testRun()");

        //Test CV_String
        ExtractColumnAsString instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("test");
        instance.run();
        assertEquals(new CV_String("odd,even,Odd,evEn,odd").value(), instance.result.value());

        //Test CV_Decimal and user defined separator
        instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("testDecimal");
        instance.separator = new CV_String("|");
        instance.run();
        assertEquals(new CV_String("1.11|10.33|5.0|23.11|6.0").value(), instance.result.value());
    }

    /**
     * Test of run method, of class ExtractColumnAsString.
     */
    @Test
    public void testRun_deDup() {
        System.out.println("* Manipulation.Table JUnit4Test: ExtractColumnAsString : testRun_deDup()");

        //Test CV_String
        ExtractColumnAsString instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("test");
        instance.dedup = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_String("odd,even,Odd,evEn").value(), instance.result.value());

        //Test CV_Timestamp and user defined separator
        instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("testTimestamp");
        instance.separator = new CV_String("|");
        instance.dedup = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_String("2015-01-01T16:09:43.328Z|2015-01-02T16:09:43.328Z|2015-01-05T16:09:43.328Z|2015-01-10T16:09:43.328Z").value(), instance.result.value());
    }

    /**
     * Test of run method, of class ExtractColumnAsString.
     */
    @Test
    public void testRun_deDup_ignoreCase() {
        System.out.println("* Manipulation.Table JUnit4Test: ExtractColumnAsString : testRun_deDup_ignoreCase()");

        //Test CV_String and ignore case
        ExtractColumnAsString instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("test");
        instance.dedup = new CV_Boolean(true);
        instance.caseSensitive = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_String("odd,even").value(), instance.result.value());
    }
    /**
     * Test of run method, of class ExtractColumnAsString.
     */
    @Test
    public void testRun_withNull() {
        System.out.println("* Manipulation.Table JUnit4Test: ExtractColumnAsString : testRun_withNull()");

        //Test CV_Integer with null
        ExtractColumnAsString instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("testInteger");
        instance.separator = new CV_String("|");
        instance.run();
        assertEquals(new CV_String("1|10|5|23|").value(), instance.result.value());
    }

    /**
     * Test of run method, of class ExtractColumnAsString.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnNotFound() {
        System.out.println("* Manipulation.Table JUnit4Test: ExtractColumnAsString : testRun_columnNotFound()");

        //Test column name not in table
        ExtractColumnAsString instance = new ExtractColumnAsString();
        instance.inputTable = testTable;
        instance.columnName = new CV_String("BACON");
        instance.run();
    }
}
