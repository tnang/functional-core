/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.extraction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class RemoveListElementTest {
    
    List<CV_String> testlist;
    ExecutionContext test_ctx;
    
    public RemoveListElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        test_ctx = new ExecutionContextImpl();

        testlist =new LinkedList(Arrays.asList(new CV_String("1"), new CV_String("2"), new CV_String("3"), new CV_String("4"), new CV_String("5"), new CV_String("6"), new CV_String("7"), new CV_String("8"), new CV_String("9"), new CV_String("10")));

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class RemoveListElement.
     */
    @Test
    public void testRun() {
        System.out.println("* Extraction JUnit4Test: RemoveListElement : testRun()");
        RemoveListElement instance = new RemoveListElement();
        instance.ctx = test_ctx;
        instance.input = testlist;
        instance.index = new CV_Integer(3);
        instance.run();
        int result = instance.result.size();
        int expResult = 9;
        assertEquals(expResult, result);
        
        List<CV_String> compare_to_result = Arrays.asList(new CV_String("1"), new CV_String("2"), new CV_String("4"), new CV_String("5"), new CV_String("6"), new CV_String("7"), new CV_String("8"), new CV_String("9"), new CV_String("10"));

        assertTrue(compare_to_result.equals(instance.result));
    }
    
    
    /**
     *Test of run method, of class ExtractListElements for ExpectedException of IndexOutOfBounds.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Extraction JUnit4Test: RemoveListElement : testExpectedException()");
        RemoveListElement instance = new RemoveListElement();
        instance.ctx = test_ctx;
        instance.input = testlist;
        instance.index = new CV_Integer(100);
        instance.run();
    }
    
    
}
