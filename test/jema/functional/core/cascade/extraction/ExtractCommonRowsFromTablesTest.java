/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.filter.DeduplicateRowsTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Cascade
 */
public class ExtractCommonRowsFromTablesTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    CV_Table testTable2;
    Map<String, List<CV_Super>> expected_rows_map = new HashMap<>();
    
    
    public ExtractCommonRowsFromTablesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
            ExtractCommonRowsFromTables instance = new ExtractCommonRowsFromTables();
            test_ctx = new ExecutionContextImpl();
            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            expected_rows_map.put(instance.createRowHash(Arrays.asList(row1)),Arrays.asList(row1));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            expected_rows_map.put(instance.createRowHash(Arrays.asList(row2)),Arrays.asList(row2));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            expected_rows_map.put(instance.createRowHash(Arrays.asList(row3)),Arrays.asList(row3));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            expected_rows_map.put(instance.createRowHash(Arrays.asList(row4)),Arrays.asList(row4));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable = testTable.toReadable();
        
            
            
            
            testTable2 = test_ctx.createTable("Test2");
            testTable2.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
            CV_Super[] row6 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row7 = {new CV_String("even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            CV_Super[] row8 = {new CV_String("odd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
            CV_Super[] row9 = {new CV_String("even"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
            //row 10 is unique
            CV_Super[] row10 = {new CV_String("UNIQUE"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
            testTable2.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable2.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable2.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable2.appendRow(new ArrayList<>(Arrays.asList(row9)));
            testTable2.appendRow(new ArrayList<>(Arrays.asList(row10)));
            testTable2 = testTable2.toReadable();
            
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class ExtractUniqueRowsFromTables.
     * General test with default case sensitivity.
     */
    @Test
    public void testRun() {
        System.out.println("* Extraction JUnit4Test: ExtractCommonRowsFromTables : testRun()");
        ExtractCommonRowsFromTables instance = new ExtractCommonRowsFromTables();
        instance.inputTable1 = testTable;
        instance.inputTable2 = testTable2;
        instance.ctx = test_ctx;
        instance.run();
        int rowcount_result = 0;
        try {
          //  System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 4;
       
        assertEquals(expected_rowcount_result, rowcount_result);
          
    }
    
     /**
     * Test of run method, of class ExtractUniqueRowsFromTables.
     * This test tests the case sensitivity functionality
     */ 
    @Test
    public void testRun2() throws Exception {
        System.out.println("* Extraction JUnit4Test: ExtractCommonRowsFromTables : testRun2()");
        
        testTable2 = test_ctx.createTable("Test2");
        testTable2.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},testTimestamp{timestamp}"));
        CV_Super[] row6 = {new CV_String("odD"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        CV_Super[] row7 = {new CV_String("Even"), new CV_Decimal(10.33), new CV_Integer(10), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
        CV_Super[] row8 = {new CV_String("oDd"), new CV_Decimal(5.0), new CV_Integer(5), new CV_Timestamp("2015-01-05T16:09:43.328Z")};
        CV_Super[] row9 = {new CV_String("eVen"), new CV_Decimal(23.11), new CV_Integer(23), new CV_Timestamp("2015-01-02T16:09:43.328Z")};
        //row 10 is common because because the case sensitivity exclude the above
        CV_Super[] row10 = {new CV_String("odd"), new CV_Decimal(6.0), new CV_Integer(6), new CV_Timestamp("2015-01-10T16:09:43.328Z")};
        testTable2.appendRow(new ArrayList<>(Arrays.asList(row6)));
        testTable2.appendRow(new ArrayList<>(Arrays.asList(row7)));
        testTable2.appendRow(new ArrayList<>(Arrays.asList(row8)));
        testTable2.appendRow(new ArrayList<>(Arrays.asList(row9)));
        testTable2.appendRow(new ArrayList<>(Arrays.asList(row10)));
        testTable2 = testTable2.toReadable();

        
        
        
        
        ExtractCommonRowsFromTables instance = new ExtractCommonRowsFromTables();
        instance.inputTable1 = testTable;
        instance.inputTable2 = testTable2;
        instance.ctx = test_ctx;
        instance.isCaseSensitive = new CV_Boolean("TRUE");
        instance.run();
        int rowcount_result = 0;
        try {
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 1;
       
        assertEquals(expected_rowcount_result, rowcount_result);
          
    }
    
        /**
     * Test of run method, of class ExtractUniqueRowsFromTables.
     * General test with default case sensitivity and checking row values;
     */
    @Test
    public void testRun3() throws Exception {
        System.out.println("* Extraction JUnit4Test: ExtractCommonRowsFromTables : testRun3()");
        ExtractCommonRowsFromTables instance = new ExtractCommonRowsFromTables();
        instance.inputTable1 = testTable;
        instance.inputTable2 = testTable2;
        instance.ctx = test_ctx;
        instance.run();
        int rowcount_result = 0;
        try {
          //  System.out.println(instance.outputTable.size());
            rowcount_result = instance.outputTable.size();
        } catch (TableException ex) {
            Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int expected_rowcount_result = 4;
       
        assertEquals(expected_rowcount_result, rowcount_result);
        

        final CV_Table table = instance.outputTable.toReadable();
        
         table.stream().forEach((List<CV_Super> current_row) -> {


             try {

                 String hash =instance.createRowHash(current_row);
                 if(expected_rows_map.containsKey(hash)){
                      assertEquals(expected_rows_map.get(hash), current_row);
                      expected_rows_map.remove(hash);
                 }
                 
                                 
                 table.readRow();
             } catch (TableException ex) {
                 Logger.getLogger(DeduplicateRowsTest.class.getName()).log(Level.SEVERE, null, ex);
             }

       });
    
    }
    
    /**
     * Test of run method, of class ExtractUniqueRowsFromTables for a expected exception. In
     * this case I expect to see an exception if the columns list between the two tables are not the exact same.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException2() throws Exception {
        System.out.println("* Extraction JUnit4Test: ExtractUniqueRowsFromTables :  testExpectedException()");
        
        CV_Table testTable3;
        testTable3 = test_ctx.createTable("Test2");
        //the following table header is slightly different that in the setup thus generating the expected exception
        testTable3.setHeader(new TableHeader("test,testDecimal{decimal},testInteger{integer},I_AM_DIFFERENT{timestamp}"));
        CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.11), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
        testTable3.appendRow(new ArrayList<>(Arrays.asList(row1)));
        testTable3 = testTable3.toReadable();
        
        
        
        ExtractCommonRowsFromTables instance = new ExtractCommonRowsFromTables();
        instance.inputTable1 = testTable;
        instance.inputTable2 = testTable3;
        instance.ctx = test_ctx;
        instance.run();

    }
    
    
    
}
