package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.PostgisUtil;

public class TableToGeohashPolygonsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(TableToGeohashPolygonsTest.class.getName());

	/** Source CSV */
	private static final String CSV_FILE_GERMANY = "germany_points.csv";

	/** Execution context */
	private static ExecutionContext ctx = null;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
			PostgisUtil.deleteTables(ctx);
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * TableToGeohash test - Base16
	 */
	@Test
	public void testTableToGeohashBase16()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			if(PostgisUtil.isActivePostgresDatabase(ctx))
			{
				// Base 16 (GeoModel)
				int geoBase = BoxToGeohashPolygons.BASE_16;

				CV_Table srcTable = ctx.createTableFromCSV(
						this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);  

				CV_String columnName = CV_String.valueOf("location");

				int[] geoLevels = {1,2,3,4,5,6,7};
				for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
				{
					int geoLevel = geoLevels[iLevel];
					LOGGER.log(Level.FINE, String.format(
							"\n%s: GeoBase=%d, GeoLevel=%d\n", 
							methodName, geoBase, geoLevel));

					// Initialize input parameters
					CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);
					CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);

					// Initialize functional
					TableToGeohashPolygons func = new TableToGeohashPolygons();
					func.srcTable = srcTable;
					func.columnName = columnName;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;

					// Run functional
					func.run();
					CV_Table destTable = func.destTable;

					TableUtils.printData(destTable, Level.FINEST, methodName, null);
					boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
					assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

					BoxToGeohashPolygonsTest.closeDestTable(methodName, destTable);
				}					

				if(srcTable != null) {srcTable.close();}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * TableToGeohash test - Base32
	 */
	@Test
	public void testTableToGeohashBase32()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			if(PostgisUtil.isActivePostgresDatabase(ctx))
			{
				// Base 32 (GeoHash)
				int geoBase = BoxToGeohashPolygons.BASE_32;

				CV_Table srcTable = ctx.createTableFromCSV(
						this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);  

				CV_String columnName = CV_String.valueOf("location");

				int[] geoLevels = {1,2,3,4,5};
				for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
				{
					int geoLevel = geoLevels[iLevel];
					LOGGER.log(Level.FINE, String.format(
							"\n%s: GeoBase=%d, GeoLevel=%d\n", 
							methodName, geoBase, geoLevel));

					// Initialize input parameters
					CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);
					CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);

					// Initialize functional
					TableToGeohashPolygons func = new TableToGeohashPolygons();
					func.srcTable = srcTable;
					func.columnName = columnName;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;

					// Run functional
					func.run();
					CV_Table destTable = func.destTable;

					TableUtils.printData(destTable, Level.FINEST, methodName, 50);
					boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
					assertTrue(
							String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
							result);

					BoxToGeohashPolygonsTest.closeDestTable(methodName, destTable);
				}					

				if(srcTable != null) {srcTable.close();}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			if(PostgisUtil.isActivePostgresDatabase(ctx))
			{
				CV_Table srcTable = ctx.createTableFromCSV(
						this.getClass().getResourceAsStream("/data/" + CSV_FILE_GERMANY), null);  

				CV_String columnName = CV_String.valueOf("location");

				// Initialize input parameters
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(16);
				CV_Integer geohashBaseLevel = CV_Integer.valueOf(5);

				try
				{
					TableToGeohashPolygons func = new TableToGeohashPolygons();
					func.srcTable = null;
					func.columnName = columnName;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;
					func.run();
					assertTrue("Source table is null" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				try
				{
					TableToGeohashPolygons func = new TableToGeohashPolygons();
					func.srcTable = srcTable;
					func.columnName = null;
					func.geohashBaseEncodingSystem = null;
					func.geohashBaseLevel = null;
					func.ctx = ctx;
					func.run();
					assertTrue("Source parameters are null" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				try
				{
					TableToGeohashPolygons func = new TableToGeohashPolygons();
					func.srcTable = srcTable;
					func.columnName = CV_String.valueOf("adminCode1");
					func.geohashBaseEncodingSystem = CV_Integer.valueOf(15);
					func.geohashBaseLevel = CV_Integer.valueOf(15);
					func.ctx = ctx;
					func.run();
					assertTrue("Invalid source parameters" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				if(srcTable != null) {srcTable.close();}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
