/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;

import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;
/**
 * @author CASCADE
 */
public class PointFeaturesToPolygonFeaturesTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Features JUnit4Test: ";

	String largePolygon =
			"POLYGON((-80.05017267509282 34.74434382739751, -79.88840306930403 35.91048563066315, -78.70649085853334 35.37064716330855, "
					+ "-76.27402442506472 36.31528292120307, -77.78588932526866 36.69700254817094, -76.63177563212939 37.4520241783309, "
					+ "-80.05017267509282 34.74434382739751))";
	String groupedPolygonNoSort1 =
			"POLYGON((-80.05017267509282 34.74434382739751, -79.88840306930403 35.91048563066315, "
					+ "-78.70649085853334 35.37064716330855, -80.05017267509282 34.74434382739751))";
	String groupedPolygonNoSort2 =
			"POLYGON((-76.27402442506472 36.31528292120307, -77.78588932526866 36.69700254817094, "
					+ "-76.63177563212939 37.4520241783309, -76.27402442506472 36.31528292120307))";
	String groupedSortedPolygon1 =
			"POLYGON((-80.05017267509282 34.74434382739751, -78.70649085853334 35.37064716330855, "
					+ "-79.88840306930403 35.91048563066315,-80.05017267509282 34.74434382739751))";
	String groupedSortedPolygon2 =
			"POLYGON((-76.27402442506472 36.31528292120307, -76.63177563212939 37.4520241783309, "
					+ "-77.78588932526866 36.69700254817094, -76.27402442506472 36.31528292120307))";
	/**
	 * Point features
	 */
	CV_Table pointFeatures;

	@Before
	public void setUp() throws Exception {
		// Load up the a Table for testing
		// Reuse the CSVToTable functional to help out
		try {
			testCtx = new ExecutionContextImpl();
			pointFeatures = testCtx.createTableFromCSV(this.getClass().getResourceAsStream("/data/PointFeatures.csv"), null).toReadable();
		} catch (Exception e) {
			fail(message + "Input Table failed to load: " + e.getMessage());
		}
	}

	@Test
	public void pointFeaturesToPolygonFeaturesNoGroupNoSortTest() {

		System.out.println(message +
				"pointFeaturesToPolygonFeaturesNoGroupNoSortTest()");
		CV_Polygon resultPolygon = new CV_Polygon(largePolygon);
		PointFeaturesToPolygonFeatures pointFeaturesToPolygonFeatures =
				new PointFeaturesToPolygonFeatures();
		pointFeaturesToPolygonFeatures.ctx = testCtx;
		pointFeaturesToPolygonFeatures.input_points = pointFeatures;
		pointFeaturesToPolygonFeatures.run();

		try {

			CV_Table result =
					pointFeaturesToPolygonFeatures.result;

			assertEquals(
					message +
							"pointFeaturesToPolygonFeaturesNoGroupNoSortTest():  One Polygon returned",
					result.size(), 1);

			result.stream()
					.forEach(
							row -> {
								CV_Polygon thePolygon;
								for (CV_Super rowValue : row) {
									if (rowValue instanceof CV_Polygon) {
										thePolygon = (CV_Polygon) rowValue;
										assertTrue(
												message +
														"pointFeaturesToPolygonFeaturesNoGroupNoSortTest(): Polygon result equals expected result",
												thePolygon
														.equals(resultPolygon));
									}
								}
							});

		} catch (TableException e) {
			fail(message +
					"pointFeaturesToPolygonFeaturesNoGroupNoSortTest(): TableExcpetion " +
					e.getMessage());
		}
	}

	@Test
	public void pointFeaturesToPolygonFeaturesGroupedNoSortTest() {

		System.out.println(message +
				"pointFeaturesToPolygonFeaturesGroupedNoSortTest()");
		CV_Polygon resultPolygon1 = new CV_Polygon(groupedPolygonNoSort1);
		CV_Polygon resultPolygon2 = new CV_Polygon(groupedPolygonNoSort2);

		PointFeaturesToPolygonFeatures pointFeaturesToPolygonFeatures =
				new PointFeaturesToPolygonFeatures();
		pointFeaturesToPolygonFeatures.ctx = testCtx;
		pointFeaturesToPolygonFeatures.input_points = pointFeatures;
		pointFeaturesToPolygonFeatures.input_discriminator =
				new CV_String("POINT_GROUP");
		pointFeaturesToPolygonFeatures.run();

		try {

			CV_Table result =
					pointFeaturesToPolygonFeatures.result.toReadable();

			assertEquals(
					message +
							"pointFeaturesToPolygonFeaturesGroupedNoSortTest():  Two Polygons returned",
					result.size(), 2);

			result.stream()
					.forEach(
							row -> {
								CV_Polygon thePolygon;
								for (CV_Super rowValue : row) {
									if (rowValue instanceof CV_Polygon) {
										thePolygon = (CV_Polygon) rowValue;
										boolean found = false;
										if (thePolygon.equals(resultPolygon1) ||
												thePolygon
														.equals(resultPolygon2)) {
											found = true;
										}
										assertTrue(
												message +
														"pointFeaturesToPolygonFeaturesGroupedNoSortTest(): Polygon result equals expected result",
												found);
									}
								}
							});

		} catch (TableException e) {
			fail(message +
					"pointFeaturesToPolygonFeaturesGroupedNoSortTest(): TableExcpetion " +
					e.getMessage());
		}
	}

	@Test
	public void pointFeaturesToPolygonFeaturesGroupedSortedTest() {

		System.out.println(message +
				"pointFeaturesToPolygonFeaturesGroupedSortedTest()");
		CV_Polygon resultPolygon1 = new CV_Polygon(groupedSortedPolygon1);
		CV_Polygon resultPolygon2 = new CV_Polygon(groupedSortedPolygon2);

		PointFeaturesToPolygonFeatures pointFeaturesToPolygonFeatures =
				new PointFeaturesToPolygonFeatures();
		pointFeaturesToPolygonFeatures.ctx = testCtx;
		pointFeaturesToPolygonFeatures.input_points = pointFeatures;
		pointFeaturesToPolygonFeatures.input_discriminator =
				new CV_String("POINT_GROUP");
		pointFeaturesToPolygonFeatures.input_sort = new CV_String("POINT_SORT");
		pointFeaturesToPolygonFeatures.run();

		try {

			CV_Table result =
					pointFeaturesToPolygonFeatures.result.toReadable();

			assertEquals(
					message +
							"pointFeaturesToPolygonFeaturesGroupedSortedTest():  Two Polygons returned",
					result.size(), 2);

			result.stream()
					.forEach(
							row -> {
								CV_Polygon thePolygon;
								for (CV_Super rowValue : row) {
									if (rowValue instanceof CV_Polygon) {
										thePolygon = (CV_Polygon) rowValue;
										boolean found = false;
										if (thePolygon.equals(resultPolygon1) ||
												thePolygon
														.equals(resultPolygon2)) {
											found = true;
										}
										assertTrue(
												message +
														"pointFeaturesToPolygonFeaturesGroupedSortedTest(): Polygon result equals expected result",
												found);
									}
								}
							});

		} catch (TableException e) {
			fail(message +
					"pointFeaturesToPolygonFeaturesGroupedSortedTest(): TableExcpetion " +
					e.getMessage());
		}
	}
}
