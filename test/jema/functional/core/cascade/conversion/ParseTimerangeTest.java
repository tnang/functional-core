package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import jema.common.types.CV_Duration;
import jema.common.types.CV_String;
import jema.common.types.CV_TemporalRange;
import jema.common.types.CV_Timestamp;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ParseTimerangeTest {

	/**
	 * Start Time
	 */
	public Instant startTime;
	/**
	 * Stop Time
	 */
	public Instant endTime;
	/**
	 * Duration
	 */
	public Duration expectedDuration;
	/**
	 * Functional input
	 */
	public CV_TemporalRange input;
	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.Time.ParseTimerange JUnit4Test: ";
	
	
	@Before
	public void setUp() throws Exception {
		// Start time is 2 days before current
		startTime = Instant.now().minus(2,  ChronoUnit.DAYS);
		// end time is 2 days after current
		endTime = Instant.now().plus(2,  ChronoUnit.DAYS);
		// duration is the difference between the two
		expectedDuration = Duration.between(startTime, endTime);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseTimerange() {
		System.out.println(message+"testParseTimerange()");
		// set up input
		input = new CV_TemporalRange(startTime, endTime);
		
		// All of the results from the functional
		CV_Timestamp startResult;
		CV_Timestamp endResult;
		CV_Duration durationResult;
		
		ParseTimerange instance = new ParseTimerange();
		instance.input = input;
		instance.run();

		startResult = instance.output_startTimestamp;
		endResult = instance.output_endTimestamp;
		durationResult = instance.output_duration;
		
		assertEquals(message+"Start Time Matches Input Start Time",startResult.value(),startTime);
		assertEquals(message+"End Time Matches Input End Time",endResult.value(),endTime);
		assertEquals(message+"Duration Matches Input Duration",durationResult.value(),expectedDuration);
	}

}
