package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Point;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.PostgisUtil;

/**
 * Unit test cases for CircleToBox functional.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.movable-type.co.uk/scripts/latlong.html">
 * Calculate distance, bearing and more between Latitude/Longitude points</a></li>
 * </ul>
 * @author CASCADE
 *
 */
public class CircleToBoxTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(CircleToBoxTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;
	
	/** Random data generator */
	private static CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * CircleToBox test
	 */
	@Test
	public void testCircleToBox()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Source geometry
			CV_Circle srcGeom = cvDataGenerator.genCircle();
			srcGeom = new CV_Circle("CIRCLE(10.0 10.0,1.0E5)");
			
			// Expected results
			CV_Box expectedGeom = GeometryUtil.circleToBox(srcGeom);
			
			// Initialize functional
			CircleToBox func = new CircleToBox();
			func.srcCircle = srcGeom;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Box destGeom = func.destBox;

			// Radius has issues with precision
			boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s\nradius (km)=%f", 
				methodName,
				result,
				(srcGeom == null ? null : srcGeom.toString()), 
				(destGeom == null ? null : destGeom.toString()),
				expectedGeom.toString(),
				srcGeom.getRadiusMeters() * 1.0e-3));			
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * CircleToBox test
	 */
	@Test
	public void testCircleToBoxCrossDateline()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Source geometry
			CV_Circle srcGeom = cvDataGenerator.genCircle();
			srcGeom = new CV_Circle("CIRCLE(180.0 0.0, 1.0E5)");
			
			// Expected results
			CV_Box expectedGeom = GeometryUtil.circleToBox(srcGeom);
			
			// Initialize functional
			CircleToBox func = new CircleToBox();
			func.srcCircle = srcGeom;
			func.crossDateline = CV_Boolean.valueOf(true);
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Box destGeom = func.destBox;

			// Radius has issues with precision
			boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s\nradius (km)=%f", 
				methodName,
				result,
				(srcGeom == null ? null : srcGeom.toString()), 
				(destGeom == null ? null : destGeom.toString()),
				expectedGeom.toString(),
				srcGeom.getRadiusMeters() * 1.0e-3));			
//			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * CircleToBox test
	 */
	@Test
	public void testMultipleCircleToBox()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
	        CV_Point pt = new CV_Point(10, 10);//cvDataGenerator.genPoint();
			for(double radius = 1.0e1; radius < 1.1e9; radius *= 1.0e1)
			{
		        CV_Circle srcGeom = new CV_Circle(pt, radius);

				// Expected results
				CV_Box wwGeom = GeometryUtil.circleToBox(srcGeom);
				CV_Box postGISGeom = PostgisUtil.instance().circleToBox(ctx, srcGeom);
				CV_Box geoToolsGeom = GeoToolsUtil.instance().circleToBox(srcGeom);
				CV_Box expectedGeom = wwGeom;
				
				// Initialize functional
				CircleToBox func = new CircleToBox();
				func.srcCircle = srcGeom;
				func.ctx = ctx;
	
				// Run functional
				func.run();
				CV_Box destGeom = func.destBox;
	
				// Radius has issues with precision
				boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
				LOGGER.log(Level.FINE, String.format(
					"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s" + 
					"\nworldWind=%s\ngeoTools=%s\npostGIS=%s\nradius=%f", 
					methodName,
					result,
					srcGeom, 
					destGeom,
					wwGeom,
					geoToolsGeom,
					postGISGeom,			
					radius * 1.0e-3));
				
				result = (destGeom != null);
				assertTrue(result);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				CircleToBox func = new CircleToBox();
				func.srcCircle = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source Circle is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
