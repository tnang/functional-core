package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Box;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.util.CommonVocabDatabaseProperties;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.PostgisUtil;

/**
 * Unit test cases for BoxToPoint functional.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.movable-type.co.uk/scripts/latlong.html">
 * Calculate distance, bearing and more between Latitude/Longitude points</a></li>
 * </ul>
 * @author CASCADE
 *
 */
public class BoxToPointTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(BoxToPointTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			//			ctx = new ExecutionContextImpl();
			ctx = new ExecutionContextImpl(
					CommonVocabDatabaseProperties.DatabasePropertiesKey.DATABASE.getPostgresValue());
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * BoxToPoint test
	 */
	@Test
	public void testBoxToPoint()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Box srcGeom = new CV_Box(39.0, 28.0, 49.0, 38.0); 

			// Expected results
			CV_Polygon cvPolygon = srcGeom.toPolygon();
			CV_Point wwGeom = GeometryUtil.polygonToPoint(cvPolygon);
			CV_Point postGISGeom = PostgisUtil.instance().polygonToPoint(ctx, cvPolygon);
			CV_Point geoToolsGeom = GeoToolsUtil.instance().polygonToPoint(cvPolygon);
			CV_Point expectedGeom = wwGeom;

			// Initialize functional
			BoxToPoint func = new BoxToPoint();
			func.srcBox = srcGeom;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Point destGeom = func.destPoint;

			// Radius has issues with precision
			boolean result = (destGeom != null) && (destGeom.equals(expectedGeom));
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s" + 
				"\nworldWind=%s\ngeoTools=%s\npostGIS=%s", 
				methodName,
				result,
				srcGeom, 
				destGeom,
				wwGeom,
				geoToolsGeom,
				postGISGeom));			
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				BoxToPoint func = new BoxToPoint();
				func.srcBox = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source Box is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
