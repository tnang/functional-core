package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

public class PointToFeaturesTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Features.PointToFeaturesTest JUnit4Test: ";

	/**
	 * Point
	 */
	// CV_Table
	CV_Point thePoint = new CV_Point(-77.00, 38.00);
	CV_String pointName = new CV_String("Point Name");
	CV_String pointDesc = new CV_String("Point Desc");

	@Before
	public void setUp() throws Exception {
		// Load up the a Table for testing
		// Reuse the CSVToTable functional to help out
		testCtx = new ExecutionContextImpl();
	}

	@Test
	public void testPointToFeatures() {
		System.out.println(message +
			"testPointToFeatures()");
		
		PointToFeatures point = new PointToFeatures();
		point.ctx = this.testCtx;
		point.inputDesc = pointDesc;
		point.inputName = pointName;
		point.inputPoint = thePoint;
		
		point.run();
		
		CV_Table results = point.result;
		
		try {
			assertTrue(results.size() == 1);
		} catch (TableException e) {
			fail(message+"testPointToFeatures()::TABLE EXCEPTION "+e.getMessage());
		}

	}

	@Test
	public void testPointToFeaturesNullPoint() {
		System.out.println(message +
			"testPointToFeaturesNullPoint()");
		
		PointToFeatures point = new PointToFeatures();
		point.ctx = this.testCtx;
		point.inputDesc = pointDesc;
		point.inputName = pointName;
		point.inputPoint = null;
		boolean caughtException = false;
		
		try {
		point.run();
		CV_Table results = point.result;
			assertTrue(results.size() == 1);
		} catch (TableException e) {
			fail(message+"testPointToFeatures()::TABLE EXCEPTION "+e.getMessage());
		} catch (Exception e1) {
			caughtException = true;
		}
		assertTrue(message+"testPointToFeaturesNullPoint()",caughtException);

	}

	@Test
	public void testPointToFeaturesNullNameDesc() {
		System.out.println(message +
			"testPointToFeaturesNullNameDesc()");
		
		PointToFeatures point = new PointToFeatures();
		point.ctx = this.testCtx;
		point.inputDesc = null;
		point.inputName = null;
		point.inputPoint = thePoint;
		
		try {
		point.run();
		CV_Table results = point.result;
			assertTrue(results.size() == 1);
		} catch (Exception e) {
			fail(message+"testPointToFeaturesNullNameDesc()::TABLE EXCEPTION "+e.getMessage());
		}

	}
}
