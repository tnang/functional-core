package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Box;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableFactory;
import jema.common.types.table.TableHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.common.types.table.sql.WriteableSQLTableSPI;
import jema.common.types.util.CommonVocabDataDuration;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.PostgisUtil;

public class BoxToGeohashPolygonsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(BoxToGeohashPolygonsTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Test duration to provide performance results */
	private static final CommonVocabDataDuration cvDataDuration = new CommonVocabDataDuration();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
			PostgisUtil.deleteTables(ctx);
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * BoxToGeohash test - Base16
	 */
	@Test
	public void testBoxToGeohashBase16()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_16;

			CV_Box srcBox = new CV_Box("BOX(0 0, 10 10)");


			int[] geoLevels = {2, 4, 6};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Base32
	 */
	@Test
	public void testBoxToGeohashBase32()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 32 (GeoHash)
			int geoBase = BoxToGeohashPolygons.BASE_32;

			CV_Box srcBox = new CV_Box("BOX(0 0, 10 10)");

			int[] geoLevels = {2, 4};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Base16
	 */
	@Test
	public void testBoxToGeohashBase16All()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_16;

			CV_Box srcBox = new CV_Box("BOX(0 0, 10 10)");

			int[] geoLevels = {1,2,3,4,5,6,7};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Base16
	 */
//	@Test
	public void testBoxToGeohashBase16ManyRec()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_16;

			CV_Box srcBox = new CV_Box("BOX(0 0, 5 5)");

			int[] geoLevels = {8};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Base16
	 */
	@Test
	public void testBoxToGeohashBase32All()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_32;

			CV_Box srcBox = new CV_Box("BOX(0 0, 10 10)");

			int[] geoLevels = {1,2,3,4,5};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Base16
	 */
//	@Test
	public void testBoxToGeohashBase32ManyRec()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_32;

			CV_Box srcBox = new CV_Box("BOX(0 0, 5 5)");

			int[] geoLevels = {6};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - 0.01 x 0.01 deg AOI
	 */
	@Test
	public void testBoxToGeohashBase16SmallAOI()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_16;

			CV_Box srcBox =  new CV_Box("BOX(-30 -30, -29.99 -29.99)");

			int[] geoLevels = {7,8,9,10,11,12};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test -
	 */
	@Test
	public void testBoxToGeohashBase32SmallAOI()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_32;

			CV_Box srcBox =  new CV_Box("BOX(-30 -30, -29.9995 -29.9995)");

			int[] geoLevels = {7,8,9,10,11};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test -
	 */
	@Test
	public void testBoxToGeohashBase32Level12TinyAOI()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Base 16 (GeoModel)
			int geoBase = BoxToGeohashPolygons.BASE_32;

			CV_Box srcBox =  new CV_Box("BOX(-30 -30, -29.99995 -29.99995)");

			int[] geoLevels = {12};
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				LOGGER.log(Level.FINE, String.format(
						"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
						methodName, geoBase, geoLevel, srcBox));

				CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

				// Initialize functional
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.srcBox = srcBox;
				func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
				func.geohashBaseLevel = geohashBaseLevel;
				func.ctx = ctx;

				// Run functional
				String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
				cvDataDuration.start(label);
				func.run();
				CV_Table destTable = func.destTable;
				cvDataDuration.stop(destTable.size(), null);

				TableUtils.printData(destTable, Level.FINEST, methodName, 50);
				boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
				assertTrue(
						String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
						result);

				closeDestTable(methodName, destTable);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * BoxToGeohash test - Earth AOI
	 */
	@Test
	public void testBoxToGeohashBaseLargeAOI()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_Box srcBox =  new CV_Box("BOX(-180.0 -90.0, 180.0 90.0)");

			int[] geoLevels = {1,2,3,4,5};
			int[] baseList = {BoxToGeohashPolygons.BASE_16, BoxToGeohashPolygons.BASE_32};
			for(int iBase = 0; iBase < baseList.length; iBase++)
			{
				int geoBase = baseList[iBase];
				for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
				{
					int geoLevel = geoLevels[iLevel];
					LOGGER.log(Level.FINE, String.format(
							"\n%s: GeoBase=%d, GeoLevel=%d, Box=%s\n", 
							methodName, geoBase, geoLevel, srcBox));

					CV_Integer geohashBaseLevel = CV_Integer.valueOf(geoLevel);
					CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(geoBase);

					// Initialize functional
					BoxToGeohashPolygons func = new BoxToGeohashPolygons();
					func.srcBox = srcBox;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;

					// Run functional
					String label = String.format("%s (Base %d, Level %d)", methodName, geoBase, geoLevel);
					cvDataDuration.start(label);
					func.run();
					CV_Table destTable = func.destTable;
					cvDataDuration.stop(destTable.size(), 50);

					TableUtils.printData(destTable, Level.FINEST, methodName, 50);
					boolean result = BoxToGeohashPolygonsTest.validateGrid(destTable);
					assertTrue(
							String.format("%s: GeoBase=%d, GeoLevel=%d", methodName, geoBase, geoLevel), 
							result);

					closeDestTable(methodName, destTable);
				}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Prototype error box
	 */
	//@Test
	public void geohashErrorTest()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		double lonError = 0;
		double latError = 0;

		int[] geoLevels = {1,2,3,4,5,6,7,8,9,10,11,12};
		int[] baseList = {BoxToGeohashPolygons.BASE_16, BoxToGeohashPolygons.BASE_32};
		for(int iBase = 0; iBase < baseList.length; iBase++)
		{
			int base = baseList[iBase];
			for(int iLevel = 0; iLevel < geoLevels.length; iLevel++)
			{
				int geoLevel = geoLevels[iLevel];
				int bitsPerLevel = (base == BoxToGeohashPolygons.BASE_16) ? 4 : 5;
				int parity = (geoLevel % 2) == 0 ? 0 : 1;
				latError = 180.0 / Math.pow(2, Math.floor((bitsPerLevel*geoLevel-parity)/2));
				lonError = 180.0 / Math.pow(2, Math.floor((bitsPerLevel*geoLevel+parity-2)/2));

				System.out.format(
						"%s_1: level=%d, base=%d, bitsPerLevel=%d, parity=%d, lonError= %15.10e, latError=%15.10e\n", 
						methodName, geoLevel, base, bitsPerLevel, parity, lonError, latError);
			}

			System.out.println();
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			if(PostgisUtil.isActivePostgresDatabase(ctx))
			{
				CV_Box srcBox = new CV_Box(180.0, -90.0, 0.0, 90.0); // Western hemisphere

				// Initialize input parameters
				CV_Integer geohashBaseEncodingSystem = CV_Integer.valueOf(16);
				CV_Integer geohashBaseLevel = CV_Integer.valueOf(5);

				try
				{
					BoxToGeohashPolygons func = new BoxToGeohashPolygons();
					func.srcBox = null;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;
					func.run();
					assertTrue("Source table is null" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				try
				{
					BoxToGeohashPolygons func = new BoxToGeohashPolygons();
					func.srcBox = srcBox;
					func.geohashBaseEncodingSystem = null;
					func.geohashBaseLevel = null;
					func.ctx = ctx;
					func.run();
					assertTrue("Source parameters are null" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				try
				{
					BoxToGeohashPolygons func = new BoxToGeohashPolygons();
					func.srcBox = srcBox;
					func.geohashBaseEncodingSystem = CV_Integer.valueOf(15);
					func.geohashBaseLevel = geohashBaseLevel;
					func.ctx = ctx;
					func.run();
					assertTrue("Geohash Base Encoding value is invalid" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

				try
				{
					BoxToGeohashPolygons func = new BoxToGeohashPolygons();
					func.srcBox = srcBox;
					func.geohashBaseEncodingSystem = geohashBaseEncodingSystem;
					func.geohashBaseLevel = CV_Integer.valueOf(15);
					func.ctx = ctx;
					func.run();
					assertTrue("Geohash Level value is invalid" ,false);
				} 
				catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	@SuppressWarnings("rawtypes")
	protected static Boolean validateGrid(CV_Table table)
	{
		Boolean result = true;

		try
		{
			if(table != null)
			{
				double EPSILON = 1.0e-9;
				int nRow = 0;
				int iHash = 0;
				int iGeom = 0;

				// Sort data to allow comparison of consecutive grid cells
				TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
				SelectBuilder sb = factory.selectBuilder();
				sb.from(table);
				ColumnHeader rowId = new ColumnHeader(WriteableSQLTableSPI.PRIMARY_KEY, ParameterType.integer);				
				sb.orderBy(rowId);				
				CV_Table readTable = TableQueryExecutor.executeQuery(sb);

				// Get geometry and geohash cell indices
				TableHeader tableHeader = readTable.getHeader();
				for(int iCol = 0; iCol < tableHeader.asList().size(); iCol++)
				{
					ColumnHeader colHeader = tableHeader.getHeader(iCol);
					if(colHeader.getType().equals(ParameterType.string)) {iHash=iCol;}
					if(colHeader.getType().equals(ParameterType.polygon)) {iGeom=iCol;}
				}

				// Read initial cell to setup previous row comparison
				List<CV_Super> gridCell = readTable.readRow();
				CV_String geohash = (gridCell == null) ? null : (CV_String) gridCell.get(iHash);
				CV_Polygon geom = (gridCell == null) ? null : (CV_Polygon) gridCell.get(iGeom);
				CV_Box boxPrev = (geom == null) ? null : GeometryUtil.polygonToBox(geom, false);
				CV_Box boxCorner = boxPrev;

				// Traverse table and compare grid cells to ensure no gaps in grid
				result = boxPrev != null;
				while(((gridCell = readTable.readRow()) != null) && result)
				{
					geohash = (CV_String) gridCell.get(iHash);
					geom = (CV_Polygon) gridCell.get(iGeom);
					CV_Box box = GeometryUtil.polygonToBox(geom);

					// Compare adjacent cells traverse column

					// bottom with top
					result = 
							(Math.abs(box.getLeft() - boxPrev.getLeft()) < EPSILON) &&
							(Math.abs(box.getRight() -  boxPrev.getRight()) < EPSILON) &&
							(Math.abs(box.getBottom() - boxPrev.getTop()) < EPSILON);

					// change column, left with right
					if(!result)
					{
						boxPrev = boxCorner;
						boxCorner = box;
						result = 
								(Math.abs(box.getLeft() - boxPrev.getRight()) < EPSILON) &&
								(Math.abs(box.getBottom() - boxPrev.getBottom()) < EPSILON) &&
								(Math.abs(box.getTop() - boxPrev.getTop()) < EPSILON);
					}

					nRow++;

					if(!result)
					{
						LOGGER.log(Level.FINE, String.format(
								"result=%s, nRow=%d, geohash=%s\npolygon=%s\nbox=%s\nboxPrev=%s", 
								result, nRow, geohash, geom, box, boxPrev));
					}

					boxPrev = box;
				}

				readTable.close();
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return result;
	}

	/**
	 * Delete database table and close object.
	 * 
	 * @param methodName Test case name
	 * @param destTable Destination table
	 */
	protected static void closeDestTable(String methodName, CV_Table destTable)
	{
		try
		{
			TableFactory.deleteTable(destTable.getUri(), destTable.getDatabaseProperties());
			destTable.close();
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
		}
	}
}
