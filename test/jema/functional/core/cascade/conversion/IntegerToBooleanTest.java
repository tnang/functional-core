 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conversion;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class IntegerToBooleanTest {
    
    public IntegerToBooleanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBooleanFromInteger method, of class IntegerToBoolean.
     */
    @Test
    public void testGetBooleanFromInteger() {
        System.out.println("* Conversion JUnit4Test: IntegerToBooleanTest : testGetBooleanFromInteger()");
        
        CV_Integer val = new CV_Integer(0);
        IntegerToBoolean instance = new IntegerToBoolean();
        CV_Boolean expResult = new CV_Boolean(false);
        CV_Boolean result = instance.getBooleanFromInteger(val);
        assertEquals(expResult, result);
        
        val = new CV_Integer(1);
        instance = new IntegerToBoolean();
        expResult = new CV_Boolean(true);
        result = instance.getBooleanFromInteger(val);
        assertEquals(expResult, result);

    }

    /**
     * Test of run method, of class IntegerToBoolean.
     */
    @Test
    public void testRun() {
        System.out.println("* Compression JUnit4Test: IntegerToBooleanTest : testRun()");
        IntegerToBoolean instance = new IntegerToBoolean();
        instance.input = new CV_Integer(1);
        instance.run();
        assertTrue("Default run - integer is true",instance.result.value());
        
        instance = new IntegerToBoolean();
        instance.input = new CV_Integer(0);
        instance.run();
        assertFalse("Default run - integer is false",instance.result.value());
    }
    
    /**
     * Test of getBooleanFromInteger method, of class IntegerToBoolean for a expected exception.
     * In this case I expect to see an exception because the input value should either be  a 0 or 1.
     */
    @Test(expected=RuntimeException.class)
    public void testExpectedException() {
        System.out.println("* Compression JUnit4Test: IntegerToBooleanTest :  testExpectedException()");
        CV_Integer val = new CV_Integer(5);
        IntegerToBoolean instance = new IntegerToBoolean();
        CV_Boolean result = instance.getBooleanFromInteger(val);
    }
    
}
