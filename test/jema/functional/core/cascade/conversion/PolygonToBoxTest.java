

package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.PostgisUtil;

public class PolygonToBoxTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(PolygonToBoxTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Random data generator */
	private static CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * PolygonToBox test
	 */
	@Test
	public void testPolygonToBoxCrossDateline()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			List<CV_Polygon> srcGeomList = new ArrayList<CV_Polygon>();
			srcGeomList.add(new CV_Polygon("POLYGON((10 -70, -10 -70, -10 70, 10 70, 10 -70))"));
			srcGeomList.add(new CV_Polygon("POLYGON((150 -70, -150 -70, -150 70, 150 70, 150 -70))"));
			srcGeomList.add(new CV_Polygon("POLYGON((-150 -70, 150 -70, 150 70, -150 70, -150 -70))"));  // Don't cross dateline
//			srcGeomList.add(new CV_Polygon("POLYGON((100 40, -170 40, 90 40, 90 -40, 100 -40, 100 40))"));
//			srcGeomList.add(new CV_Polygon("POLYGON((-90 -70, 150 -70, -150 -70, -150 70, 150 70, -90 70, -90 -70))"));

			// Expected results
			List<CV_Box> expectedGeomList = new ArrayList<CV_Box>();
			expectedGeomList.add(new CV_Box("BOX(10 -70, -10.0 70.0)"));
			expectedGeomList.add(new CV_Box("BOX(150.0 -70.0, -150.0 70.0)"));
			expectedGeomList.add(new CV_Box("BOX(-150.0 -70.0, 150.0 70.0)"));  // Don't cross dateline
//			expectedGeomList.add(new CV_Box("BOX(100 -40, 90 40)"));
//			expectedGeomList.add(new CV_Box("BOX(-90 -70, -150.0 70.0)"));
			
			List<CV_Boolean> crossDatelineList = new ArrayList<CV_Boolean>();
			crossDatelineList.add(CV_Boolean.valueOf(true));
			crossDatelineList.add(CV_Boolean.valueOf(true));
			crossDatelineList.add(CV_Boolean.valueOf(false));
//			crossDatelineList.add(CV_Boolean.valueOf(true));
//			crossDatelineList.add(CV_Boolean.valueOf(true));

			// Initialize functional
			for(int iTest = 0; iTest < srcGeomList.size(); iTest++)
			{
				CV_Polygon srcGeom = srcGeomList.get(iTest);
				CV_Box expectedGeom = expectedGeomList.get(iTest);
				CV_Boolean crossDateline = crossDatelineList.get(iTest);
				
				PolygonToBox func = new PolygonToBox();
				func.srcPolygon = srcGeom;
				func.crossDateline = crossDateline;
				func.ctx = ctx;

				// Run functional
				func.run();
				CV_Box destGeom = func.destBox;

				boolean result = (destGeom != null) && (destGeom.equals(expectedGeom));
				LOGGER.log(Level.FINE, String.format(
					"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s", 
					methodName,
					result,
					srcGeom, 
					destGeom,
					expectedGeom));
					
//				result = (destGeom != null);
				assertTrue(result);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * PolygonToBox test
	 */
	@Test
	public void testPolygonToBox()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			double radius = 1.0e7;
			CV_Polygon srcGeom = null;
			//				srcGeom = new CV_Polygon("POLYGON((39.0 28.0, 49.0 28.0, 49.0 38.0, 39.0 38.0, 39.0 28.0))");
			CV_Circle circle = new CV_Circle(cvDataGenerator.genPoint(), radius);
			srcGeom = circle.generatePolygon();

			// Expected results
			CV_Box wwGeomCircle2Box = GeometryUtil.circleToBox(circle);
			CV_Box wwGeomPoly2Box = GeometryUtil.polygonToBox(srcGeom);
			CV_Box postGISGeom = PostgisUtil.instance().polygonToBox(ctx, srcGeom);
			CV_Box geoToolsGeom = GeoToolsUtil.instance().polygonToBox(srcGeom);
			CV_Box expectedGeom = wwGeomCircle2Box;

			// Initialize functional
			PolygonToBox func = new PolygonToBox();
			func.srcPolygon = srcGeom;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Box destGeom = func.destBox;

			boolean result = (destGeom != null) && (destGeom.equals(expectedGeom));
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s" + 
				"\ncircle=%s\nworldWindCircle2Box=%s\nworldWindPoly2Box=%s\ngeoTools=%s\npostGIS=%s\nradius=%f", 
				methodName,
				result,
				srcGeom, 
				destGeom,
				circle,
				wwGeomCircle2Box,
				wwGeomPoly2Box,
				geoToolsGeom,
				postGISGeom,			
				radius * 1.0e-3));
			
			result = (destGeom != null);
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * PolygonToBox test
	 */
	@Test
	public void testPolygonToBoxMany()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			for(double lon = -135.0; lon < 180.0; lon += 45.0)
			{
				for(double lat = -75; lat < 75; lat += 25)
				{
					//						CV_Point pt = cvDataGenerator.genPoint();
					CV_Point pt = new CV_Point(lon, lat);
					for(double radius = 1.0e3; radius < 1.1e7; radius *= 1.0e1)
					{
						CV_Circle circle = new CV_Circle(pt, radius);
						CV_Polygon srcGeom = circle.generatePolygon();

						// Expected results
						CV_Box wwGeomCircle2Box = GeometryUtil.circleToBox(circle);
						CV_Box wwGeomPoly2Box = GeometryUtil.polygonToBox(srcGeom);
						CV_Box postGISGeom = PostgisUtil.instance().polygonToBox(ctx, srcGeom);
						CV_Box geoToolsGeom = GeoToolsUtil.instance().polygonToBox(srcGeom);
						CV_Box expectedGeom = wwGeomCircle2Box;

						// Initialize functional
						PolygonToBox func = new PolygonToBox();
						func.srcPolygon = srcGeom;
						func.ctx = ctx;

						// Run functional
						func.run();
						CV_Box destGeom = func.destBox;

						// Radius has issues with precision
						boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
						LOGGER.log(Level.FINE, String.format(
							"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s" + 
							"\ncircle=%s\nwwCircle2Box=%s\nwwPoly2Box=%s\ngeoTools=%s\npostGIS=%s\nradius=%f", 
							methodName,
							result,
							srcGeom, 
							destGeom,
							circle,
							wwGeomCircle2Box,
							wwGeomPoly2Box,
							geoToolsGeom,
							postGISGeom,
							radius * 1.0e-3));			

						result = destGeom != null;
						assertTrue(result);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			try
			{
				PolygonToBox func = new PolygonToBox();
				func.srcPolygon = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source geometry is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
