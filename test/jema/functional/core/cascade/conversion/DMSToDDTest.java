/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 31, 2015 12:26:35 PM
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class DMSToDDTest {

    ExecutionContext test_ctx;
    CV_Table testTable;

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("DMS{string},DD Lat{decimal},DD Lon{string}"));
            CV_Super[] row1 = {new CV_String("30°15'57.1926\"N 079°58'56\"W"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row2 = {new CV_String("36°10′30″N -115°08′11″"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row3 = {new CV_String("37°38'24\" 112°10'12\""), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row4 = {new CV_String("-27°00'00\" 133°00'00\"E"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row5 = {new CV_String("301557.1926N 0795856.0W"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row6 = {new CV_String("301557N 0795856W"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row7 = {new CV_String("30 15 57 N 0795856W"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row8 = {new CV_String("751557S 2795856E"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row9 = {new CV_String("951557S 0795856W"), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row10 = {new CV_String(""), new CV_Decimal(Double.NaN), new CV_String("")};
            CV_Super[] row11 = {new CV_String(null), new CV_Decimal(Double.NaN), new CV_String("")};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row9)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row10)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row11)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(DMSToDDTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class DMSToDD.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_DMSLat() throws TableException {
        System.out.println("* Conversion JUnit4Test: DMSToDD : testRun_DMSLat()");

        DMSToDD instance = new DMSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDMS = new CV_String("DMS");
        instance.latColumnNameDD = new CV_String("DD Lat Out");
        instance.lonColumnNameDD = new CV_String("DD Lon Out");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(30.265886833333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(36.175, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(37.64, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(-27.0, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(30.265886833333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 5) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(30.265833333333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 7) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertEquals(-75.26583333333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 8) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 9) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 10) {
                    CV_Super thiscolumnrowvalue = current_row.get(3);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DMSToDDTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class DMSToDD.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_DMSLon() throws TableException {
        System.out.println("* Conversion JUnit4Test: DMSToDD : testRun_DMSLon()");

        DMSToDD instance = new DMSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDMS = new CV_String("DMS");
        instance.latColumnNameDD = new CV_String("DD Lat Out");
        instance.lonColumnNameDD = new CV_String("DD Lon Out");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(-79.98222222222222, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(-115.1363888888889, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(112.17, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(133.0, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(-79.98222222222222, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 5) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(-79.98222222222222, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 7) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 8) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertEquals(-79.98222222222222, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 9) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 10) {
                    CV_Super thiscolumnrowvalue = current_row.get(4);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DMSToDDTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class DMSToDD.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnNotFound() {
        System.out.println("* Conversion JUnit4Test: DMSToDD : testRun_columnNotFound()");

        //Test column name not in table
        DMSToDD instance = new DMSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDMS = new CV_String("BACON");
        instance.latColumnNameDD = new CV_String("DD Lat Out");
        instance.lonColumnNameDD = new CV_String("DD Lon Out");
        instance.run();
    }

    /**
     * Test of run method, of class DMSToDD.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_columnExists() throws TableException {
        System.out.println("* Conversion JUnit4Test: DMSToDD : testRun_columnExists()");

        DMSToDD instance = new DMSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDMS = new CV_String("DMS");
        instance.latColumnNameDD = new CV_String("DD Lat");
        instance.lonColumnNameDD = new CV_String("DD Lon Out");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                if (table.getCurrentRowNum() == 0) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(30.265886833333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 1) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(36.175, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 2) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(37.64, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 3) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(-27.0, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 4) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(30.265886833333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 5) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(30.265833333333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 6) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 7) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertEquals(-75.26583333333333, thiscolumnrowvalue.value());
                } else if (table.getCurrentRowNum() == 8) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 9) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                } else if (table.getCurrentRowNum() == 10) {
                    CV_Super thiscolumnrowvalue = current_row.get(1);
                    assertNull(thiscolumnrowvalue);
                }
                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DMSToDDTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Test of run method, of class DMSToDD.
     *
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnExists_wrongType() {
        System.out.println("* Conversion JUnit4Test: DMSToDD : testRun_columnExists_wrongType()");

        DMSToDD instance = new DMSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.columnNameDMS = new CV_String("DMS");
        instance.latColumnNameDD = new CV_String("DD Lat Out");
        instance.lonColumnNameDD = new CV_String("DD Lon");
        instance.run();
    }
}
