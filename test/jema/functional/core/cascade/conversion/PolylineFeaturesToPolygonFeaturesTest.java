package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

public class PolylineFeaturesToPolygonFeaturesTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Features.PolylineFeaturesToPolygonFeaturesTest JUnit4Test: ";

	/**
	 * Point features
	 */
	CV_Table polylineFeatures;

	@Before
	public void setUp() throws Exception {
		// Load up the a Table for testing
		// Reuse the CSVToTable functional to help out
		try {
			testCtx = new ExecutionContextImpl();
			polylineFeatures =
				testCtx.createTableFromCSV(this.getClass().getResourceAsStream("/data/geospatial_data/polylineFeatureCollection.csv"), null).toReadable();
		} catch (Exception e) {
			e.printStackTrace();
			fail(message + "Input Table failed to load: " + e.getMessage());
		}
	}

	@Test
	public void polylineFeaturesToPolygonFeaturesTestTest() {
		System.out.println(message + "polylineFeaturesToPolygonFeaturesTestTest()");
		PolylineFeaturesToPolygonFeatures func = new PolylineFeaturesToPolygonFeatures();
		func.ctx = testCtx;
		func.input = polylineFeatures;
		func.run();
		
		try {
			int size = func.result.size();
			int columns = func.result.getHeader().asList().size();
			assertTrue(message+" Results ",size == 2);
			assertTrue(message+" Column Count",columns == 3);
		} catch (TableException e) {
			fail(message+": Table exception:"+e.getMessage());
		}
		
		
	}

}

