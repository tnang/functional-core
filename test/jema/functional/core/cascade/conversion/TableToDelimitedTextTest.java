/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Oct 5, 2015
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class TableToDelimitedTextTest {
    
    ExecutionContext testCtx;
    CV_Table testTable;
    String testingString;
    
    public TableToDelimitedTextTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        try {
            testCtx = new ExecutionContextImpl();

            testTable = testCtx.createTable("Test");
            testTable.setHeader(new TableHeader("one{string},two{string},three{string},four{string}"));
            CV_Super[] row1 = {new CV_String("one"),new CV_String("in"),new CV_String("the"),new CV_String("same")};
            CV_Super[] row2 = {new CV_String("for"),new CV_String("all"),new CV_String("intensive"),new CV_String("purposes")};
            CV_Super[] row3 = {new CV_String("it's a"),new CV_String("doggy"),new CV_String("dog"),new CV_String("world")};
            
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(TableToJsonTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        testingString = "one{string},two{string},three{string},four{string}\none, in, the, same\nfor, all, intensive, purposes\nit's a, doggy, dog, world\n";
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class TableToDelimitedText.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: TableToDelimitedText : testRun()");
        TableToDelimitedText instance = new TableToDelimitedText();
        instance.inputTable = testTable;
        instance.ctx = testCtx;
        instance.inputString_newLineDelimiter = null;
        instance.run();
        String results = instance.outputString.toString();
        assertTrue(results.equals(testingString));
    }
}
