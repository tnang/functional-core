/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 25, 2015
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class MGRSToDDTest {
    
    ExecutionContext test_ctx;
    CV_Table testTable;
    
    public MGRSToDDTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();
            testTable = test_ctx.createTable("Test");
            // random table with some MGRS in column MGRS
            testTable.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal},MGRS{string},testTimestamp1{timestamp},testTimestamp2{timestamp},testTimestamp3{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111), new CV_String("4QFJ12345678"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111), new CV_Decimal(1.211111), new CV_String("19TDJ3858897366"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111), new CV_Decimal(1.111111), new CV_String("19TDJ 38588 97366"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000), new CV_Decimal(1.111111), new CV_String("16SGL01GF4554"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable = testTable.toReadable();
        } catch (Exception ex) {
            Logger.getLogger(TableToJsonTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of latLonFromMgrs method, of class MGRSToDD.
     * This is for single conversion to a double[]
     */
    @Test
    public void testLatLonFromMgrs() {
        System.out.println("* Conversion JUnit4Test: MgrsToDD : testLatLonFromMgrs()");
        String mgrs = "4QFJ12345678";
        // double[] expResult = {21.3094332, -157.9168674};
        double[] result = MGRSToDD.latLonFromMgrs(mgrs);
        assertEquals("21.309433233733703", Double.toString(result[0]));
        assertEquals("-157.91686743821978", Double.toString(result[1]));
    }

    /**
     * Test of run method, of class MGRSToDD.
     * @throws jema.common.types.table.TableException
     */
    
    @Test
    public void testRun() throws TableException {
        System.out.println("* Conversion JUnit4Test: MgrsToDD : testRun()");
        MGRSToDD instance = new MGRSToDD();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.inputString_column = new CV_String("MGRS");
        instance.run();
        CV_Table outputTable = instance.output_table.toReadable();
        assertTrue(outputTable.readRow().get(7).toString().matches("21.309433233733703"));
        assertTrue(outputTable.readRow().get(7).toString().matches("44.226935457427416"));
        assertTrue(outputTable.readRow().get(7).toString().matches("44.226935457427416"));
        assertTrue(outputTable.readRow().get(7).toString().matches("Invalid MGRS"));
    }
    
}
