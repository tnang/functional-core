// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 23, 2015
 */
package jema.functional.core.cascade.conversion;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class DecimalToIntegerTest {
    
    public DecimalToIntegerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DecimalToInteger.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion.Numbers JUnit4Test: DecimalToInteger : testRun(()");
        DecimalToInteger instance;
        
        System.out.println("\tTesting no rounding decimal 10.1 = int 10");
        instance = new DecimalToInteger();
        instance.input = new CV_Decimal(10.1);
        instance.rounding = new CV_Boolean(false);
        instance.run();
        String test1 = Integer.toString(instance.output.value().intValue());
        assertTrue(test1.equals("10"));
        
        System.out.println("\tTesting no rounding decimal 10.6 = int 10");
        instance = new DecimalToInteger();
        instance.input = new CV_Decimal(10.6);
        instance.rounding = new CV_Boolean(false);
        instance.run();
        String test2 = Integer.toString(instance.output.value().intValue());
        assertTrue(test2.equals("10"));
        
        System.out.println("\tTesting w/rounding decimal 10.1 = int 10");
        instance = new DecimalToInteger();
        instance.input = new CV_Decimal(10.1);
        instance.rounding = new CV_Boolean(true);
        instance.run();
        String test3 = Integer.toString(instance.output.value().intValue());
        assertTrue(test3.equals("10"));
        
        System.out.println("\tTesting w/rounding decimal 10.6 = int 11");
        instance = new DecimalToInteger();
        instance.input = new CV_Decimal(10.6);
        instance.rounding = new CV_Boolean(true);
        instance.run();
        String test4 = Integer.toString(instance.output.value().intValue());
        assertFalse(test4.equals("10"));
        assertTrue(test4.equals("11"));
        
        
    }
    
}
