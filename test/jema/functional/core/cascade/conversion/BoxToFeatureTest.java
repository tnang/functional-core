package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Box;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

public class BoxToFeatureTest {
	
	// Test polygon
		public final CV_Box box= new CV_Box(-76.98, 24, -72.76, 26.8);
		
		// Result Polygon
		public final CV_Polygon resultPolygon = new CV_Polygon(box);

		// Execution Context
		public ExecutionContext testCtx;

		// Test message used for pretty printing
		String message = "* Conversion.DataType.ToFeatureCollection JUnit4Test: BoxToFeature: ";
		
		@BeforeClass
		public static void setUpBeforeClass() throws Exception {
		}

		@AfterClass
		public static void tearDownAfterClass() throws Exception {
		}

		@Before
		public void setUp() throws Exception {
			testCtx = new ExecutionContextImpl();
		}

		@After
		public void tearDown() throws Exception {
		}
		
		/**
		 * This test will submit a Box 
		 */
		@Test
		public void submitBox() {
			
			System.out.println(message+"submitBox()");
			BoxToFeature instance = new BoxToFeature();
			instance.input = box;
			instance.ctx = testCtx;
			instance.run();

			try {
				// Test to see if there is a single entry in the table
				int expectedRows = 1;
				assertEquals(message+"submitBox(): Number of Results Rows Equals Expected Results",instance.result.size(), expectedRows);

				final CV_Table table = instance.result;
				table.stream().forEach(
						(List<CV_Super> current_row) -> {

							try {
								if (table.getCurrentRowNum() == 0) {
									CV_Super thiscolumnrowvalue1 = current_row
											.get(0);
									assertTrue(message+"submitBox(): Starting and Ending Polygons Match",resultPolygon
											.equals(thiscolumnrowvalue1));
								}

								table.readRow();
							} catch (Exception e) {
								fail(message+"submitBox(): Fatal Error Has Occured "+e.getMessage());
								Logger.getLogger(
										PolygonStringToFeatureTest.class.getName())
										.log(Level.SEVERE, null, e);
							}

						});
			} catch (TableException ex) {
				fail(message+"submitBox(): Fatal Error Has Occured "+ex.getMessage());
				Logger.getLogger(PolygonStringToFeatureTest.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
}
