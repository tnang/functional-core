/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class TableToJsonTest {

    ExecutionContext test_ctx;
    CV_Table testTable;

    public TableToJsonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("Test");
            testTable.setHeader(new TableHeader("test,testcolumn1{decimal},testcolumn2{decimal},testInteger{integer},testTimestamp1{timestamp},testTimestamp2{timestamp},testTimestamp3{timestamp}"));
            CV_Super[] row1 = {new CV_String("odd"), new CV_Decimal(1.111111), new CV_Decimal(1.111111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row2 = {new CV_String("even"), new CV_Decimal(1.111111), new CV_Decimal(1.211111), new CV_Integer(1), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row3 = {new CV_String("odd"), new CV_Decimal(1.121111111), new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row4 = {new CV_String("even"), new CV_Decimal(1.1311000), new CV_Decimal(1.111111), new CV_Integer(2), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row5 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row6 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row7 = {new CV_String("odd"), new CV_Decimal(1.14110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};
            CV_Super[] row8 = {new CV_String("even"), new CV_Decimal(1.15110001), new CV_Decimal(1.111111), new CV_Integer(3), new CV_Timestamp("2015-01-05T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z"), new CV_Timestamp("2015-01-01T16:09:43.328Z")};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(TableToJsonTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class TableToJson.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: TableToJsonTest : testRun()");
        TableToJson instance = new TableToJson();
        instance.inputTable = testTable;
        instance.run();
        String result = instance.output.value();

        JSONObject jsonObject = new JSONObject(result);
        JSONArray array = jsonObject.getJSONArray("table");

        //By asserting the jsonObject has a "table" key, I have confirmed that 
        // it is a valid JSONObject otherwise an exception would be thrown.
        assertTrue(jsonObject.has("table"));

        int array_result = array.length();
        int expResult = 9;
        //There are nine rows total include the header
        assertEquals(expResult, array_result);

    }

}
//UNCLASSIFIED
