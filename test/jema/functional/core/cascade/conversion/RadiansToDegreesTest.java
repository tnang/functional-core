package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import jema.common.types.CV_Decimal;
import jema.functional.api.ExecutionContext;

import org.junit.Test;

public class RadiansToDegreesTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Numbers.RadiansToDegreesTest JUnit4Test: ";

	double RADIANS_TO_DEGREES = 180d / Math.PI;
	
	/*
	 * Inputs
	 */
	double[] inputs = new double[] {0.0,0.7853981633974483,1.0471975511965976,1.5707963267948966,3.141592653589793,6.283185307179586 };


	@Test
	public void radiansToDegreesTest() {
		System.out.println(message + "::radiansToDegreesTest");
		RadiansToDegrees instance = new RadiansToDegrees();

		for (int i = 0; i < inputs.length; i++) {
			instance.input = new CV_Decimal(inputs[i]);

			System.out.println("\tTesting radians = " + inputs[i]);

			instance.run();
        	Double answer = instance.output.value();
        	double degrees = inputs[i] * RADIANS_TO_DEGREES;
        	System.out.println("Output degrees = "+degrees);
        	
        	assertTrue(answer.equals(degrees));
		}
	}

}