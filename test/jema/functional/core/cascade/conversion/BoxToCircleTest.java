package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Polygon;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.PostgisUtil;

/**
 * Unit test cases for BoxToCircle functional.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.movable-type.co.uk/scripts/latlong.html">
 * Calculate distance, bearing and more between Latitude/Longitude points</a></li>
 * </ul>
 * @author CASCADE
 *
 */
public class BoxToCircleTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(BoxToCircleTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * BoxToCircle test
	 * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li><a href="http://www.fai.org/distance_calculation/">
	 * WORLD DISTANCE CALCULATOR</a></li>
	 * </ul>
	 */
	@Test
	public void testBoxToCircle()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Expected dist = 782.8 km for (5,5) to (10, 10)
//			CV_Box srcGeom = new CV_Box(39.0, 28.0, 49.0, 38.0);  //cvDataGenerator.genBox(); //
			CV_Box srcGeom = new CV_Box(55, 55, 60.0, 60.0);  //cvDataGenerator.genBox(); //

			// Expected results
			CV_Polygon cvPolygon = srcGeom.toPolygon();
			CV_Circle wwGeom = GeometryUtil.polygonToCircle(cvPolygon);
			CV_Circle postGISGeom = PostgisUtil.instance().polygonToCircle(ctx, cvPolygon);
			CV_Circle geoToolsGeom = GeoToolsUtil.instance().polygonToCircle(cvPolygon);
			CV_Circle expectedGeom = wwGeom;

			// Initialize functional
			BoxToCircle func = new BoxToCircle();
			func.srcBox = srcGeom;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Circle destGeom = func.destCircle;

			// Radius has issues with precision
			boolean result = (destGeom != null) && (destGeom.getCenter().equals(expectedGeom.getCenter()));
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s" + 
				"\nworldWind=%s\ngeoTools=%s\npostGIS=%s", 
				methodName,
				result,
				srcGeom, 
				destGeom,
				wwGeom,
				geoToolsGeom,
				postGISGeom));			
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				BoxToCircle func = new BoxToCircle();
				func.srcBox = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source Box is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
