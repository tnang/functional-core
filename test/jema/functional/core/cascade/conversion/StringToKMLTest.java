/**
 * 
 */
package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hinklejo
 *
 */
public class StringToKMLTest {

	/**
	 * KML Test string loaded from FILE
	 */
	public String kmlTestString;
	
	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;
	
	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.StringToKML JUnit4Test: ";
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Load up the kmlTestString 
		try {
			InputStream kmltestFileInputStream = this.getClass().getResourceAsStream("/data/KMLTestFile.kml");	
			kmlTestString = IOUtils.toString(kmltestFileInputStream, "UTF-8");  
		} catch (IOException ex) {
			kmlTestString = "";
		}
		
		testCtx = new ExecutionContextImpl();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStringToKML() {
		
		System.out.println(message+"StringToKMLTest : testStringToKML()");
		// Make sure the KML String loaded correctly, if it is empty fail the test
		if (kmlTestString.isEmpty()) {
			fail(message+"The KML Test File failed to load");
		}

		
		StringToKML instance = new StringToKML();
		instance.input = new CV_String(kmlTestString);
		instance.ctx = testCtx;
		instance.run();
		
		CV_WebResource result = instance.output;
		
		// Here is a stupid check, but I think it's need.  Does the file end with .kml
		String fileName = result.toString();
		assertTrue(message+"Output ends with .kml",fileName.endsWith(".kml"));
		// Now read the result contents and compare it with the original
		try {
			String resultContents = IOUtils.toString(result.getURL().openStream(), "UTF-8");
			assertTrue(message+"Proccessed Results equals Input",kmlTestString.equals(resultContents));
			
		} catch (IOException | RuntimeException e) {
			fail(message+"Fatal Error has occured "+e.getMessage() );
			Logger.getLogger(StringToKMLTest.class.getName()).log(
					Level.SEVERE, null, e);
		}
	}

}
