 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conversion;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class BooleanToIntegerTest {
    
    public BooleanToIntegerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIntegerFromBoolean method, of class BooleanToInteger.
     */
    @Test
    public void testGetIntegerFromBoolean() {
        System.out.println("* Conversion JUnit4Test: BooleanToIntegerTest : testGetIntegerFromBoolean()");
        
        CV_Boolean val = new CV_Boolean(true);
        BooleanToInteger instance = new BooleanToInteger();
        CV_Integer expResult = new CV_Integer(1);
        CV_Integer result = instance.getIntegerFromBoolean(val);
        assertEquals(expResult, result);
        
        val = new CV_Boolean(false);
        instance =  new BooleanToInteger();
        expResult = new CV_Integer(0);
        result = instance.getIntegerFromBoolean(val);
        assertEquals(expResult, result);

    }

    /**
     * Test of run method, of class BooleanToInteger.
     */
    @Test
    public void testRun() {
        System.out.println("* Compression JUnit4Test: BooleanToIntegerTest : testRun()");
        BooleanToInteger instance = new BooleanToInteger();
        instance.input = new CV_Boolean(true);
        instance.run();
        assertEquals(new Long(1), instance.result.value());
        
        instance = new BooleanToInteger();
        instance.input = new CV_Boolean(false);
        instance.run();
       assertEquals(new Long(0), instance.result.value());
    }
    
}
