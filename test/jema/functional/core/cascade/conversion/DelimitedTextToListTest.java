// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 23, 2015
 */


package jema.functional.core.cascade.conversion;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class DelimitedTextToListTest {
    
    public DelimitedTextToListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DelimitedTextToList.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: IpAddressInRange : testRun(()");
        DelimitedTextToList instance;
        
        List<String> testList = new ArrayList<>();
        testList.add("test file top line");
        testList.add("second line");
        testList.add("third line");
        testList.add("fourth line \n divided");
        testList.add("last line");
        
        
        
        System.out.println("\tTesting no web resource exception");
        instance = new DelimitedTextToList();
        instance.input_strDelimiter = new CV_String("e");
        instance.input_boolHeader = new CV_Boolean(false);
        instance.input_excludeEmpty = new CV_Boolean(false);
        try {
            instance.run();
            System.out.println("test");
            fail("Should have thrown an IllegalStateException because no WebResource was specified!");
        } catch (IllegalStateException e) {
            assertEquals("You have not added an input file!", e.getMessage());
        }
        
        
        System.out.println("\tTesting row counts exception");
        instance = new DelimitedTextToList();
        instance.input_strDelimiter = new CV_String("e");
        instance.input_boolHeader = new CV_Boolean(true);
        instance.input_excludeEmpty = new CV_Boolean(false);
        instance.input_intTopLines = new CV_Integer(4);
        
        try {
           instance.input_textFile = new CV_WebResource(new URI("http://www.fakewebsite"));
        } catch (Exception e) {
            fail("Could not build URI: " + e.getMessage());
        }
        
        try {
            instance.run();
            System.out.println("test");
            fail("Should have thrown an IllegalStateException because input_boolHeader and input_intTopLines both have values!");
        } catch (IllegalStateException e) {
            assertEquals("You have selected 'Delete top row' while 'delete top X rows' is set to higher than 0! Please use one option or the other", e.getMessage());
        }
        
        System.out.println("\tTesting negative number catcher exception");
        instance = new DelimitedTextToList();
        instance.input_strDelimiter = new CV_String("e");
        instance.input_boolHeader = new CV_Boolean(true);
        instance.input_excludeEmpty = new CV_Boolean(true);
        instance.input_intTopLines = new CV_Integer(-4);
        
        try {
           instance.input_textFile = new CV_WebResource(new URI("http://www.fakewebsite"));
        } catch (Exception e) {
            fail("Could not build URI: " + e.getMessage());
        }
        
        try {
            instance.run();
            System.out.println("test");
            fail("Should have thrown an IllegalStateException because input_intTopLines has a negative value!");
        } catch (IllegalStateException e) {
            assertEquals("You have attempted to delete a negative number(" + instance.input_intTopLines.value().intValue() + ") of rows from the top!", e.getMessage());
        }        
        
        
        
    }
    
}
