package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PolygonStringToFeatureTest {

	// Test polygon
	public final String polygonString = "10.689697265625 -25.0927734375, 34.595947265625 "
			+ "-20.1708984375, 38.814697265625 -35.6396484375, 13.502197265625 "
			+ "-39.1552734375, 10.689697265625 -25.0927734375";

	// Result Polygon String
	public final CV_Polygon resultPolygon = new CV_Polygon("POLYGON(("
			+ polygonString + "))");

	// Execution Context
	public ExecutionContext testCtx;

	// Test message used for pretty printing
	String message = "* Conversion.DataType.ToFeatureCollection JUnit4Test: PolygonStringToFeature: ";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testCtx = new ExecutionContextImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * This test will submit a properly formated WKT Polygon string
	 */
	@Test
	public void submitProperWKTPolygon() {
		
		System.out.println(message+"submitProperWKTPolygon()");
		PolygonStringToFeature instance = new PolygonStringToFeature();
		instance.input = new CV_String("POLYGON((" + polygonString + "))");
		instance.ctx = testCtx;
		instance.run();

		try {
			// Test to see if there is a single entry in the table
			int expectedRows = 1;
			assertEquals(message+"submitProperWKTPolygon(): Number of Results Rows Equals Expected Results",instance.result.size(), expectedRows);

			final CV_Table table = instance.result;
			table.stream().forEach(
					(List<CV_Super> current_row) -> {

						try {
							if (table.getCurrentRowNum() == 0) {
								CV_Super thiscolumnrowvalue1 = current_row
										.get(0);
								assertTrue(message+"submitProperWKTPolygon(): Starting and Ending Polygons Match",resultPolygon
										.equals(thiscolumnrowvalue1));
							}

							table.readRow();
						} catch (Exception e) {
							fail(message+"submitProperWKTPolygon(): Fatal Error Has Occured "+e.getMessage());
							Logger.getLogger(
									PolygonStringToFeatureTest.class.getName())
									.log(Level.SEVERE, null, e);
						}

					});
		} catch (TableException ex) {
			fail(message+"submitProperWKTPolygon(): Fatal Error Has Occured "+ex.getMessage());
			Logger.getLogger(PolygonStringToFeatureTest.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}

	/**
	 * This test will submit a legacy formated WKT Polygon string
	 */
	@Test
	public void submitLegacyWKTPolygon() {
		System.out.println(message+"submitLegacyWKTPolygon()");
		PolygonStringToFeature instance = new PolygonStringToFeature();
		instance.input = new CV_String(polygonString);
		instance.ctx = testCtx;
		instance.run();
		try {
			// Test to see if there is a single entry in the table
			int expectedRows = 1;
			assertEquals(message+"submitLegacyWKTPolygon(): Number of Results Rows Equals Expected Results",instance.result.size(), expectedRows);

			final CV_Table table = instance.result;
			table.stream().forEach(
					(List<CV_Super> current_row) -> {
						try {
							if (table.getCurrentRowNum() == 0) {
								CV_Super thiscolumnrowvalue1 = current_row
										.get(0);
								assertTrue(message+"submitLegacyWKTPolygon(): Starting and Ending Polygons Match",resultPolygon
										.equals(thiscolumnrowvalue1));
							}
							table.readRow();
						} catch (Exception e) {
							fail(message+"submitLegacyWKTPolygon(): Fatal Error Has Occured "+e.getMessage());
							Logger.getLogger(
									PolygonStringToFeatureTest.class.getName())
									.log(Level.SEVERE, null, e);
						}
					});
		} catch (TableException ex) {
			fail(message+"submitLegacyWKTPolygon(): Fatal Error Has Occured "+ex.getMessage());
			Logger.getLogger(PolygonStringToFeatureTest.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}
}
