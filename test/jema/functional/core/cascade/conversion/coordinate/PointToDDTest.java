/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 3:10:22 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class PointToDDTest {

    private static final double DELTA = 1e-14;

    /**
     * Test of run method, of class PointToDD.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: PointToDD : testRun()");

        PointToDD instance = new PointToDD();
        instance.point = new CV_Point(-79.98222222, 30.26588683);
        instance.run();

        assertEquals(30.26588683, instance.latitude.value(), DELTA);
        assertEquals(-79.98222222, instance.longitude.value(), DELTA);
    }
}
