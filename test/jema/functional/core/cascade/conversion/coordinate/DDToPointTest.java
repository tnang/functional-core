/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 12:32:38 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Decimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class DDToPointTest {

    private static final double DELTA = 1e-14;

    /**
     * Test of run method, of class DDToPoint.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: DDToPoint : testRun()");

        DDToPoint instance = new DDToPoint();
        instance.latitude = new CV_Decimal(30.26588683);
        instance.longitude = new CV_Decimal(-79.98222222);
        instance.run();
        assertEquals(30.26588683, instance.point.getY(), DELTA);
        assertEquals(-79.98222222, instance.point.getX(), DELTA);
    }
}
