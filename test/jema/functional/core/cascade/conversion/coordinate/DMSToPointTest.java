/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 14, 2015 12:10:14 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class DMSToPointTest {

    private static final double DELTA = 1e-7;

    /**
     * Test of run method, of class DMSToPoint.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: DMSToPoint : testRun()");

        DMSToPoint instance = new DMSToPoint();
        instance.dms = new CV_String("361030.00N 1150811.00W");
        instance.run();

        CV_Point point = instance.point;
        assertEquals(36.175, point.getY(), DELTA);
        assertEquals(-115.1363889, point.getX(), DELTA);
    }

    /**
     * Test of run method, of class DMSToPoint.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_lonOutOfRange() {
        System.out.println("* Conversion JUnit4Test: DMSToPoint : testRun_lonOutOfRange()");

        DMSToPoint instance = new DMSToPoint();
        instance.dms = new CV_String("751557S 2795856E");
        instance.run();
    }

    /**
     * Test of run method, of class DMSToPoint.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_invalidFormat() {
        System.out.println("* Conversion JUnit4Test: DMSToPoint : testRun_invalidFormat()");

        DMSToPoint instance = new DMSToPoint();
        instance.dms = new CV_String("75 1557S 27 95856E");
        instance.run();
    }
}
