/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 3:28:23 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class PointToUTMTest {

    /**
     * Test of run method, of class PointToUTM.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: PointToUTM : testRun()");

        PointToUTM instance = new PointToUTM();
        instance.point = new CV_Point(-115.1363889, 36.175);
        instance.run();

        assertEquals("11 N 667600.005359161E 4004967.9738034457N", instance.utm.value());
    }
}
