/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 26, 2015
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author trask
 */
public class MGRSToPointTest {

    public MGRSToPointTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class MGRSToPoint.
     */
    @Test
    public void testRun() {
        System.out.println("\n\n▄████▄   ▄▄▄        ██████  ▄████▄   ▄▄▄      ▓█████▄ ▓█████ \n▒██▀ ▀█  ▒████▄    ▒██    ▒ ▒██▀ ▀█  ▒████▄    ▒██▀ ██▌▓█   ▀ \n▒▓█    ▄ ▒██  ▀█▄  ░ ▓██▄   ▒▓█    ▄ ▒██  ▀█▄  ░██   █▌▒███   \n▒▓▓▄ ▄██▒░██▄▄▄▄██   ▒   ██▒▒▓▓▄ ▄██▒░██▄▄▄▄██ ░▓█▄   ▌▒▓█  ▄ \n▒ ▓███▀ ░ ▓█   ▓██▒▒██████▒▒▒ ▓███▀ ░ ▓█   ▓██▒░▒████▓ ░▒████▒\n░ ░▒ ▒  ░ ▒▒   ▓▒█░▒ ▒▓▒ ▒ ░░ ░▒ ▒  ░ ▒▒   ▓▒█░ ▒▒▓  ▒ ░░ ▒░ ░\n ░  ▒     ▒   ▒▒ ░░ ░▒  ░ ░  ░  ▒     ▒   ▒▒ ░ ░ ▒  ▒  ░ ░  ░\n░          ░   ▒   ░  ░  ░  ░          ░   ▒    ░ ░  ░    ░   \n░ ░            ░  ░      ░  ░ ░            ░  ░   ░       ░  ░\n░                           ░                   ░             \n\n\n");
        System.out.println("* Conversion JUnit4Test: MgrsToPoint : testRun()");
        MGRSToPoint instance = new MGRSToPoint();
        instance.mgrs = new CV_String("04QFJ 12340 56780");
        instance.run();

        CV_Point point = instance.point;
        double x = point.getY();
        double y = point.getX();
        assertTrue(x == 21.309433233733703);
        assertTrue(y == -157.91686743821978);
    }
}
