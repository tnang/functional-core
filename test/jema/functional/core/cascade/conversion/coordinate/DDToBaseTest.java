/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 24, 2015 7:13:30 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class DDToBaseTest {

    /**
     * Execution Context
     */
    public ExecutionContext testCtx;

    /**
     * Test Table
     */
    CV_Table testTable;

    /**
     * Message string to be printed out for every test
     */
    String message = "* Conversion.Coordinate.DDToBase JUnit4Test: ";

    @Before
    public void setUp() {
        try {
            testCtx = new ExecutionContextImpl();
            testTable = testCtx.createTableFromCSV(this.getClass().getResourceAsStream("/data/CoordinatesConversion.csv"), null).toReadable();
        } catch (Exception ex) {
            Logger.getLogger(DDToBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of findAndVerifyColumnsExist method, of class DDToBase.
     */
    @Test(expected = RuntimeException.class)
    public void testFindAndVerifyColumnsExist_latNotFound() {
        System.out.println(message + "testFindAndVerifyColumnsExist_latNotFound()");

        //Test column name not in table
        DDToBaseImpl instance = new DDToBaseImpl();
        instance.ctx = testCtx;
        instance.inputTable = testTable;
        instance.latColumnNameDD = new CV_String("WRONGCOLUMNNAME");
        instance.lonColumnNameDD = new CV_String("DD LONGITUDE");

        instance.findAndVerifyColumnsExist();
    }

    /**
     * Test of findAndVerifyColumnsExist method, of class DDToBase.
     */
    @Test(expected = RuntimeException.class)
    public void testFindAndVerifyColumnsExist_lonNotFound() {
        System.out.println(message + "testFindAndVerifyColumnsExist_lonNotFound()");

        //Test column name not in table
        DDToBaseImpl instance = new DDToBaseImpl();
        instance.ctx = testCtx;
        instance.inputTable = testTable;
        instance.latColumnNameDD = new CV_String("DD LATITUDE");
        instance.lonColumnNameDD = new CV_String("WRONGCOLUMNNAME");

        instance.findAndVerifyColumnsExist();
    }

    public class DDToBaseImpl extends DDToBase {

    }
}
