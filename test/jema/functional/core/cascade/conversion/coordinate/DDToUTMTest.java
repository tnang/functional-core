/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 14, 2015 9:53:48 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * @author CASCADE
 */
public class DDToUTMTest {

    ExecutionContext test_ctx;
    CV_Table testTable;

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            testTable = test_ctx.createTable("DDToUTM");
            testTable.setHeader(new TableHeader("DD Lat{string},DD Lon{decimal},UTM Output{string},UTM{decimal}"));
            CV_Super[] row1 = {new CV_String("30.26588683"), new CV_Decimal(-79.98222222), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row2 = {new CV_String("36.175"), new CV_Decimal(-115.1363889), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row3 = {new CV_String("-37.64"), new CV_Decimal(112.17), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row4 = {new CV_String("-27"), new CV_Decimal(133.0001303), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row5 = {new CV_String("30.26588683"), new CV_Decimal(-279.98222222), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row6 = {new CV_String("90.26588683"), new CV_Decimal(-79.98222222), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row7 = {new CV_String(""), new CV_Decimal("0"), new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row8 = {new CV_String("-75.26583333"), null, new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row9 = {new CV_String(null), null, new CV_String(""), new CV_Decimal(Double.NaN)};
            CV_Super[] row10 = {null, null, new CV_String(""), new CV_Decimal(Double.NaN)};

            testTable.appendRow(new ArrayList<>(Arrays.asList(row1)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row2)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row3)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row4)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row5)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row6)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row7)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row8)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row9)));
            testTable.appendRow(new ArrayList<>(Arrays.asList(row10)));
            testTable = testTable.toReadable();

        } catch (Exception ex) {
            Logger.getLogger(DDToUTMTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class DDToUTM.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun() throws TableException {
        System.out.println("* Conversion JUnit4Test: DDToUTM : testRun()");

        DDToUTM instance = new DDToUTM();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.latColumnNameDD = new CV_String("DD Lat");
        instance.lonColumnNameDD = new CV_String("DD Lon");
        instance.outColumnName = new CV_String("UTM Out");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                CV_Super thiscolumnrowvalue;
                switch (table.getCurrentRowNum()) {
                    case 0:
                        thiscolumnrowvalue = current_row.get(4);
                        assertEquals("17 N 597902.1291788172E 3348686.6558349514N", thiscolumnrowvalue.value().toString());
                        break;
                    case 1:
                        thiscolumnrowvalue = current_row.get(4);
                        assertEquals("11 N 667600.005359161E 4004967.9738034457N", thiscolumnrowvalue.value().toString());
                        break;
                    case 2:
                        thiscolumnrowvalue = current_row.get(4);
                        assertEquals("49 S 603224.8387191169E 5833482.811410458N", thiscolumnrowvalue.value().toString());
                        break;
                    case 3:
                        thiscolumnrowvalue = current_row.get(4);
                        assertEquals("53 S 301558.6981850687E 7011992.066653093N", thiscolumnrowvalue.value().toString());
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        thiscolumnrowvalue = current_row.get(4);
                        assertEquals("", thiscolumnrowvalue.value().toString());
                        break;
                }

                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DDToUTMTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        );
    }

    /**
     * Test of run method, of class DDToUTM.
     *
     * @throws jema.common.types.table.TableException
     */
    @Test
    public void testRun_columnExists() throws TableException {
        System.out.println("* Conversion JUnit4Test: DDToUTM : testRun_columnExists()");

        DDToUTM instance = new DDToUTM();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.latColumnNameDD = new CV_String("DD Lat");
        instance.lonColumnNameDD = new CV_String("DD Lon");
        instance.outColumnName = new CV_String("UTM Output");
        instance.run();

        CV_Table table = instance.result.toReadable();

        table.stream().forEach((List<CV_Super> current_row) -> {
            try {
                CV_Super thiscolumnrowvalue;
                switch (table.getCurrentRowNum()) {
                    case 0:
                        thiscolumnrowvalue = current_row.get(2);
                        assertEquals("17 N 597902.1291788172E 3348686.6558349514N", thiscolumnrowvalue.value().toString());
                        break;
                    case 1:
                        thiscolumnrowvalue = current_row.get(2);
                        assertEquals("11 N 667600.005359161E 4004967.9738034457N", thiscolumnrowvalue.value().toString());
                        break;
                    case 2:
                        thiscolumnrowvalue = current_row.get(2);
                        assertEquals("49 S 603224.8387191169E 5833482.811410458N", thiscolumnrowvalue.value().toString());
                        break;
                    case 3:
                        thiscolumnrowvalue = current_row.get(2);
                        assertEquals("53 S 301558.6981850687E 7011992.066653093N", thiscolumnrowvalue.value().toString());
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        thiscolumnrowvalue = current_row.get(2);
                        assertEquals("", thiscolumnrowvalue.value().toString());
                        break;
                }

                table.readRow();
            } catch (TableException ex) {
                Logger.getLogger(DDToUTMTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        );
    }

    /**
     * Test of run method, of class DDToUTM.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_columnExists_wrongType() {
        System.out.println("* Conversion JUnit4Test: DDToUTM : testRun_columnExists_wrongType()");

        DDToUTM instance = new DDToUTM();
        instance.inputTable = testTable;
        instance.ctx = test_ctx;
        instance.latColumnNameDD = new CV_String("DD Lat");
        instance.lonColumnNameDD = new CV_String("DD Lon");
        instance.outColumnName = new CV_String("UTM");
        instance.run();
    }
}
