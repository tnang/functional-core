/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 8:28:20 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class UTMToPointTest {

    private static final double DELTA = 1e-8;

    /**
     * Test of run method, of class UTMToPoint.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: UTMToPoint : testRun()");

        UTMToPoint instance = new UTMToPoint();
        instance.utm = new CV_String("11 N 667600.005359161E 4004967.9738034457N");
        instance.run();

        CV_Point point = instance.point;
        assertEquals(36.175, point.getY(), DELTA);
        assertEquals(-115.1363889, point.getX(), DELTA);
    }

    /**
     * Test of run method, of class UTMToPoint.
     */
    @Test
    public void testRun_noSuffix() {
        System.out.println("* Conversion JUnit4Test: UTMToPoint : testRun_noSuffix()");

        UTMToPoint instance = new UTMToPoint();
        instance.utm = new CV_String("11 N 667600.005359161 4004967.9738034457");
        instance.run();

        CV_Point point = instance.point;
        assertEquals(36.175, point.getY(), DELTA);
        assertEquals(-115.1363889, point.getX(), DELTA);
    }

    /**
     * Test of run method, of class UTMToPoint.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_invalidFormat() {
        System.out.println("* Conversion JUnit4Test: UTMToPoint : testRun_invalidFormat()");

        UTMToPoint instance = new UTMToPoint();
        instance.utm = new CV_String("11 N 667600.0053591614004967.9738034457");
        instance.run();
    }
}
