/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 2:43:10 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class PointToMGRSTest {

    /**
     * Test of run method, of class PointToMGRS.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: PointToMGRS : testRun()");

        PointToMGRS instance = new PointToMGRS();
        instance.point = new CV_Point(-157.91686743821978, 21.309433233733703);
        instance.run();

        assertTrue(instance.mgrs.value().equalsIgnoreCase("04QFJ 12340 56780"));
    }
}
