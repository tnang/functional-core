package jema.functional.core.cascade.conversion.coordinate;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

public class DDToMGRSTest {

    /**
     * Array of Latitudes to convert
     */
    public double[] latitudes = {51.50727, -41.28707, 51.06235,
        -26.91131, 50.90353, 90.26588683, 30.26588683};
    /**
     * Array of Longitudes to convert
     */
    public double[] longitudes = {-0.12765, 174.78103, -1.31409,
        -48.67086, -1.40421, -79.98222222, -279.98222222};
    /**
     * Latitude column name for input table
     */
    public String latitudeColumnName = "DD_LAT";
    /**
     * Longitude column name for input table
     */
    public String longitudeColumnName = "DD_LON";
    /**
     * MRGS column name for output table
     */
    public String mgrsColumnName = "MGRS";

    public String[] mgrsExpectedConversion = {"30UXC 99327 10150",
        "60GUV 14187 27000", "30UXB 18137 58111", "22JGR 31305 21259",
        "30UXB 12205 40310", "", ""};
    /**
     * Functional input
     */
    public CV_Table inputSingleValue;
    /**
     * Functional input
     */
    public CV_Table inputMultipleValues;
    /**
     * Functional input
     */
    public CV_Table inputMultipleStringValues;
    /**
     * Message string to be printed out for every test
     */
    String message = "* Conversion.Coordinate.DDToMGRS JUnit4Test: ";
    /**
     * Execution Context
     */
    public ExecutionContext testCtx;

    @Before
    public void setUp() throws Exception {

        testCtx = new ExecutionContextImpl();

        // Column headers for result table
        List<ColumnHeader> columns = new ArrayList<>();
        columns.add(new ColumnHeader(latitudeColumnName, ParameterType.decimal));
        columns.add(new ColumnHeader(longitudeColumnName, ParameterType.decimal));

        // Column headers for result table
        List<ColumnHeader> strColumns = new ArrayList<>();
        strColumns.add(new ColumnHeader(latitudeColumnName, ParameterType.string));
        strColumns.add(new ColumnHeader(longitudeColumnName, ParameterType.string));

        // Results Result Table
        try {
            inputSingleValue = testCtx.createTable("Single Input Conversion");
            inputSingleValue.setHeader(new TableHeader(columns));

            inputMultipleValues
                    = testCtx.createTable("Multiple Input Conversion");
            inputMultipleValues.setHeader(new TableHeader(columns));

            inputMultipleStringValues = testCtx.createTable("Multiple Input String Column Conversion");
            inputMultipleStringValues.setHeader(new TableHeader(strColumns));

            CV_Super[] row
                    = new CV_Super[]{new CV_Decimal(latitudes[0]),
                        new CV_Decimal(longitudes[0])};
            inputSingleValue.appendRow(Arrays.asList(row));

            for (int i = 0; i < latitudes.length; i++) {
                row
                        = new CV_Super[]{new CV_Decimal(latitudes[i]),
                            new CV_Decimal(longitudes[i])};
                inputMultipleValues.appendRow(Arrays.asList(row));

                row
                        = new CV_Super[]{new CV_String("" + latitudes[i]), new CV_String("" + longitudes[i])};
                inputMultipleStringValues.appendRow(Arrays.asList(row));

            }

            inputSingleValue = inputSingleValue.toReadable();
            inputMultipleValues = inputMultipleValues.toReadable();
            inputMultipleStringValues = inputMultipleStringValues.toReadable();

        } catch (Exception e) {
            fail(message + ": INPUT TABLE SETUP FAILURE " + e.getMessage());
        }
    }

    @Test
    public void testSingleMGRSConversion() {
        System.out.println(message + ": testSingleMGRSConversion()");

        DDToMGRS instance = new DDToMGRS();
        instance.inputTable = inputSingleValue;
        instance.latColumnNameDD = new CV_String(latitudeColumnName);
        instance.lonColumnNameDD = new CV_String(longitudeColumnName);
        instance.outColumnName = new CV_String(mgrsColumnName);
        instance.ctx = testCtx;
        instance.run();

        CV_Table result = instance.result;
        int latIdx = result.findIndex(latitudeColumnName);
        int lonIdx = result.findIndex(longitudeColumnName);
        int mgrsIdx = result.findIndex(mgrsColumnName);

        assertTrue(message
                + "testSingleMGRSConversion():  MGRS Column Found", mgrsIdx != -1);

        try {
            assertEquals(
                    message
                    + "testSingleMGRSConversion():  One Conversion Result Returned",
                    result.size(), 1);

            result.stream()
                    .forEach(
                            row -> {
                                double lat
                                = ((CV_Decimal) row.get(latIdx)).value();
                                double lon
                                = ((CV_Decimal) row.get(lonIdx)).value();
                                String mgrs
                                = ((CV_String) row.get(mgrsIdx)).value();
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper Latitude Result Returned",
                                        (lat == this.latitudes[0]));
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper Longitude Result Returned",
                                        (lon == this.longitudes[0]));
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper MGRS Result Returned",
                                        mgrs.equals(this.mgrsExpectedConversion[0]));
                            });

        } catch (TableException e) {
            fail(message + "testSingleMGRSConversion(): TableExcpetion "
                    + e.getMessage());
        }

    }

    @Test
    public void testMultipleMGRSConversion() {
        System.out.println(message + ": testMultipleMGRSConversion()");

        DDToMGRS instance = new DDToMGRS();
        instance.inputTable = inputMultipleValues;
        instance.latColumnNameDD = new CV_String(latitudeColumnName);
        instance.lonColumnNameDD = new CV_String(longitudeColumnName);
        instance.outColumnName = new CV_String(mgrsColumnName);
        instance.ctx = testCtx;
        instance.run();

        try {

            CV_Table result = instance.result.toReadable();
            int latIdx = result.findIndex(latitudeColumnName);
            int lonIdx = result.findIndex(longitudeColumnName);
            int mgrsIdx = result.findIndex(mgrsColumnName);

            assertTrue(message
                    + "testMultipleMGRSConversion():  MGRS Column Found", mgrsIdx != -1);

            assertEquals(
                    message
                    + "testMultipleMGRSConversion():  One Conversion Result Returned",
                    result.size(), 7);

            AtomicInteger count = new AtomicInteger(0);
            result.stream()
                    .forEach(
                            row -> {
                                double lat
                                = ((CV_Decimal) row.get(latIdx)).value();
                                double lon
                                = ((CV_Decimal) row.get(lonIdx)).value();
                                String mgrs
                                = ((CV_String) row.get(mgrsIdx)).value();
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper Latitude Result Returned",
                                        (lat == this.latitudes[count.get()]));
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper Longitude Result Returned",
                                        (lon == this.longitudes[count.get()]));
                                assertTrue(
                                        message
                                        + "testSingleMGRSConversion(): Proper MGRS Result Returned",
                                        mgrs.equals(this.mgrsExpectedConversion[count
                                                .get()]));
                                count.incrementAndGet();

                            });

        } catch (TableException e) {
            fail(message + "testMultipleMGRSConversion(): TableExcpetion "
                    + e.getMessage());
        }

    }

    @Test
    public void testMultipleStringColumnMGRSConversion() {
        System.out.println(message + ": testMultipleStringColumnMGRSConversion()");

        DDToMGRS instance = new DDToMGRS();
        instance.inputTable = inputMultipleStringValues;
        instance.latColumnNameDD = new CV_String(latitudeColumnName);
        instance.lonColumnNameDD = new CV_String(longitudeColumnName);
        instance.outColumnName = new CV_String(mgrsColumnName);
        instance.ctx = testCtx;
        instance.run();

        try {

            CV_Table result = instance.result.toReadable();
            int latIdx = result.findIndex(latitudeColumnName);
            int lonIdx = result.findIndex(longitudeColumnName);
            int mgrsIdx = result.findIndex(mgrsColumnName);

            assertTrue(message
                    + "testMultipleStringColumnMGRSConversion():  MGRS Column Found", mgrsIdx != -1);

            assertEquals(
                    message
                    + "testMultipleStringColumnMGRSConversion():  One Conversion Result Returned",
                    result.size(), 7);

            AtomicInteger count = new AtomicInteger(0);
            result.stream()
                    .forEach(
                            row -> {
                                double lat = Double.parseDouble(((CV_String) row.get(latIdx)).value());
                                double lon = Double.parseDouble(((CV_String) row.get(lonIdx)).value());
                                String mgrs = ((CV_String) row.get(mgrsIdx)).value();
                                assertTrue(
                                        message
                                        + "testMultipleStringColumnMGRSConversion(): Proper Latitude Result Returned",
                                        (lat == this.latitudes[count.get()]));
                                assertTrue(
                                        message
                                        + "testMultipleStringColumnMGRSConversion(): Proper Longitude Result Returned",
                                        (lon == this.longitudes[count.get()]));
                                assertTrue(
                                        message
                                        + "testMultipleStringColumnMGRSConversion(): Proper MGRS Result Returned",
                                        mgrs.equals(this.mgrsExpectedConversion[count
                                                .get()]));
                                count.incrementAndGet();

                            });

        } catch (TableException e) {
            fail(message + "testMultipleStringColumnMGRSConversion(): TableExcpetion "
                    + e.getMessage());
        }
    }
}
