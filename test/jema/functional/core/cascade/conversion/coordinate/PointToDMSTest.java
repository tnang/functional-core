/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 14, 2015 11:42:59 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class PointToDMSTest {

    /**
     * Test of run method, of class PointToDMS.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion JUnit4Test: PointToDMS : testRun()");

        PointToDMS instance = new PointToDMS();
        instance.point = new CV_Point(-115.1363889, 36.175);
        instance.run();

        assertEquals("361030.00N 1150811.00W", instance.dms.value());
    }

}
