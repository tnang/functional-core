// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 23, 2015
 */
package jema.functional.core.cascade.conversion;

import jema.common.types.CV_Integer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class IntegerToDecimalTest {
    
    public IntegerToDecimalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class IntegerToDecimal.
     */
    @Test
    public void testRun() {
        System.out.println("* Conversion.Numbers JUnit4Test: IntegerToDecimal : testRun(()");
        IntegerToDecimal instance;
        
        System.out.println("\tTesting that output is correct type."); //this is a dumb test, but I couldn't think of anything else to test for with this functional...
        instance = new IntegerToDecimal();
        instance.input = new CV_Integer(10);
        instance.run();
        assertEquals(instance.output.getClass().toString(), "class jema.common.types.CV_Decimal");
       
    }
    
}
