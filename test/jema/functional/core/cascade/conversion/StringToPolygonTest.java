package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_String;
import jema.common.types.CV_Polygon;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

public class StringToPolygonTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(StringToPolygonTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * StringToPolygon test
	 */
	@Test
	public void testStringToPolygon()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			CV_String srcGeom = new CV_String("POLYGON((39.0 38.0,39.0 28.0,49.0 28.0,49.0 38.0,39.0 38.0))"); // Iraq
			
			// Expected results
			CV_Polygon expectedGeom = new CV_Polygon("POLYGON((39.0 38.0,39.0 28.0,49.0 28.0,49.0 38.0,39.0 38.0))");

			// Initialize functional
			StringToPolygon func = new StringToPolygon();
			func.srcString = srcGeom;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Polygon destGeom = func.destPolygon;

			boolean result = (destGeom != null) && (destGeom.equals(expectedGeom));
			LOGGER.log(Level.FINE, String.format(
					"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s", 
					methodName,
					result,
					(srcGeom == null ? null : srcGeom.toString()), 
					(destGeom == null ? null : destGeom.toString()),
					expectedGeom.toString()));
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				StringToPolygon func = new StringToPolygon();
				func.srcString = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source String is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
