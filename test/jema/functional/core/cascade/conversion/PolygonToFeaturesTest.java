package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

public class PolygonToFeaturesTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Features.PolygonToFeaturesTest JUnit4Test: ";

	String polygonStr =
		"POLYGON((-80.05017267509282 34.74434382739751, -79.88840306930403 35.91048563066315, -78.70649085853334 35.37064716330855, "
				+ "-76.27402442506472 36.31528292120307, -77.78588932526866 36.69700254817094, -76.63177563212939 37.4520241783309, "
				+ "-80.05017267509282 34.74434382739751))";
	// CV_Table
	CV_Polygon thePoint = new CV_Polygon(polygonStr);
	CV_String name = new CV_String("Name");
	CV_String description = new CV_String("Description");

	@Before
	public void setUp() throws Exception {
		// Load up the a Table for testing
		// Reuse the CSVToTable functional to help out
		testCtx = new ExecutionContextImpl();
	}

	@Test
	public void testPolygonToFeatures() {
		System.out.println(message + "testPolygonToFeatures()");

		PolygonToFeatures func = new PolygonToFeatures();
		func.ctx = this.testCtx;
		func.inputDesc = name;
		func.inputName = description;
		func.inputPolygon = thePoint;

		func.run();

		CV_Table results = func.result;

		try {
			assertTrue(results.size() == 1);
		} catch (TableException e) {
			fail(message + "testPolygonToFeatures()::TABLE EXCEPTION " + e.getMessage());
		}

	}

	@Test
	public void testPolygonToFeaturesNullPolygon() {
		System.out.println(message + "testPolygonToFeaturesNullPolygon()");

		PolygonToFeatures func = new PolygonToFeatures();
		func.ctx = this.testCtx;
		func.inputDesc = name;
		func.inputName = description;
		func.inputPolygon = null;

		boolean caughtException = false;

		try {
			func.run();
			CV_Table results = func.result;
			assertTrue(results.size() == 1);
		} catch (TableException e) {
			fail(message + "testPolygonToFeaturesNullPolygon()::TABLE EXCEPTION " + e.getMessage());
		} catch (Exception e1) {
			caughtException = true;
		}
		assertTrue(message + "testPolygonToFeaturesNullPolygon()", caughtException);

	}

	@Test
	public void testPointToFeaturesNullNameDesc() {
		System.out.println(message + "testPointToFeaturesNullNameDesc()");

		PolygonToFeatures func = new PolygonToFeatures();
		func.ctx = this.testCtx;
		func.inputDesc = null;
		func.inputName = null;
		func.inputPolygon = thePoint;

		try {
			func.run();
			CV_Table results = func.result;
			assertTrue(results.size() == 1);
		} catch (Exception e) {
			fail(message + "testPointToFeaturesNullNameDesc()::TABLE EXCEPTION " + e.getMessage());
		}

	}
}
