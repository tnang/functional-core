package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Table;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

public class DelimitedTextToTableTest {
	
	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;
	List<CV_String> headerList;
	
	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.delimitedTextToTable JUnit4Test: ";
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		// Load up the kmlTestString 
		headerList = new ArrayList<CV_String>();
		headerList.add(new CV_String("Column"));
		headerList.add(new CV_String("Colum@"));
		headerList.add(new CV_String("Column"));
		
		testCtx = new ExecutionContextImpl();
	}
	
	

	@Test
	public void testHeaderInFile() throws TableException
	{
		URL source = null;
		DelimitedTextToTable func = null;		
		try
		{
			source = this.getClass().getResource("/data/DelimitedTestFile.txt");
			func = new DelimitedTextToTable();
			func.input_textFile = new CV_WebResource(source);
			func.input_strDelimiter = new CV_String ("-");
			func.ctx = testCtx;
			func.input_boolHeader = new CV_Boolean(true);
			func.input_headers = headerList;			
			func.run();			
		
			CV_Table testTable = func.outputTable;
			assertNotNull(testTable);	
			assertEquals(4, testTable.size());
		}
		catch(IllegalStateException ie)
		{
			throw ie;
		}
		catch(Exception e)
		{
			assertTrue(String.format("source=%s, in=%s", source, func.input_textFile), false);
		}
	}
	
	@Test
	public void testHeaderInList() throws TableException
	{
		URL source = null;
		DelimitedTextToTable func = null;
		try
		{
			source = this.getClass().getResource("/data/DelimitedTestFile.txt");
			func = new DelimitedTextToTable();
			func.input_textFile = new CV_WebResource(source);
			func.input_strDelimiter = new CV_String ("-");
			func.ctx = testCtx;
			func.input_boolHeader = new CV_Boolean(false);
			func.input_headers = headerList;
			func.run();			
		
			CV_Table testTable = func.outputTable;
			assertNotNull(testTable);		
			assertEquals(5, testTable.size());
		}
		catch(IllegalStateException ie)
		{
			throw ie;
		}
		catch(Exception e)
		{
			assertTrue(String.format("source=%s, in=%s", source, func.input_textFile), false);
		}
	}
		
	@Test
	public void testValidStartStop() throws TableException
	{
		URL source = null;
		DelimitedTextToTable func = null;
		try
		{
			source = this.getClass().getResource("/data/DelimitedTestFile.txt");
			func = new DelimitedTextToTable();
			func.input_textFile = new CV_WebResource(source);
			func.ctx = testCtx;
			func.input_strDelimiter = new CV_String ("-");
			func.input_boolHeader = new CV_Boolean(false);
			func.input_headers = headerList;
			func.startRow = new CV_Integer(1);
			func.stopRow = new CV_Integer(3);
			func.run();			
		
			CV_Table testTable = func.outputTable;
			assertNotNull(testTable);	
			assertEquals(3, testTable.size());
		}
		catch(IllegalStateException ie)
		{
			throw ie;
		}
		catch(Exception e)
		{
			
		}
	}
			
	@Test(expected=IllegalStateException.class)
	public void testInValidStartStop() throws TableException
	{
		URL source = null;
		DelimitedTextToTable func = null;
		try
		{
			source = this.getClass().getResource("/data/DelimitedTestFile.txt");
			func = new DelimitedTextToTable();
			func.input_textFile = new CV_WebResource(source);
			func.ctx = testCtx;
			func.input_strDelimiter = new CV_String ("-");
			func.input_boolHeader = new CV_Boolean(false);
			func.input_headers = headerList;
			func.startRow = new CV_Integer(2);
			func.stopRow = new CV_Integer(1);		
			
			func.run();			
		
			CV_Table testTable = func.outputTable;
			assertNull(testTable);
			
		}
		catch(IllegalStateException ie)
		{
			throw ie;
		}
		catch(Exception e)
		{
			
		}
	}
	
	@Test
	public void testDefaultDelimiter() 
	{
		URL source = null;
		DelimitedTextToTable func = null;
		try
		{
			source = this.getClass().getResource("/data/DelimitedTestFile2.txt");
			func = new DelimitedTextToTable();
			func.input_textFile = new CV_WebResource(source);
			func.ctx = testCtx;		
			func.input_boolHeader = new CV_Boolean(true);			
			func.run();			
		
			CV_Table testTable = func.outputTable;
			assertNotNull(testTable);	
			assertEquals(4, testTable.size());
		}
		catch(IllegalStateException ie)
		{
			throw ie;
		}
		catch(Exception e)
		{
			
		}
	}
}
