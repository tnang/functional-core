/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

/**
 * @author CASCADE
 */
public class PointFeaturesToLinesOfBearingTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;
	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Features.PointFeaturesToLinesOfBearing JUnit4Test: ";

	/**
	 * Point features
	 */
	CV_Table pointFeatures;

	/**
	 * Column name for Points
	 */
	CV_String pointColumn = new CV_String("point");
	/**
	 * Column name for Header
	 */
	CV_String headerColumn = new CV_String("azimuth");
	/**
	 * Column name for Distance
	 */
	CV_String distanceColumn = new CV_String("distance");

	@Before
	public void setUp() throws Exception {
		// Load up the a Table for testing
		// Reuse the CSVToTable functional to help out
		try {
			testCtx = new ExecutionContextImpl();
			pointFeatures = testCtx.createTableFromCSV(this.getClass().getResourceAsStream("/data/geospatial_data/PointToLOB.csv"), null).toReadable();
		} catch (Exception e) {
			fail(message + "Input Table failed to load: " + e.getMessage());
		}
	}

	@Test
	public void pointFeaturesToLinesOfBearingNormalTest() {
		System.out.println(message + "pointFeaturesToLinesOfBearingNormalTest()");

		PointFeaturesToLinesOfBearing func = new PointFeaturesToLinesOfBearing();
		func.ctx = testCtx;
		func.inputPointFeatures = pointFeatures;
		func.inputHeading = headerColumn;
		func.inputDistance = distanceColumn;

		func.run();

		try {
			int resultsSize = func.output.size();
			assertTrue(resultsSize == 7);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

	@Test(expected = RuntimeException.class)
	public void pointFeaturesToLinesOfBearingNullTableTest() {
		System.out.println(message + "pointFeaturesToLinesOfBearingNullTableTest()");

		PointFeaturesToLinesOfBearing func = new PointFeaturesToLinesOfBearing();
		func.ctx = testCtx;
		func.inputPointFeatures = null;
		func.inputHeading = headerColumn;
		func.inputDistance = distanceColumn;

		func.run();

	}

	@Test(expected = RuntimeException.class)
	public void pointFeaturesToLinesOfBearingNullHeaderTest() {
		System.out.println(message + "pointFeaturesToLinesOfBearingNullHeaderTest()");

		PointFeaturesToLinesOfBearing func = new PointFeaturesToLinesOfBearing();
		func.ctx = testCtx;
		func.inputPointFeatures = pointFeatures;
		func.inputHeading = null;
		func.inputDistance = distanceColumn;

		func.run();

	}
	
	@Test
	public void pointFeaturesToLinesOfBearingNullDistanceTest() {
		System.out.println(message + "pointFeaturesToLinesOfBearingNullDistanceTest()");

		PointFeaturesToLinesOfBearing func = new PointFeaturesToLinesOfBearing();
		func.ctx = testCtx;
		func.inputPointFeatures = pointFeatures;
		func.inputHeading = headerColumn;
		func.inputDistance = null;

		func.run();

		try {
			int resultsSize = func.output.size();
			assertTrue(resultsSize == 7);
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}	
}
