package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;

/**
 * Unit test cases for TableHeaderToList functional.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.movable-type.co.uk/scripts/latlong.html">
 * Calculate distance, bearing and more between Latitude/Longitude points</a></li>
 * </ul>
 * @author CASCADE
 *
 */
public class TableHeaderToListTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(TableHeaderToListTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;

	/** Source CSV */
	private static final String CSV_FILE = "germany_points.csv";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * TableHeaderToList test
	 */
	@Test
	public void testTableHeaderToList()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			CV_Table srcTable = ctx.createTableFromCSV(
				this.getClass().getResourceAsStream("/data/" + CSV_FILE), null);                

			// Expected results
			List<ColumnHeader> expectedResult = srcTable.getHeader().asList();

			// Initialize functional
			TableHeaderToList func = new TableHeaderToList();
			func.srcTable = srcTable;
			func.ctx = ctx;

			// Run functional
			func.run();
			List<CV_String> columnNameList = func.columnNameList;
			List<CV_String> columnTypeList = func.columnTypeList;

			boolean result = 
				(columnNameList != null) && 
				(columnTypeList != null) &&
				(columnNameList.size() == columnTypeList.size()) &&
				(columnNameList.size() == expectedResult.size());

			for(int iCol = 0; iCol < expectedResult.size() && result; iCol++)
			{
				ColumnHeader colHeader = expectedResult.get(iCol);
				CV_String colName = columnNameList.get(iCol);
				CV_String colType = columnTypeList.get(iCol);
				result = colHeader.getName().equals(colName.value());
				if(result) {result = colHeader.getType().name().equals(colType.value());}
			}
			
			if(srcTable != null) {srcTable.close();}
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			try
			{
				TableHeaderToList func = new TableHeaderToList();
				func.srcTable = null;
				func.ctx = ctx;
				func.run();
				assertTrue("Source Table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
