package jema.functional.core.cascade.conversion;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.measures.length.LengthUnit;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Length;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * Unit test cases for PointToBox functional.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.movable-type.co.uk/scripts/latlong.html">
 * Calculate distance, bearing and more between Latitude/Longitude points</a></li>
 * </ul>
 * @author CASCADE
 *
 */
public class PointToBoxTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(PointToBoxTest.class.getName());

	/** Execution context */
	private static ExecutionContext ctx = null;
	
	/** Random data generator */
	private static CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
			ctx = TableUtils.executionContext();
		} 
		catch (Exception e) 
		{
			String msg = String.format("CV setup failed - %s", e.toString());
			LOGGER.log(Level.INFO, msg);
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			String msg = String.format("Table teardown failed - %s", e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}    	
	}

	/**
	 * PointToBox test
	 */
	@Test
	public void testPointToBox()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
			// Source geometry
			CV_Circle circle = cvDataGenerator.genCircle();
			CV_Point srcGeom = circle.getCenter();
			CV_Decimal radius = CV_Decimal.valueOf(circle.getRadiusMeters());
			CV_String radiusUnit = CV_String.valueOf(LengthUnit.METER.name());
			
			// Expected results
			CV_Box expectedGeom = GeometryUtil.circleToBox(circle);
			
			// Initialize functional
			PointToBox func = new PointToBox();
			func.srcPoint = srcGeom;
			func.radius = radius;
			func.radiusUnit = radiusUnit;
			func.ctx = ctx;

			// Run functional
			func.run();
			CV_Box destGeom = func.destBox;

			// Radius has issues with precision
			boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
			LOGGER.log(Level.FINE, String.format(
				"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s", 
				methodName,
				result,
				srcGeom, 
				destGeom,
				expectedGeom));			
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * PointToBox test
	 */
	@Test
	public void testMultiplePointToBox()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
	        CV_Point pt = new CV_Point(10, 10);//cvDataGenerator.genPoint();
			for(double r = 1.0e1; r < 1.1e9; r *= 1.0e1)
			{
		        CV_Circle circle = new CV_Circle(pt, r);
				CV_Point srcGeom = circle.getCenter();
				CV_Decimal radius = CV_Decimal.valueOf(circle.getRadiusMeters());
				CV_String radiusUnit = CV_String.valueOf(LengthUnit.METER.name());
				
				// Expected results
				CV_Box expectedGeom = GeometryUtil.circleToBox(circle);
				
				// Initialize functional
				PointToBox func = new PointToBox();
				func.srcPoint = srcGeom;
				func.radius = radius;
				func.radiusUnit = radiusUnit;
				func.ctx = ctx;
	
				// Run functional
				func.run();
				CV_Box destGeom = func.destBox;
	
				// Radius has issues with precision
				boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
				LOGGER.log(Level.FINE, String.format(
					"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s", 
					methodName,
					result,
					srcGeom, 
					destGeom,
					expectedGeom));			
				assertTrue(destGeom != null);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * PointToBox test
	 */
	@Test
	public void testPointToBoxUnits()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{	
	        CV_Point pt = new CV_Point(10, 10);
	        Double r = 1.0e5;
			for(LengthUnit lu : LengthUnit.values())
			{
				Double len = CV_Length.toLength(lu, r);
		        CV_Circle circle = new CV_Circle(pt, len);
				CV_Point srcGeom = circle.getCenter();
				CV_Decimal radius = CV_Decimal.valueOf(circle.getRadiusMeters());
				CV_String radiusUnit = CV_String.valueOf(lu.name());
				
				// Expected results
				CV_Box expectedGeom = GeometryUtil.circleToBox(circle);
				
				// Initialize functional
				PointToBox func = new PointToBox();
				func.srcPoint = srcGeom;
				func.radius = radius;
				func.radiusUnit = radiusUnit;
				func.ctx = ctx;
	
				// Run functional
				func.run();
				CV_Box destGeom = func.destBox;
	
				// Radius has issues with precision
				boolean result = GeometryUtil.compareBox(destGeom, expectedGeom);
				LOGGER.log(Level.FINE, String.format(
					"%s:\nresult=%s\nsrcGeom=%s\ndestGeom=%s\nexpectedGeom=%s", 
					methodName,
					result,
					(srcGeom == null ? null : srcGeom.toString()), 
					(destGeom == null ? null : destGeom.toString()),
					expectedGeom.toString()));			
				assertTrue(destGeom != null);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			e.printStackTrace();
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Source geometry
			CV_Circle circle = cvDataGenerator.genCircle();
			CV_Point srcGeom = circle.getCenter();
			CV_Decimal radius = CV_Decimal.valueOf(circle.getRadiusMeters());
			CV_String radiusUnit = CV_String.valueOf(LengthUnit.METER.name());
			
			try
			{
				PointToBox func = new PointToBox();
				func.srcPoint = null;
				func.radius = radius;
				func.radiusUnit = radiusUnit;
				func.ctx = ctx;
				func.run();
				assertTrue("Source Point is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				PointToBox func = new PointToBox();
				func.srcPoint = srcGeom;
				func.radius = null;
				func.radiusUnit = radiusUnit;
				func.ctx = ctx;
				func.run();
				assertTrue("Radius is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				PointToBox func = new PointToBox();
				func.srcPoint = srcGeom;
				func.radius = radius;
				func.radiusUnit = CV_String.valueOf("EARTH_RADII");
				func.ctx = ctx;
				func.run();
				assertTrue("Radius unit is invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
