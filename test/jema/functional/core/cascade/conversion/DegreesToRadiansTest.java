package jema.functional.core.cascade.conversion;

import static org.junit.Assert.*;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.ExecutionContext;

import org.junit.Before;
import org.junit.Test;

public class DegreesToRadiansTest {

	/**
	 * ExecutionContext
	 */
	public ExecutionContext testCtx;
	
	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Conversion.DataType.Numbers.DegreesToRadians JUnit4Test: ";
	
	double DEGREES_TO_RADIANS = Math.PI / 180d;

	/*
	 * Inputs
	 */
	double[] inputs = new double[] { 0.0, 45.0, 60.0, 90.0, 180.0, 360.0 };
	
	@Test
	public void degreesToRadiansTest() {
        System.out.println(message+"::degreesToRadiansTest");
        DegreesToRadians instance = new DegreesToRadians();
        
        for (int i=0;i<inputs.length;i++) {
        	instance.input = new CV_Decimal(inputs[i]);
        
        	System.out.println("\tTesting degrees = "+inputs[i]);

        	instance.run();
			Double answer = instance.output.value();
			double radians = inputs[i] * DEGREES_TO_RADIANS;
			System.out.println("Output degrees = " + radians);

			assertTrue(answer.equals(radians));
        }
	}

}
