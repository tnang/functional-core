/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * May 31, 2015 7:23:46 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class ComputeSineTest {

    private static final double DELTA = 1e-15;

    /**
     * Test of run method, of class ComputeSine.
     */
    @Test
    public void testRun_radians() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeSineTest : testRun_radians()");

        ComputeSine instance = new ComputeSine();
        instance.angle = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(Math.PI / 6);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(.5).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(Math.PI / 3);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(Math.sqrt(3) / 2).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(Math.PI / 2);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(1.0).value(), instance.result.value(), DELTA);
    }

    /**
     * Test of run method, of class ComputeSine.
     */
    @Test
    public void testRun_degrees() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeSineTest : testRun_degrees()");

        ComputeSine instance = new ComputeSine();
        instance.angle = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(30.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(.5).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(60.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(Math.sqrt(3) / 2).value(), instance.result.value(), DELTA);

        instance = new ComputeSine();
        instance.angle = new CV_Decimal(90.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(instance.result.value(), new CV_Decimal(1.0).value(), DELTA);
    }
}
