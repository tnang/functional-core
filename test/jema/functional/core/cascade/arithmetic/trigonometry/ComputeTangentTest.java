/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 15, 2015 10:21:44 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class ComputeTangentTest {

    private static final double DELTA = 1e-15;

    /**
     * Test of run method, of class ComputeTangent.
     */
    @Test
    public void testRun_radians() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeTangentTest : testRun_radians()");

        ComputeTangent instance = new ComputeTangent();
        instance.angle = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);

        instance = new ComputeTangent();
        instance.angle = new CV_Decimal(Math.PI / 6);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(1 / Math.sqrt(3)).value(), instance.result.value(), DELTA);

        instance = new ComputeTangent();
        instance.angle = new CV_Decimal(Math.PI / 3);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(new CV_Decimal(Math.sqrt(3)).value(), instance.result.value(), DELTA);

        // TODO: Determine how to test tangent of 90 degrees
//        instance = new ComputeTangent();
//        instance.angle = new CV_Decimal(Math.PI / 2);
//        instance.isDegrees = new CV_Boolean(false);
//        instance.run();
//        assertEquals(new CV_Decimal().value(), instance.result.value(), DELTA);
    }

    /**
     * Test of run method, of class ComputeTangent.
     */
    @Test
    public void testRun_degrees() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeTangentTest : testRun_degrees()");

        ComputeTangent instance = new ComputeTangent();
        instance.angle = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);

        instance = new ComputeTangent();
        instance.angle = new CV_Decimal(30.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(1 / Math.sqrt(3)).value(), instance.result.value(), DELTA);

        instance = new ComputeTangent();
        instance.angle = new CV_Decimal(60.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(Math.sqrt(3)).value(), instance.result.value(), DELTA);

        // TODO: Determine how to test tangent of 90 degrees
//        instance = new ComputeTangent();
//        instance.angle = new CV_Decimal(90.0);
//        instance.isDegrees = new CV_Boolean(true);
//        instance.run();
//        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);
    }

}
