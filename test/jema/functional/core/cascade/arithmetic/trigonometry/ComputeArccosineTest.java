/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 30, 2015 7:20:09 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class ComputeArccosineTest {

    private static final double DELTA = 1e-14;

    /**
     * Test of run method, of class ComputeArccosine.
     */
    @Test
    public void testRun_radians() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeArccosineTest : testRun_radians()");

        ComputeArccosine instance = new ComputeArccosine();
        instance.value = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(instance.result.value(), new CV_Decimal(Math.PI / 2).value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(.5);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(instance.result.value(), new CV_Decimal(Math.PI / 3).value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(Math.sqrt(3) / 2);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(instance.result.value(), new CV_Decimal(Math.PI / 6).value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(1.0);
        instance.isDegrees = new CV_Boolean(false);
        instance.run();
        assertEquals(instance.result.value(), new CV_Decimal(0.0).value(), DELTA);
    }

    /**
     * Test of run method, of class ComputeArccosine.
     */
    @Test
    public void testRun_degrees() {
        System.out.println("* Arithmetic.Trigonometry JUnit4Test: ComputeArccosineTest : testRun_degrees()");

        ComputeArccosine instance = new ComputeArccosine();
        instance.value = new CV_Decimal(0.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(90.0).value(), instance.result.value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(.5);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(60.0).value(), instance.result.value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(Math.sqrt(3) / 2);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(30.0).value(), instance.result.value(), DELTA);

        instance = new ComputeArccosine();
        instance.value = new CV_Decimal(1.0);
        instance.isDegrees = new CV_Boolean(true);
        instance.run();
        assertEquals(new CV_Decimal(0.0).value(), instance.result.value(), DELTA);
    }
}
