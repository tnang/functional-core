package jema.functional.core.cascade.arithmetic.trigonometry;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.util.TableUtils;

public class CalculateSineOfColumnsTest 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(CalculateSineOfColumnsTest.class.getName());

	/** Sine column name */
	private static final String OUTPUT_COL_NAME = "SINE_VAL";

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();

	/** Test data header column names */
	private enum TEST_COLS
	{
		NAME(ParameterType.string),
		ANG_DEG_INT(ParameterType.integer),
		ANG_DEG_DEC(ParameterType.decimal);

		ParameterType parameterType;

		private TEST_COLS(ParameterType parameterType)
		{
			this.parameterType = parameterType;
		}

		public ParameterType getParameterType() {return parameterType;}
	}

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Sine in degrees test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSineDeg()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			for(int iTestCols = 0; iTestCols < TEST_COLS.values().length; iTestCols++)
			{
				// Create sample data
				List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
				for(int iAng = -180; iAng <= 180; iAng += 30)
				{
					List<CV_Super> row = new ArrayList<CV_Super>();
					String name = iAng < 0 ? String.format("Name_Neg%d", iAng*-1) : String.format("Name_%d", iAng);
					row.add(CV_String.valueOf(name));
					double angDeg = iAng;
					CV_Integer intVal = CV_Integer.valueOf(angDeg);
					CV_Decimal decVal = CV_Decimal.valueOf(angDeg);
					row.add(TEST_COLS.ANG_DEG_INT.ordinal(), intVal);
					row.add(TEST_COLS.ANG_DEG_DEC.ordinal(), decVal);
					data.add(row);
				}

				CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

				if(iTestCols == TEST_COLS.NAME.ordinal()) {continue;}
				TEST_COLS testCols = TEST_COLS.values()[iTestCols];
				CV_String dataColumnName = CV_String.valueOf(testCols.name());

				// Expected results
				CV_Boolean isInputInDegrees = CV_Boolean.valueOf(true);
				List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
				for(List<CV_Super> row : expectedResults)
				{
					int iCol = testCols.ordinal();
					CV_Super<?> cell = row.get(iCol);
					CV_Decimal val = cell instanceof CV_Integer ? CV_Decimal.valueOf(((CV_Integer) cell).value()) : (CV_Decimal) cell;
					double valRad = Math.toRadians(val.value());
					double value = Math.sin(valRad);
					row.add(CV_Decimal.valueOf(value));
				}

				// Initialize functional
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = srcTable;
				func.dataColumnName = dataColumnName;
				func.isInputInDegrees = isInputInDegrees;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();

				// Run functional
				func.run();
				CV_Table destTable = func.destTable;

				// Validate results
				boolean result = TableUtils.compareTable(
						methodName, uriTestTable, destTable, expectedResults);
				
				if(destTable != null) {destTable.close();}
				if(srcTable != null) {srcTable.close();}

				assertTrue(result);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Sine in radians test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSineRad()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iAng = -180; iAng <= 180; iAng += 30)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				String name = iAng < 0 ? String.format("Name_Neg%d", iAng*-1) : String.format("Name_%d", iAng);
				row.add(CV_String.valueOf(name));
				double angDeg = iAng;
				double angRad = Math.toRadians(angDeg);
				CV_Integer intVal = CV_Integer.valueOf(angRad);
				CV_Decimal decVal = CV_Decimal.valueOf(angRad);
				row.add(TEST_COLS.ANG_DEG_INT.ordinal(), intVal);
				row.add(TEST_COLS.ANG_DEG_DEC.ordinal(), decVal);
				data.add(row);
			}

			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column names
			CV_String dataColumnName = CV_String.valueOf(TEST_COLS.ANG_DEG_DEC.name());

			// Expected results
			CV_Boolean isInputInDegrees = CV_Boolean.valueOf(false);
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				int iCol = TEST_COLS.ANG_DEG_DEC.ordinal();
				CV_Super<?> cell = row.get(iCol);
				CV_Decimal val = cell instanceof CV_Integer ? CV_Decimal.valueOf(((CV_Integer) cell).value()) : (CV_Decimal) cell;
				double valRad = val.value();
				double value = Math.sin(valRad);
				row.add(CV_Decimal.valueOf(value));
			}

			// Initialize functional
			CalculateSineOfColumns func = new CalculateSineOfColumns();
			func.srcTable = srcTable;
			func.dataColumnName = dataColumnName;
			func.isInputInDegrees = isInputInDegrees;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();

			// Run functional
			func.run();
			CV_Table destTable = func.destTable;

			// Validate results
			boolean result = TableUtils.compareTable(
					methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				CV_String name = CV_String.valueOf(String.format("Name_%d", iRow));
				row.add(name);
				CV_Point point = cvDataGenerator.genPoint();
				double angDeg = point.getX();
				double angRad = Math.toRadians(angDeg);
				CV_Integer intVal = CV_Integer.valueOf(angRad);
				CV_Decimal decVal = CV_Decimal.valueOf(angRad);
				row.add(TEST_COLS.ANG_DEG_INT.ordinal(), intVal);
				row.add(TEST_COLS.ANG_DEG_DEC.ordinal(), decVal);
				data.add(row);
			}

			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column names
			CV_String dataColumnName = CV_String.valueOf(TEST_COLS.ANG_DEG_DEC.name());

			// Units
			CV_Boolean isInputInDegrees = CV_Boolean.valueOf(false);

			try
			{
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = null;
				func.dataColumnName = dataColumnName;
				func.isInputInDegrees = isInputInDegrees;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = srcTable;
				func.dataColumnName = null;
				func.isInputInDegrees = isInputInDegrees;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Data column name list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = null;
				func.dataColumnName = null;
				func.isInputInDegrees = null;
				func.outputColumnName = null;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("All arguments are null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = srcTable;
				func.dataColumnName = CV_String.valueOf("BadColName");
				func.isInputInDegrees = isInputInDegrees;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Bad column names" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				CalculateSineOfColumns func = new CalculateSineOfColumns();
				func.srcTable = srcTable;
				func.dataColumnName = CV_String.valueOf(TEST_COLS.NAME.name());
				func.isInputInDegrees = isInputInDegrees;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Bad data type" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
}
