package jema.functional.core.cascade.arithmetic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.functional.api.ExecutionContextImpl;

public class CalculateSumOfColumnsTest 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(CalculateSumOfColumnsTest.class.getName());

	/** Test Data column prefix */
	private static final String COL_PREFIX = "tt_";
	
	/** Sum column name */
	private static final String OUTPUT_COL_NAME = "SUM_VAL";
	
	/** Summation constant */
	private static final Double CONSTANT_VALUE = 1000.0;

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Simple summation test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSimpleSum()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
					case string:
					case decimal:
					case length:
					case integer:
						String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
						columnHeaderList.add(new ColumnHeader(colName, parameterType));   
						break;
					default:
						break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			for(ColumnHeader columnHeader : columnHeaderList)
			{
				ParameterType parameterType = columnHeader.getType();
				switch(parameterType)
				{
					case decimal:
					case length:
					case integer:
						columnNameList.add(CV_String.valueOf(columnHeader.getName()));
						break;
					
					default:
						break;
				}
			}
			
			// Expected results
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				Double value = 0.0;
				for(CV_String colName : columnNameList)
				{
					int iCol = tableHeader.findIndex(colName.value());
					CV_Super cell = row.get(iCol);
					if(cell instanceof CV_Decimal) {value += ((CV_Decimal) cell).value();}
					else if(cell instanceof CV_Length) {value += ((CV_Length) cell).value();}
					else if(cell instanceof CV_Integer) {value += ((CV_Integer) cell).value();}
				}
				
				row.add(CV_Decimal.valueOf(value));
			}
			
			// Initialize functional
			CalculateSumOfColumns func = new CalculateSumOfColumns();
			func.srcTable = srcTable;
			func.columnNameList = columnNameList;
			func.constantValue = null;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = cvDataGenerator.compareTable(
				methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple summation test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSimpleSumWithConstant()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
					case string:
					case decimal:
					case length:
					case integer:
						String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
						columnHeaderList.add(new ColumnHeader(colName, parameterType));   
						break;
					default:
						break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			for(ColumnHeader columnHeader : columnHeaderList)
			{
				ParameterType parameterType = columnHeader.getType();
				switch(parameterType)
				{
					case decimal:
					case length:
					case integer:
						columnNameList.add(CV_String.valueOf(columnHeader.getName()));
						break;
					
					default:
						break;
				}
			}
			
			CV_Decimal constantValue = CV_Decimal.valueOf(CONSTANT_VALUE);
			
			// Expected results
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				Double value = 0.0;
				for(CV_String colName : columnNameList)
				{
					int iCol = tableHeader.findIndex(colName.value());
					CV_Super cell = row.get(iCol);
					if(cell instanceof CV_Decimal) {value += ((CV_Decimal) cell).value();}
					else if(cell instanceof CV_Length) {value += ((CV_Length) cell).value();}
					else if(cell instanceof CV_Integer) {value += ((CV_Integer) cell).value();}
				}
				
				if(constantValue != null) {value += constantValue.value();}
				
				row.add(CV_Decimal.valueOf(value));
			}
			
			// Initialize functional
			CalculateSumOfColumns func = new CalculateSumOfColumns();
			func.srcTable = srcTable;
			func.columnNameList = columnNameList;
			func.constantValue = constantValue;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = cvDataGenerator.compareTable(
				methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
				columnHeaderList.add(new ColumnHeader(colName, parameterType));   
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column name list
			List<CV_String> columnNameList = new ArrayList<CV_String>();
			for(ColumnHeader columnHeader : columnHeaderList)
			{
				ParameterType parameterType = columnHeader.getType();
				switch(parameterType)
				{
					case decimal:
					case length:
					case integer:
						columnNameList.add(CV_String.valueOf(columnHeader.getName()));
						break;
					
					default:
						break;
				}
			}
			
			CV_Decimal constantValue = CV_Decimal.valueOf(CONSTANT_VALUE);
			
			try
			{
				CalculateSumOfColumns func = new CalculateSumOfColumns();
				func.srcTable = null;
				func.columnNameList = columnNameList;
				func.constantValue = constantValue;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateSumOfColumns func = new CalculateSumOfColumns();
				func.srcTable = srcTable;
				func.columnNameList = null;
				func.constantValue = null;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column name list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateSumOfColumns func = new CalculateSumOfColumns();
				func.srcTable = srcTable;
				List<CV_String> invalidColumnNameList = new ArrayList<CV_String>(columnNameList);
				invalidColumnNameList.add(CV_String.valueOf("BadName"));
				func.columnNameList = invalidColumnNameList;
				func.constantValue = null;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column name list is invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateSumOfColumns func = new CalculateSumOfColumns();
				func.srcTable = null;
				func.columnNameList = null;
				func.constantValue = null;
				func.outputColumnName = null;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("All arguments are null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateSumOfColumns func = new CalculateSumOfColumns();
				func.srcTable = srcTable;
				func.columnNameList = columnNameList;
				func.constantValue = constantValue;
				CV_String dupColName = columnNameList.get(0);
				func.outputColumnName = dupColName;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Duplicate output column name" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param tableJava Destination table containing JAVA computed data
	 * @param tableSQL Destination table containing SQL computed data
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	public boolean compareResults(
		String methodName,
		List<CV_String> expectedResultList,
		List<CV_String> actualResultList) throws Exception
	{
		boolean result = (expectedResultList != null) && (actualResultList != null);

		try
		{
			if(result)
			{
				int actualListSize = actualResultList.size();
				int expectedListSize = expectedResultList.size();
				result = (actualListSize == expectedListSize);

				for(int iExpected = 0; iExpected < expectedListSize && result; iExpected++)
				{
					int iActual = iExpected;
					CV_String expectedResult = expectedResultList.get(iExpected);
					CV_String actualResult = expectedResultList.get(iActual);
					if(expectedResult == null && actualResult == null) {result = true;}
					else if(expectedResult == null || actualResult == null) {result = false;}
					else {result = expectedResult.equals(actualResult);}			
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s - List compare failed: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}

		return result;
	}
}
