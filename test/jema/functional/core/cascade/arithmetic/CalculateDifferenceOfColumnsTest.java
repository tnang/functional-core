package jema.functional.core.cascade.arithmetic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.functional.api.ExecutionContextImpl;

public class CalculateDifferenceOfColumnsTest 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(CalculateDifferenceOfColumnsTest.class.getName());

	/** Test Data column prefix */
	private static final String COL_PREFIX = "tt_";
	
	/** Difference column name */
	private static final String OUTPUT_COL_NAME = "DIFF_VAL";

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Difference test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testDifference()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
					case string:
						String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
						columnHeaderList.add(new ColumnHeader(colName, parameterType));   
						break;
					case decimal:
					case length:
					case integer:
						for(int iCol = 0; iCol < 2; iCol++)
						{
							colName = String.format("%s%s_%d", COL_PREFIX, parameterType.name(), iCol);
							columnHeaderList.add(new ColumnHeader(colName, parameterType));   
						}
						break;
					default:
						break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column name list
			CV_String firstOperandColumn = this.selectColumnName(tableHeader);
			CV_String secondOperandColumn = this.selectColumnName(tableHeader);
			
			// Expected results
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				Double value = 0.0;
				int iCellFirst = tableHeader.findIndex(firstOperandColumn.value());
				CV_Super cellFirst = row.get(iCellFirst);
				if(cellFirst instanceof CV_Decimal) {value = ((CV_Decimal) cellFirst).value();}
				else if(cellFirst instanceof CV_Length) {value = ((CV_Length) cellFirst).value();}
				else if(cellFirst instanceof CV_Integer) {value = (double) ((CV_Integer) cellFirst).value();}
				
				int iCellSecond = tableHeader.findIndex(secondOperandColumn.value());
				CV_Super cellSecond = row.get(iCellSecond);
				if(cellSecond instanceof CV_Decimal) {value -= ((CV_Decimal) cellSecond).value();}
				else if(cellSecond instanceof CV_Length) {value -= ((CV_Length) cellSecond).value();}
				else if(cellSecond instanceof CV_Integer) {value -= (double) ((CV_Integer) cellSecond).value();}
				
				row.add(CV_Decimal.valueOf(value));
			}
			
			// Initialize functional
			CalculateDifferenceOfColumns func = new CalculateDifferenceOfColumns();
			func.srcTable = srcTable;
			func.firstOperandColumn = firstOperandColumn;
			func.secondOperandColumn = secondOperandColumn;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = cvDataGenerator.compareTable(
				methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
				columnHeaderList.add(new ColumnHeader(colName, parameterType));   
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column name list
			CV_String firstOperandColumn = this.selectColumnName(tableHeader);
			CV_String secondOperandColumn = this.selectColumnName(tableHeader);
			
			try
			{
				CalculateDifferenceOfColumns func = new CalculateDifferenceOfColumns();
				func.srcTable = null;
				func.firstOperandColumn = firstOperandColumn;
				func.secondOperandColumn = secondOperandColumn;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateDifferenceOfColumns func = new CalculateDifferenceOfColumns();
				func.srcTable = srcTable;
				func.firstOperandColumn = null;
				func.secondOperandColumn = secondOperandColumn;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("First column name is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateDifferenceOfColumns func = new CalculateDifferenceOfColumns();
				func.srcTable = srcTable;
				func.firstOperandColumn = firstOperandColumn;
				func.secondOperandColumn = null;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Second column name is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateDifferenceOfColumns func = new CalculateDifferenceOfColumns();
				func.srcTable = srcTable;
				func.firstOperandColumn = CV_String.valueOf("BadFirstOperandColumn");
				func.secondOperandColumn = CV_String.valueOf("BadSecondOperandColumn");;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column names are invalid" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}
	
	/**
	 * Select numeric column name from table header.
	 * @param tableHeader Table header
	 * @return Numeric column header
	 */
	private CV_String selectColumnName(TableHeader tableHeader)
	{
		CV_String colName = null;
		Random r = new Random();
		List<String> colNameList = new ArrayList<String>();
		for(ColumnHeader colHeader : tableHeader)
		{
			switch(colHeader.getType())
			{
				case decimal:
				case length:
				case integer:
					colNameList.add(colHeader.getName());
					break;
				default:
					break;
			}
			
		}

		int iCol = r.nextInt(colNameList.size());
		colName = CV_String.valueOf(colNameList.get(iCol));
		
		return colName;
	}
}
