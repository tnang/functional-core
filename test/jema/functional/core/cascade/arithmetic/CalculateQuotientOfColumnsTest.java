package jema.functional.core.cascade.arithmetic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.functional.api.ExecutionContextImpl;

public class CalculateQuotientOfColumnsTest 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(CalculateQuotientOfColumnsTest.class.getName());
	
	/** Quotient column name */
	private static final String OUTPUT_COL_NAME = "QUOT_VAL";

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();
	
	/** Test data header column names */
	private enum TEST_COLS
	{
		NAME(ParameterType.string),
		NUMERATOR(ParameterType.decimal),
		DENOMINATOR(ParameterType.decimal);
		
		ParameterType parameterType;
		
		private TEST_COLS(ParameterType parameterType)
		{
			this.parameterType = parameterType;
		}
		
		public ParameterType getParameterType() {return parameterType;}
	}

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Simple quotient test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSimpleQuotient()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				CV_String name = CV_String.valueOf(String.format("Name_%d", iRow));
				row.add(name);
				CV_Decimal numerator = CV_Decimal.valueOf((iRow+1) * 22.2);
				CV_Decimal denominator = CV_Decimal.valueOf((iRow+1) * 1234.2);
				row.add(TEST_COLS.NUMERATOR.ordinal(), numerator);
				row.add(TEST_COLS.DENOMINATOR.ordinal(), denominator);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column names
			CV_String denominatorColumnName = CV_String.valueOf(TEST_COLS.DENOMINATOR.name());
			CV_String numeratorColumnName = CV_String.valueOf(TEST_COLS.NUMERATOR.name());
			
			// Expected results
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				int iColNum = TEST_COLS.NUMERATOR.ordinal();
				int iColDenom = TEST_COLS.DENOMINATOR.ordinal();
				CV_Super<?> cellNum = row.get(iColNum);
				CV_Super<?> cellDenom = row.get(iColDenom);
				Double valNum = ((CV_Decimal) cellNum).value();
				Double valDenom = ((CV_Decimal) cellDenom).value();
				Double value = valDenom == 0 ? null : valNum / valDenom;
				if(value == null) {row.add(null);}
				else {row.add(CV_Decimal.valueOf(value));}
			}
			
			// Initialize functional
			CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
			func.srcTable = srcTable;
			func.denominatorColumnName = denominatorColumnName;
			func.numeratorColumnName = numeratorColumnName;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = this.compareTable(
				methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Simple quotient test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSimpleQuotientWithZero()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				CV_String name = CV_String.valueOf(String.format("Name_%d", iRow));
				row.add(name);
				CV_Decimal numerator = cvDataGenerator.genDecimal();
				CV_Decimal denominator = 
					(iRow % 3) == 0 ? 
					CV_Decimal.valueOf(0.0) : 
					cvDataGenerator.genDecimal();
				row.add(TEST_COLS.NUMERATOR.ordinal(), numerator);
				row.add(TEST_COLS.DENOMINATOR.ordinal(), denominator);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column names
			CV_String denominatorColumnName = CV_String.valueOf(TEST_COLS.DENOMINATOR.name());
			CV_String numeratorColumnName = CV_String.valueOf(TEST_COLS.NUMERATOR.name());
			
			// Expected results
			List<List<CV_Super>> expectedResults = new ArrayList<List<CV_Super>>(data);
			for(List<CV_Super> row : expectedResults)
			{
				int iColNum = TEST_COLS.NUMERATOR.ordinal();
				int iColDenom = TEST_COLS.DENOMINATOR.ordinal();
				CV_Super<?> cellNum = row.get(iColNum);
				CV_Super<?> cellDenom = row.get(iColDenom);
				Double valNum = ((CV_Decimal) cellNum).value();
				Double valDenom = ((CV_Decimal) cellDenom).value();
				Double value = valDenom == 0 ? null : valNum / valDenom;
				if(value == null) {row.add(null);}
				else {row.add(CV_Decimal.valueOf(value));}
			}
			
			// Initialize functional
			CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
			func.srcTable = srcTable;
			func.denominatorColumnName = denominatorColumnName;
			func.numeratorColumnName = numeratorColumnName;
			func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = this.compareTable(
				methodName, uriTestTable, destTable, expectedResults);
			
			if(destTable != null) {destTable.close();}
			if(srcTable != null) {srcTable.close();}
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Test data
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(TEST_COLS testCol : TEST_COLS.values())
			{
				columnHeaderList.add(new ColumnHeader(testCol.name(), testCol.getParameterType()));
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 50;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
			
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Column names
			CV_String denominatorColumnName = CV_String.valueOf(TEST_COLS.DENOMINATOR.name());
			CV_String numeratorColumnName = CV_String.valueOf(TEST_COLS.NUMERATOR.name());
			
			try
			{
				CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
				func.srcTable = null;
				func.denominatorColumnName = denominatorColumnName;
				func.numeratorColumnName = numeratorColumnName;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
				func.srcTable = srcTable;
				func.denominatorColumnName = null;
				func.numeratorColumnName = numeratorColumnName;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Denominator column name list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
				func.srcTable = srcTable;
				func.denominatorColumnName = denominatorColumnName;
				func.numeratorColumnName = null;
				func.outputColumnName = CV_String.valueOf(OUTPUT_COL_NAME);
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Numerator column name list is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
				func.srcTable = null;
				func.denominatorColumnName = null;
				func.numeratorColumnName = null;
				func.outputColumnName = null;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("All arguments are null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				CalculateQuotientOfColumns func = new CalculateQuotientOfColumns();
				func.srcTable = srcTable;
				func.denominatorColumnName = CV_String.valueOf("BadDenomColName");
				func.numeratorColumnName = CV_String.valueOf("BadNumColName");
				func.outputColumnName = CV_String.valueOf(TEST_COLS.DENOMINATOR.name());
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Bad column names" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param uri Table URI
	 * @param destTable Destination table containing computed data
	 * @param testData Test comparison data
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	@SuppressWarnings("rawtypes")
	public boolean compareTable(
			String methodName,
			URI uri,
			CV_Table destTable,
			List<List<CV_Super>> testData) throws Exception
	{
		boolean result = (destTable != null);

		try
		{
			if((destTable != null) && (testData != null))
			{
				int testTableRowSize = testData.size();
				int destTableRowSize = destTable.size();
				result = (testTableRowSize == destTableRowSize);

				for(int iRow = 0; iRow < destTableRowSize && result; iRow++)
				{
					List<CV_Super> testRow = testData.get(iRow);
					List<CV_Super> destRow = destTable.readRow();

					int testTableColSize = testRow.size();
					int destTableColSize = destRow.size();
					result = (testTableColSize == destTableColSize);

					for(int iCol = 0; iCol < destTableColSize && result; iCol++)
					{
						CV_Super<?> testCell = testRow.get(iCol);
						CV_Super<?> destCell = destRow.get(iCol);
						if(testCell == null && destCell == null) {result = true;}
						else if(testCell == null || destCell == null) {result = false;}
						else {result = cvDataGenerator.equalColumnAlt(testCell, destCell);}
						
						if(!result)
						{
							String msg = String.format("%s (%s) - Table compare failed: testVal=%s, destVal=%s", methodName, uri.toString(), testRow.get(iCol), destRow.get(iCol));
							LOGGER.log(Level.WARNING, msg);
						}
					}  
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s (%s) - Table compare failed: %s", methodName, uri.toString(), e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}

		return result;
	}
}
