/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.statistics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataDuration;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.common.types.util.CommonVocabDatabaseProperties;
import jema.common.types.util.CommonVocabDatabasePropertiesFactory;
import jema.functional.api.ExecutionContextImpl;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for SummaryStatistics functional.
 * <p>
 * <b>Notes:</b>
 * <ul>
 * </ul>
 * 
 * @author CASCADE
 */
public class SummaryStatisticsTest
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(SummaryStatisticsTest.class.getName());

	/** Test Data column prefix */
	private static final String COL_PREFIX = "tt_";

	/** Test duration to provide performance results */
	private static final CommonVocabDataDuration cvDataDuration = new CommonVocabDataDuration();

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();
	
	/** Enumerated list of test column names */
	protected enum LonLatColumnHeader
	{
		NAME(ParameterType.string),
		ID(ParameterType.string),
		REGION(ParameterType.string),
		LON(ParameterType.decimal),
		LAT(ParameterType.decimal);
		
		ColumnHeader columnHeader;
		
		/**
		 * Constructor
		 * @param parameterType Parameter type associated with column name
		 */
		private LonLatColumnHeader(ParameterType parameterType)
		{
			this.columnHeader = new ColumnHeader(this.name(), parameterType);
		}
		
		/**
		 * Accessor to retrieve the column header.
		 * @return Column header
		 * @return
		 */
		public ColumnHeader getColumnHeader(){return this.columnHeader;}
	}

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Compare Summary Statistics vs Summary Statistics SQL
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testCompareFunc1()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{		
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
				case bool:
				case box: 
				case circle: 
				case decimal: 
				case duration: 
				case ellipse: 
//				case geometryCollection: 
				case integer: 
				case length: 
				case lineString: 
				case multiLineString: 
				case multiPoint: 
				case multiPolygon: 
				case point: 
				case polygon: 
				case string: 
				case temporalRange: 
				case timestamp: 
					String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
					columnHeaderList.add(new ColumnHeader(colName, parameterType));   
					break;

				default:
					break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 500;
			List<List<CV_Super>> data = this.genData(numRows, tableHeader);

			// Create SQL Source table
			CV_Table srcTableSummaryStatSQL = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Create JAVA Source table
			CV_Table srcTableSummaryStat = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);

			// JAVA Summary Statistics
			SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
					srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStat.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
			if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}	

			// SQL Summary Statistics
			SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
					srcTableSummaryStatSQL, null, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStatSQL.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
			if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}

			// Compare Results
			boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);

			destTableSummaryStatSQL.close();
			destTableSummaryStat.close();
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare Summary Statistics vs Summary Statistics SQL
	 */
	@SuppressWarnings("rawtypes")
	//@Test
	public void testCompareFunc2()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{		
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
				case bool:
//				case box: 
//				case circle: 
				case decimal: 
//				case duration: 
				case ellipse: 
//				case geometryCollection: 
				case integer: 
				case length: 
//				case lineString: 
//				case multiLineString: 
//				case multiPoint: 
//				case multiPolygon: 
//				case point: 
//				case polygon: 
//				case string: 
//				case temporalRange: 
//				case timestamp: 
					String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
					columnHeaderList.add(new ColumnHeader(colName, parameterType));   
					break;

				default:
					break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 5000;
			List<List<CV_Super>> data = this.genData(numRows, tableHeader);

			// Create SQL Source table
			CV_Table srcTableSummaryStatSQL = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Create JAVA Source table
			CV_Table srcTableSummaryStat = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);

			// JAVA Summary Statistics
			SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
					srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStat.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
			if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}	

			// SQL Summary Statistics
			SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
					srcTableSummaryStatSQL, null, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStatSQL.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
			if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}

			// Compare Results
			boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);

			destTableSummaryStatSQL.close();
			destTableSummaryStat.close();
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare Summary Statistics vs Summary Statistics SQL
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testCompareFunc3()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{		
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				switch(parameterType)
				{
//				case bool:
//				case box: 
//				case circle: 
				case decimal: 
//				case duration: 
//				case ellipse: 
////				case geometryCollection: 
//				case integer: 
//				case length: 
//				case lineString: 
//				case multiLineString: 
//				case multiPoint: 
//				case multiPolygon: 
//				case point: 
//				case polygon: 
//				case string: 
//				case temporalRange: 
//				case timestamp: 
					String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
					columnHeaderList.add(new ColumnHeader(colName, parameterType));   
					break;

				default:
					break;
				}
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 1000;
			List<List<CV_Super>> data = this.genData(numRows, tableHeader);

			// Create SQL Source table
			CV_Table srcTableSummaryStatSQL = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Create JAVA Source table
			CV_Table srcTableSummaryStat = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			
			for(SummaryStatisticsBase.StatCalcType statCalcType : SummaryStatisticsBase.StatCalcType.values())
			{
				doCalcList.clear();
				doCalcList.add(statCalcType);
	
				// JAVA Summary Statistics
				SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
						srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);
	
				String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
				cvDataDuration.start(label);
				funcSummaryStat.run();
				cvDataDuration.stop(data.size(), null);
				CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
				if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}	
	
				// SQL Summary Statistics
				SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
						srcTableSummaryStatSQL, null, discriminatorColumnsList, statisticsColumnsList, doCalcList);
	
				label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
				cvDataDuration.start(label);
				funcSummaryStatSQL.run();
				cvDataDuration.stop(data.size(), null);
				CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
				if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}
	
				// Compare Results
				boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);
	
				destTableSummaryStatSQL.close();
				destTableSummaryStat.close();
	
				assertTrue(result);
			}
			
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Run with default statisticsColumnsList
	 */
	@SuppressWarnings("rawtypes")
	//@Test
	public void testDefaultStatisticsColumnsList()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			// Table columns
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(LonLatColumnHeader testColumnName : LonLatColumnHeader.values())
			{
				columnHeaderList.add(testColumnName.getColumnHeader());
			}

			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Sample data 
			int numRows = 50;
			List<List<CV_Super>> data = this.genData(methodName, numRows);

			// Create SQL Source table
			CommonVocabDatabaseProperties dbProperties = CommonVocabDatabasePropertiesFactory.createDefaultInstance();
			CV_Table srcTableSummaryStatSQL = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStatSQL.setHeader(tableHeader);

			// Create JAVA source table
			CV_Table srcTableSummaryStat = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStat.setHeader(tableHeader);
			
			// Populate Java and SQL Source table 
			for(List<CV_Super> row : data)
			{
				srcTableSummaryStatSQL.appendRow(row);
				srcTableSummaryStat.appendRow(row);
			}

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();
			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.REGION.getColumnHeader().getName()));
			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.ID.getColumnHeader().getName()));
			
			this.printSortedData(methodName, data, tableHeader, discriminatorColumnsList);

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = null;

			// WHERE Clause
			String discriminatorClause = null;

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);

			// JAVA Summary Statistics
			SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
					srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStat.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
			if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}			

			// SQL Summary Statistics
			SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
					srcTableSummaryStatSQL, discriminatorClause, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStatSQL.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
			if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}

			// Compare Results
			boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);

			destTableSummaryStatSQL.close();
			destTableSummaryStat.close();
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Test WHERE clause
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testCompareFunc_WHERE()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			// Table columns
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			columnHeaderList.add(new ColumnHeader("COL_DEC", ParameterType.decimal));
			//			columnHeaderList.add(new ColumnHeader("COL_LEN", ParameterType.length));
			columnHeaderList.add(new ColumnHeader("COL_INT", ParameterType.integer));

			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create SQL Source table
			CommonVocabDatabaseProperties dbProperties = CommonVocabDatabasePropertiesFactory.createDefaultInstance();
			CV_Table srcTableSummaryStatSQL = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStatSQL.setHeader(tableHeader);

			// Create JAVA source table
			CV_Table srcTableSummaryStat = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStat.setHeader(tableHeader);

			// Sample data
			Long discrimValue = 3L;
			List<Long> data = new ArrayList<>(Arrays.asList(
					1L, 2L, 3L, 4L, 5L, 5L, 5L, 4L, 3L, 2L, 1L, 0L, 1000L));

			// Populate SQL Source table 
			StringBuilder sb = new StringBuilder();
			for(int iCell = 0; iCell < data.size(); iCell++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				Long value = data.get(iCell);
				sb.append(",").append(value);
				for(ColumnHeader colHeader : columnHeaderList)
				{
					if(colHeader.getType().equals(ParameterType.decimal)){row.add(CV_Decimal.valueOf(value));}
					else if(colHeader.getType().equals(ParameterType.length)){row.add(new CV_Length(value.doubleValue()));}
					else if(colHeader.getType().equals(ParameterType.integer)){row.add(CV_Integer.valueOf(value));}
				}
				if(!row.isEmpty()){srcTableSummaryStatSQL.appendRow(row);}
			}
			LOGGER.log(Level.FINE, String.format("%s_SQL: %s", methodName, sb.substring(1)));

			// Populate JAVA Source table 
			for(int iCell = 0; iCell < data.size(); iCell++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				Long value = data.get(iCell);
				for(ColumnHeader colHeader : columnHeaderList)
				{
					if(colHeader.getType().equals(ParameterType.decimal)){row.add(CV_Decimal.valueOf(value));}
					else if(colHeader.getType().equals(ParameterType.length)){row.add(new CV_Length(value.doubleValue()));}
					else if(colHeader.getType().equals(ParameterType.integer)){row.add(CV_Integer.valueOf(value));}
				}
				if((value > discrimValue) && !row.isEmpty()){srcTableSummaryStat.appendRow(row);}
			}

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			// WHERE Clause
			String discriminatorClause = String.format("%s > %d", columnHeaderList.get(0).getName(), discrimValue);

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);

			// JAVA Summary Statistics
			SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
					srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStat.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
			if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}			

			// SQL Summary Statistics
			SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
					srcTableSummaryStatSQL, discriminatorClause, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStatSQL.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
			if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}

			// Compare Results
			boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);

			destTableSummaryStatSQL.close();
			destTableSummaryStat.close();
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();

			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Test discriminator list
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testCompareFunc_Discrim_AllCalc()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			// Table columns
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(LonLatColumnHeader testColumnName : LonLatColumnHeader.values())
			{
				columnHeaderList.add(testColumnName.getColumnHeader());
			}

			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Sample data 
			int numRows = 100;
			List<List<CV_Super>> data = this.genData(methodName, numRows);

			// Create SQL Source table
			CommonVocabDatabaseProperties dbProperties = CommonVocabDatabasePropertiesFactory.createDefaultInstance();
			CV_Table srcTableSummaryStatSQL = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStatSQL.setHeader(tableHeader);

			// Create JAVA source table
			CV_Table srcTableSummaryStat = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStat.setHeader(tableHeader);
			
			// Populate Java and SQL Source table 
			for(List<CV_Super> row : data)
			{
				srcTableSummaryStatSQL.appendRow(row);
				srcTableSummaryStat.appendRow(row);
			}

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();
			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.REGION.getColumnHeader().getName()));
			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.ID.getColumnHeader().getName()));
			
			this.printSortedData(methodName, data, tableHeader, discriminatorColumnsList);

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			// WHERE Clause
			String discriminatorClause = null;

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);
//			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
//			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);

			// JAVA Summary Statistics
			SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
					srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStat.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
			if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}			

			// SQL Summary Statistics
			SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
					srcTableSummaryStatSQL, discriminatorClause, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
			cvDataDuration.start(label);
			funcSummaryStatSQL.run();
			cvDataDuration.stop(data.size(), null);
			CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
			if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}

			// Compare Results
			boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);

			destTableSummaryStatSQL.close();
			destTableSummaryStat.close();
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();
//			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Test discriminator list
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testCompareFunc_Discrim_EachCalc()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{
			// Table columns
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(LonLatColumnHeader testColumnName : LonLatColumnHeader.values())
			{
				columnHeaderList.add(testColumnName.getColumnHeader());
			}

			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Sample data 
			int numRows = 50;
			List<List<CV_Super>> data = this.genData(methodName, numRows);

			// Create SQL Source table
			CommonVocabDatabaseProperties dbProperties = CommonVocabDatabasePropertiesFactory.createDefaultInstance();
			CV_Table srcTableSummaryStatSQL = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStatSQL.setHeader(tableHeader);

			// Create JAVA source table
			CV_Table srcTableSummaryStat = new CV_Table(uriTestTable, dbProperties);
			srcTableSummaryStat.setHeader(tableHeader);
			
			// Populate Java and SQL Source table 
			for(List<CV_Super> row : data)
			{
				srcTableSummaryStatSQL.appendRow(row);
				srcTableSummaryStat.appendRow(row);
			}

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();
			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.REGION.getColumnHeader().getName()));
//			discriminatorColumnsList.add(CV_String.valueOf(LonLatColumnHeader.ID.getColumnHeader().getName()));
			
			this.printSortedData(methodName, data, tableHeader, discriminatorColumnsList);

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			// WHERE Clause
			String discriminatorClause = null;

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			for(SummaryStatisticsBase.StatCalcType statCalcType : SummaryStatisticsBase.StatCalcType.values())
			{
				doCalcList.clear();
				doCalcList.add(statCalcType);
	
				// JAVA Summary Statistics
				SummaryStatistics funcSummaryStat = this.makeSummaryStatistics(
						srcTableSummaryStat, discriminatorColumnsList, statisticsColumnsList, doCalcList);
	
				String label = String.format("%s_%s", methodName, funcSummaryStat.getClass().getSimpleName());
				cvDataDuration.start(label);
				funcSummaryStat.run();
				cvDataDuration.stop(data.size(), null);
				CV_Table destTableSummaryStat = funcSummaryStat.destTable;			
//				if(destTableSummaryStat != null){SummaryStatisticsTest.printResults(destTableSummaryStat, label + " Results");}			
	
				// SQL Summary Statistics
				SummaryStatisticsSQL funcSummaryStatSQL = this.makeSummaryStatisticsSQL(
						srcTableSummaryStatSQL, discriminatorClause, discriminatorColumnsList, statisticsColumnsList, doCalcList);
	
				label = String.format("%s_%s", methodName, funcSummaryStatSQL.getClass().getSimpleName());
				cvDataDuration.start(label);
				funcSummaryStatSQL.run();
				cvDataDuration.stop(data.size(), null);
				CV_Table destTableSummaryStatSQL = funcSummaryStatSQL.destTable;			
//				if(destTableSummaryStatSQL != null){SummaryStatisticsTest.printResults(destTableSummaryStatSQL, label + " Results");}
	
				// Compare Results
				boolean result = this.compareTables(methodName, destTableSummaryStat, destTableSummaryStatSQL, doCalcList, discriminatorColumnsList);
	
				destTableSummaryStatSQL.close();
				destTableSummaryStat.close();
//				assertTrue(result);
			}
			
			srcTableSummaryStatSQL.close();
			srcTableSummaryStat.close();
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Test Summary Statistics Errors
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSummaryStatisticsError()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		CV_Table srcTable = null;

		try 
		{
			int numRows = 20;

			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
				columnHeaderList.add(new ColumnHeader(colName, parameterType));   
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Test Data
			List<List<CV_Super>> data = this.genData(numRows, tableHeader);

			// Create Table
			srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);

			SummaryStatistics func = this.makeSummaryStatistics(
					srcTable, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			try
			{
				func.srcTable = null;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = statisticsColumnsList;
				func.run();
				assertTrue(String.format("%s: srcTable is null\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				func.srcTable = srcTable;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = new ArrayList<CV_String>();;
				func.run();
				assertTrue(String.format("%s: statisticsColumnsList is empty\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				List<CV_String> statisticsColumnsListErr = new ArrayList<CV_String>(statisticsColumnsList);
				statisticsColumnsListErr.add(CV_String.valueOf("BadColumnName"));
				func.srcTable = srcTable;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = statisticsColumnsListErr;
				func.run();
				assertTrue(String.format("%s: statisticsColumnsList contains invalid column name\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s (%s)", methodName, srcTable);
			LOGGER.log(Level.FINE, msg, e);
			assertTrue(methodName, true);
		}
	}

	/**
	 * Test Summary Statistics Errors
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testSummaryStatisticsSQLError()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		CV_Table srcTable = null;

		try 
		{
			int numRows = 20;

			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			for(ParameterType parameterType : ParameterType.values())
			{
				String colName = String.format("%s%s", COL_PREFIX, parameterType.name());
				columnHeaderList.add(new ColumnHeader(colName, parameterType));   
			}
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Test Data
			List<List<CV_Super>> data = this.genData(numRows, tableHeader);

			// Create Table
			srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);

			// Discriminator Columns lists
			List<CV_String> discriminatorColumnsList = new ArrayList<CV_String>();

			// Statistics Columns lists
			List<CV_String> statisticsColumnsList = new ArrayList<CV_String>();
			for(ColumnHeader colHeader : tableHeader.asList())
			{
				statisticsColumnsList.add(CV_String.valueOf(colHeader.getName()));
			}

			List<SummaryStatisticsBase.StatCalcType> doCalcList = new ArrayList<SummaryStatisticsBase.StatCalcType>();
			doCalcList.add(SummaryStatisticsBase.StatCalcType.COUNT);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.FIRST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.LAST);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAD);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MAX);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MEDIAN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MIN);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.MODE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.RANGE);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SDEV);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.SUM);
			doCalcList.add(SummaryStatisticsBase.StatCalcType.UNIQUE);

			SummaryStatisticsSQL func = this.makeSummaryStatisticsSQL(
					srcTable, null, discriminatorColumnsList, statisticsColumnsList, doCalcList);

			try
			{
				func.srcTable = null;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = statisticsColumnsList;
				func.run();
				assertTrue(String.format("%s: srcTable is null\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				func.srcTable = srcTable;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = new ArrayList<CV_String>();;
				func.run();
				assertTrue(String.format("%s: statisticsColumnsList is empty\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			try
			{
				List<CV_String> statisticsColumnsListErr = new ArrayList<CV_String>(statisticsColumnsList);
				statisticsColumnsListErr.add(CV_String.valueOf("BadColumnName"));
				func.srcTable = srcTable;
				func.discriminatorColumnsList = discriminatorColumnsList;
				func.statisticsColumnsList = statisticsColumnsListErr;
				func.run();
				assertTrue(String.format("%s: statisticsColumnsList contains invalid column name\n%s", methodName, func.toString()), false);
			}
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}

			if(srcTable != null) {srcTable.close();}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s (%s)", methodName, srcTable);
			LOGGER.log(Level.FINE, msg, e);
			assertTrue(methodName, true);
		}
	}

	/**
	 * Generate source data list.
	 * 
	 * @param methodName Test case name
	 * @param numRows Number of data rows
	 * @return source data
	 */
	@SuppressWarnings("rawtypes")
	private List<List<CV_Super>> genData(String methodName, int numRows)
	{
		List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();

		try 
		{
			Random r = new Random();
			
			// US boundaries (-124.848974, 24.396308), (-66.885444, 49.384358)
			double minLon = -124.848974;
			double maxLon = -66.885444;
			double minLat = 24.396308;
			double maxLat = 49.384358;

			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				row.add(CV_String.valueOf(String.format("%s_%d", LonLatColumnHeader.REGION.getColumnHeader().getName(), iRow)));
				row.add(CV_String.valueOf(String.format("%s_%d", LonLatColumnHeader.ID.getColumnHeader().getName(), iRow%2)));
				double lat = minLat + (r.nextDouble() * (maxLat - minLat));
				double lon = minLon + (r.nextDouble() * (maxLon - minLon));
				StringBuilder region = new StringBuilder();
				if(lat < minLat + (maxLat - minLat) / 2) {region.append("S");}
				else {region.append("N");}
				if(lon < minLon + (maxLon - minLon) / 2) {region.append("W");}
				else {region.append("E");}
				row.add(CV_String.valueOf(region.toString()));
				row.add(CV_Decimal.valueOf(lon));
				row.add(CV_Decimal.valueOf(lat));
				data.add(row);
			}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.FINE, msg);
			throw e;
		}
		
		return data;
	}

	/**
	 * Generate source table.
	 * 
	 * @param methodName The test method name
	 * @return source table
	 */
	@SuppressWarnings("rawtypes")
	private List<List<CV_Super>> genData(int numRows, TableHeader tableHeader)
	{
		List<List<CV_Super>> data = null;

		try 
		{
			// Test Data
			data = new ArrayList<List<CV_Super>>();

			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = cvDataGenerator.genRow(tableHeader);
				data.add(row);
			}
		} 
		catch (Exception e) 
		{
			throw e;
		}

		return data;
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param tableJava Destination table containing JAVA computed data
	 * @param tableSQL Destination table containing SQL computed data
	 * @param doCalcList List of statistical calculation types
	 * @param discriminatorColumnsList Discriminator column list
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	@SuppressWarnings({ "rawtypes" })
	public boolean compareTables(
			String methodName,
			CV_Table tableJava,
			CV_Table tableSQL,
			List<SummaryStatisticsBase.StatCalcType> doCalcList,
			List<CV_String> discriminatorColumnsList) throws Exception
	{
		boolean result = (tableJava != null) && (tableSQL != null);
		ColumnHeader columnHeaderJavaFail = null;
		ColumnHeader columnHeaderSQLFail = null;
		CV_Super<?> cellJavaFail = null;
		CV_Super<?> cellSQLFail = null;
		List<ColumnHeader> columnHeaderGroupFailList = new ArrayList<ColumnHeader>();
		List<CV_Super<?>> cellGroupFailList = new ArrayList<CV_Super<?>>();
		List<ColumnHeader> columnHeaderTempList = new ArrayList<ColumnHeader>();
		List<CV_Super<?>> cellTempList = new ArrayList<CV_Super<?>>();

		try
		{
			if(result)
			{
				int tableSizeJava = tableJava.size();
				int tableSizeSQL = tableSQL.size();
				result = (tableSizeJava == tableSizeSQL);

				if(result)
				{	
					CV_Table readableTableJava = tableJava.toReadable();
					CV_Table readableTableSQL = tableSQL.toReadable();
					List<List<CV_Super>> tableSQLData = new ArrayList<List<CV_Super>>();
					List<CV_Super> row = null;
					
					while(null != (row = readableTableSQL.readRow()))
					{
						tableSQLData.add(row);
					}
					
					for(int iRow = 0; iRow < tableSizeJava && result; iRow++)
					{
						columnHeaderGroupFailList.clear();
						cellGroupFailList.clear();	
						
						List<CV_Super> rowJava = readableTableJava.readRow();
						for(List<CV_Super> rowSQL : tableSQLData)
						{	
							result = true;
							int rowSizeSQL = (rowSQL == null) ? -1 : rowSQL.size();
							columnHeaderTempList.clear();
							cellTempList.clear();
							
							for(int iCellSQL = 0; iCellSQL < rowSizeSQL && result; iCellSQL++)
							{
								ColumnHeader columnHeaderSQL = tableSQL.getHeader().getHeader(iCellSQL);
								ParameterType parameterTypeSQL = columnHeaderSQL.getType();

								int iCellJava = tableJava.getHeader().findIndex(columnHeaderSQL.getName());
								ColumnHeader columnHeaderJava = (iCellJava == -1) ? null : tableJava.getHeader().getHeader(iCellJava);
								ParameterType parameterTypeJava = (columnHeaderJava == null) ? null : columnHeaderJava.getType();

								CV_Super<?> cellJava = (iCellJava == -1) ? null : rowJava.get(iCellJava);
								CV_Super<?> cellSQL = (iCellSQL == -1) ? null : rowSQL.get(iCellSQL);
								
								if(iCellSQL < discriminatorColumnsList.size())
								{
									columnHeaderTempList.add(columnHeaderJava);
									cellTempList.add(cellJava);									
								}
								else
								{
									columnHeaderJavaFail = columnHeaderJava;
									cellJavaFail = cellJava;
									columnHeaderSQLFail = columnHeaderSQL;
									cellSQLFail = cellSQL;
									if(cellGroupFailList.isEmpty()){cellGroupFailList.addAll(cellTempList);}
									if(columnHeaderGroupFailList.isEmpty()){columnHeaderGroupFailList.addAll(columnHeaderTempList);}
								}
								
								if((cellSQL == null) && (cellJava == null)) {result = true;}
								else if((cellSQL == null) || (cellJava == null))
								{
									result = true;
								}
								else if(parameterTypeSQL.equals(parameterTypeJava))
								{
									switch(parameterTypeSQL)
									{
									case decimal:
									case length:
										BigDecimal bigValueJava = BigDecimal.valueOf((Double) cellJava.value());
										bigValueJava = bigValueJava.round(MathContext.DECIMAL64);
										BigDecimal bigValueSQL = BigDecimal.valueOf((Double) cellSQL.value());
										bigValueSQL = bigValueJava.round(MathContext.DECIMAL64);
										result = bigValueJava.equals(bigValueSQL);
										break;
									default:
										result = cellJava.equals(cellSQL);
										break;
									}
								}
								else
								{
									CV_Super valueJava = null;
									CV_Super valueSQL = null;

									switch(parameterTypeJava)
									{
									case decimal:
									case length:
										valueJava = CV_Integer.valueOf((Double) cellJava.value());
										break;
									default:
										valueJava = cellJava;
										break;
									}

									switch(parameterTypeSQL)
									{
									case decimal:
									case length:
										valueSQL = CV_Integer.valueOf((Double) cellSQL.value());
										break;
									default:
										valueSQL = cellSQL;
										break;
									}

									CV_Super cell = null;
									if(valueJava.getClass() == valueSQL.getClass())
									{
										cell = valueSQL;
									}
									else
									{
										cell = cvDataGenerator.genSuper(columnHeaderJava.getType(), valueSQL.value().toString(), true);
									}
									result = valueJava.equals(cell); 
								}
							}
							
							if(result)
							{
								tableSQLData.remove(rowSQL);
								break;
							}
						}
					}
					
					if(!result)
					{
						String msg = String.format(
								"%s - Table compare failed: Group [%s, %s], TableJava %s=%s, TableSQL %s=%s\n", 
								methodName, 
								columnHeaderGroupFailList,
								cellGroupFailList,
								columnHeaderJavaFail,
								cellJavaFail, 
								columnHeaderSQLFail,
								cellSQLFail);
						LOGGER.log(Level.WARNING, msg);
					}

					if(readableTableJava != null) {readableTableJava.close();}
					if(readableTableSQL != null) {readableTableSQL.close();}
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s - Table compare failed: %s\n", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}
		
		LOGGER.log(Level.INFO, String.format("%s : %s [%s]\n", methodName, (result ? "PASSED" : "FAILED"), doCalcList));

		return result;
	}

	/**
	 * Print data.
	 * 
	 * @param tableHeader Table header
	 * @param data CV table data
	 * @param label row label
	 */
	@SuppressWarnings("rawtypes")
	protected void printData(
			TableHeader tableHeader,
			List<List<CV_Super>> data, 
			String label)
	{
		try
		{
			if(data != null)
			{
				int sizeTable = data.size();			

				StringBuilder sb = new StringBuilder();
				sb.append("\n").append(tableHeader.toString()).append("\n");

				for(int iRow = 0; iRow < sizeTable; iRow++)
				{
					List<CV_Super> row = data.get(iRow);
					sb.append(String.format("%s: [%d] [%s]\n", label, iRow, row));
				}

				LOGGER.log(Level.FINE, String.format("Table [%s] size = %d", label, sizeTable));
				LOGGER.log(Level.FINE, sb.toString());
			}
		}
		catch(Exception e)
		{
			String msg = String.format("Debug print error: %s", e.toString());
			LOGGER.log(Level.FINE, msg);
		}
	}

	/**
	 * Print resultant table.
	 * 
	 * @param destTable Destination CV table
	 * @param label row label
	 */
	@SuppressWarnings("rawtypes")
	static public void printResults(
			CV_Table destTable, 
			String label)
	{
		try		
		{
			if(destTable != null)
			{
				TableHeader tableHeader = destTable.getHeader();			
				StringBuilder sb = new StringBuilder(String.format("%s:\n", label));
				List<CV_Super> row = null;
				while((row = destTable.readRow()) != null)
				{
					for(int iCol = 0; iCol < row.size(); iCol++)
					{
						ColumnHeader colHeader = tableHeader.getHeader(iCol);
						CV_Super cell = row.get(iCol);
						sb.append(String.format("%s = [%s]\n", colHeader.toString(), cell));
					}
					sb.append("\n");
				}
				LOGGER.log(Level.FINE, sb.toString());
			}
		}
		catch(Exception e)
		{
			String msg = String.format("Debug print error: %s", e.toString());
			LOGGER.log(Level.FINE, msg);
		}
	}

	/**
	 * Instantiate and populate SummaryStatisticsSQL object.
	 * @param srcTable
	 * @param discriminatorClause
	 * @param discriminatorColumnsList
	 * @param statisticsColumnsList
	 * @param doCalcList
	 * @return SummaryStatisticsSQL object
	 */
	private SummaryStatisticsSQL makeSummaryStatisticsSQL(
			CV_Table srcTable,
			String discriminatorClause,
			List<CV_String> discriminatorColumnsList,
			List<CV_String> statisticsColumnsList,
			List<SummaryStatisticsBase.StatCalcType> doCalcList)
	{
		SummaryStatisticsSQL func = null;
		try
		{
			func = new SummaryStatisticsSQL();
			func.srcTable = srcTable;
			func.discriminatorClause = (discriminatorClause == null) ? null : CV_String.valueOf(discriminatorClause);
			func.discriminatorColumnsList = discriminatorColumnsList;
			func.statisticsColumnsList = statisticsColumnsList;
			func.doCountCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.COUNT));
			func.doFirstCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.FIRST));
			func.doLastCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.LAST));
			func.doMadCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MAD));
			func.doMaxCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MAX));
			func.doMeanCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MEAN));
			func.doMedianCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MEDIAN));
			func.doMinCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MIN));
			func.doModeCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MODE));
			func.doRangeCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.RANGE));
			func.doSDevCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.SDEV));
			func.doSumCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.SUM));
			func.doUniqueCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.UNIQUE));
			func.ctx = new ExecutionContextImpl();
		}
		catch(Exception e)
		{
			throw e;
		}

		return func;
	}

	/**
	 * Instantiate and populate SummaryStatistics object.
	 * @param srcTable
	 * @param discriminatorColumnsList
	 * @param statisticsColumnsList
	 * @param doCalcList
	 * @return SummaryStatistics object
	 * @throws Exception 
	 */
	private SummaryStatistics makeSummaryStatistics(
			CV_Table srcTable,
			List<CV_String> discriminatorColumnsList,
			List<CV_String> statisticsColumnsList,
			List<SummaryStatisticsBase.StatCalcType> doCalcList) throws Exception
	{
		SummaryStatistics func = null;

		try
		{
			func = new SummaryStatistics();
			func.srcTable = srcTable;
			func.discriminatorColumnsList = discriminatorColumnsList;
			func.statisticsColumnsList = statisticsColumnsList;
			func.doCountCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.COUNT));
			func.doFirstCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.FIRST));
			func.doLastCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.LAST));
			func.doMadCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MAD));
			func.doMaxCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MAX));
			func.doMeanCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MEAN));
			func.doMedianCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MEDIAN));
			func.doMinCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MIN));
			func.doModeCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.MODE));
			func.doRangeCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.RANGE));
			func.doSDevCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.SDEV));
			func.doSumCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.SUM));
			func.doUniqueCalc = CV_Boolean.valueOf(doCalcList.contains(SummaryStatisticsBase.StatCalcType.UNIQUE));
			func.ctx = new ExecutionContextImpl();
		}
		catch(Exception e)
		{
			throw e;
		}

		return func;
	}
	
	/**
	 * Print source data sorted by discrimated column list.
	 * @param data Source data
	 */
	@SuppressWarnings("rawtypes")
	private void printSortedData(
		String methodName,
		List<List<CV_Super>> data,
		TableHeader tableHeader,
		List<CV_String> discriminatorColumnsList)
	{
		int COL_ID = 0;
		
		List<List<CV_Super>> sortedData = new ArrayList<List<CV_Super>>(data);
		for(int iRow = 0; iRow < data.size(); iRow++)
		{
			List<CV_Super> row = data.get(iRow);
			row.add(COL_ID, CV_Integer.valueOf(iRow));
		}
		
		List<Integer> discrimColIndexList = new ArrayList<Integer>();
		for(CV_String colName : discriminatorColumnsList)
		{
			String name = colName.value();
			int iCol = tableHeader.findIndex(name);
			discrimColIndexList.add(Integer.valueOf(iCol+1)); 
		}
		
		Collections.sort(sortedData, new Comparator<Object>() 
		{
			@SuppressWarnings({ "unchecked" })
			@Override
			public int compare(Object o1, Object o2) 
			{
				int result = 0;
				List<CV_Super> row1 = (List<CV_Super>) o1;
				List<CV_Super> row2 = (List<CV_Super>) o2;
				for(int iDiscrim = 0; iDiscrim < discrimColIndexList.size() && (result == 0); iDiscrim++)
				{
					Integer iCell = discrimColIndexList.get(iDiscrim);
					CV_Super cell1 = (CV_Super) row1.get(iCell);
					CV_Super cell2 = (CV_Super) row2.get(iCell);
					result = cell1.compareTo(cell2);								
				}

				// Sort by row_id
				if(result == 0)
				{
					CV_Super cell1 = (CV_Super) row1.get(COL_ID);
					CV_Super cell2 = (CV_Super) row2.get(COL_ID);
					result = cell1.compareTo(cell2);								
				}

				return result;
			}
		});
		
		StringBuilder sb = new StringBuilder(String.format("%s: [%s]\n", methodName, discriminatorColumnsList));
		sb.append(String.format("%s\n", tableHeader));
		for(int iRow = 0; iRow < sortedData.size(); iRow++)
		{
			List<CV_Super> list = sortedData.get(iRow);
			sb.append(String.format("[%d] %s\n", iRow, list));
		}
		LOGGER.log(Level.FINE, sb.toString());
	}
}
