/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.statistics;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

/**
 * @author CASCADE
 */
public class PivotSummaryTest {

	/**
	 * Input table
	 */
	private CV_Table table;
	/**
	 * Text Execution Context
	 */
	private ExecutionContext testCtx;
	/**
	 * Column Headers for input table
	 */
	private List<ColumnHeader> columns = new ArrayList<ColumnHeader>();
	/**
	 * Generic List objects to test
	 */
	private List<List<Object>> rows = new ArrayList<List<Object>>();
	/**
	 * Generic header list
	 */
	private List<Object> header;
	/**
	 * Data Operations list
	 */
	private List<CV_String> dataOps = Arrays.asList(new CV_String[] { new CV_String("COUNT"), new CV_String("SUM"), new CV_String("MIN"), new CV_String("MAX"),
		new CV_String("AVERAGE") });

	/**
	 * Message string to be printed out for every test
	 */
	String message = "* Statistics.PivotSummaryTest JUnit4Test: ";

	@Before
	public void setUp() throws Exception {

		header = Arrays.asList(new Object[] { "Region", "Gender", "Item", "Units", "Price", "Time" });
		rows.add(Arrays.asList(new Object[] { "East", "Boy", "Tee", 10, 12.00, Instant.parse("2015-09-15T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "East", "Boy", "Golf", 15, 20.00, Instant.parse("2015-09-14T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "East", "Girl", "Tee", 8, 14.00, Instant.parse("2015-09-15T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "East", "Girl", "Golf", 20, 24.00, Instant.parse("2015-09-14T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "West", "Boy", "Tee", 5, 12.00, Instant.parse("2015-09-15T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "West", "Boy", "Golf", 12, 20.00, Instant.parse("2015-09-14T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "West", "Girl", "Tee", 15, 14.00, Instant.parse("2015-09-15T10:00:00Z") }));
		rows.add(Arrays.asList(new Object[] { "West", "Girl", "Golf", 10, 24.00, Instant.parse("2015-09-14T10:00:00Z") }));

		testCtx = new ExecutionContextImpl();

		// Column headers for result table
		columns.add(new ColumnHeader((String) header.get(0), ParameterType.string));
		columns.add(new ColumnHeader((String) header.get(1), ParameterType.string));
		columns.add(new ColumnHeader((String) header.get(2), ParameterType.string));
		columns.add(new ColumnHeader((String) header.get(3), ParameterType.integer));
		columns.add(new ColumnHeader((String) header.get(4), ParameterType.decimal));
		columns.add(new ColumnHeader((String) header.get(5), ParameterType.timestamp));

		// Results Result Table
		try {
			table = testCtx.createTable("PivotTable");
			table.setHeader(new TableHeader(columns));

			rows.stream().forEach(r -> {
				List<CV_Super> cv_row = r.stream().map(cv -> {
					if (cv instanceof String)
						return new CV_String((String) cv);
					if (cv instanceof Integer)
						return new CV_Integer((Integer) cv);
					if (cv instanceof Double)
						return new CV_Decimal((Double) cv);
					if (cv instanceof Instant)
						return new CV_Timestamp((Instant) cv);
					return new CV_String((String) cv);
				}).collect(Collectors.toList());
				try {
					table.appendRow(cv_row);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			);
			table = table.toReadable();
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testInvalidColumn() {
		System.out.println(message + ": testInvalidColumn()");

		CV_String col = new CV_String("INVALIDCOLUMNNAME");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;
		pivot.input_dataOperation = dataOps.get(0);
		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		boolean exception = false;
		try {
			pivot.run();
		} catch (RuntimeException e) {
			System.out.println(message + ": testInvalidColumn(): Exception Thrown:" + e.getMessage());
			exception = true;
		}

		assertTrue(message + ": testInvalidColumn(): Exception Thrown", exception);

	}

	@Test
	public void testInvalidRow() {
		System.out.println(message + ": testInvalidRow()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("INVALIDROW") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;
		pivot.input_dataOperation = dataOps.get(0);
		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		boolean exception = false;
		try {
			pivot.run();
		} catch (RuntimeException e) {
			System.out.println(message + ": testInvalidRow(): Exception Thrown:" + e.getMessage());
			exception = true;
		}

		assertTrue(message + ": testInvalidRow(): Exception Thrown", exception);

	}

	@Test
	public void testInvalidRowSecondRow() {
		System.out.println(message + ": testInvalidRowSecondRow()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender"), new CV_String("INVALIDSECONDROW") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;
		pivot.input_dataOperation = dataOps.get(0);
		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		boolean exception = false;
		try {
			pivot.run();
		} catch (RuntimeException e) {
			System.out.println(message + ": testInvalidRowSecondRow(): Exception Thrown:" + e.getMessage());
			exception = true;
		}

		assertTrue(message + ": testInvalidRowSecondRow(): Exception Thrown", exception);

	}

	@Test
	public void testInvalidSummerizingColumn() {
		System.out.println(message + ": testInvalidSummerizingColumn()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("INVALIDSUMMERIZINGCOLUMN");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;
		pivot.input_dataOperation = dataOps.get(0);
		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		boolean exception = false;
		try {
			pivot.run();
		} catch (RuntimeException e) {
			System.out.println(message + ": testInvalidSummerizingColumn(): Exception Thrown:" + e.getMessage());
			exception = true;
		}

		assertTrue(message + ": testInvalidSummerizingColumn(): Exception Thrown", exception);

	}

	@Test
	public void testAllDataOperationsNoTotals() {
		System.out.println(message + ": testAllDataOperationsNoTotals()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			for (CV_String op : dataOps) {
				System.out.println(message + ": testAllDataOperationsNoTotals() " + op);
				pivot.input_dataOperation = op;
				pivot.run();
				// Only 2 rows should be returned
				assertTrue((pivot.result.size() == 2));
				// Only 3 columns should be returned
				assertTrue((pivot.result.getHeader().size() == 3));
			}
		} catch (RuntimeException | TableException e) {
			System.out.println(message + ": testAllDataOperationsNoTotals(): Exception Thrown:" + e.getMessage());
			fail(message + ": testAllDataOperationsNoTotals(): Exception Thrown:" + e.getMessage());
		}
	}

	@Test
	public void testAllDataOperationsRowTotals() {
		System.out.println(message + ": testAllDataOperationsRowTotals()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_totalRow = new CV_Boolean(true);
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			for (CV_String op : dataOps) {
				System.out.println(message + ": testAllDataOperationsRowTotals() " + op);
				pivot.input_dataOperation = op;
				pivot.run();
				// Only 3 rows should be returned
				assertTrue((pivot.result.size() == 3));
				// Only 3 columns should be returned
				assertTrue((pivot.result.getHeader().size() == 3));
			}
		} catch (RuntimeException | TableException e) {
			System.out.println(message + ": testAllDataOperationsRowTotals(): Exception Thrown:" + e.getMessage());
			fail(message + ": testAllDataOperationsRowTotals(): Exception Thrown:" + e.getMessage());
		}

	}

	@Test
	public void testAllDataOperationsColumnTotals() {
		System.out.println(message + ": testAllDataOperationsColumnTotals()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_totalColumn = new CV_Boolean(true);
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			for (CV_String op : dataOps) {
				System.out.println(message + ": testAllDataOperationsColumnTotals() " + op);
				pivot.input_dataOperation = op;
				pivot.run();
				// Only 3 rows should be returned
				assertTrue((pivot.result.size() == 2));
				// Only 3 columns should be returned
				assertTrue((pivot.result.getHeader().size() == 4));
			}
		} catch (RuntimeException | TableException e) {
			System.out.println(message + ": testAllDataOperationsColumnTotals(): Exception Thrown:" + e.getMessage());
			fail(message + ": testAllDataOperationsColumnTotals(): Exception Thrown:" + e.getMessage());
		}

	}

	@Test
	public void testAllDataOperationsRowandColumnTotals() {
		System.out.println(message + ": testAllDataOperationsRowandColumnTotals()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_totalColumn = new CV_Boolean(true);
		pivot.input_totalRow = new CV_Boolean(true);
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			for (CV_String op : dataOps) {
				System.out.println(message + ": testAllDataOperationsRowandColumnTotals() " + op);
				pivot.input_dataOperation = op;
				pivot.run();
				// Only 3 rows should be returned
				assertTrue((pivot.result.size() == 3));
				// Only 3 columns should be returned
				assertTrue((pivot.result.getHeader().size() == 4));
			}
		} catch (RuntimeException | TableException e) {
			System.out.println(message + ": testAllDataOperationsRowandColumnTotals(): Exception Thrown:" + e.getMessage());
			fail(message + ": testAllDataOperationsRowandColumnTotals(): Exception Thrown:" + e.getMessage());
		}
	}

	@Test
	public void testNotAllowedDataColumns() {
		System.out.println(message + ": testNotAllowedDataColumns()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Gender");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Price") });

		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_totalColumn = new CV_Boolean(true);
		pivot.input_totalRow = new CV_Boolean(true);
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			for (CV_String op : dataOps) {
				System.out.println(message + ": testNotAllowedDataColumns() " + op);
				pivot.input_dataOperation = op;
				pivot.run();
			}
		} catch (RuntimeException e) {
			// Should throw an exception
			assertTrue(message + ": testNotAllowedDataColumns(): Exception Thrown:" + e.getMessage(),true);
		}
	}
	
	@Test
	public void testDataColumnTypeRetained() {
		System.out.println(message + ": testDataColumnTypeRetained()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Units");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });
		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			// Test Integer
			for (int i = 1; i < dataOps.size() - 1; i++) {
				System.out.println(message + ": testDataColumnTypeRetained(): INTEGER " + dataOps.get(i));
				pivot.input_dataOperation = dataOps.get(i);
				pivot.run();
				List<ColumnHeader> headList = pivot.result.getHeader().asList();
				if (headList.size() != 3) {
					fail(message + ": testDataColumnTypeRetained(): Wrong number of columns returns");
				} else {
					assertTrue(headList.get(0).getType().getJavaType().equals(CV_String.class));
					assertTrue(headList.get(1).getType().getJavaType().equals(CV_Integer.class));
					assertTrue(headList.get(2).getType().getJavaType().equals(CV_Integer.class));
				}
			}
			// Test Double
			pivot.input_summarizedColumn = new CV_String("Price");
			for (int i = 1; i < dataOps.size() - 1; i++) {
				System.out.println(message + ": testDataColumnTypeRetained(): DOUBLE " + dataOps.get(i));
				pivot.input_dataOperation = dataOps.get(i);
				pivot.run();
				List<ColumnHeader> headList = pivot.result.getHeader().asList();
				if (headList.size() != 3) {
					fail(message + ": testDataColumnTypeRetained(): Wrong number of columns returns");
				} else {
					assertTrue(headList.get(0).getType().getJavaType().equals(CV_String.class));
					assertTrue(headList.get(1).getType().getJavaType().equals(CV_Decimal.class));
					assertTrue(headList.get(2).getType().getJavaType().equals(CV_Decimal.class));
				}
			}

		} catch (RuntimeException e) {
			System.out.println(message + ": testDataColumnTypeRetained(): Exception Thrown:" + e.getMessage());
			fail(message + ": testDataColumnTypeRetained(): Exception Thrown:" + e.getMessage());
		}
	}

	@Test
	public void testTimestamp() {
		System.out.println(message + ": testTimestamp()");

		CV_String col = new CV_String("Region");
		CV_String val = new CV_String("Time");
		List<CV_String> row = Arrays.asList(new CV_String[] { new CV_String("Gender") });
		PivotSummary pivot = new PivotSummary();
		pivot.input_columnname = col;

		pivot.input_rowdata = row;
		pivot.input_summarizedColumn = val;
		pivot.input_table = table;
		pivot.myCtx = testCtx;

		try {
			// COUNT
			System.out.println(message + ": testTimestamp():  Testing COUNT");
			pivot.input_dataOperation = dataOps.get(0);
			pivot.run();
			List<ColumnHeader> headList = pivot.result.getHeader().asList();
			if (headList.size() != 3) {
				fail(message + ": testDataColumnTypeRetained(): Wrong number of columns returns");
			} else {
				assertTrue(headList.get(0).getType().getJavaType().equals(CV_String.class));
				assertTrue(headList.get(1).getType().getJavaType().equals(CV_Integer.class));
				assertTrue(headList.get(2).getType().getJavaType().equals(CV_Integer.class));
			}
			// MIN
			System.out.println(message + ": testTimestamp():  Testing MIN");
			pivot.input_dataOperation = dataOps.get(2);
			pivot.run();
			headList = pivot.result.getHeader().asList();
			if (headList.size() != 3) {
				fail(message + ": testDataColumnTypeRetained(): Wrong number of columns returns");
			} else {
				assertTrue(headList.get(0).getType().getJavaType().equals(CV_String.class));
				assertTrue(headList.get(1).getType().getJavaType().equals(CV_Timestamp.class));
				assertTrue(headList.get(2).getType().getJavaType().equals(CV_Timestamp.class));
			}
			// MAX
			System.out.println(message + ": testTimestamp():  Testing MAX");
			pivot.input_dataOperation = dataOps.get(3);
			pivot.run();
			headList = pivot.result.getHeader().asList();
			if (headList.size() != 3) {
				fail(message + ": testDataColumnTypeRetained(): Wrong number of columns returns");
			} else {
				assertTrue(headList.get(0).getType().getJavaType().equals(CV_String.class));
				assertTrue(headList.get(1).getType().getJavaType().equals(CV_Timestamp.class));
				assertTrue(headList.get(2).getType().getJavaType().equals(CV_Timestamp.class));
			}
		} catch (RuntimeException e) {
			System.out.println(message + ": testDataColumnTypeRetained(): Exception Thrown:" + e.getMessage());
			fail(message + ": testDataColumnTypeRetained(): Exception Thrown:" + e.getMessage());
		}
	}
}
