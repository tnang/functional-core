 /**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.temporal;

import jema.common.types.CV_Integer;
import jema.common.types.CV_Timestamp;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author CASCADE
 */
public class DeltaBetweenTimestampsTest {
    
    public DeltaBetweenTimestampsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DeltaBetweenTimestamps.
     */
    @Test
    public void testRun() {
        System.out.println("* Temporal JUnit4Test: DeltaBetweenTimestampsTest : testRun()");
        DeltaBetweenTimestamps instance = new DeltaBetweenTimestamps();
        //One day difference between timestamps
        instance.startTimestamp = new CV_Timestamp("2014-09-26T16:09:43.328Z");
        instance.endTimestamp = new CV_Timestamp("2014-09-27T16:09:43.328Z");
        instance.run();
        CV_Integer expResultInDays = new CV_Integer(1);
        CV_Integer expResultInHours = new CV_Integer(24);
        CV_Integer expResultInMinutes = new CV_Integer(1440);
        CV_Integer expResultInSeconds= new CV_Integer(86400);
        assertEquals(expResultInDays, instance.days);
        assertEquals(expResultInHours, instance.hours);
        assertEquals(expResultInMinutes, instance.minutes);
        assertEquals(expResultInSeconds, instance.seconds);
        
        //One day difference between timestampd where start timestamp is greater than end timestamp
        instance = new DeltaBetweenTimestamps();
        instance.startTimestamp = new CV_Timestamp("2014-09-28T16:09:43.328Z");
        instance.endTimestamp = new CV_Timestamp("2014-09-27T16:09:43.328Z");
        instance.run();
        expResultInDays = new CV_Integer(-1);
        expResultInHours = new CV_Integer(-24);
        expResultInMinutes = new CV_Integer(-1440);
        expResultInSeconds= new CV_Integer(-86400);
        assertEquals(expResultInDays, instance.days);
        assertEquals(expResultInHours, instance.hours);
        assertEquals(expResultInMinutes, instance.minutes);
        assertEquals(expResultInSeconds, instance.seconds);
        
        //Two day difference between timestampd
        instance = new DeltaBetweenTimestamps();
        instance.startTimestamp = new CV_Timestamp("2014-09-27T16:09:43.328Z");
        instance.endTimestamp = new CV_Timestamp("2014-09-29T16:09:43.328Z");
        instance.run();
        expResultInDays = new CV_Integer(2);
        expResultInHours = new CV_Integer(48);
        expResultInMinutes = new CV_Integer(2880);
        expResultInSeconds= new CV_Integer(172800);
        assertEquals(expResultInDays, instance.days);
        assertEquals(expResultInHours, instance.hours);
        assertEquals(expResultInMinutes, instance.minutes);
        assertEquals(expResultInSeconds, instance.seconds);
        
        //One day and one hour difference between timestamps
        instance = new DeltaBetweenTimestamps();
        instance.startTimestamp = new CV_Timestamp("2014-09-27T16:09:43.328Z");
        instance.endTimestamp = new CV_Timestamp("2014-09-28T17:09:43.328Z");
        instance.run();
        expResultInDays = new CV_Integer(1);
        expResultInHours = new CV_Integer(25);
        expResultInMinutes = new CV_Integer(1500);
        expResultInSeconds= new CV_Integer(90000);
        assertEquals(expResultInDays, instance.days);
        assertEquals(expResultInHours, instance.hours);
        assertEquals(expResultInMinutes, instance.minutes);
        assertEquals(expResultInSeconds, instance.seconds);
        
        //One day and one minute difference between timestamps
        instance = new DeltaBetweenTimestamps();
        instance.startTimestamp = new CV_Timestamp("2014-09-27T16:09:43.328Z");
        instance.endTimestamp = new CV_Timestamp("2014-09-28T16:10:43.328Z");
        instance.run();
        expResultInDays = new CV_Integer(1);
        expResultInHours = new CV_Integer(24);
        expResultInMinutes = new CV_Integer(1441);
        expResultInSeconds= new CV_Integer(86460);
       
        assertEquals(expResultInDays, instance.days);
        assertEquals(expResultInHours, instance.hours);
        assertEquals(expResultInMinutes, instance.minutes);
        assertEquals(expResultInSeconds, instance.seconds);
        
        
    }
    
}
