/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 16, 2015 9:22:54 AM
 */
package jema.functional.core.cascade.temporal;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_TemporalRange;
import jema.common.types.CV_Timestamp;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.Before;

/**
 * @author CASCADE
 */
public class IntersectionOfTimerangesTest {

    ExecutionContext test_ctx;
    String method = "* DataManipulation JUnit4Test: IntersectionOfTimeranges : ";

    public CV_TemporalRange range1, range2, range3, range4;

    @Before
    public void setUp() {
        try {
            test_ctx = new ExecutionContextImpl();

            Instant time1 = Instant.now().minus(10, ChronoUnit.DAYS);
            Instant time2 = Instant.now().minus(5, ChronoUnit.DAYS);
            Instant time3 = Instant.now().minus(1, ChronoUnit.DAYS);

            Instant time4 = Instant.now().plus(2, ChronoUnit.DAYS);
            Instant time5 = Instant.now().plus(5, ChronoUnit.DAYS);
            Instant time6 = Instant.now().plus(7, ChronoUnit.DAYS);

            range1 = new CV_TemporalRange(new CV_Timestamp(time1), new CV_Timestamp(time4));
            range2 = new CV_TemporalRange(new CV_Timestamp(time2), new CV_Timestamp(time3));
            range3 = new CV_TemporalRange(new CV_Timestamp(time5), new CV_Timestamp(time6));
            range4 = new CV_TemporalRange(new CV_Timestamp(time6), new CV_Timestamp(time6));
        } catch (Exception ex) {
            Logger.getLogger(IntersectionOfTimerangesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of run method, of class IntersectionOfTimeranges.
     */
    @Test
    public void testRun() {
        System.out.println(method + "testRun()");

        IntersectionOfTimeranges instance = new IntersectionOfTimeranges();
        instance.ctx = test_ctx;
        instance.ranges = new ArrayList<>();

        instance.ranges.add(range1);
        instance.ranges.add(range2);
        instance.run();

        assertEquals(range2, instance.intersection);
        assertEquals(range2.getBegin(), instance.startTime.value());
        assertEquals(range2.getEnd(), instance.stopTime.value());
    }

    /**
     * Test of run method, of class IntersectionOfTimeranges.
     */
    @Test
    public void testRun_noIntersection() {
        System.out.println(method + "testRun_noIntersection()");

        IntersectionOfTimeranges instance = new IntersectionOfTimeranges();
        instance.ctx = test_ctx;
        instance.ranges = new ArrayList<>();

        instance.ranges.add(range1);
        instance.ranges.add(range2);
        instance.ranges.add(range3);
        instance.run();

        assertNull(instance.intersection);
        assertNull(instance.startTime);
        assertNull(instance.stopTime);
    }

    /**
     * Test of run method, of class IntersectionOfTimeranges.
     */
    @Test(expected = RuntimeException.class)
    public void testRun_null() {
        System.out.println(method + "testRun_null()");

        IntersectionOfTimeranges instance = new IntersectionOfTimeranges();
        instance.ctx = test_ctx;
        instance.run();
    }

    /**
     * Test of run method, of class IntersectionOfTimeranges.
     */
    @Test
    public void testRun_oneRange() {
        System.out.println(method + "testRun_oneRange()");

        IntersectionOfTimeranges instance = new IntersectionOfTimeranges();
        instance.ctx = test_ctx;
        instance.ranges = new ArrayList<>();
        instance.ranges.add(range4);
        instance.run();

        assertEquals(range4, instance.intersection);
        assertEquals(range4.getBegin(), instance.startTime.value());
        assertEquals(range4.getEnd(), instance.stopTime.value());
    }
}
