package jema.functional.core.cascade.temporal;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDataPath;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.filter.FilterListByRegex;

public class DeltaBetweenTableTimestampsTest 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(DeltaBetweenTableTimestampsTest.class.getName());

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();

	/** SQL table URI */
	private final URI uriTestTable = CommonVocabDataPath.getUriSql();
	
	/** Column labels */
	private static final String DATE_LABEL_1 = "Date1";
	private static final String DATE_LABEL_2 = "Date2";

	/**
	 * Setup test data.
	 */
	@BeforeClass
	public static void setUp()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table setup failed - %s", e.toString()));
		}    	
	}

	/**
	 * Tear down test data.
	 */
	@AfterClass
	public static void teardown()
	{
		try 
		{   
		} 
		catch (Exception e) 
		{
			fail(String.format("Table teardown failed - %s", e.toString()));
		}    	
	}

	/**
	 * Simple test
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testTimestamp1()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			columnHeaderList.add(new ColumnHeader("Label", ParameterType.string));
			columnHeaderList.add(new ColumnHeader(DATE_LABEL_1, ParameterType.timestamp));
			columnHeaderList.add(new ColumnHeader(DATE_LABEL_2, ParameterType.timestamp));
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 20;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			long deltaSeconds = 60*45;
			
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				row.add(CV_String.valueOf(String.format("%s_%d", methodName, iRow)));
				row.add(cvDataGenerator.genTimestamp(Instant.now()));
				row.add(cvDataGenerator.genTimestamp(deltaSeconds*(iRow+1), false));
				data.add(row);
			}
			
			// Expected Results
			List<List<CV_Super>> testData = new ArrayList<List<CV_Super>>();
			for(List<CV_Super> row : data)
			{
				List<CV_Super> testRow = new ArrayList<CV_Super>(row);
				CV_Timestamp date1 = (CV_Timestamp) row.get(1);
				CV_Timestamp date2 = (CV_Timestamp) row.get(2);
				long milliSec = date2.value().toEpochMilli() - date1.value().toEpochMilli();
		        double seconds = ((double) milliSec) * 1.0E-3;
		        double minutes = seconds / 60;
		        double hours = minutes / 60;
		        double days = hours / 24;
		        testRow.add(CV_Decimal.valueOf(days));
		        testRow.add(CV_Decimal.valueOf(hours));
		        testRow.add(CV_Decimal.valueOf(minutes));
		        testRow.add(CV_Decimal.valueOf(seconds));
				
				testData.add(testRow);
			}
			
			// Create source table
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);
			
			// Initialize functional
			DeltaBetweenTableTimestamps func = new DeltaBetweenTableTimestamps();
			func.srcTable = srcTable;
			func.dateColumnName1 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_1).getName());
			func.dateColumnName2 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_2).getName());
			func.ctx = new ExecutionContextImpl();
			
			// Run functional
			func.run();
			CV_Table destTable = func.destTable;
			
			// Validate results
			boolean result = this.compareTable(
				methodName, 
				uriTestTable, 
				destTable, 
				testData);
						
			assertTrue(result);
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Invalid required input test
	 */
	@Test
	public void testInvalidRequiredInputs()
	{
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();

		try 
		{			
			// Header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			columnHeaderList.add(new ColumnHeader("Label", ParameterType.string));
			columnHeaderList.add(new ColumnHeader(DATE_LABEL_1, ParameterType.timestamp));
			columnHeaderList.add(new ColumnHeader(DATE_LABEL_2, ParameterType.timestamp));
			TableHeader tableHeader = new TableHeader(columnHeaderList);

			// Create sample data
			int numRows = 20;
			List<List<CV_Super>> data = new ArrayList<List<CV_Super>>();
			long deltaSeconds = 60*45;
			
			for(int iRow = 0; iRow < numRows; iRow++)
			{
				List<CV_Super> row = new ArrayList<CV_Super>();
				row.add(CV_String.valueOf(String.format("%s_%d", methodName, iRow)));
				row.add(cvDataGenerator.genTimestamp(Instant.now()));
				row.add(cvDataGenerator.genTimestamp(deltaSeconds*(iRow+1), false));
				data.add(row);
			}
			
			// Expected Results
			List<List<CV_Super>> testData = new ArrayList<List<CV_Super>>();
			for(List<CV_Super> row : data)
			{
				List<CV_Super> testRow = new ArrayList<CV_Super>(row);
				CV_Timestamp date1 = (CV_Timestamp) row.get(1);
				CV_Timestamp date2 = (CV_Timestamp) row.get(2);
				long milliSec = date2.value().toEpochMilli() - date1.value().toEpochMilli();
		        double seconds = ((double) milliSec) * 1.0E-3;
		        double minutes = seconds / 60;
		        double hours = minutes / 60;
		        double days = hours / 24;
		        testRow.add(CV_Decimal.valueOf(days));
		        testRow.add(CV_Decimal.valueOf(hours));
		        testRow.add(CV_Decimal.valueOf(minutes));
		        testRow.add(CV_Decimal.valueOf(seconds));
				
				testData.add(testRow);
			}
			
			// Create source table
			CV_Table srcTable = cvDataGenerator.genTable(uriTestTable, null, null, tableHeader, data);
			
			// Initialize functional
			DeltaBetweenTableTimestamps func = new DeltaBetweenTableTimestamps();
			
			try
			{
				func.srcTable = null;
				func.dateColumnName1 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_1).getName());
				func.dateColumnName2 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_2).getName());
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Source table is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				func.srcTable = srcTable;
				func.dateColumnName1 = null;
				func.dateColumnName2 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_2).getName());
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column name 1 is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				func.srcTable = srcTable;
				func.dateColumnName1 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_1).getName());
				func.dateColumnName2 = null;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column name 2 is null" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				func.srcTable = srcTable;
				func.dateColumnName1 = CV_String.valueOf("BadColumnName");
				func.dateColumnName2 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_2).getName());
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column 1 has invalid column name" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				func.srcTable = srcTable;
				func.dateColumnName1 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_1).getName());
				func.dateColumnName2 = CV_String.valueOf("BadColumnName");
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column 2 has invalid column name" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
			
			try
			{
				func.srcTable = srcTable;
				func.dateColumnName1 = CV_String.valueOf(tableHeader.getHeader(DATE_LABEL_1).getName());
				func.dateColumnName2 = CV_String.valueOf(tableHeader.getHeader(0).getName());;
				func.ctx = new ExecutionContextImpl();
				func.run();
				assertTrue("Column 2 has invalid parameter type" ,false);
			} 
			catch(Exception e) {LOGGER.log(Level.FINE, e.toString());assertTrue(methodName, true);}
		} 
		catch (Exception e) 
		{
			String msg = String.format("%s: %s", methodName, e.toString());
			LOGGER.log(Level.WARNING, msg);
			fail(msg);
		}
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param uri Table URI
	 * @param destTable Destination table containing computed data
	 * @param testData Test comparison data
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	public boolean compareTable(
			String methodName,
			URI uri,
			CV_Table destTable,
			List<List<CV_Super>> testData) throws Exception
	{
		boolean result = (destTable != null);

		try
		{
			if((destTable != null) && (testData != null))
			{
				int testTableRowSize = testData.size();
				int destTableRowSize = destTable.size();
				result = (testTableRowSize == destTableRowSize);

				for(int iRow = 0; iRow < destTableRowSize && result; iRow++)
				{
					List<CV_Super> testRow = testData.get(iRow);
					List<CV_Super> destRow = destTable.readRow();

					int testTableColSize = testRow.size();
					int destTableColSize = destRow.size();
					result = (testTableColSize == destTableColSize);

					for(int iCol = 0; iCol < destTableColSize && result; iCol++)
					{
						result = this.equalColumnAlt(testRow.get(iCol), destRow.get(iCol)); 
						if(!result)
						{
							String msg = String.format("%s (%s) - Table compare failed: testVal=%s, destVal=%s", methodName, uri.toString(), testRow.get(iCol), destRow.get(iCol));
							LOGGER.log(Level.WARNING, msg);
						}
					}  
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s (%s) - Table compare failed: %s", methodName, uri.toString(), e.toString());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}

		return result;
	}

	/**
	 * Compare input Common Vocabulary data of different types.
	 * @param col1 Common vocabulary data 1
	 * @param col2 Common vocabulary data 2
	 * @return true=data is equal, false=otherwise
	 */
	public boolean equalColumnAlt(CV_Super<?> col1, CV_Super<?> col2)
	{
		boolean result = false;

		if(col1 instanceof CV_String && col2 instanceof CV_Integer)
		{
			CV_Integer iCol2 = (CV_Integer) col2;
			CV_String col2a = new CV_String(iCol2.value().toString());
			result = cvDataGenerator.equalColumn(col1, col2a);
		}
		else if(col2 instanceof CV_String && col1 instanceof CV_Integer)
		{
			CV_Integer iCol1 = (CV_Integer) col1;
			CV_String col1a = new CV_String(iCol1.value().toString());
			result = cvDataGenerator.equalColumn(col1a, col2);
		}
		else if(col2 instanceof CV_Decimal && col1 instanceof CV_Decimal)
		{
			double val1 = ((CV_Decimal) col1).value();
			double val2 = ((CV_Decimal) col2).value();
			if(val1 == 0 && val2 == 0) result = true;
			else if(val1 == 0 || val2 == 0) {result = (Math.abs(val1 - val2) < 1.0E-6);}
			else {result = (Math.abs(val1/val2 - 1) < 1.0E-6);} // IBM recommended
		}
		else
		{
			result = cvDataGenerator.equalColumn(col1, col2);
		}
		return result;
	}
}
