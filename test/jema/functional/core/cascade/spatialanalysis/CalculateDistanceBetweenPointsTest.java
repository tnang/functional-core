package jema.functional.core.cascade.spatialanalysis;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.stream.IntStream;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Before;
import org.junit.Test;

public class CalculateDistanceBetweenPointsTest {

    /**
     * ExecutionContext
     */
    public ExecutionContext testCtx;

    /**
     * Message string to be printed out for every test
     */
    String message = "* SpatialAnalysis.Calculation.CalculateDistanceBetweenPoints JUnit4Test: ";

    /**
     * Input Point Arrays
     */
    CV_Point[] point1 = new CV_Point[] {new CV_Point(-86.67, 36.12),new CV_Point(-190.67, 36.12),
    	new CV_Point(-86.67, 90.12),new CV_Point(-86.67, 36.12),new CV_Point(-86.67, 36.12),null,
    	new CV_Point(-86.67, 36.12)};
    CV_Point[] point2 = new CV_Point[] {new CV_Point(-118.40, 33.94),new CV_Point(-118.40, 33.94),
    	new CV_Point(-118.40, 33.94),new CV_Point(-181.40, 33.94),new CV_Point(-118.40, 90.94),
    	new CV_Point(-118.40, 33.94),null};
    
    CV_Decimal[] expectedResults = new CV_Decimal[]{new CV_Decimal(1558.555314708959), new CV_Decimal(274.59357410546284)};
    
	@Test
	public void testCalculateDistanceBetweenPointsValid() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoints");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[0];
		pointDistance.inputPointTwo = point2[0];
		pointDistance.run();
		System.out.println("DISTANCE = "+pointDistance.outputDistance.value());
		System.out.println("HEADING = "+pointDistance.outputAzmuth.value());
		CV_Decimal resultDistance = pointDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = pointDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}

	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint1LatInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint1LatInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[1];
		pointDistance.inputPointTwo = point2[1];
		pointDistance.run();
		System.out.println("DISTANCE = "+pointDistance.outputDistance.value());
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint1LonInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint1LonInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[2];
		pointDistance.inputPointTwo = point2[2];
		pointDistance.run();
		System.out.println("DISTANCE = "+pointDistance.outputDistance.value());
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint2LatInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint2LatInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[4];
		pointDistance.inputPointTwo = point2[4];
		pointDistance.run();
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint2LonInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint2LonInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[3];
		pointDistance.inputPointTwo = point2[3];
		pointDistance.run();
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint1Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint1Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[5];
		pointDistance.inputPointTwo = point2[5];
		pointDistance.run();
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenPoint2Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenPoint2Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenPoints pointDistance = new CalculateDistanceBetweenPoints();
		pointDistance.ctx = testCtx;

		pointDistance.inputPointOne = point1[6];
		pointDistance.inputPointTwo = point2[6];
		pointDistance.run();
		CV_Decimal result = pointDistance.outputDistance;
		assertTrue(result.equals(expectedResults[0]));
	}
}
