package jema.functional.core.cascade.spatialanalysis;

import static org.junit.Assert.*;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.junit.Test;

public class CalculateDistanceBetweenCoordinatesTest {

	/**
     * ExecutionContext
     */
    public ExecutionContext testCtx;

    /**
     * Message string to be printed out for every test
     */
    String message = "* SpatialAnalysis.Calculation.CalculateDistanceBetweenPoints JUnit4Test: ";

    /**
     * Input Coordinates Arrays
     */
    CV_Decimal[] lat1 = new CV_Decimal[] {new CV_Decimal(36.12),new CV_Decimal(90.12),null};
    CV_Decimal[] lon1 = new CV_Decimal[] {new CV_Decimal(-86.67),new CV_Decimal(-190.67),null};

    CV_Decimal[] lat2 = new CV_Decimal[] {new CV_Decimal(33.94),new CV_Decimal(90.94),null};
    CV_Decimal[] lon2 = new CV_Decimal[] {new CV_Decimal(-118.40),new CV_Decimal(-181.40),null}; 
    
    CV_Decimal[] expectedResults = new CV_Decimal[]{new CV_Decimal(1558.555314708959), new CV_Decimal(274.59357410546284)};
    
	@Test
	public void testCalculateDistanceBetweenCoordintesValid() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintesValid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}

	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintes1LatInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintes1LatInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[2];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintes1LonInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintes1LonInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[2];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintes2LatInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintes2LatInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[2];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintes2LonInvalid() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintes2LonInvalid");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[2];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintesLat1Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintesLat1Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[3];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}

	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintesLon1Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintesLon1Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[3];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintesLat2Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintesLat2Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[3];
		coordinateDistance.inputLongitudeTwo = lon2[0];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
	
	@Test(expected=RuntimeException.class)
	public void testCalculateDistanceBetweenCoordintesLon2Null() {
		
		System.out.println(message+"testCalculateDistanceBetweenCoordintes2Null");
		testCtx = new ExecutionContextImpl();
		
		CalculateDistanceBetweenCoordinates coordinateDistance = new CalculateDistanceBetweenCoordinates();
		coordinateDistance.ctx = testCtx;

		coordinateDistance.inputLatitudeOne = lat1[0];
		coordinateDistance.inputLongitudeOne = lon1[0];
		coordinateDistance.inputLatitudeTwo = lat2[0];
		coordinateDistance.inputLongitudeTwo = lon2[3];		
		coordinateDistance.run();
		System.out.println("DISTANCE = "+coordinateDistance.outputDistance.value());
		System.out.println("HEADING = "+coordinateDistance.outputAzmuth.value());
		CV_Decimal resultDistance = coordinateDistance.outputDistance;
		assertTrue(resultDistance.equals(expectedResults[0]));
		CV_Decimal resultBearing = coordinateDistance.outputAzmuth;
		assertTrue(resultBearing.equals(expectedResults[1]));
	}
}

