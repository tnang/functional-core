package jema.functional.core.cascade.spatialanalysis;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import jema.common.types.CV_WebResource;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Kml;

public class MergeKMLTest {

    /**
     * ExecutionContext
     */
    public ExecutionContext testCtx;

    /**
     * Message string to be printed out for every test
     */
    String message = "* SpatialAnalysis.Merge.MergeKML JUnit4Test: testMergeKML() ";

    /**
     * Functional Resources
     */
    List<CV_WebResource> inputResources;
    
    /**
     * Expected number of Items 
     */
    private int expectedKMLItems = 2;
    
    @Before
    public void setUp() throws Exception {
	inputResources = new ArrayList<CV_WebResource>();
	CV_WebResource kml1 = new CV_WebResource(this.getClass().getResource("/data/KMLTestFile.kml").toURI());
	CV_WebResource kml2 = new CV_WebResource(this.getClass().getResource("/data/KMLTestFile2.kml").toURI());
	inputResources.add(kml1);
	inputResources.add(kml2);
	testCtx = new ExecutionContextImpl();
    }

    @Test
    public void testMergeKML() throws IOException {
	
	System.out.println(message);
	MergeKML instance = new MergeKML();
	instance.ctx = testCtx;
	instance.input = inputResources;
	instance.run();
	
	CV_WebResource result = instance.output;
	
	// Here is a stupid check, Does the file end with .kml
	String fileName = result.toString();
	assertTrue(message+"Output ends with .kml",fileName.endsWith(".kml"));
	String str = IOUtils.toString(result.getURL().openStream(), "UTF-8");

	// Try and unmarshall the KML..
	try {
		JAXBContext jc = JAXBContext.newInstance(Kml.class);
		Unmarshaller u = jc.createUnmarshaller();
		Kml kmlItem = (Kml) u.unmarshal(result.getURL().openStream());
		int items = ((Document)kmlItem.getFeature()).getFeature().size();
		assertTrue(message+"Merged correct number of items",items==expectedKMLItems);
	} catch (Exception e) {
	    fail(message+": KML Failed to Unmarshall :"+e.getMessage());
	}

    }

}
