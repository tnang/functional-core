/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Aug 5, 2015 10:01:40 AM
 */
package jema.functional.core.cascade.util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author CASCADE
 */
public class CoordinateConversionTest {

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test
    public void testDMStoDD_emptyString() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDMStoDD_emptyString()");

        //""
        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("");
        assertEquals(null, result);

        //" "
        converter = new CoordinateConversion();
        result = converter.DMStoDD(" ");
        assertEquals(null, result);
    }

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test
    public void testDMStoDD_lat() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDMStoDD_lat()");

        //DDMMSS.S(N|S)
        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("301557.1926N");
        assertEquals(Double.valueOf(30.265886833333333), result);

        //DD MM SS.S (N|S)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("30 15 57.1926 N");
        assertEquals(Double.valueOf(30.265886833333333), result);

        //DDMMSS(N|S)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("301557N");
        assertEquals(Double.valueOf(30.265833333333333), result);

        //(-) DDMMSS.S
        converter = new CoordinateConversion();
        result = converter.DMStoDD("+ 301557.1926");
        assertEquals(Double.valueOf(30.265886833333333), result);

        //DD°MM'SS.S(N|S)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("30°15'57.1926\"N");
        assertEquals(Double.valueOf(30.265886833333333), result);

        //DD° MM' SS.S" (N|S)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("30° 15' 57.1926\" N");
        assertEquals(Double.valueOf(30.265886833333333), result);

        //DD°MM'SS"(N|S)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("30°15'57\"S");
        assertEquals(Double.valueOf(-30.265833333333333), result);

        //(-)DD°MM′SS″
        converter = new CoordinateConversion();
        result = converter.DMStoDD("+36°10′30″");
        assertEquals(Double.valueOf(36.175), result);
    }

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test
    public void testDMStoDD_lon() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDMStoDD_lon()");

        //DDDMMSS.S(E|W)
        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("0795856.0W");
        assertEquals(Double.valueOf(-79.98222222222222), result);

        //DDDMMSS(E|W)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("0795856W");
        assertEquals(Double.valueOf(-79.98222222222222), result);

        //(-)DDDMMSS.S
        converter = new CoordinateConversion();
        result = converter.DMStoDD("-0795856.0");
        assertEquals(Double.valueOf(-79.98222222222222), result);

        //DDD°MM'SS.S"(E|W)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("079°58'56.0\"W");
        assertEquals(Double.valueOf(-79.98222222222222), result);

        //DDD° MM' SS.S" (E|W)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("079° 58' 56.0\" W");
        assertEquals(Double.valueOf(-79.98222222222222), result);

        //DDD°MM'SS"(E|W)
        converter = new CoordinateConversion();
        result = converter.DMStoDD("079°58'56\"E");
        assertEquals(Double.valueOf(79.98222222222222), result);

        //(-) DDD°MM′SS″
        converter = new CoordinateConversion();
        result = converter.DMStoDD("- 115°08′11″");
        assertEquals(Double.valueOf(-115.1363888888889), result);
    }

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDMStoDD_latOutOfRange() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDMStoDD_latOutOfRange()");

        //Latitude out of range (not b/w -90 and 90)
        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("91°15'57.1926\"N");
    }

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDMStoDD_lonOutOfRange() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDMStoDD_lonOutOfRange()");

        //Longitude out of range (not b/w -180 and 180)
        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("2795856.0W");
    }

    /**
     * Test of DMStoDD method, of class CoordinateConversion.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDMStoDD_invalidFormat() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testRun_invalidInput()");

        CoordinateConversion converter = new CoordinateConversion();
        Double result = converter.DMStoDD("27");
    }

    /**
     * Test of DDtoDMS method, of class CoordinateConversion
     */
    @Test
    public void testDDtoDMS() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDDtoDMS()");

        CoordinateConversion converter = new CoordinateConversion();
        String DMSresult = converter.DDtoDMS(30.26588683, false);
        assertEquals("301557.19N", DMSresult);

        converter = new CoordinateConversion();
        DMSresult = converter.DDtoDMS(-115.1363889, true);
        assertEquals("1150811.00W", DMSresult);

        converter = new CoordinateConversion();
        DMSresult = converter.DDtoDMS(-79.98222222, true);
        assertEquals("0795856.00W", DMSresult);
    }

    /**
     * Test of DDtoDMS method, of class CoordinateConversion.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDDtoDMS_latOutOfRange() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDDtoDMS_latOutOfRange()");

        //Latitude out of range (not b/w -90 and 90)
        CoordinateConversion converter = new CoordinateConversion();
        String result = converter.DDtoDMS(90.26588683, false);
    }

    /**
     * Test of DDtoDMS method, of class CoordinateConversion.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDDtoDMS_lonOutOfRange() {
        System.out.println("* Util JUnit4Test: CoordinateConversion : testDDtoDMS_lonOutOfRange()");

        //Longitude out of range (not b/w -180 and 180)
        CoordinateConversion converter = new CoordinateConversion();
        String result = converter.DDtoDMS(-215.1363889, true);
    }
}
