/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.retrieval;

import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import jema.common.security.DN;
import jema.common.security.TrustChainRequest;
import jema.common.types.CV_Table;
import jema.common.util.Tagged;
import jema.functional.api.ExecutionContext;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;


/**
 *
 * @author CASCADE
 */
public class GetMyDNTest {
    
    public static final String DN_NAME = "CN=TEST MOCK, O=MOCK, C=US";
    
    public GetMyDNTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDN method, of class GetMyDN.
     */
    @Test
    public void testGetDN() {
        System.out.println("* Retrieval JUnit4Test: GetMyDNTest : testGetDN()");
        GetMyDN instance = new GetMyDN();
        instance.ctx = new MockExecutionService();
        String dn = instance.getDN();
        assertTrue("The DN value matches mock object value",dn.equals(DN_NAME));
    }
    
    /**
     * Test of run method, of class GetMyDN.
     */
    @Test
    public void testRun() {
        System.out.println("* Retrieval JUnit4Test: GetMyDNTest : testRun()");
        GetMyDN instance = new GetMyDN();
        instance.ctx = new MockExecutionService();
        instance.run();
        assertTrue("Thread did run without error or exception",instance.result.value().equals(DN_NAME));

    }
    
}
    /**
     * Mock ExecutionContext strictly for unit testing of the GetDN functionality.
     */
class MockExecutionService implements ExecutionContext{

    @Override
    public CV_Table createTable(String name) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CV_Table createTable(Tagged<String> name) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CV_Table createTable(String name, String capco) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CV_Table createTable(Tagged<String> name, String capco) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File createTempDir() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File createTempFile() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File createTempFileWithExtension(String ext) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CV_Table createWritableTableCopy(CV_Table src) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TrustChainRequest getConnection(URL url) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public boolean hasVisual() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void log(MsgLogLevel level, String msg) {
        System.out.println("* Retrieval JUnit4Test output = "+msg);
    }

    @Override
    public void postProgress(int percent, String shortMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> Future<T> submitWork(Callable<T> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DN getPrincipalDN() {
        DN mockDN = DN.fromString(GetMyDNTest.DN_NAME);
        return mockDN;
    }

    @Override
    public DN getIssuerDN() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addSpecialAccessTag(String tag, String outputName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    @Override
    public TrustChainRequest getConnection(URL url, String string) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CV_Table createTable(String tableName, String tableNameCapco, String dataCapco) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getServerNetwork() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
