/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 24, 2015
 */
package jema.functional.core.cascade.datamanagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cascade
 */
public class GenerateRandomDecimalTest {
    
    public GenerateRandomDecimalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRandomLongInclusive method, of class GenerateRandomDecimal.
     */
    @Test
    public void testGetRandomLongInclusive() {
        System.out.println("* Datamanagement.definedata JUnit4Test: GenerateRandomDecimal : testGetRandomLongInclusive()");
        double min = 0.0;
        double max = 1.0;
        double temp;
        List<Double> listCheck = new ArrayList(Arrays.asList());
        for(int i=0; i<10000000; i++) {
           temp = GenerateRandomDecimal.getRandomDoubleInclusive(min, max);
           if (min>temp||temp>max) {
               fail("Generated value outside of range.");
               
           }
        }
    }

    /**
     * Test of getRandomLongExclusive method, of class GenerateRandomDecimal.
     */
    @Test
    public void testGetRandomLongExclusive() {
        System.out.println("* Datamanagement.definedata JUnit4Test: GenerateRandomDecimal : testGetRandomLongInclusive()");
        double min = 0.0;
        double max = 1.0;
        double temp;
        List<Double> listCheck = new ArrayList(Arrays.asList());
        for(int i=0; i<10000000; i++) {
           temp = GenerateRandomDecimal.getRandomDoubleExclusive(min, max);
           if (min>=temp||temp>=max) {
               fail("Generated value outside of range.");
           }
        } 
    }

    /*
     *
     * Not sure a testRun is needed for this functional
    
    @Test
    public void testRun() {
        System.out.println("* Datamanagement.definedata JUnit4Test: GenerateRandomDecimal : testRun()");
        GenerateRandomDecimal instance = new GenerateRandomDecimal();
        instance.run();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
     *
     *
     */
   
}
