/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;


import jema.functional.core.cascade.filter.FilterTableRowBase;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Integer;
import jema.common.types.ParameterType;
import jema.functional.api.Input;



/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Filter table by a range of integer values"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        name = @CAPCO("Filter Table By Integer Range"),
        uuid = "325d0757-919e-4d19-b58f-35a2a3a93728",
        tags = {"table", "filter","integer" }
        
)
public final class FilterTableRowByIntegerRange extends FilterTableRowBase {

   
    @Input(name = @CAPCO("Maximum Integer Value"),
            desc = @CAPCO("Maximum decimal value to filter against")
    )
    public CV_Integer maxValue;
    
    @Input(name = @CAPCO("Minimum Integer Value"),
            desc = @CAPCO("Minimum decimal value to filter against")
    )
    public CV_Integer minValue;

    
    @Override
    public void run() {
        
       compare(minValue,maxValue,ParameterType.integer);
    
    }

}
//UNCLASSIFIED