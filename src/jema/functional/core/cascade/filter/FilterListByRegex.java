/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * Functional to filter an input list using a regular expression. Regular expression patterns 
 * can be specified as '^ab\w\d' or, for compatibility with "Filter Table Rows by Regex" 
 * '/ab\w\d/' and '/^ab\w\d/i' for pattern case insensitivity.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>List of Strings to perform regex against</li>
 * <li>List of Strings containing regular expressions</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>List of Strings containing filtered list items</li>
 * </ul>
 * <p>
 * <b>References</b>
 * <ul>
 * <li>Algorithm based upon jema.functional.core.manipulation.table.FilterTableRows</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(name = @CAPCO("Filter List by Regex"),
        desc = @CAPCO("Filter an input list using a regular expression."),
        creator = "CASCADE",
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        uuid = "2a0ad256-9ba0-443b-98ba-1080c68d7aa2",
        tags = {"list", "filter", "regex"}
)
public class FilterListByRegex implements Runnable {

    @Input(name = @CAPCO("Input data list"),
            desc = @CAPCO("List of Strings to perform regex against."),
            required = true
    )
    public List<CV_String> inputList;
    
    @Input(name = @CAPCO("Input regular expression (regex) list"),
            desc = @CAPCO("List of Strings to perform regex against."),
            required = true
    )
    public List<CV_String> regexList;

    @Output(name = @CAPCO("Filtered List"),
            desc = @CAPCO("List of Strings containing filtered list items.")
    )
    public List<CV_String> filteredList;

    @Context
    ExecutionContext ctx;
    
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(FilterListByRegex.class.getName());

    /**
     * Run functional to filter an input list using a regular expression.
     */
    @Override
    public void run() 
    {
		try 
		{
			// Validate input parameters
			this.validate();
			
			// Filter the data
			this.filteredList = this.filterByRegexValue();		
		} 
		catch(IllegalArgumentException e) {throw e;}
        catch(RuntimeException e) {throw e;}
        catch(Exception e) {throw new RuntimeException(e.toString(), e);}
    }

    /**
     * Filter input list by list of regular expressions.
     * 
     * @return List of Strings containing filtered list items.
     */
    private List<CV_String> filterByRegexValue()
    { 
    	List<CV_String> filteredList = new ArrayList<CV_String>();
    	
    	// Prepare regular expression and initialize pattern
        List<Pattern> patterns = prepValues();
        
        // Filter input list and put results in output
        this.inputList.stream().filter(Objects::nonNull).forEach(str -> 
        {
            String value = str == null ? null : str.value();
            if (hasMatch(patterns, value)) 
            {
                try {filteredList.add(CV_String.valueOf(value));} 
                catch (RuntimeException e) {throw e;}
                catch (Exception e) {throw new RuntimeException(e.toString(), e);}
            }
        }); 
        
        String msg = String.format("Filtered list size = %d, Input list size = %d", filteredList.size(), this.inputList.size());
        ctx.log(ExecutionContext.MsgLogLevel.INFO, msg);
        
        return filteredList;
    }

    /**
     * Determine if the next subsequence of the input sequence matches the pattern
     * 
     * @param patterns List of compiled patterns
     * @param value Input sequence
     * @return true=a subsequence of the input sequence matches this matcher's pattern, false=otherwise
     */
    private boolean hasMatch(
    	List<Pattern> patterns,
    	String value) 
    {
        boolean found = false;
        
        for(Pattern p : patterns) 
        {
            if(p.matcher(value).find())
            {
            	found = true; 
            	break;
            }
        }
        
        return found;
    }

    /**
     * Prepare regular expression and initialize pattern.  
     * <p>
     * <b>Notes:</b>
     * <ul>
     * <li>Search for leading and trailing separators (/) and
     * strip the separators from the string.</li>
     * <li>Check for case-insensitive indicator (i).</li>
     * <li>Compile the pattern</li>
     * </ul>
     * 
     * @return List of compiled patterns
     */
    private List<Pattern> prepValues() 
    {
        List<Pattern> patterns = new ArrayList<>();    
    	
        this.regexList.stream().map((s) -> s.value()).forEach((checkValue) -> 
        {
            if (checkValue.startsWith("/") && (checkValue.endsWith("/") || checkValue.endsWith("/i"))) 
            {
                checkValue = checkValue.substring(1);
                Pattern p;
                if (checkValue.endsWith("i")) {
                    checkValue = checkValue.substring(0, checkValue.length() - 2);
                    p = Pattern.compile(checkValue, Pattern.CASE_INSENSITIVE);
                } else {
                    checkValue = checkValue.substring(0, checkValue.length() - 1);
                    p = Pattern.compile(checkValue, Pattern.CASE_INSENSITIVE);
                }

                patterns.add(p);
            } 
            else 
            {
            	try {Pattern p = Pattern.compile(checkValue); patterns.add(p);}
                catch (Exception e) {throw new RuntimeException(e.toString(), e);}
            }
        });
        
        return patterns;
    }
	
	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	private Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");
		
		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null").append("\n");
			isValid = false;
		}
		
		// Input list
		if(this.inputList == null)
		{
			sb.append("Input list is null").append("\n");
			isValid = false;
		}
		
		// REGEX list
		if(this.regexList == null)
		{
			sb.append("REGEX list is null").append("\n");
			isValid = false;
		}
		
		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
