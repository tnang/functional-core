/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.common.types.table.query.builder.WhereBuilder;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
public abstract class FilterTableRowBase implements Runnable {
    @Context
    ExecutionContext ctx;  
    
    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table from which to filter rows.")
    )
    public CV_Table inputTable;
    
    @Output(name = @CAPCO("Filtered Table"),
            desc = @CAPCO("A new table with the rows filtered.")
    )
    public CV_Table outputTable;
    
    @Input(name = @CAPCO("Column Name"),
            desc = @CAPCO("Column Name String containing values to filter against.")
    )
    public CV_String columnName;
    
   
    public void compare(CV_Super minValue, CV_Super maxValue, ParameterType type) {
        try {

            TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
            SelectBuilder selectBuilder = factory.selectBuilder();
            WhereBuilder whereBuilder = factory.whereBuilder();
            //Note that the columnheader type has to be well defined to a type (e.g. name{datatype}) in the table header.
            //here we are getting the index of the column we want to filter on
            int a = inputTable.getHeader().findIndex(columnName.value());
            //here we get the correct columnheader based on the index
            ColumnHeader column = (a < 0) ? null : inputTable.getHeader().getHeader(a);
            if (column != null && column.getType().equals(type)) {

                whereBuilder.and(factory.whereBuilder().gte(column, minValue), factory.whereBuilder().lte(column, maxValue));
                selectBuilder.where(whereBuilder).from(inputTable);
                outputTable = TableQueryExecutor.executeQuery(selectBuilder);

            } else {
                String msg = String.format("Invalid argument: colName=%s, Column=%s", columnName, column);
                throw new Exception(msg);
            }

        } catch (Exception ex) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Exception " + ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }

    }
    
}
//UNCLASSIFIED