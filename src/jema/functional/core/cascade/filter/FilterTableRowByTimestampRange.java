/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import jema.functional.core.cascade.filter.FilterTableRowBase;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.functional.api.Input;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Filter table by a range of timestamp values"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        name = @CAPCO("Filter Table By Timestamp Range"),
        uuid = "76c0613f-e13f-485d-b2a7-9fddbcf65d94",
        tags = {"table", "filter","decimal" }
        
)
public final class FilterTableRowByTimestampRange extends FilterTableRowBase {

    
    @Input(name = @CAPCO("Maximum Timestamp Value"),
            desc = @CAPCO("Maximum timestamp value to filter against")
    )
    public CV_Timestamp maxValue;
    
    @Input(name = @CAPCO("Minimum Timestamp Value"),
            desc = @CAPCO("Minimum timestamp value to filter against")
    )
    public CV_Timestamp minValue;


    @Override
    public void run() {
        
       compare(minValue,maxValue,ParameterType.timestamp);
    
    }
    
}
//UNCLASSIFIED