/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import jema.functional.core.cascade.util.SHA1;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jema.common.types.CV_Boolean;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Deduplicate Table Rows. Remove rows from a table based on duplicate values existing in a specified set of columns. "+
                "Rows containing the same values in the specified columns are considered duplicates. "+
                "The table is processed from top to bottom with successive duplicates removed. "+
                "Sort the table prior to use of this functional if you want to better control which duplicates get removed. "+
                "Precision specification: "+
                "timestampfield:2d,decimalfield:3 "+
                "Date precision is +/- seconds(s), minutes(m), hours(h), or days(d). "+
                "Numeric precision indicates the floating point precision used in the comparision."+
                "Rounding if set to TRUE will result in a 'HALF_UP' rounding operation which equates to the following decimal '1.225' at precision 2 to rounded up to 1.23"+
                "Rounding if set to FALSE will result in a 'FLOOR' rounding operation which equates to the following decimal '1.225' at precision 2 to be floored to 1.22"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        name = @CAPCO("Deduplicate Rows"),
        uuid = "e2c4abb2-78c1-45af-ac27-306bb16e3a75",
        tags = {"table", "filter", "dedup" }
        
)
public final class DeduplicateRows implements Runnable {
    private static final Pattern secPat = Pattern.compile("(\\d+)s");
    private static final Pattern minPat = Pattern.compile("(\\d+)m");
    private static final Pattern hourPat = Pattern.compile("(\\d+)h");
    private static final Pattern dayPat = Pattern.compile("(\\d+)d");
    private RoundingMode rounding=RoundingMode.FLOOR;

    @Context
    public ExecutionContext ctx;
    
    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table from which to filter rows based on duplicate values with other rows."),
            required = true
    )
    public CV_Table inputTable;
    
    @Input(name = @CAPCO("Column(s) Name and Precision"),
            desc = @CAPCO("The column(s) names and the precision level in this format : column:precision[,column:precision] (e.g. column1:4,column2:4d)."),
            required = true
    )
    public CV_String input_columns; // the string should resemble: "testcolumn1:4,testcolumn2:4";
    
    
    @Input(name = @CAPCO("Rounding?"),
            desc = @CAPCO("If True a HALF_UP rounding operation will be performed."
                    + "If False a FLOOR rounding operation will be performed."))
    public CV_Boolean isRounded = new CV_Boolean(false);

    @Output(name = @CAPCO("Filtered Table"),
            desc = @CAPCO("A new table with successive duplicate rows removed.")
    )
    public CV_Table outputTable;



    @Override
    public void run() {

        if (isRounded.value()) {
            rounding = RoundingMode.HALF_UP;
        }

        String[] column_input_strings = input_columns.value().split(",");
        try {
            
            //use the properties of the inputTable to create the outputTable.
            List<ColumnHeader> columns = new ArrayList<>();
            inputTable.getHeader().forEach(columnheader -> {
                columns.add(columnheader);
            });
            //create a new table 'outputTable' for the returned results.
            outputTable = ctx.createTable(inputTable.getMetadata().getLabel());
            //set the headers for the 'outputTable'
            outputTable.setHeader(new TableHeader(columns));
            outputTable.setMetadata(inputTable.getMetadata());
            
            //The Treemap will contain each column index in sorted order with the type of column and level of precision
            TreeMap<Integer,ColumnInfo> column_info_map = new TreeMap<>();
            for(int x=0;x<column_input_strings.length;x++)
            {
                String []a_column = column_input_strings[x].split(":");

                int columnheaderindex = inputTable.getHeader().findIndex(a_column[0]);
                if(columnheaderindex<0){
                     throw new IllegalArgumentException("The column '"+ a_column[0]+ "' could not be found in the input table.");
                }
                //get the type of values within the column
                ColumnInfo thiscolumn = new ColumnInfo();
                ParameterType type = columns.get(columnheaderindex).getType();
                //set the column type (eg decimal, timestamp, integer)
                thiscolumn.setType(type);
                //set the precision of each column
                if(type.equals(ParameterType.decimal)){
                    thiscolumn.setDecimalPrecision(Integer.parseInt(a_column[1]));
                }else if(type.equals(ParameterType.timestamp)){
                    thiscolumn.setDatePrecision(datePrecisionParser(a_column[1]));
                }
                //set the column index for the table
                thiscolumn.setIndex(columnheaderindex);
               
                column_info_map.put(x, thiscolumn);
            }

            //process each row in the table
            processTableRows(column_info_map);
            
        }catch(Exception ex){
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception "+ex.getMessage());
            throw new RuntimeException(ex);
        }
        
    }
    
    /**
    * Process the table for duplicates
    * @param column_info_map  information on the types of columns found in the rows
    */
    private void processTableRows(TreeMap<Integer,ColumnInfo> column_info_map) throws TableException {
        //In memory store of row hashes.
        Map<String,List<List<CV_Super>>> row_map = new HashMap<>();
        
        // for each row in the table add the elements from each column that we are filtering on.
        inputTable.stream().forEach((List<CV_Super> current_row) -> {

            try {
                
                //ctx.log(ExecutionContext.MsgLogLevel.WARNING,"Row index=" + input.getCurrentRowNum());
                  inputTable.readRow();

                  //Generate a row hash.
                  String hashed_row = createRowHash(current_row,column_info_map);
                  //check to see if the hashedrow is unique or not.
                   if(!row_map.containsKey(hashed_row)){
                       //hashrow is unique and entered in the outputtable and row_map for future lookup
                       outputTable.appendRow(current_row);
                       row_map.put(hashed_row, new ArrayList());
                       row_map.get(hashed_row).add(current_row);
                       //ctx.log(ExecutionContext.MsgLogLevel.INFO, "Appended a row to output table =" + current_row);

                    }else{//hashed_row is NOT unique
                       boolean isNew = false;
                       //for each column
                       for (Integer entry : column_info_map.keySet()) {
                           //iterate through the rows in the hashmap
                           for (List<CV_Super> rows : row_map.get(hashed_row)) {   
                               ColumnInfo thiscolumn = column_info_map.get(entry);
                               //We only need to process timestamp column values because the row hash takes care of all other types.
                               if (ParameterType.timestamp == thiscolumn.getType()) {
                                   //get the current row column value and the row column value from the map
                                   CV_Super current_row_column_cell = current_row.get(thiscolumn.getIndex());
                                   CV_Super hashed_row_column_cell = rows.get(thiscolumn.getIndex());
                                   //determine if duplicates
                                  // ctx.log(ExecutionContext.MsgLogLevel.INFO, "Are these duplicates?" + this_row_column_cell + "       " + this_current_row_column_cell + "at precesion " + thiscolumn.getDatePrecision());
                                   if (TimestampCompare((CV_Timestamp) current_row_column_cell, (CV_Timestamp) hashed_row_column_cell, thiscolumn.getDatePrecision())) {
                                   //    ctx.log(ExecutionContext.MsgLogLevel.INFO, "Yes");
                                       isNew = false;
                                       break;//no need to compare any additional hashed_rows

                                   } else {
                                    //   ctx.log(ExecutionContext.MsgLogLevel.INFO, "No");
                                       isNew = true;

                                   }
                               }
                           }
                       }
                       
                       if(isNew){
                           row_map.get(hashed_row).add(current_row);
                           outputTable.appendRow(current_row);
                           //ctx.log(ExecutionContext.MsgLogLevel.INFO,"Appended a row =" + current_row);
                       }
                   }
            } catch (TableException ex) {
                ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Exception "+ex.getMessage());             
            }  
        });
    }
   /**
    * Returns a String SHA1 has value of the input row
    * @param  row  value of the row to hash
    * @param  column_info_map  information on the types of columns found in this row
    * @return  String SHA1 hashed row value
    */
    private String createRowHash(List<CV_Super> row, TreeMap<Integer, ColumnInfo> column_info_map) {

        StringBuilder rowhash = new StringBuilder();
        
        for (Integer entry : column_info_map.keySet()) {
            ColumnInfo thiscolumn = column_info_map.get(entry);
            if (ParameterType.timestamp == thiscolumn.getType()) {
                // do not add this to the hash ... 
            } else if (ParameterType.decimal == thiscolumn.getType()) {
                CV_Super thiscolumnrowvalue = row.get(thiscolumn.getIndex());
                if(thiscolumnrowvalue!=null){
                    //convert to precision
                    BigDecimal bd1 = new BigDecimal((double) thiscolumnrowvalue.value()).setScale(thiscolumn.getDecimalPrecision(),rounding);
                    rowhash.append(bd1);
                }
            } else {
                CV_Super thiscolumnrowvalue = row.get(thiscolumn.getIndex());
                rowhash.append(thiscolumnrowvalue.value());
            }
        }

        SHA1 md = new SHA1();
        md.reset();
        md.update(rowhash.toString().getBytes());
        md.finish();
        
        return  md.digout();
    }

    /**
    * Returns a boolean of if the timetocheck param is in range of the timestamp_range param
    * @param  timetocheck  CV_Timestamp value to check
    * @param  timestamp_range CV_Timestamp value of the range
    * @param  amount long value of the amount of time for that range in seconds
    * @return  boolean result if time is in range.
    */
    public static boolean TimestampCompare(CV_Timestamp timetocheck, CV_Timestamp timestamp_range, long amount) {
        //this accounts for a potential null value
        if(timetocheck==null){
            return false;
        }

        Instant low = timestamp_range.value().minus(amount, ChronoUnit.SECONDS);
        Instant high = timestamp_range.value().plus(amount, ChronoUnit.SECONDS);

        return low.isBefore(timetocheck.value()) && high.isAfter(timetocheck.value());
    }
    
    /**
    * Returns a value in the amount of seconds that represent the input string.
    *
    * @param  fmt  string representation of the amount of time for the range
    * @return  long value of the time in seconds
    */
    private long datePrecisionParser(String fmt) {
        long secs = 0;
        Matcher m = secPat.matcher(fmt);
        if (m.matches()) {
            secs += Integer.parseInt(m.group(1));
        }

        m = minPat.matcher(fmt);

        if (m.matches()) {
            secs += 60 * Integer.parseInt(m.group(1));
        }

        m = hourPat.matcher(fmt);

        if (m.matches()) {
            secs += 3600 * Integer.parseInt(m.group(1));
        }

        m = dayPat.matcher(fmt);

        if (m.matches()) {
            secs += 86400 * Integer.parseInt(m.group(1));
        }
        return secs;
    }
        
}
//Simple class to encapsulte properties of a column in a table.
class ColumnInfo {
    
    private ParameterType type;
    private long date_precision;
    private int decimal_precision;
    private int index;
    /**
     * @return the type
     */
    public ParameterType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ParameterType type) {
        this.type = type;
    }

    /**
     * @return the precision
     */
    public long getDatePrecision() {
        return date_precision;
    }

    /**
     * @param precision the precision to set
     */
    public void setDatePrecision(long precision) {
        this.date_precision = precision;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }


    /**
     * @return the decimal_precision
     */
    public int getDecimalPrecision() {
        return decimal_precision;
    }

    /**
     * @param decimal_precision the decimal_precision to set
     */
    public void setDecimalPrecision(int decimal_precision) {
        this.decimal_precision = decimal_precision;
    }
    
}
//UNCLASSIFIED