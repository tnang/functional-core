/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;


import jema.functional.core.cascade.filter.FilterTableRowBase;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Decimal;
import jema.common.types.ParameterType;
import jema.functional.api.Input;



/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Filter table by a range of decimal values"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        name = @CAPCO("Filter Table By Decimal Range"),
        uuid = "5a86771c-30b7-4a99-8c6f-95a39f2aee84",
        tags = {"table", "filter","decimal" }
        
)
public final class FilterTableRowByDecimalRange extends FilterTableRowBase {

   
    @Input(name = @CAPCO("Maximum Decimal Value"),
            desc = @CAPCO("Maximum decimal value to filter against")
    )
    public CV_Decimal maxValue;
    
    @Input(name = @CAPCO("Minimum Decimal Value"),
            desc = @CAPCO("Minimum decimal value to filter against")
    )
    public CV_Decimal minValue;
    
    
    @Override
    public void run() {
        
       compare(minValue,maxValue,ParameterType.decimal);
    
    }

}
//UNCLASSIFIED