/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.filter;

import java.util.ArrayList;
import java.util.List;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Filter For Odd Or Even Table Rows"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        name = @CAPCO("Filter Table By Odd Or Even Rows"),
        uuid = "6d7ef976-be41-4b67-b96c-66336055cbed",
        tags = {"table", "filter", "odd", "even" }
        
)
public final class FilterTableOddEven implements Runnable {


    @Context
    public ExecutionContext ctx;
    
    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table from which to filter rows.")
    )
    public CV_Table inputTable;
    
    @Input(name = @CAPCO("Filter for Even or Odd Rows"),
            desc = @CAPCO("True for returning 'Even' rows and False for returning 'Odd' rows")
    )
    public CV_Boolean even;

    @Output(name = @CAPCO("Filtered Table"),
            desc = @CAPCO("A new table with the rows filtered.")
    )
    public CV_Table outputTable;

    private  int z = 1;
    @Override
    public void run() {
        try{
            outputTable = ctx.createTable(inputTable.getMetadata().getLabel());
            List<ColumnHeader> columns = new ArrayList<>();
            inputTable.getHeader().forEach(columnheader -> {
                columns.add(columnheader);
            });
            outputTable.setHeader(new TableHeader(columns));
           
            //Iterate over each row in the input table
            inputTable.stream().forEach(row -> {
                try {
                    if (z % 2 == 0) {
                        //Even rows     
                        if(even.value()){
                            outputTable.appendRow(row);
                        }
                    } else {
                         //Odd rows
                        if(!even.value()){
                            outputTable.appendRow(row);
                        }
                    }
                } catch (TableException ex) {
                    ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception "+ex.getMessage());
                }
                z++;
            });
        
        }catch(Exception ex){
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception "+ex.getMessage());
            throw new RuntimeException(ex);
        }
        
    }
}
//UNCLASSIFIED