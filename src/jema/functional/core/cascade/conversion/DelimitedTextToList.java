// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 23, 2015
 */

package jema.functional.core.cascade.conversion;


import java.io.IOException;
import java.util.ArrayList;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.joining;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_WebResource;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;


/**
 *
 * @author cascade
 */

@Functional(
        uuid = "b1ccef38-04cc-4d02-af29-9a90022a9056",
        name = @CAPCO("Text File to List"),
        desc = @CAPCO("This takes a text file and turns it into a list based on a delimiter. "
                + "New lines also act as delimiters. If you do not input a delimiter your "
                + "input will only be delimited with new lines. If you select the option to "
                + "remove the top line from your file it will remove the first item in the "
                + "resulting list, whether from new line delimination or from string "
                + "delimination. Finally, you may also choose to delete the top N lines from "
                + "your list on output, this will option is overwritten by the first line "
                + "option, if both are selected."),

        displayPath = {"Conversion", "File Format", "Text File"},
        tags = {"Conversion", "File Format", "Text File", "text", "delimited", "list"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public final class DelimitedTextToList implements Runnable {

    @Context
    public ExecutionContext ctx; // i know.
    
    @Input(
            name = @CAPCO("Input .txt file"),
            desc = @CAPCO("Input a text file (.txt)."),
            metadata = {"text/plain", "text/*"},
            required = false
    )
    public CV_WebResource input_textFile; 
    
    @Input(
            name = @CAPCO("Delimiter"),
            desc = @CAPCO("This is a character or string used as a delimiter"),
            required = false
    )
    public CV_String input_strDelimiter;
    
    @Input(
            name = @CAPCO("Delete top row"),
            desc = @CAPCO("Choose to delete top row of textfile"),
            required = true
    )
    public CV_Boolean input_boolHeader = new CV_Boolean(false);

    @Input(
            name = @CAPCO("Delete top X rows"),
            desc = @CAPCO("Choose to delete top X rows of textfile. Zero is default value."),
            required = false
    )
    public CV_Integer input_intTopLines = new CV_Integer(0);
    
    @Input(
            name = @CAPCO("Exclude empty values"),
            desc = @CAPCO("Removes empty values from output list"),
            required = true
    )
    public CV_Boolean input_excludeEmpty = new CV_Boolean(false);
    
    @Output(
            name = @CAPCO("Output List"),
            desc = @CAPCO("List output")
    )
    public List<CV_String> outputList;
    
    @Output(
            name = @CAPCO("debug"),
            desc = @CAPCO("debug")
    )
    public CV_String debugOutput;    
    
    public int deleter; //this is created way up here because it was giving me errors in other places
       
    @Override
    public void run() {

        if (input_textFile == null) {
            throw new IllegalStateException("You have not added an input file!");
        }
        
        //if (input_textFile.value().)
        
        // set up lists, not sure if lines and protoLines need to be set up beforehand but doing it anyways
        List<CV_String> tempList = new ArrayList<>();
        List<String> lines;
        List<String> protoLines;
        List<String> extraLines;
        List<String> secondaryLines = new ArrayList<>();
        
        int toDelete = input_intTopLines.value().intValue(); // i type this so many times, i'm just making it shorter
        
        if (input_boolHeader.value() && toDelete>0) {
            throw new IllegalStateException("You have selected 'Delete top row' while 'delete top X rows' is set to higher than 0! Please use one option or the other");
        }
        
        if (toDelete<0) {
            throw new IllegalStateException("You have attempted to delete a negative number(" + Integer.toString(toDelete) + ") of rows from the top!");
        }
        
        
        // figure out how many lines, if any, to delete from the top of the list once created
        if (input_boolHeader.value()) {
            deleter = 1;
        } else if (toDelete>0) {
            deleter = toDelete;
        } else {
            deleter = 0;
        }

        try {
            URI uri = input_textFile.getURI(); // get file
            protoLines = Files.readAllLines(Paths.get(uri)); // delimit input file by new line characters
            Boolean exclude = input_excludeEmpty.value();
            
            
            if (input_strDelimiter != null) { // adding next delimiter
                String delimit = input_strDelimiter.value(); // creates delimiter
                String hi = protoLines.stream().collect(joining(delimit)); //joins together text and adds delimiter between joined elements
                debugOutput = new CV_String(hi);
                
                extraLines = Arrays.asList(protoLines.stream().collect(joining(delimit)).split(delimit)); // splits back up on original and on added delimiters
                
                if (exclude) { // removes empty and null values from list
                    for (String each: extraLines) {
                        if (each != null && !each.isEmpty()) {
                            secondaryLines.add(each);
                        }
                    }    
                } else secondaryLines = extraLines;
                
            } else { // moving along when no extra delimiter has been added
                secondaryLines = protoLines;
            }
            
            int end = secondaryLines.size(); // get list length for ranges
            
            if (toDelete>end) {
                throw new IllegalStateException("You have attempted to delete more rows(" + Integer.toString(toDelete) + ") than actually exist(" + Integer.toString(end) + ") !");
            }
            
            lines = secondaryLines.subList(deleter, end); // delete top lines if needed

                
            // convert List<String> to List<CV_String>
            lines.stream().forEach((x) -> tempList.add(new CV_String(x)));
            
            
        } catch (IOException ex) { // catch for the URI get, i did this block to prevent lots of squiggly lines
            Logger.getLogger(DelimitedTextToList.class.getName()).log(Level.SEVERE, null, ex);
        }    
           
        outputList = tempList; // i like keeping this separate
        
    }
    
}

// UNCLASSIFIED
