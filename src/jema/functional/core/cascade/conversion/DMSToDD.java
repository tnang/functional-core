/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 31, 2015 10:56:08 AM
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a column of coordinates in a table from Degrees Minutes Seconds (DMS) format "
                + "to two new columns of latitude and longitude coordinates in Decimal Degrees (DD) format. "
                + "Allows N, S, E, W suffixes and signs. "
                + "Example formats: "
                + "301557.1926N 0795856.0W, +301557.1926 -0795856.0, 36°10'30\"S 115°08'11E\", -36°10'30\" 115°08'11\""),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("DMS -> DD"),
        uuid = "56e80c56-e8ba-4b1a-85b1-53b2d889a706",
        tags = {"conversion", "DMS", "DD", "coordinate"}
)
public final class DMSToDD implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(DMSToDD.class.getName());

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table containing the column of coordinates (DMS format) for conversion."),
            required = true
    )
    public CV_Table inputTable;

    @Input(name = @CAPCO("DMS Column Name"),
            desc = @CAPCO("The name of the column containing the coordinates (DMS format) for conversion. "
                    + "Example formats: "
                    + "301557.1926N 0795856.0W, +301557.1926 -0795856.0, 36°10'30\"S 115°08'11E\", -36°10'30\" 115°08'11\""),
            required = true
    )
    public CV_String columnNameDMS;

    @Input(name = @CAPCO("DD Latitude Output Column Name"),
            desc = @CAPCO("The name of the new column containing the converted latitude (DD format). "
                    + "If the supplied column exists in the table, column data will be overwritten. "
                    + "Default Column Name: DD Lat Output"),
            required = true
    )
    public CV_String latColumnNameDD = new CV_String("DD Lat Output");

    @Input(name = @CAPCO("DD Longitude Output Column Name"),
            desc = @CAPCO("The name of the new column containing the converted longitude (DD format). "
                    + "If the supplied column exists in the table, column data will be overwritten. "
                    + "Default Column Name: DD Lon Output"),
            required = true
    )
    public CV_String lonColumnNameDD = new CV_String("DD Lon Output");

    @Output(name = @CAPCO("Output Table"),
            desc = @CAPCO("The original table with two additional columns containing "
                    + "the converted latitude and longitude (DD format).")
    )
    public CV_Table result;

    @Override
    public void run() {
        try {
            //Get index of DMS formatted column
            int columnIndex = inputTable.findIndex(columnNameDMS.value());
            if (columnIndex == -1) {
                throw new RuntimeException("The DMS Column: " + columnNameDMS + " is not in the table.");
            }

            //Get index of lat and lon DD columns and check parameter type
            List<ColumnHeader> headers = inputTable.getHeader().asList();
            int latColumnIndex = inputTable.findIndex(latColumnNameDD.value());
            int lonColumnIndex = inputTable.findIndex(lonColumnNameDD.value());
            if (latColumnIndex != -1) {
                ParameterType type = headers.get(latColumnIndex).getType();
                if (!type.equals(ParameterType.decimal)) {
                    throw new RuntimeException("The DD Lat Output Column exists, but has an incorrect type: " + type.toString() + ". Expected type: decimal");
                }
            }
            if (lonColumnIndex != -1) {
                ParameterType type = headers.get(lonColumnIndex).getType();
                if (!type.equals(ParameterType.decimal)) {
                    throw new RuntimeException("The DD Lon Output Column exists, but has an incorrect type: " + type.toString() + ". Expected type: decimal");
                }
            }

            //For each row in the table add the elements from the column to a list
            List<String> columnValues = new ArrayList<>();
            inputTable.stream().forEach(row -> {
                if (row.get(columnIndex) != null) {
                    columnValues.add(row.get(columnIndex).toString());
                } else {
                    columnValues.add("");
                }
            });

            List<CV_Decimal> latConvertedValues = new ArrayList<>();
            List<CV_Decimal> lonConvertedValues = new ArrayList<>();

            int rowFailCount = 0;
            int latFailCount = 0;
            int lonFailCount = 0;

            //For each DMS formatted string, split into lat & lon and convert to Decimal Degrees
            CoordinateConversion converter = new CoordinateConversion();
            for (String dmsString : columnValues) {
                String[] coords = dmsString.split(" ");
                if (coords.length == 2) {
                    try {
                        Double latVal = converter.DMStoDD(coords[0]);
                        if (latVal != null) {
                            latConvertedValues.add(new CV_Decimal(latVal));
                        } else {
                            latConvertedValues.add(null);
                        }
                    } catch (IllegalArgumentException ex) {
                        latFailCount++;
                        latConvertedValues.add(null);
                    }

                    try {
                        Double lonVal = converter.DMStoDD(coords[1]);
                        if (lonVal != null) {
                            lonConvertedValues.add(new CV_Decimal(lonVal));
                        } else {
                            lonConvertedValues.add(null);
                        }
                    } catch (IllegalArgumentException ex) {
                        lonFailCount++;
                        lonConvertedValues.add(null);
                    }
                } else {
                    rowFailCount++;
                    latConvertedValues.add(null);
                    lonConvertedValues.add(null);
                }
            }

            //Notify user of number of rows that were not converted.
            if (rowFailCount > 0 || latFailCount > 0 || lonFailCount > 0) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, String.valueOf(rowFailCount) + " row(s) could not be converted due to empty value or invalid format.");
                ctx.log(ExecutionContext.MsgLogLevel.INFO, String.valueOf(latFailCount) + " latitude value(s) not within the range -90 to 90.");
                ctx.log(ExecutionContext.MsgLogLevel.INFO, String.valueOf(lonFailCount) + " longitude value(s) not within the range -180 to 180.");
            } else {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "All rows were converted successfully.");
            }

            //Add new lat and lon DD columns if they do not exist
            ParameterType paramType = ParameterType.decimal;
            if (latColumnIndex == -1) {
                headers.add(new ColumnHeader(latColumnNameDD.value(), paramType));
            }
            if (lonColumnIndex == -1) {
                headers.add(new ColumnHeader(lonColumnNameDD.value(), paramType));
            }

            //Create the new writable table
            CV_Table newTable = ctx.createTable(inputTable.getMetadata().getLabel().getValue() + "_DMStoDD");
            newTable.setHeader(new TableHeader(headers));

            //Insert the converted lat and lon values into the appropriate column
            inputTable.stream().forEach(row -> {
                try {
                    CV_Decimal latVal = latConvertedValues.get(inputTable.getCurrentRowNum());
                    if (latColumnIndex == -1) {
                        row.add(latVal);
                    } else {
                        row.set(latColumnIndex, latVal);
                    }

                    CV_Decimal lonVal = lonConvertedValues.get(inputTable.getCurrentRowNum());
                    if (lonColumnIndex == -1) {
                        row.add(lonVal);
                    } else {
                        row.set(lonColumnIndex, lonVal);
                    }

                    newTable.appendRow(row);
                    inputTable.readRow();
                } catch (TableException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            });

            result = newTable.toReadable();
        } catch (RuntimeException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            String message = "Error converting DMS to DD: ";
            logger.log(Level.SEVERE, message + ex.getMessage(), ex);
            throw new RuntimeException(message + ex.getMessage(), ex);
        }
    }
}

//UNCLASSIFIED
