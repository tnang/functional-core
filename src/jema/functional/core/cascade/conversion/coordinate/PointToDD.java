/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 2:59:45 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a Point coordinate to a latitude and longitude coordinate in decimal degrees."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("Point -> DD"),
        uuid = "97bac536-3f49-442b-bef5-d67b4d08b44e",
        tags = {"Point", "DD"}
)
public final class PointToDD implements Runnable {

    @Input(name = @CAPCO("Point"),
            desc = @CAPCO("The Point coordinate for conversion."),
            required = true
    )
    public CV_Point point;

    @Output(name = @CAPCO("DD Latitude"),
            desc = @CAPCO("The converted latitude coordinate (DD format).")
    )
    public CV_Decimal latitude;

    @Output(name = @CAPCO("DD Longitude"),
            desc = @CAPCO("The converted longitude coordinate (DD format).")
    )
    public CV_Decimal longitude;

    @Override
    public void run() {
        latitude = new CV_Decimal(point.getY());
        longitude = new CV_Decimal(point.getX());
    }
}

//UNCLASSIFIED
