/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 1:02:16 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts columns of latitude and longitude coordinates in a table from Decimal Degrees (DD) format "
                + "to a new column of coordinates in Degrees Minutes Seconds (DMS) format. "
                + "Example format: 18 N 362290E 4349429N"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("DD -> UTM"),
        uuid = "281928b2-8c0b-4a47-89eb-1e0f1e4d6f9c",
        tags = {"DD", "UTM"}
)
public final class DDToUTM extends DDToBase implements Runnable {

    @Input(name = @CAPCO("UTM Output Column Name"),
            desc = @CAPCO("The name of the new column containing the converted coordinates (UTM format). "
                    + "If the supplied column exists in the table, column data will be overwritten. "
                    + "Default Column Name: UTM"),
            required = true
    )
    public CV_String outColumnName = new CV_String("UTM");

    private static final Logger logger = Logger.getLogger(DDToDMS.class.getName());

    @Override
    public void run() {
        findAndVerifyColumnsExist();

        //Get index of output column and check parameter type
        List<ColumnHeader> headers = inputTable.getHeader().asList();
        int outColumnIndex = inputTable.findIndex(outColumnName.value());
        if (outColumnIndex != -1) {
            ParameterType type = headers.get(outColumnIndex).getType();
            if (!type.equals(ParameterType.string)) {
                throw new RuntimeException("The Output Column exists, but has an incorrect type: " + type.toString()
                        + ". Expected type: string");
            }
        }

        //Add new output column if it does not exist
        if (outColumnIndex == -1) {
            headers.add(new ColumnHeader(outColumnName.value(), ParameterType.string));
        }

        try {
            //Create the new writable table
            CV_Table output = ctx.createTable(inputTable.getMetadata().getLabel().getValue() + "_DMS");
            output.setHeader(new TableHeader(headers));

            //Insert the converted UTM strings into the appropriate column
            int rowFailCount = 0;
            String latitude = "";
            String longitude = "";
            CV_String utmString;

            for (int i = 0; i < inputTable.size(); i++) {
                try {
                    List<CV_Super> row = inputTable.readRow();
                    List<CV_Super> newRow = row;

                    if (row.get(latIndex) != null) {
                        latitude = row.get(latIndex).toString();
                    } else {
                        latitude = "";
                    }

                    if (row.get(lonIndex) != null) {
                        longitude = row.get(lonIndex).toString();
                    } else {
                        longitude = "";
                    }

                    //Convert Decimal Degree coordinate pair to Degrees Minutes Seconds (DMS)
                    try {
                        utmString = convertCoordinates(latitude, longitude);
                    } catch (IllegalArgumentException ex) {
                        rowFailCount++;
                        utmString = new CV_String("");
                    }

                    if (outColumnIndex == -1) {
                        newRow.add(utmString);
                    } else {
                        newRow.set(outColumnIndex, utmString);
                    }

                    output.appendRow(newRow);
                } catch (TableException ex) {
                    String message = "Unexpected Error has occured: " + ex.getMessage();
                    ctx.log(ExecutionContext.MsgLogLevel.WARNING, message);
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            }

            result = output.toReadable();

            //Notify user of number of rows that were not converted
            if (rowFailCount > 0) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, String.valueOf(rowFailCount) + " row(s) could not be converted due to empty value or invalid format.");
            } else {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "All rows were converted successfully.");
            }
        } catch (Exception ex) {
            String message = "Error converting DD to DMS: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new RuntimeException(message, ex);
        }
    }

    /**
     * Converts Decimal Degree latitude and longitude coordinates to Degrees
     * Minutes Seconds
     *
     * @param lat
     * @param lon
     * @return UTM string
     */
    private CV_String convertCoordinates(String lat, String lon) {
        CoordinateConversion converter = new CoordinateConversion();
        return new CV_String(converter.DDtoUTM(Double.parseDouble(lat), Double.parseDouble(lon)));
    }
}

//UNCLASSIFIED
