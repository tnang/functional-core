/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 14, 2015 11:37:11 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a Point coordinate to a DMS coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("Point -> DMS"),
        uuid = "63f26972-9ba4-41c9-8957-65ec648246a7",
        tags = {"Point", "DMS"}
)
public final class PointToDMS implements Runnable {

    @Input(name = @CAPCO("Point"),
            desc = @CAPCO("The Point for conversion."),
            required = true
    )
    public CV_Point point;

    @Output(name = @CAPCO("DMS"),
            desc = @CAPCO("The converted coordinate in DMS format."))
    public CV_String dms;

    @Override
    public void run() {
        CoordinateConversion cc = new CoordinateConversion();
        String dmsLat = cc.DDtoDMS(point.getY(), false);
        String dmsLon = cc.DDtoDMS(point.getX(), true);
        dms = new CV_String(dmsLat + " " + dmsLon);
    }
}

//UNCLASSIFIED
