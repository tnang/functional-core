/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 2:27:25 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a Point coordinate to a UTM coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("Point -> UTM"),
        uuid = "4e8d61b6-9227-4b72-8f2c-4ef12907351f",
        tags = {"Point", "UTM"}
)
public final class PointToUTM implements Runnable {

    private static final Logger logger = Logger.getLogger(PointToUTM.class.getName());

    @Input(name = @CAPCO("Point"),
            desc = @CAPCO("The Point for conversion."),
            required = true
    )
    public CV_Point point;

    @Output(name = @CAPCO("UTM"),
            desc = @CAPCO("The converted coordinate in UTM format."))
    public CV_String utm;

    @Override
    public void run() {
        try {
            CoordinateConversion cc = new CoordinateConversion();
            utm = new CV_String(cc.DDtoUTM(point.getY(), point.getX()));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}

//UNCLASSIFIED
