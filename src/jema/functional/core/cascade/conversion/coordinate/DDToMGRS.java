/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts columns of latitude and longitude coordinates in a table from Decimal Degrees (DD) format "
                + "to a new column of coordinates in Military Grid Reference System (MGRS) format. "
                + "Example format: 30.155719 -79.58560"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("DD -> MGRS"),
        uuid = "9d55a423-0b65-4015-93f0-30db4eafb7ea",
        tags = {"conversion", "DD", "MGRS", "coordinate"}
)
public class DDToMGRS extends DDToBase implements Runnable {

    @Input(name = @CAPCO("MGRS Output Column Name"),
            desc = @CAPCO("The name of the new column containing the converted coordinates (MGRS format). "
                    + "If the supplied column exists in the table, column data will be overwritten. "
                    + "Default Column Name: MGRS"),
            required = true
    )
    public CV_String outColumnName = new CV_String("MGRS");

    private static final Logger logger = Logger.getLogger(DDToMGRS.class.getName());

    @Override
    public void run() {

        findAndVerifyColumnsExist();

        //Get index of output column and check parameter type
        List<ColumnHeader> headers = inputTable.getHeader().asList();
        int outColumnIndex = inputTable.findIndex(outColumnName.value());
        if (outColumnIndex != -1) {
            ParameterType type = headers.get(outColumnIndex).getType();
            if (!type.equals(ParameterType.string)) {
                throw new RuntimeException("The Output Column exists, but has an incorrect type: " + type.toString()
                        + ". Expected type: string");
            }
        }

        //Add new output column if it does not exist
        if (outColumnIndex == -1) {
            headers.add(new ColumnHeader(outColumnName.value(), ParameterType.string));
        }

        try {
            //Create the new writable table
            CV_Table output = ctx.createTable(inputTable.getMetadata().getLabel().getValue() + "_MGRS");
            output.setHeader(new TableHeader(headers));

            int rowFailCount = 0;
            String latitude = "";
            String longitude = "";
            CV_String mgrsString;

            // Loop through the table and put the results in a new table
            for (int i = 0; i < inputTable.size(); i++) {
                try {
                    List<CV_Super> row = inputTable.readRow();
                    List<CV_Super> newRow = row;

                    if (row.get(latIndex) != null) {
                        latitude = row.get(latIndex).toString();
                    } else {
                        latitude = "";
                    }

                    if (row.get(lonIndex) != null) {
                        longitude = row.get(lonIndex).toString();
                    } else {
                        longitude = "";
                    }

                    try {
                        mgrsString = convertCoordinates(latitude, longitude);
                    } catch (IllegalArgumentException ex) {
                        rowFailCount++;
                        mgrsString = new CV_String("");
                    }

                    //Convert Decimal Degree coordinate pair to MGRS
                    if (outColumnIndex == -1) {
                        newRow.add(mgrsString);
                    } else {
                        newRow.set(outColumnIndex, mgrsString);
                    }

                    output.appendRow(newRow);
                } catch (NumberFormatException | TableException ex) {
                    String message = "Unexpected Error has occured: " + ex.getMessage();
                    ctx.log(ExecutionContext.MsgLogLevel.WARNING, message);
                    throw new RuntimeException(message, ex);
                }
            }

            result = output.toReadable();

            //Notify user of number of rows that were not converted
            if (rowFailCount > 0) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, String.valueOf(rowFailCount) + " row(s) could not be converted due to empty value or invalid format.");
            } else {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "All rows were converted successfully.");
            }
        } catch (Exception ex) {
            String message = "Error converting DD to MGRS: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new RuntimeException(message, ex);
        }
    }

    private CV_String convertCoordinates(String lat, String lon) {
        CoordinateConversion cc = new CoordinateConversion();
        return new CV_String(cc.DDtoMGRS(Double.parseDouble(lat), Double.parseDouble(lon)));
    }
}
