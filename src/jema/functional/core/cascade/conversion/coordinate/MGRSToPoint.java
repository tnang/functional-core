/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 26, 2015
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 *
 * @author cascade
 */
@Functional(
        uuid = "de3a1db8-cb59-41fb-b4ee-5295fc16a8e5",
        name = @CAPCO("MGRS -> Point"),
        desc = @CAPCO("Converts a single MGRS coordinate to a single Point coordinate."),
        displayPath = {"Conversion", "Coordinate"},
        tags = {"conversion", "point", "MGRS", "coordinate", "coords"},
        lifecycleState = LifecycleState.TESTING,
        creator = "cascade"
)

public class MGRSToPoint implements Runnable {

    @Input(
            name = @CAPCO("MGRS"),
            desc = @CAPCO("The MGRS coordinate for conversion."),
            required = true
    )
    public CV_String mgrs;

    @Output(
            name = @CAPCO("Point"),
            desc = @CAPCO("The converted Point coordinate.")
    )
    public CV_Point point;

    @Override
    public void run() {
        CoordinateConversion cc = new CoordinateConversion();
        double[] coords = cc.MGRStoDD(mgrs.value());
        point = new CV_Point(coords[1], coords[0]);
    }
}

// UNCLASSIFIED
