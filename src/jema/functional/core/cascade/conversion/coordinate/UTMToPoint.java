/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 1:21:35 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.logging.Logger;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a coordinate in UTM format to a Point coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("UTM -> Point"),
        uuid = "0fb73ac0-155b-447c-bc73-349190505cd7",
        tags = {"UTM", "Point"}
)
public final class UTMToPoint implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(UTMToPoint.class.getName());

    @Input(name = @CAPCO("UTM"),
            desc = @CAPCO("The coordinate (UTM format) for conversion."),
            required = true
    )
    public CV_String utm;

    @Output(name = @CAPCO("Point"),
            desc = @CAPCO("The converted Point coordinate."))
    public CV_Point point;

    @Override
    public void run() {
        try {
            CoordinateConversion cc = new CoordinateConversion();
            double[] coords = cc.UTMToDD(utm.value());
            point = new CV_Point(coords[1], coords[0]);
        } catch (NumberFormatException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}

//UNCLASSIFIED
