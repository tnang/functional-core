/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 20, 2015 11:34:18 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
public abstract class DDToBase {

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table containing the latitude and longitude coordinate columns (DD format) for conversion."),
            required = true
    )
    public CV_Table inputTable;

    @Input(name = @CAPCO("DD Latitude Column Name"),
            desc = @CAPCO("The name of the column containing the latitude coordinate (DD format) for conversion."),
            required = true
    )
    public CV_String latColumnNameDD;

    @Input(name = @CAPCO("DD Longitude Column Name"),
            desc = @CAPCO("The name of the column containing the longitude coordinate (DD format) for conversion."),
            required = true
    )
    public CV_String lonColumnNameDD;

    @Output(name = @CAPCO("Output Table"),
            desc = @CAPCO("The original table with an additional column containing the converted coordinates.")
    )
    public CV_Table result;

    @Context
    public ExecutionContext ctx;

    protected int latIndex, lonIndex;

    /**
     * Find lat and lon DD formatted columns and verify they exist.
     */
    public void findAndVerifyColumnsExist() {

        latIndex = inputTable.findIndex(latColumnNameDD.value());
        lonIndex = inputTable.findIndex(lonColumnNameDD.value());

        if (latIndex == -1) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "The DD Latitude Column: " + latColumnNameDD + " is not in the table.");
            throw new RuntimeException("The DD Latitude Column: " + latColumnNameDD + " is not in the table.");
        }

        if (lonIndex == -1) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "The DD Longitude Column: " + lonColumnNameDD + " is not in the table.");
            throw new RuntimeException("The DD Longitude Column: " + lonColumnNameDD + " is not in the table.");
        }
    }
}

//UNCLASSIFIED
