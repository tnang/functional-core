/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 11:36:35 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a coordinate in decimal degrees to a Point coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("DD -> Point"),
        uuid = "419d7330-954c-49ca-8aea-2d8089e52479",
        tags = {"conversion", "DD", "Point", "coordinate"}
)
public final class DDToPoint implements Runnable {

    @Input(name = @CAPCO("DD Latitude"),
            desc = @CAPCO("The latitude coordinate (DD format) for conversion."),
            required = true
    )
    public CV_Decimal latitude;

    @Input(name = @CAPCO("DD Longitude"),
            desc = @CAPCO("The longitude coordinate (DD format) for conversion."),
            required = true
    )
    public CV_Decimal longitude;

    @Output(name = @CAPCO("Point"),
            desc = @CAPCO("The converted Point coordinate."))
    public CV_Point point;

    @Override
    public void run() {
        point = new CV_Point(longitude.value(), latitude.value());
    }
}

//UNCLASSIFIED
