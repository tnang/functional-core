/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 14, 2015 11:48:59 AM
 */
package jema.functional.core.cascade.conversion.coordinate;

import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a coordinate in DMS format to a Point coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("DMS -> Point"),
        uuid = "74e86a60-8d02-4975-9fc9-7c5fd6c96ab0",
        tags = {"DMS", "Point"}
)
public final class DMSToPoint implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(DMSToPoint.class.getName());

    @Input(name = @CAPCO("DMS"),
            desc = @CAPCO("The coordinate (DMS format) for conversion."),
            required = true
    )
    public CV_String dms;

    @Output(name = @CAPCO("Point"),
            desc = @CAPCO("The converted Point coordinate."))
    public CV_Point point;

    @Override
    public void run() {
        try {
            CoordinateConversion cc = new CoordinateConversion();

            String[] coords = dms.value().split(" ");
            if (coords.length == 2) {
                point = new CV_Point(cc.DMStoDD(coords[1]), cc.DMStoDD(coords[0]));
            } else {
                throw new IllegalArgumentException("Input is invalid format.");
            }
        } catch (IllegalArgumentException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            String message = "Error converting DMS to Point: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new RuntimeException(message, ex);
        }
    }
}

//UNCLASSIFIED
