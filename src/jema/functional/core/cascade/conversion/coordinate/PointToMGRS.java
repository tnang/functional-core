/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Dec 12, 2015 2:29:54 PM
 */
package jema.functional.core.cascade.conversion.coordinate;

import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a Point coordinate to a MGRS coordinate."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Coordinate"},
        name = @CAPCO("Point -> MGRS"),
        uuid = "6688c485-a1c0-4214-8d5f-31b8d3fe5859",
        tags = {"Point", "MGRS"}
)
public final class PointToMGRS implements Runnable {

    @Input(name = @CAPCO("Point"),
            desc = @CAPCO("The Point for conversion."),
            required = true
    )
    public CV_Point point;

    @Output(name = @CAPCO("MGRS"),
            desc = @CAPCO("The converted coordinate in MGRS format."))
    public CV_String mgrs;

    @Override
    public void run() {
        CoordinateConversion cc = new CoordinateConversion();
        mgrs = new CV_String(cc.DDtoMGRS(point.getY(), point.getX()));
    }
}

//UNCLASSIFIED
