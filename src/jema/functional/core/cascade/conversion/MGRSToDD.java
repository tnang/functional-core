/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 25, 2015
 */

package jema.functional.core.cascade.conversion;


import java.util.ArrayList;
import java.util.List;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.core.cascade.util.geo.coords.MGRSCoord;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "59149057-519c-4cb4-91b4-a268c7ee2738",
        name = @CAPCO("MGRS -> DD"),
        desc = @CAPCO(  "Converts MGRS to DD. Input a table with a column containing MGRS values, " +
                        "and/or input a single MGRS value in the String input. There cannot be any" +
                        " input column headers labeled as 'longitude' or 'latitude'"),

        displayPath = {"Conversion", "Coordinate"},
        tags = {"conversion", "DD", "MGRS", "coordinate", "degrees", "decimal", "coords"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public class MGRSToDD implements Runnable {

    @Input(
            name = @CAPCO("Input MGRS"),
            desc = @CAPCO("Input MGRS String"),
            required = false
    )
    public CV_String inputString_mgrs; 
    
    @Input(
            name = @CAPCO("Input Table"),
            desc = @CAPCO("The table containing a coordinate column (MGRS format) for conversion to DD."),
            required = false
    )
    public CV_Table inputTable;
    
    @Input(
            name = @CAPCO("Input MGRS Column Name"),
            desc = @CAPCO("Input MGRS Column Name"),
            required = false
    )
    public CV_String inputString_column; 
    
    @Output(
            name = @CAPCO("Output"),
            desc = @CAPCO("Output")
    )
    public CV_String output_string;  
    
    @Output(
            name = @CAPCO("Output"),
            desc = @CAPCO("Output")
    )
    public CV_Table output_table;  

    // convert mgrs to dd
    public static double[] latLonFromMgrs(String mgrs){
	MGRSCoord coordinate = MGRSCoord.fromString(mgrs);
	return new double[] { 
            coordinate.getLatitude().degrees,
            coordinate.getLongitude().degrees
	};
    }

    // make header value for new table, check if columns labeled Latitude and Longitude already exist and throw error if they do
    public TableHeader fixHeaders() throws Exception {
        
        TableHeader tempHeader = inputTable.getHeader();
        List<ColumnHeader> tempColHeader = tempHeader.asList();
        
        if (!tempColHeader.contains("Latitude")) {
            tempColHeader.add(new ColumnHeader("LATITUDE", ParameterType.string));
        } else {
            throw new Exception("Input table already contains column labeled 'LATITUDE'!");
        }
        
        if (!tempColHeader.contains("Longitude")) {
            tempColHeader.add(new ColumnHeader("LONGITUDE", ParameterType.string));
        } else {
            throw new Exception("Input table already contains column labeled 'LONGITUDE'!");
        }

        return new TableHeader(tempColHeader);
    }
    
    public static final String MGRS_REGEX = "^\\d{1,2}[^ABIOYZabioyz][A-Za-z]{2}([0-9][0-9])+$";
    
    @Context
    public ExecutionContext ctx;
    
    @Override
    public void run() throws RuntimeException {

        // check to make sure that user input at least one of the two input fields
        try {
            if ((inputString_mgrs == null) && (null == inputTable)) {
                throw new RuntimeException("You must have either an input table or input string!");
            } 
            if ((inputTable != null) && (inputString_column == null)) {
                throw new RuntimeException("You must indicate column name for column containing MGRS values!");
            }
            
        } catch (Exception e) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, e.toString() + " ::: unable to check for valid inputs!");
            throw new RuntimeException (e);
        }
        
        // single MGRS conversion if provided
        if (null != inputString_mgrs) {
            if (inputString_mgrs.value().replaceAll(" ", "").matches(MGRS_REGEX)) {
                double[] latLonString = latLonFromMgrs(inputString_mgrs.value());
                double latDouble = latLonString[0];
                double lonDouble = latLonString[1];
                output_string = new CV_String(Double.toString(latDouble) + " " + Double.toString(lonDouble));
            } else {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "Invalid MGRS format in input string!");
                output_string = new CV_String("Invalid MGRS");
            }
            
        }
        
        // if there was a table input then convert that column
        if (null != inputTable) {
            
            TableHeader tableHeader;
            int rowCount;

            try {
                // create table and assign headers and count rows and find column with mgrs in it
                CV_Table newTable = ctx.createTable(inputTable.getMetadata().getLabel().getValue());
                tableHeader = fixHeaders();
                newTable.setHeader(tableHeader);
                int columnCount = tableHeader.size();
                int colPlacement = tableHeader.findIndex(inputString_column.value());
                rowCount = inputTable.size();
                
                // start looping through rows
                for (int i=1; i<=rowCount; i++) {
                    List<String> tempStringArray = new ArrayList<>();
                    List<CV_Super> tempSuperArray;
                    tempSuperArray = inputTable.readRow();
                    // start looping through columns in row
                    for (int j=0; j<columnCount-2; j++) {
                        if (null != tempSuperArray.get(j)) {
                            // add to new generated row
                            tempStringArray.add(tempSuperArray.get(j).toString());
                            //ctx.log(ExecutionContext.MsgLogLevel.INFO, tempStringArray.toString());
                        } else {
                            tempStringArray.add("");
                        }
                    }

                    // in prep for failure
                    double[] latLonTable;
                    String outOne = "Invalid MGRS";
                    String outTwo = "Invalid MGRS";
                    String mgrs = tempSuperArray.get(colPlacement).toString();
                    
                    if (mgrs.replaceAll(" ", "").matches(MGRS_REGEX)) {
                        try {
                            // attempt to make conversion, set outputs for later addition to row
                            latLonTable = latLonFromMgrs(mgrs);
                            double latTable = latLonTable[0];
                            double lonTable = latLonTable[1];
                            outOne = Double.toString(latTable);
                            outTwo = Double.toString(lonTable);
                        }
                        
                        // If MGRS is invalid or missing or otherwise bad, tell user of row
                        catch (Exception e) {
                            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Invalid MGRS format on row :" + Integer.toString(i+1));
                        }
                    } else {
                        ctx.log(ExecutionContext.MsgLogLevel.INFO, "Invalid MGRS format on row :" + Integer.toString(i+1));
                    }

                    tempStringArray.add(outOne);
                    tempStringArray.add(outTwo);
                    newTable.appendRow(tempStringArray);
                }
                output_table = newTable.toReadable();
                
            } catch (Exception ex) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.toString());
            }
        }
    }
}

// UNCLASSIFIED
