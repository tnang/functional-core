package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_Box;
import jema.common.types.CV_Geometry;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;



/**
 * @author CASCADE
 */
@Functional(
		creator = "CASCADE",
		desc = @CAPCO("Convert a Box into a Feature Collection Typed Table with optional Name and Description"),
		lifecycleState = LifecycleState.TESTING,
		displayPath = { "Conversion", "Data Type", "Features" },
		name = @CAPCO("Box -> Features"),
		uuid = "40f5869c-eca0-40e1-9439-1f81cf9bff8d",
		tags = { "feature collection", "box", "feature" })

public class BoxToFeature implements Runnable{
	
	@Input(
			name = @CAPCO("Box"),
			desc = @CAPCO("Box to be converted into a Feature Collection"),
			required = true)
	public CV_Box input;

	@Input(
			name = @CAPCO("Name"),
			desc = @CAPCO("Name of Feature Collection (optional)"),
			required = false)
	public CV_String name = new CV_String("Polygon");

	@Input(
			name = @CAPCO("Description"),
			desc = @CAPCO("Description of the Feature Collection (optional)"),
			required = false)
	public CV_String desc;
	
	@Output(
			name = @CAPCO("Polygon Feature Collection"),
			desc = @CAPCO("A Feature Collection will be output as a Typed Table representing the input Box"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	public void run() {
		try {			
		
			result = getFeatureCollectionFromBox(input).toReadable();
			ctx.log(ExecutionContext.MsgLogLevel.INFO, "Box " +
					input.value() + " transformed in to Feature Collection");

		} catch (Exception e) {
			String errorMsg = "BoxToFeature: Fatal Error Processing Results: "+e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
		
	}
	
	
	/**
	 * Method creates a CV_Table Feature Collection from a JEMA Box
	 * 
	 * @param box	 *            
	 * @return CV_Table
	 * @throws Exception
	 */
	private CV_Table getFeatureCollectionFromBox(
			CV_Box box) {

		CV_Polygon thePolygon;
		CV_Table polygonFeatures;

		try {
			String featureName = (name == null) ? "Polygon" : name.value();
			String featureDesc = (desc == null) ? "" : desc.value();
			
			polygonFeatures = ctx.createTable("polygon");
			
			thePolygon = new CV_Polygon(box);

			List<ColumnHeader> columns = new ArrayList<>();
			columns.add(new ColumnHeader("POLYGON", ParameterType.polygon));
			columns.add(new ColumnHeader("name", ParameterType.string));
			columns.add(new ColumnHeader("description", ParameterType.string));
			polygonFeatures.setHeader(new TableHeader(columns));
			polygonFeatures.appendRow(addGeometries(thePolygon, featureName,
					featureDesc));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return polygonFeatures;
	}
	
	/**
	 * Add a geomtry to a Array List that will be stuffed in to the table
	 * 
	 * @param pGeometry
	 * @param pDesc
	 * @param pName
	 * @return
	 */
	private List<CV_Super> addGeometries(
			CV_Geometry pGeometry,
			String pName,
			String pDesc) {
		List<CV_Super> row = new ArrayList<>();
		row.add(pGeometry);
		row.add(new CV_String(pName));
		row.add(new CV_String(pDesc));
		return row;
	}

}
