/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Polygon;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * Functional to return a CV Polygon surrounding the given CV Bounding Box.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Polygon</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Box -> Polygon"),
		desc = @CAPCO("Return a CV Polygon surrounding the given CV Bounding Box"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "6f1aaf09-a6f9-4923-8369-8e090bb45210",
		tags = {"box", "polygon"}
		)
public class BoxToPolygon implements Runnable 
{   
	@Input(name = @CAPCO("Source Box"),
			desc = @CAPCO("Source Box."),
			required = true
			)
	public CV_Box srcBox;

	@Output(name = @CAPCO("Destination Polygon"),
			desc = @CAPCO("Destination Polygon."),
			required = true
			)
	public CV_Polygon destPolygon;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(BoxToPolygon.class.getName());

	/**
	 * Run functional to perform calculations.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			this.destPolygon = this.srcBox.toPolygon();
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcBox == null)
		{
			String msg = String.format("Source box is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
