/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;

/**
 * Functional to return the center point of a given box.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Circle</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Box -> Point"),
		desc = @CAPCO("Return the center point of a given box"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "65abc7b0-de2f-424b-a901-d79da163bb48",
		tags = {"box", "point"}
		)
public class BoxToPoint implements Runnable 
{   
	@Input(name = @CAPCO("Source Box"),
			desc = @CAPCO("Source Box."),
			required = true
			)
	public CV_Box srcBox;

	@Output(name = @CAPCO("Destination Point"),
			desc = @CAPCO("Point containing the center of the box."),
			required = true
			)
	public CV_Point destPoint;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(BoxToPoint.class.getName());

	/**
	 * Run functional to return the center point of a given box.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			CV_Polygon cvPolygon = this.srcBox.toPolygon();
//			this.destPoint = PostgisUtil.instance().polygonToPoint(ctx, cvPolygon);
			this.destPoint = GeoToolsUtil.instance().polygonToPoint(cvPolygon);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcBox == null)
		{
			String msg = String.format("Source box is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
