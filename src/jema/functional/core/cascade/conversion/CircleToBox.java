/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Polygon;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * Functional to return a CV Box surrounding a given CV Circle.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Circle</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Notes</b>
 * <ul>
 * <li>If crossDateline is true, then all negative lon are shifted 360 degrees for calculations</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Circle -> Box"),
		desc = @CAPCO("Return a CV Box surrounding a given CV Circle"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "ccb40e5c-f299-4857-8486-c91f55449227",
		tags = {"box", "circle"}
		)
public class CircleToBox implements Runnable 
{   
	@Input(name = @CAPCO("Source Circle"),
			desc = @CAPCO("Source Circle."),
			required = true
			)
	public CV_Circle srcCircle;

	@Input(name = @CAPCO("Polygon in both hemispheres. Default is false."),
			desc = @CAPCO("true=polygon points go from E Hemisphere to W Hemisphere across " +
					      "dateline, false=polygon lines do not cross dateline"),
			required = false
			)
	public CV_Boolean crossDateline;

	@Output(name = @CAPCO("Destination Box"),
			desc = @CAPCO("Destination Box."),
			required = true
			)
	public CV_Box destBox;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(CircleToBox.class.getName());

	/**
	 * Run functional to perform calculations.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			CV_Polygon cvPolygon = this.srcCircle.generatePolygon();
			Boolean crossDateline = this.crossDateline == null ? false : this.crossDateline.value();
			if(crossDateline) {this.destBox = GeometryUtil.polygonToBox(cvPolygon, crossDateline);}
			else {this.destBox = GeoToolsUtil.instance().polygonToBox(cvPolygon);}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcCircle == null)
		{
			String msg = String.format("Source circle is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
