/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * Functional that takes the headers of a table and outputs a list of them.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>List</li>
 * </ul>
 * <p>
 * <b>Notes</b>
 * <ul>
 * <li>ColumnHeader is not a supported CV Type, thus may not be passed between functionals.</li>
 * <li>The output lists will be column names list and corresponding column type list.</li>
 * </ul>
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Table Header -> List"),
		desc = @CAPCO("Create list of column headers from table header"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "From Table"},
		uuid = "c188adef-b4da-4a0c-944f-bef0c6c268e0",
		tags = {"table", "header", "list"}
		)
public class TableHeaderToList implements Runnable 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table containing table header."),
			required = true
			)
	public CV_Table srcTable;

	@Output(name = @CAPCO("Table column name list"),
			desc = @CAPCO("List of column headers contained in table header."),
			required = true
			)
	public List<CV_String> columnNameList;

	@Output(name = @CAPCO("Table column type list"),
			desc = @CAPCO("List of column header types contained in table header."),
			required = true
			)
	public List<CV_String> columnTypeList;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(TableHeaderToList.class.getName());

	/**
	 * Run functional to create list of column headers from table header.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			this.columnNameList = new ArrayList<CV_String>();
			this.columnTypeList = new ArrayList<CV_String>();
			
			for(ColumnHeader colHeader : this.srcTable.getHeader().asList())
			{
				this.columnNameList.add(CV_String.valueOf(colHeader.getName()));
				this.columnTypeList.add(CV_String.valueOf(colHeader.getType().name()));
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcTable == null)
		{
			String msg = String.format("Source table is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
