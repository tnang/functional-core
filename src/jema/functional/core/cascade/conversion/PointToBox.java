/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.measures.length.LengthUnit;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Length;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;

/**
 * Functional to convert a Point and given radius to a Box.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Point representing the center of the box</li>
 * <li>Radius distance from the center to the edge of the box</li>
 * <li>Radius units (meters, miles, etc)</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Point -> Box"),
		desc = @CAPCO("Convert a Point and given Radius to a Box"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "f71e252a-8ef3-4b14-82a4-0e8766b23a94",
		tags = {"box", "point", "radius"}
		)
public class PointToBox implements Runnable 
{   
	@Input(name = @CAPCO("Box Center Point"),
			desc = @CAPCO("Point representing the center of the box."),
			required = true
			)
	public CV_Point srcPoint;

	@Input(name = @CAPCO("Radius distance"),
			desc = @CAPCO("Radius distance from the center to the edge of the box."),
			required = true
			)
	public CV_Decimal radius;

	@Input(name = @CAPCO("Radius units.  Default is METER"),
			desc = @CAPCO("Radius units (FOOT, INCH, KM, METER, MILE, NM, YARD)."),
			required = false
			)
	public CV_String radiusUnit;

	@Output(name = @CAPCO("Destination Box"),
			desc = @CAPCO("Destination Box."),
			required = true
			)
	public CV_Box destBox;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(PointToBox.class.getName());

	/** Circle containing box center point and radius in meters */
	private CV_Circle circle;
	
	/**
	 * Run functional to convert a Point and given radius to a Box.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			CV_Polygon cvPolygon = this.circle.generatePolygon();
			this.destBox = GeoToolsUtil.instance().polygonToBox(cvPolygon);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder();

		if(this.srcPoint == null)
		{
			String msg = String.format("Source point is null");
			sb.append(msg).append("\n");
			isValid = false;			
		}

		if(this.radius == null)
		{
			String msg = String.format("Radius is null");
			sb.append(msg).append("\n");
			isValid = false;			
		}

		LengthUnit lu = null;
		if(this.radiusUnit != null)
		{
			try {lu = LengthUnit.valueOf(this.radiusUnit.value().toUpperCase());}
			catch(Exception e) 
			{
				StringBuilder sbUnits = new StringBuilder();
				for(LengthUnit unit : LengthUnit.values())
				{
				   sbUnits.append(",").append(unit.name());
				}
				String msg = String.format(
					"Invalid Radius units (%s)\nValid units are %s",
					this.radiusUnit,
					sbUnits.substring(1));
				sb.append(msg).append("\n");
				isValid = false;							
			}
		}
		else {lu = LengthUnit.METER;}

		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}
		
		else
		{
            CV_Length radiusMeters = CV_Length.fromLength(lu, this.radius.value());
            this.circle = new CV_Circle(this.srcPoint, radiusMeters.value());
		}
		
		return isValid;
	}
}
