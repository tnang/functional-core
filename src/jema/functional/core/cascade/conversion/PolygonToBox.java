/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Box;
import jema.common.types.CV_Polygon;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * Functional to return a CV Bounding Box surrounding the given CV Polygon.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Polygon</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Notes</b>
 * <ul>
 * <li>If crossDateline is true, then all negative lon are shifted 360 degrees for calculations</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Polygon -> Box"),
		desc = @CAPCO("Return a CV Bounding Box surrounding the given CV Polygon"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "c0258911-5435-4577-abc5-4348b01bc1af",
		tags = {"box", "polygon"}
		)
public class PolygonToBox implements Runnable 
{   
	@Input(name = @CAPCO("Source Polygon"),
			desc = @CAPCO("Source Polygon."),
			required = true
			)
	public CV_Polygon srcPolygon;

	@Input(name = @CAPCO("Polygon in both hemispheres. Default is false."),
			desc = @CAPCO("true=polygon points go from E Hemisphere to W Hemisphere across " +
					      "dateline, false=polygon lines do not cross dateline"),
			required = false
			)
	public CV_Boolean crossDateline;

	@Output(name = @CAPCO("Destination Box"),
			desc = @CAPCO("Destination Bounding Box."),
			required = true
			)
	public CV_Box destBox;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(PolygonToBox.class.getName());

	/**
	 * Run functional to return a CV Bounding Box surrounding the given CV Polygon.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Calculate bounding box.  Take a center point and use 
	 * to test for dateline issues.
	 */
	protected void doCalculation()
	{
		try
		{
//			this.destBox = PostgisUtil.instance().polygonToBox(ctx, this.srcPolygon);
			Boolean crossDateline = this.crossDateline == null ? false : this.crossDateline.value();
			if(crossDateline) {this.destBox = GeometryUtil.polygonToBox(this.srcPolygon, crossDateline);}
			else {this.destBox = GeoToolsUtil.instance().polygonToBox(this.srcPolygon);}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcPolygon == null)
		{
			String msg = String.format("Source polygon is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
