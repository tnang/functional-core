// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * May 15, 2015
 */

package jema.functional.core.cascade.conversion;

import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Boolean;

import java.util.logging.Logger;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "64ea2ea3-d5ff-440c-83ac-5ed968d13403",
        name = @CAPCO("Decimal -> Integer"),
        desc = @CAPCO("Takes a decimal number (double) and converts it to an integer by rounding to the nearest integer.\n\n"
                     +"There is an option(checkbox) to choose not to round and instead just delete the fractional part of the input number."),

        displayPath = {"Conversion", "Data Type", "Numbers"},
        tags = {"Conversion", "Data Type", "Numbers", "integer", "decimal"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public final class DecimalToInteger implements Runnable {

    private static final Logger log =
            Logger.getLogger(DecimalToInteger.class.getName());

    @Context
    public ExecutionContext ctx;

    @Input(
      name = @CAPCO("Decimal Input"),
      desc = @CAPCO("Input a decimal (numeric) value, otherwise known as a double")
    )
    public CV_Decimal input;

    @Input(
      name = @CAPCO("Use rounding"),
      desc = @CAPCO("If selected (true), enables rounding of tenths value"),
      required = true
    )
    public CV_Boolean rounding = new CV_Boolean(false);

    @Output(
      name = @CAPCO("Integer Output"),
      desc = @CAPCO("This is an integer, created from the input decimal value")
    )
    public CV_Integer output;

    @Override
    public void run() {

      try {

        double inputDouble = Double.parseDouble(input.toString());

        // Do not use rounding
        if (rounding.toString().equals("true")) {
          output = new CV_Integer((int)Math.round(inputDouble));
        }

        // Use rounding
        else {
          output = new CV_Integer((int)inputDouble);

        }

      } catch (Exception e) {
          throw new RuntimeException("Problem, maybe input value not of type: double.");
      }

    }
}
// UNCLASSIFIED
