/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.query.builder.FunctionBuilder;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;

/**
 * Functional to calculate the geohash grids as polygon features 
 * across the area specified for the required base and level.
 * 
 * A Geohash system is a hierarchical spatial data structure which subdivides 
 * space into buckets of grid shape. The references remain static for a given 
 * geohash system (with the same base and geohash).
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Point Feature class table</li>
 * <li>Geohash Base Encoding System (16 or 32 bit)</li>
 * <li>Geohash Level Integer (1-12 determines the size and shape of the geohash grids)</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Polygon feature collection table</li>
 * </ul>
 * <b>Notes:</b>
 * <ul>
 * <li><a href="https://code.google.com/p/geomodel/">GeoModel</a> uses base 16 strings for encoding, 
 * with the length of the string describing the resolution. Requires resolution * 4 bytes. 
 * Supports any resolution that is a multiple of 4.</li>
 * <li><a href="https://en.wikipedia.org/wiki/Geohash">Geohash</a> Uses specialized for 
 * readability base 32 encoding, with the length of the string describing the resolution. 
 * Requires resolution * 4/5 bytes. Supports any resolution that is a multiple of 5. 
 * The odd resolution step size implies alternating aspect ratio between resolution steps</li>
 * </ul>
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://www.bigfastblog.com/geohash-intro">Geohash Intro</a></li>
 * <li><a href="http://www.bigdatamodeling.org/2013/01/intuitive-geohash.html">Big Data Modeling</a></li>
 * <li><a href="https://www.factual.com/blog/how-geohashes-work">How Geohashes Work</a></li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Table -> Geohash Grid"),
		desc = @CAPCO("Calculate the geohash grids as polygon features" + 
				" across the area specified for the required base and level"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "65e10a2a-8714-41dd-bf99-f6c371775252",
		tags = {"table", "polygon", "geohash"}
		)
public class TableToGeohashPolygons extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table"),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Column name"),
			desc = @CAPCO("Column name of column containing WKT geometry"),
			required = true
			)
	public CV_String columnName;

	@Input(name = @CAPCO("Geohash Base Encoding System (16 or 32 bit)"),
			desc = @CAPCO("Geohash Base Encoding System (16 or 32 bit)."),
			required = true
			)
	public CV_Integer geohashBaseEncodingSystem;

	@Input(name = @CAPCO("Geohash Level"),
			desc = @CAPCO("Geohash Level 1-12 determines the size and shape of the geohash grids)."),
			required = true
			)
	public CV_Integer geohashBaseLevel;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Polygon feature collection table."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(TableToGeohashPolygons.class.getName());

	/**
	 * Create and execute SQL statement.
	 */
	@Override
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Obtain bounding box from source table
			CV_Box boundingBox = this.boxFromTable();

			// Calculate geohash 
			if(boundingBox != null)
			{
				BoxToGeohashPolygons func = new BoxToGeohashPolygons();
				func.geohashBaseEncodingSystem = this.geohashBaseEncodingSystem;
				func.geohashBaseLevel = this.geohashBaseLevel;
				func.srcBox = boundingBox;
				func.ctx = this.ctx;
				func.run();
				table = func.destTable;
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		
		this.setDestinationTable(table);
	}

	/**
	 * Retrieve bounding box from geometry column in source table.
	 * 
	 * @return Bounding box retrieved from geometry column in source table
	 */
	@SuppressWarnings("rawtypes")
	protected CV_Box boxFromTable()
	{
		CV_Box boundingBox = null;
		
		try
		{
			// Query builder factory
			TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
			
			// Geometry column
			ColumnHeader colHeader = this.srcTable.getHeader().getHeader(this.columnName.value());
						
			// Function
			FunctionBuilder functionBuilder = factory.functionBuilder();
			functionBuilder.addFunction(
				FunctionBuilder.Functions.EXTENT, 
				colHeader);
			functionBuilder.as("CAS_" + FunctionBuilder.Functions.EXTENT.toString());
			
			// Build query 
			SelectBuilder selectBuilder = factory.selectBuilder();
			selectBuilder.selectColumn(functionBuilder);
			selectBuilder.from(this.srcTable);
			
			// Execure query
			CV_Table table = TableQueryExecutor.executeQuery(selectBuilder);
			
			// Get polygon
			if(table != null)
			{
				CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();
				List<CV_Super> row = table.readRow();
				CV_Super s = row.get(0);
				CV_Polygon cvPolygon = (CV_Polygon) cvDataGenerator.genSuper(
					ParameterType.polygon, 
					s.toString(), 
					true);
				
				boundingBox = GeoToolsUtil.instance().polygonToBox(cvPolygon);
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		
		return boundingBox;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		ParameterType[] parameterTypeList = {
			ParameterType.geometryCollection, 
			ParameterType.lineString, 
			ParameterType.multiLineString, 
			ParameterType.multiPoint, 
			ParameterType.multiPolygon, 
			ParameterType.point, 
			ParameterType.polygon};

		isValid = this.validateColumnName(
			this.srcTable, 
			this.columnName, 
			sb, 
			"Column Name", 
			parameterTypeList, 
			isValid);
		
		Double[] validValues = {BoxToGeohashPolygons.BASE_16.doubleValue(), BoxToGeohashPolygons.BASE_32.doubleValue()};
		isValid = this.validateNumericContent(
			this.geohashBaseEncodingSystem, 
            sb, 
            "Geohash Base Encoding System",
            ParameterType.integer,
            null,
            null,
            validValues,
            isValid);

		isValid = this.validateNumericContent(
			this.geohashBaseLevel, 
            sb, 
            "Geohash Level",
            ParameterType.integer,
            1.0,
            12.0,
            null,
            isValid);
		
		return isValid;
	}
	
	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}
	
	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	protected CV_Table getSourceTable() {return this.srcTable;}
}
