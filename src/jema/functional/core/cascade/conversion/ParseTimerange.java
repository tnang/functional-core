/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Aug 26, 2015
 */
package jema.functional.core.cascade.conversion;

import jema.common.types.CV_Duration;
import jema.common.types.CV_String;
import jema.common.types.CV_TemporalRange;
import jema.common.types.CV_Timestamp;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
*
* @author cascade
*/
@Functional(
	       uuid = "6b83acc2-83e7-4992-81d0-ef254da43e6b",
	       name = @CAPCO("Parse Timerange"),
	       desc = @CAPCO("Given a timernage, parse it into the associated start timestamp, end timestampe, and duration."),
	       displayPath = {"Conversion", "Time" },
	       tags = {"Conversion", "Timerange", "timestamp", "parse", "temporal"},
	       lifecycleState = LifecycleState.TESTING,
	       creator = "cascade"
	)
public class ParseTimerange implements Runnable {

    @Input(name = @CAPCO("Time range"),
            desc = @CAPCO("Time range to be parsed.")
    )
    public CV_TemporalRange input;
    
    @Output(name = @CAPCO("Start Timestamp"),
            desc = @CAPCO("The beginning timestamp associated with the given time range.")
    )
    public CV_Timestamp output_startTimestamp;
    
    @Output(name = @CAPCO("End Timestamp"),
            desc = @CAPCO("The ending timestamp associated with the given time range.")
    )
    public CV_Timestamp output_endTimestamp;
	
    @Output(name = @CAPCO("Duration"),
            desc = @CAPCO("The total duration for the associated time range.")
    )
    public CV_Duration output_duration;
    
	@Override
	public void run() {
		output_startTimestamp = new CV_Timestamp(input.getBegin()); 
		output_endTimestamp = new CV_Timestamp(input.getEnd()); 
		output_duration = new CV_Duration(input.getDuration());
	}

}
