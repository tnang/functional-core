/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_Geometry;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", desc = @CAPCO("Convert a Polygon into a Feature Collection"), lifecycleState = LifecycleState.TESTING,
	displayPath = { "Conversion", "Data Type", "Features" }, name = @CAPCO("Polygon -> Feature Collection"),
	uuid = "693708dc-4fde-4645-b8e2-1068eb8452d0", tags = { "feature collection", "polygon" }) 
public class PolygonToFeatures implements Runnable {

	@Input(name = @CAPCO("Polygon"), desc = @CAPCO("Polygon"), required = true)
	public CV_Polygon inputPolygon;

	@Input(name = @CAPCO("Polygon Name"), desc = @CAPCO("Name for the polygon"), required = false)
	public CV_String inputName;

	@Input(name = @CAPCO("Polygon Description"), desc = @CAPCO("Descript for the polygon"), required = false)
	public CV_String inputDesc;

	@Output(name = @CAPCO("Polygon Feature Collection"), desc = @CAPCO("Feature Collection created from the Polygon and Metadata"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	@Override
	public void run() {
		try {
			if (inputPolygon == null) {
				String errorMsg = "PolygonToFeatures: Input Polygon is a required parameter.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			}

			result = getFeatureCollectionFromPoint(inputPolygon, inputName, inputDesc).toReadable();
			ctx.log(ExecutionContext.MsgLogLevel.INFO, "Polygon transformed in to Feature Collection");

		} catch (Exception e) {
			String errorMsg = "PolygonToFeatures: Fatal Error Processing Results: " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
	}

	/**
	 * Method creates a CV_Table Feature Collection from a Polygon, Name and
	 * Description
	 * 
	 * @param pPolygon
	 *            CV_Polygon object will be added to the feature collection
	 * @param pName
	 *            Name of Polygon
	 * @param pDesc
	 *            Description of Polygon
	 * @return CV_Table
	 * @throws Exception
	 */
	private CV_Table getFeatureCollectionFromPoint(CV_Polygon pPolygon, CV_String pName, CV_String pDesc) {

		CV_Table polygonFeatures;

		try {
			String featureName = (pName == null) ? "Polygon" : pName.value();
			String featureDesc = (pDesc == null) ? "" : pDesc.value();

			polygonFeatures = ctx.createTable("polygon");

			List<ColumnHeader> columns = new ArrayList<>();
			columns.add(new ColumnHeader("POLYGON", ParameterType.polygon));
			columns.add(new ColumnHeader("name", ParameterType.string));
			columns.add(new ColumnHeader("description", ParameterType.string));
			polygonFeatures.setHeader(new TableHeader(columns));
			polygonFeatures.appendRow(addGeometries(pPolygon, featureName, featureDesc));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return polygonFeatures;
	}

	/**
	 * Add a geomtry to a Array List that will be stuffed in to the table
	 * 
	 * @param pGeometry
	 * @param pDesc
	 * @param pName
	 * @return
	 */
	private List<CV_Super> addGeometries(CV_Geometry pGeometry, String pName, String pDesc) {
		List<CV_Super> row = new ArrayList<>();
		row.add(pGeometry);
		row.add(new CV_String(pName));
		row.add(new CV_String(pDesc));
		return row;
	}
}
