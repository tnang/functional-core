package jema.functional.core.cascade.conversion;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_WebResource;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
*
* @author cascade
*/

@Functional(
       uuid = "79b1f2ad-995f-4bc5-ac91-536787ea7fae",
       name = @CAPCO("Text File to Table"),
       desc = @CAPCO("This takes a text file and turns it into a table based on a delimiter. "
               + "New lines also act as delimiters. If you do not input a delimiter your "
               + "input will only be delimited with new lines. If you select the option to "
               + "remove the top line from your file it will remove the first item in the "
               + "resulting table, whether from new line delimination or from string "
               + "delimination."),

       displayPath = {"Conversion", "File Format", "Text File"},
       tags = {"Conversion", "File Format", "Text File", "text", "delimited", "table"},

       lifecycleState = LifecycleState.TESTING,

       creator = "cascade"

)

public class DelimitedTextToTable implements Runnable{

	 @Context
	    public ExecutionContext ctx; 
	    
	    @Input(
	            name = @CAPCO("Input .txt file"),
	            desc = @CAPCO("Input a text file (.txt)."),
	            metadata = {"text/plain", "text/*"},
	            required = true
	    )
	    public CV_WebResource input_textFile; 
	    
	    @Input(
	            name = @CAPCO("Separator"),
	            desc = @CAPCO("This is a character used as field separator.  A single character only, a comma is the default"),
	            required = false
	    )
	    public CV_String input_strDelimiter = new CV_String(",");;
	    
	    @Input(
	            name = @CAPCO("Contains Header"),
	            desc = @CAPCO("If this box is checked, the component will assume the first row (or start row, "
	            		+ "if the \"Start Row\" parameter is used) of the file contains the column headers.  If the box is "
	            		+ "unchecked, the component will treat all of the rows in the file as data rows; column headers will "
	            		+ "be added to the output table base on the information provided in the \"Headers\" list."
	            		),
	            required = false
	    )
	    public CV_Boolean input_boolHeader = new CV_Boolean(true);
	    
	    
	    
	    @Input(
	            name = @CAPCO("Headers"),
	            desc = @CAPCO("In the case that the input file does not contain colum headers(\"Contains Header\" is unchecked, "
	            		+ "this parameter allows the user to specify the column header names to use in the output table. "
	            		+ "The number of headers provided must match the number of columns. "
	            		+ "If \"Contain Header\" is checked then this parameter will be ignored, even if populated."
	            		),
	            required = false
	    )
	    public List<CV_String> input_headers;
	    
	    
	    @Input(
	            name = @CAPCO("Start Row"),
	            desc = @CAPCO("This is the 1-based index of the first row of the file to be read.  If the input table contains header, "
	            		+ "this will be the header row, if it does not contains a header then this is the first row of data. "
	            		+ "If no Start Row value is provided then the first line of the file is the start row."
	            		),
	            required = false
	    )
	    public CV_Integer startRow = new CV_Integer(1);
	    
	    @Input(
	            name = @CAPCO("Stop Row"),
	            desc = @CAPCO("This is the 1-based index of the the last row of data to be read from the input file. "
	            		+ "if no stop row index is provided then data is read until the end of the file. "
	            		),
	            required = false
	    )
	    public CV_Integer stopRow ;
	    
	    @Output(
	            name = @CAPCO("Output Table"),
	            desc = @CAPCO("Table output")
	    )
	    public CV_Table outputTable;
	
	public void run() {
		
		List<CV_Super> tempList = new ArrayList<>();     
        List<String> protoLines;
       
		if (input_textFile == null) {
            throw new IllegalStateException("You have not added an input file!");
        }
		
		if(input_strDelimiter.value().length() > 1)
		{
			throw new IllegalStateException("Separator can only be one character long.");
		}
		
		 try {
			 
			 CV_Table table = ctx.createTable("DelimitedTextToTable");
	         URI uri = input_textFile.getURI(); // get file
	           
	         protoLines = Files.readAllLines(Paths.get(uri)); // delimit input file by new line characters
	          
	         if(protoLines.isEmpty())
	         {
	          	throw new IllegalStateException("File submitted is empty!");
	         }
	         
	         
	       //if stopRow is a higher value than the actually number of rows then set it to the last row
	         if(startRow != null )
	         {
	        	 if(startRow.value() < 1)
	        	 {
	        		 throw new IllegalStateException("StartRow must be a 1 or higher");
	        	 }
	        	 if( startRow.value() > protoLines.size()){
	         		throw new IllegalStateException("StartRow can not be greater than the number of rows in the input file.");
	        	 }
	         }
	         else{
	        	 startRow = new CV_Integer(1);
	         }
	 		
	 		//if stopRow is null or is a higher value than the actually number of rows then set it to the last row
	         if(stopRow == null || stopRow.value() > protoLines.size())
	         {
	        	 stopRow = new CV_Integer(protoLines.size());
	         }
	         
	         if(startRow.value() >= stopRow.value())
	         {
	        	 throw new IllegalStateException("StartRow must be smaller than StopRow.");
	         }
	         
	         String delimit = ",";
	         if (input_strDelimiter != null)
	           	delimit = input_strDelimiter.value(); // creates delimiter	        
	       
	      
	         List <String> headerList = new ArrayList<String>();	       	 
	         if(input_boolHeader.value()){
	        	 headerList = Arrays.asList(protoLines.get(startRow.value().intValue() - 1).split(delimit));	        	 
		     }	  
	         else
	         {	 
	        	 //storing list into temp first to
	        	 //get around the "final or effective final" java error 
	        	 //because java see that headerList is being used in both the if and else
	        	  List <String> tempInputHeaderList = new ArrayList<String>();
	        	 input_headers.stream().forEach((x) -> tempInputHeaderList.add(x.toString()));	        	 
	        	 headerList = tempInputHeaderList;
	        	 
	        	 //set the value to base 0 indexing
	        	 startRow = new CV_Integer(startRow.value() - 1);
	        	
	         }
	         
	         headerList.replaceAll(p -> p.trim());
	         //replace whitespace with underscore
	         headerList.replaceAll(p -> p.replaceAll("\\s",  "_"));
	         //replace invalid characters and make sure the name doesn't end with an underscore
	         headerList.replaceAll(p -> p.replaceAll("\\W+", "_"));
	         headerList.replaceAll(p -> p.replaceAll("_+$", ""));
	         headerList.replaceAll(p -> p.replaceAll("^_+", ""));
	         
	         List<ColumnHeader> columns = new ArrayList<>();
	         Set<String> set = new HashSet<String>(headerList.size());
	         Pattern numberPattern = Pattern.compile("(\\w+)_(\\d+)$");
	        
	         for(String header: headerList){
	        	 
	        	 //check for duplicate header name, if so update header name to name_1234
	        	 if(set.contains(header)){
	        		 Matcher m = numberPattern.matcher(header);
	        		 int counter = 1;
		        	 if(m.matches()){
		        		 header = m.group(1);
		        		 counter = Integer.parseInt(m.group(2)) + 1;
		        	 }
		        	 String newName = null;
		        	 do{
		        		 newName = String.format("%s_%d", header, counter++);
		        	 }while(set.contains(newName));
		        	 
		        	 header = newName;
	        	 }
	        	 set.add(header);
	        	 columns.add(new ColumnHeader(header));
	         } 
			 table.setHeader(new TableHeader(columns));
			 //subList toIndex is 'exclusive'
			 List<String> subList = protoLines.subList(startRow.value().intValue(), stopRow.value().intValue());	        
	         
	         for (String each: subList) {
                 if (each != null && !each.isEmpty()) {
                	 tempList =createRow(Arrays.asList(each.split(delimit)));
                	 try {        	           	 
        					table.appendRow(tempList);
        				
        	           	} catch (Exception e) {
        					// TODO Auto-generated catch block
        	           		throw new RuntimeException(e);
        				} 
                 }
             }
	         outputTable = table;
	     } 		 
		 catch (IOException ex) { // catch for the URI get, i did this block to prevent lots of squiggly lines
	            Logger.getLogger(DelimitedTextToList.class.getName()).log(Level.SEVERE, null, ex);
	           
	     } 
		 catch(IllegalStateException ie)
		 {
			 throw ie;
		 }
		 catch (Exception e1) {
			// TODO Auto-generated catch block
			 throw new RuntimeException(e1);
		}  
	}
	
	private List<CV_Super> createRow(List <String> line) {
		List<CV_Super> tempList = new ArrayList<>();		
		line.stream().forEach((x) -> tempList.add(new CV_String(x)));
		return tempList;
	}
}
