/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */

package jema.functional.core.cascade.conversion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
*
* @author cascade
*/

@Functional(
		name = @CAPCO("String -> KML"), 
		desc = @CAPCO("Convert a string to KML web resource. The output will be a KML web resource."), 
		creator = "Cascade", 
		lifecycleState = LifecycleState.TESTING, 
		displayPath = {"Conversion", "File Format", "KML" }, 
		uuid = "2e9c0679-1822-4d0c-9f58-c3627f042ad4", 
		tags = {"Conversion", "KML", "String" })

public class StringToKML implements Runnable {

    @Input(name = @CAPCO("Input KML String"),
            desc = @CAPCO("Input KML String.")
    )
    public CV_String input;
    
    @Output(name = @CAPCO("KML file"),
            desc = @CAPCO("A KML web resource to convert to a string."),
            metadata = "application/vnd.google-earth.kml+xml"
    )
    public CV_WebResource output;
    
    @Context
    ExecutionContext ctx;

    @Override
    public void run() {
        try {
        	File kmlOut = ctx.createTempFileWithExtension(".kml");
        	OutputStream outputStream = new FileOutputStream(kmlOut);
        	IOUtils.write(input.value(), outputStream);
        	
            output = new CV_WebResource(kmlOut.toURI().toURL());
            
        } catch (IOException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "The KML was not generated due to the following error : " + ex.getMessage());
            throw new RuntimeException("The KML was not generated due to the following error : " + ex.getMessage(), ex);
        }

    }
    
}
