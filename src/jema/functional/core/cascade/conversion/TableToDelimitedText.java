/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 29, 2015
 */

package jema.functional.core.cascade.conversion;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.CV_WebResource;
import jema.common.types.table.TableException;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import org.apache.commons.io.IOUtils;


/**
 *
 * @author cascade
 */

@Functional(
        uuid = "a3410c3f-8b1c-4d52-80de-9d81d25e13cc",
        name = @CAPCO("Table -> Delimited Text"),
        desc = @CAPCO("Converts a table to a delimited text file and to a delimited string output."),

        displayPath = {"Conversion", "Data Type", "From Table"},
        tags = {"test", "table", "conversion", "delimited"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public class TableToDelimitedText implements Runnable {

    @Context
    ExecutionContext ctx;
    
    @Input(
            name = @CAPCO("Input Table"),
            desc = @CAPCO("Input a table."),
            required = true
    )
    public CV_Table inputTable;
    
    @Input(
            name = @CAPCO("Input New Line Delimiter"),
            desc = @CAPCO("Optional - newline character by default. Input any string you wish to have instead of a newline at the end of each row of your table. (Will turn your table into a single row.)"),
            required = false
    )
    public CV_String inputString_newLineDelimiter; 

    @Output(
            name = @CAPCO("Output Text File"),
            desc = @CAPCO("Output a text file representing the input table."),
            metadata = {"*/*"}
    )
    public CV_WebResource outputText;  
       
    @Output(
            name = @CAPCO("Output String"),
            desc = @CAPCO("Output a string representation of the input table.")
    )
    public CV_String outputString; 
    
    @Override
    public void run() {

        File outputFile = ctx.createTempFileWithExtension(".txt");
        String newLineDelimiter = "\n";
        
        if (null != inputString_newLineDelimiter) {
            newLineDelimiter = inputString_newLineDelimiter.toString();
        }
        
        
        String check = inputTable.getHeader().toString() + newLineDelimiter;
        
        try {
            int length = inputTable.size();
            for (int i=0; i<length; i++) {
                String temp;
                temp = inputTable.readRow().toString();
                temp = temp.substring(1,temp.length()-1);
                check = check + temp + newLineDelimiter;
            }
            
            OutputStream outputStream = new FileOutputStream(outputFile);
            IOUtils.write(check, outputStream);
            outputText = new CV_WebResource(outputFile.toURI().toURL());
            outputString = new CV_String(check);
            
        } catch (IOException | TableException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.toString());
        }
    }
}

// UNCLASSIFIED
