/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;
import jema.functional.core.cascade.util.geo.GeoModel;
import jema.functional.core.cascade.util.geo.Geohash;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * Functional to calculate the geohash grids as polygon features across the area 
 * specified for the required base and level.
 * 
 * A Geohash system is a hierarchical spatial data structure which subdivides 
 * space into buckets of grid shape. The references remain static for a given 
 * geohash system (with the same base and geohash).
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Area of Interest Box</li>
 * <li>Geohash Base Encoding System (16 or 32 bit)</li>
 * <li>Geohash Level Integer (1-12 determines the size and shape of the geohash grids)</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Polygon feature collection table</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li><a href="https://code.google.com/p/geomodel/">GeoModel</a> uses base 16 strings for encoding, 
 * with the length of the string describing the resolution. Requires resolution * 4 bytes. 
 * Supports any resolution that is a multiple of 4.</li>
 * <li><a href="https://en.wikipedia.org/wiki/Geohash">Geohash</a> Uses specialized for 
 * readability base 32 encoding, with the length of the string describing the resolution. 
 * Requires resolution * 4/5 bytes. Supports any resolution that is a multiple of 5. 
 * The odd resolution step size implies alternating aspect ratio between resolution steps</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Box -> Geohash Grid"),
		desc = @CAPCO("Calculate the geohash grids as polygon features" + 
				" across the area specified for the required base and level"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "2a984a81-e2c9-434f-a784-1f51b74a17a7",
		tags = {"box", "polygon", "geohash", "base16", "base32"}
		)
public class BoxToGeohashPolygons extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Area of Interest Box"),
			desc = @CAPCO("Area of Interest Box."),
			required = true
			)
	public CV_Box srcBox;

	@Input(name = @CAPCO("Geohash Base Encoding System (16 or 32 bit)"),
			desc = @CAPCO("Geohash Base Encoding System (16 or 32 bit)."),
			required = true
			)
	public CV_Integer geohashBaseEncodingSystem;

	@Input(name = @CAPCO("Geohash Level"),
			desc = @CAPCO("Geohash Level 1-12 determines the size and shape of the geohash grids.  Default is 4"),
			required = false
			)
	public CV_Integer geohashBaseLevel;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Polygon feature collection table.  Default is 32"),
			required = false
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(BoxToGeohashPolygons.class.getName());
	
	/** Geohash Base Encoding System (16 bit) */
	public final static Integer BASE_16 = 16;
	
	/** Geohash Base Encoding System (32 bit) */
	public final static Integer BASE_32 = 32;
	
	/** Geohash Base Encoding System (true = base-16, false = base-32) */
	private Boolean isBase16;
	
	/** Grid Cell Width Index */
	private int WIDTH_IDX = 0;
	
	/** Grid Cell Height Index */
	private int HEIGHT_IDX = 1;

	/**
	 * Create and execute SQL statement.
	 */
	@Override
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Initialize destination table
			table = this.initTable();
			
			// Determine geohash grid end points
			CV_Box box = this.getGridBoundary();
			
			// Determine geohash grid step size
			Double[] cellSize = this.getGridCellSize();
			
			// Create grid
			this.createGrid(table, box, cellSize);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		
		this.setDestinationTable(table);
	}
	
	/**
	 * Initialize the destination table.
	 * 
	 * @return Destination table
	 */
	protected CV_Table initTable()
	{
		CV_Table table = null;
		try
		{
			// Create table
			table = this.ctx.createTable("BoxToGeoHash");
			
			// Create header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
			columnHeaderList.add(new ColumnHeader("GeoHash", ParameterType.string));
			columnHeaderList.add(new ColumnHeader("Point", ParameterType.point));
			columnHeaderList.add(new ColumnHeader("GridCell", ParameterType.polygon));
			TableHeader tableHeader = new TableHeader(columnHeaderList);
			table.setHeader(tableHeader);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		return table;
	}
	
	/**
	 * Calculate and return grid boundary.
	 * 
	 * @return Grid boundary
	 */
	protected CV_Box getGridBoundary()
	{
		CV_Box box = null;
		try
		{
			if(this.srcBox == null) {box = new CV_Box(-180.0, -90.0, 180.0, 90.0);}
			else {box = new CV_Box(this.srcBox);}
			
			LOGGER.log(Level.FINEST, String.format("\nBOUNDING BOX = %s", box));
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		return box;
	}
	
	/**
	 * Calculate and return grid cell size.
	 * 
	 * @return Grid cell size (width x height in degrees)
	 */
	protected Double[] getGridCellSize()
	{
		Double[] cellSize = new Double[2];
		
		try
		{
			// Geo Model
			if(this.isBase16)
			{
				Double exponent = Long.valueOf(this.geohashBaseLevel.value()) * 2.0;
				Double val = Math.pow(2, exponent);
				cellSize[WIDTH_IDX] = 360.0 / val;
				cellSize[HEIGHT_IDX] = 180.0 / val;
			}
			
			// Geohash
			else
			{
				int parity = (this.geohashBaseLevel.value() % 2 == 0) ? 0 : 1;
				Double exponent = (Long.valueOf(this.geohashBaseLevel.value())*5.0+parity)/2-1;
				cellSize[WIDTH_IDX] = 180.0 / Math.pow(2,exponent);

				exponent = (Long.valueOf(this.geohashBaseLevel.value())*5.0-parity)/2;
				cellSize[HEIGHT_IDX] = 180.0 / Math.pow(2,exponent);				
			}
			
			LOGGER.log(Level.FINER, String.format(
				"CELL_SIZE=[%g]/[%g], BASE=%d, LEVEL=%d", 
				cellSize[WIDTH_IDX], cellSize[HEIGHT_IDX], this.geohashBaseEncodingSystem.value(), this.geohashBaseLevel.value()));
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		return cellSize;
	}
	
	/**
	 * Calculate and return geohash value.
	 * 
	 * @param lon Coordinate point longitude (deg)
	 * @param lat Coordinate point latitude (deg)
	 * @return Geohash value
	 */
	protected String getGeohash(
		Double lon,
		Double lat)
	{
		String geohash = null;
		
		try
		{
			// Geo Model
			if(this.isBase16)
			{
				Integer level = this.geohashBaseLevel.value().intValue();
				geohash = GeoModel.instance().encode(lon, lat, level);
			}
			
			// Geohash
			else
			{
				Integer level = this.geohashBaseLevel.value().intValue();
				geohash = Geohash.instance().encode(lat, lon, level);
				geohash = geohash.substring(0, level);
			}
			
			LOGGER.log(Level.FINEST, String.format(
				"GEOHASH=%s [%g/%g], BASE=%d, LEVEL=%d", 
				geohash, lon, lat, this.geohashBaseEncodingSystem.value(), this.geohashBaseLevel.value()));
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		return geohash;
	}
	
	/**
	 * Calculate and return geohash grid cell for specified geohash and cell size.
	 * 
	 * @param geohash Geohash code
	 * @param cellSize Geohash grid cell size for specified geolevel
	 * @return Geohash grid cell
	 */
	protected CV_Polygon getGridCell(
		String geohash,
		Double[] cellSize)
	{
		CV_Polygon gridCell = null;
		double[] coord = null;
		
		try
		{
			// Geo Model
			if(this.isBase16)
			{
				CV_Box box = GeoModel.instance().computeBox(geohash);
				gridCell = box.toPolygon();
			}
			
			// Geohash
			else
			{
				coord = Geohash.instance().decode(geohash);
				CV_Box box = new CV_Box(
					coord[Geohash.LON_IDX] - cellSize[WIDTH_IDX]/2,
					coord[Geohash.LAT_IDX] - cellSize[HEIGHT_IDX]/2,
					coord[Geohash.LON_IDX] + cellSize[WIDTH_IDX]/2,
					coord[Geohash.LAT_IDX] + cellSize[HEIGHT_IDX]/2);
				gridCell = box.toPolygon();
			}
			
			LOGGER.log(Level.FINEST, String.format(
				"BASE=%d, LEVEL=%d, GEOHASH=%s, GRID_CELL=%s", 
				this.geohashBaseEncodingSystem.value(), this.geohashBaseLevel.value(), geohash, gridCell));
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		return gridCell;
	}
	
	/**
	 * Create the geohash grid.
	 * 
	 * @param table Destination table
	 * @param boundary Grid boundary
	 */
	@SuppressWarnings("rawtypes")
	protected void createGrid(
		CV_Table table,
		CV_Box boundary,
		Double[] cellSize)
	{
		try
		{
			double width = cellSize[WIDTH_IDX];
			double height = cellSize[HEIGHT_IDX];
			
			Double left = boundary.getLeft();
			Double bottom = boundary.getBottom();
			Double top = boundary.getTop() +  height/2;
			Double right = boundary.getRight() + width/2;
			
			CV_Point coord = GeometryUtil.normalizeCoord(left, bottom);
			left = coord.getX();
			bottom = coord.getY();
			
			coord = GeometryUtil.normalizeCoord(right, top);
			right = coord.getX();
			top = coord.getY();
			
			for(Double lon = left; lon < right; lon += width)
			{
				for(Double lat = bottom; lat < top; lat += height)
				{
					List<CV_Super> row = new ArrayList<CV_Super>();
					
					String geohash = this.getGeohash(lon, lat);
					CV_Polygon gridCell = this.getGridCell(geohash, cellSize);
					
					row.add(CV_String.valueOf(geohash));
					row.add(new CV_Point(lon, lat));
					row.add(gridCell);
					table.appendRow(row);
				}				
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}
	
	/**
	 * Validate the existence of the input source table.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param srcTableRequired true=source table is required input, false=otherwise
	 * @param sb String builder containing list of error messages
     * @param parameterType Column parameter type
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Source table exists, false=otherwise
	 */
	@Override
	protected Boolean validateSourceTable(
		CV_Table srcTable,
		Boolean srcTableRequired,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		return isValid;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		// Source table
		if(this.srcBox == null)
		{
			sb.append("Source AOI box is null").append("\n");
			isValid = false;
		}
		
		Double[] validValues = {(double) BASE_16, (double) BASE_32};
		isValid = this.validateNumericContent(
			this.geohashBaseEncodingSystem, 
            sb, 
            "Geohash Base Encoding System",
            ParameterType.integer,
            null,
            null,
            validValues,
            isValid);
		
		if(isValid) {this.isBase16 = (this.geohashBaseEncodingSystem.value() == Long.valueOf(BASE_16));}

		isValid = this.validateNumericContent(
			this.geohashBaseLevel, 
            sb, 
            "Geohash Level",
            ParameterType.integer,
            1.0,
            12.0,
            null,
            isValid);
		
		return isValid;
	}
	
	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}
	
	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return null;}
}
