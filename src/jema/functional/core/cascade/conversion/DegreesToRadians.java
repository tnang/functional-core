// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 */

package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.Angle;

/**
 *
 * @author cascade
 */

@Functional(uuid = "0743aea2-9df0-4941-8f57-5b243dd79079", name = @CAPCO("Degrees -> Radians"), desc = @CAPCO("Takes degrees and converts it to radians."),

displayPath = { "Conversion", "Data Type", "Numbers" }, tags = { "Conversion", "Data Type", "Numbers", "degrees", "radians" },

lifecycleState = LifecycleState.TESTING,

creator = "cascade"

)
public class DegreesToRadians implements Runnable {

	private static final Logger log = Logger.getLogger(IntegerToDecimal.class.getName());

	@Context
	public ExecutionContext ctx;

	@Input(name = @CAPCO("Degrees Input"), desc = @CAPCO("Degrees input"))
	public CV_Decimal input;

	@Output(name = @CAPCO("Radians Output"), desc = @CAPCO("Radians output"))
	public CV_Decimal output;

	@Override
	public void run() {
		output = new CV_Decimal(Angle.fromDegrees(input.value()).radians);
	}

}
