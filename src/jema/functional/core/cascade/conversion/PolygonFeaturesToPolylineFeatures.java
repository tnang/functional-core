/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jema.common.types.CV_LineString;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", desc = @CAPCO("Convert a Polygon Feature Collection into a Polyline Feature Collection"),
	lifecycleState = LifecycleState.TESTING, displayPath = { "Conversion", "Data Type", "Features" },
	name = @CAPCO("Polygon Feature Collection -> Polyline Feature Collection"), uuid = "098ecd6c-5f88-4717-a929-516b13c01608", tags = { "feature collection",
		"polygon", "polyline" })
public class PolygonFeaturesToPolylineFeatures implements Runnable {

	@Input(name = @CAPCO("Polygon Feature Collection"), desc = @CAPCO("A Feature Collection of Polygon, or Multi-Polygons"), required = true)
	public CV_Table input;

	@Output(name = @CAPCO("Polyline Feature Collection"), desc = @CAPCO("Polyline Feature Collection created from " + " the Polygon Feature Collection"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	/**
	 * Results table
	 */
	private CV_Table resultTable;

	/** Index of the polygon column in the input table */
	protected int polygonIndex;

	/** Column headers */
	protected List<ColumnHeader> columns = new ArrayList<ColumnHeader>();

	@Override
	public void run() {
		if (input == null) {
			String errorMsg = "PolygonFeaturesToPolylineFeatures: Input Polygon Feature Colection is a required parameter.";
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
		try {
			polygonIndex = GeometryUtil.findGeometryColumnPolygon(input);

			// Build results table
			columns = input.getHeader().asList();
			columns.set(polygonIndex, new ColumnHeader("POLYLINE", ParameterType.lineString));
			resultTable = ctx.createTable("POLYLINE_FEATURES");
			TableHeader header = new TableHeader(columns);
			resultTable.setHeader(header);

			if (polygonIndex == -1) {
				String errorMsg = "PolygonFeaturesToPolylineFeatures: Polygon Feature not found in input table.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			} else {
				input.forEach(row -> {
					CV_Polygon poly = (CV_Polygon) row.get(polygonIndex);
					try {
						appendPolylines(row, poly);
					} catch (TableException e) {
						throw new RuntimeException(e);
					}
				});
			}
			result = resultTable.toReadable();
		} catch (TableException e) {
			String errorMsg = "PolygonFeaturesToPolylineFeatures: Trouble processing Table. " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		} catch (Exception e) {
			String errorMsg = "PolygonFeaturesToPolylineFeatures: Unknown Error has occured. " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
	}

	/**
	 * Append the old row for each line in the Polygon
	 * 
	 * @param pRow
	 * @param pPoly
	 * @throws TableException
	 */
	private void appendPolylines(List<CV_Super> pRow, CV_Polygon pPoly) throws TableException {

		if (pPoly != null) {
			List<CV_LineString> lines = pPoly.value();
			List<CV_Super> newRow = new ArrayList<CV_Super>(); //cloneRowWithOutPolygon(pRow);
			for (CV_LineString line : lines) {
				newRow.add(line);
				newRow.addAll(cloneRowWithOutPolygon(pRow));
				resultTable.appendRow(newRow);
			}
		} else {
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, "PolygonFeaturesToPolylineFeatures: Polygon in table is null: " + pRow.toString());
		}
	}

	/**
	 * Clone the Row with out the polygon
	 * 
	 * @param pRow
	 * @return
	 */
	private List<CV_Super> cloneRowWithOutPolygon(List<CV_Super> pRow) {
		List<CV_Super> retVal = new ArrayList<CV_Super>();
		for (int i = 0; i < pRow.size(); i++) {
			if (i != polygonIndex) {
				retVal.add(pRow.get(i));
			}
		}
		return retVal;
	}

}
