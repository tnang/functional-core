/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_Geometry;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", desc = @CAPCO("Convert a Point into a Feature Collection"), lifecycleState = LifecycleState.TESTING, displayPath = {
	"Conversion", "Data Type", "Features" }, name = @CAPCO("Point -> Feature Collection"), uuid = "c53f1331-d6c8-44fb-b265-8dbd02003436", tags = {
	"feature collection", "point" })
public class PointToFeatures implements Runnable {

	@Input(name = @CAPCO("Point"), desc = @CAPCO("Point"), required = true)
	public CV_Point inputPoint;
	
	@Input(name = @CAPCO("Point Name"), desc = @CAPCO("Name for the point"), required = false)
	public CV_String inputName;
	
	@Input(name = @CAPCO("Point Description"), desc = @CAPCO("Descript for the point"), required = false)
	public CV_String inputDesc;	

	@Output(name = @CAPCO("Point Feature Collection"), desc = @CAPCO("Feature Collection created from the Point and Metadata"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	@Override
	public void run() {
		try {
			if (inputPoint == null) {
				String errorMsg = "PointToFeatures: Input Point is a required parameter.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);				
			}
			
			result = getFeatureCollectionFromPoint(inputPoint,inputName,inputDesc).toReadable();
			ctx.log(ExecutionContext.MsgLogLevel.INFO, "Point transformed in to Feature Collection");

		} catch (Exception e) {
			String errorMsg = "PointToFeatures: Fatal Error Processing Results: "+e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
	}

	/**
	 * Method creates a CV_Table Feature Collection from a Point, Name and Description
	 * 
	 * @param pPoint CV_Point object will be added to the feature collection
	 * @param pName Name of Point
	 * @param pDesc Description of Point
	 * @return CV_Table
	 * @throws Exception
	 */
	private CV_Table getFeatureCollectionFromPoint(
			CV_Point pPoint, CV_String pName, CV_String pDesc) {

		CV_Table pointFeatures;

		try {
			String featureName = (pName == null) ? "Point" : pName.value();
			String featureDesc = (pDesc == null) ? "" : pDesc.value();

			pointFeatures = ctx.createTable("point");

			List<ColumnHeader> columns = new ArrayList<>();
			columns.add(new ColumnHeader("POINT", ParameterType.point));
			columns.add(new ColumnHeader("name", ParameterType.string));
			columns.add(new ColumnHeader("description", ParameterType.string));
			pointFeatures.setHeader(new TableHeader(columns));
			pointFeatures.appendRow(addGeometries(pPoint, featureName,
					featureDesc));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return pointFeatures;
	}

	/**
	 * Add a geomtry to a Array List that will be stuffed in to the table
	 * 
	 * @param pGeometry
	 * @param pDesc
	 * @param pName
	 * @return
	 */
	private List<CV_Super> addGeometries(
			CV_Geometry pGeometry,
			String pName,
			String pDesc) {
		List<CV_Super> row = new ArrayList<>();
		row.add(pGeometry);
		row.add(new CV_String(pName));
		row.add(new CV_String(pDesc));
		return row;
	}	
}
