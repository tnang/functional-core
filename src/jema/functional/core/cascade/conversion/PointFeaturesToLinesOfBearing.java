/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

import jema.common.types.CV_LineString;
import jema.common.types.CV_Point;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.Angle;
import jema.functional.core.cascade.util.geo.GeometryUtil;
import jema.functional.core.cascade.util.geo.LatLon;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", desc = @CAPCO("Convert a Point Feature Collection into Line of Bearing Feature Collection"),
	lifecycleState = LifecycleState.TESTING, displayPath = { "Conversion", "Data Type", "Features" }, name = @CAPCO("Point Features -> Lines of Bearing"),
	uuid = "364b9a5a-3c74-4305-a683-72465c4dd063", tags = { "feature collection", "point", "line of bearing", "lob", "bearing" })
public class PointFeaturesToLinesOfBearing implements Runnable {

	@Input(name = @CAPCO("Point Features"), desc = @CAPCO("Point Features"), required = true)
	public CV_Table inputPointFeatures;

	@Input(name = @CAPCO("Azimuth Column"), desc = @CAPCO("Azimuth column name"), required = true)
	public CV_String inputHeading;

	@Input(name = @CAPCO("Distance Column"), desc = @CAPCO("Distance column name, if none is provided a default value " + "of 135nm will be used."),
		required = false)
	public CV_String inputDistance;

	@Output(name = @CAPCO("Lines of Bearing"), desc = @CAPCO("A feature collection with Lines of Bearing"))
	public CV_Table output;

	@Context
	public ExecutionContext ctx;

	/** default distance in NM */
	private long defaultDistance = 0;

	/** results table */
	private CV_Table results;

	/** point index column */
	private int pointIndex = -1;

	/** heading index */
	private int headingIndex = -1;

	/** distance index */
	private int distanceIndex = -1;

	/** GeometryFactory */
	private GeometryFactory gf;

	@Override
	public void run() {
		try {

			gf = new GeometryFactory();

			if (inputPointFeatures == null) {
				String errorMsg = "PointFeaturesToLinesOfBearing: Input Point Features is a required parameter.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			}

			if (inputHeading == null) {
				String errorMsg = "PointFeaturesToLinesOfBearing: Input Azimuth is a required parameter.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			} else {
				headingIndex = inputPointFeatures.findIndex(inputHeading.value());
				if (headingIndex == -1) {
					String errorMsg = "PointFeaturesToLinesOfBearing: Input Azimuth not found in Input Table.";
					ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
					throw new RuntimeException(errorMsg);
				}
			}

			if (inputDistance == null) {
				ctx.log(ExecutionContext.MsgLogLevel.INFO, "PointFeaturesToLinesOfBearing: Input Distance is null, using default of 135 nm.");
				distanceIndex = -1;
				defaultDistance = 135;
			} else {
				distanceIndex = inputPointFeatures.findIndex(inputDistance.value());
			}

			// Create results table
			results = ctx.createTable("LinesOfBearing");
			List<ColumnHeader> columnHeaders = inputPointFeatures.getHeader().asList();

			// INSERT FIND INDEX CODE HERE
			pointIndex = GeometryUtil.findGeometryColumnPoint(inputPointFeatures);
			if (pointIndex == -1) {
				String errorMsg = "PointFeaturesToLinesOfBearing: A Point Feature was not found in Input Table.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			} else {
				columnHeaders.remove(pointIndex);
				columnHeaders.add(pointIndex, new ColumnHeader("LINESTRING", ParameterType.lineString));
				results.setHeader(new TableHeader(columnHeaders));
			}

			this.inputPointFeatures.stream().forEach(row -> {
				try {
					if (distanceIndex != -1 && row.get(distanceIndex) != null) {
						defaultDistance = (long) row.get(distanceIndex).value();
					} else {
						String errorMsg = "PointFeaturesToLinesOfBearing: Input Distance not found in Input Table, using 135nm default";
						ctx.log(ExecutionContext.MsgLogLevel.INFO, errorMsg);
						defaultDistance = 135;
					}
					CV_Point thePoint = (CV_Point) row.get(pointIndex);
					double heading = (double) row.get(headingIndex).value();

					results.appendRow(this.createNewRow(this.calculateLOB(thePoint, heading, defaultDistance), row));

				} catch (Exception e) {
					String errorMsg = "PointFeaturesToLinesOfBearing: Table Error Processing Results: " + e.getMessage();
					ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				}
			});

			ctx.log(ExecutionContext.MsgLogLevel.INFO, "" + results.size() + " Points transformed in to LOBs");
			output = results.toReadable();

		} catch (Exception e) {
			String errorMsg = "PointFeaturesToLinesOfBearing: Fatal Error Processing Results: " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}

	}

	/**
	 * Create a new row based on the old row plus one line string
	 * 
	 * @param pLine
	 * @param pOldRow
	 */
	protected List<CV_Super> createNewRow(CV_LineString pLine, List<CV_Super> pOldRow) {

		List<CV_Super> retValue = new ArrayList<CV_Super>();
		retValue.add(pLine);
		for (int i = 0; i < pOldRow.size(); i++) {
			if (i != pointIndex) {
				retValue.add(pOldRow.get(i));
			}
		}

		return retValue;
	}

	/**
	 * Create a line string from a CV Point Header and Distance
	 * 
	 * @param pPoint
	 * @param pHeader
	 * @param pDistance
	 * @return
	 */
	private CV_LineString calculateLOB(CV_Point pPoint, double pHeading, double pDistance) {

		LatLon origin = LatLon.fromDegrees(pPoint.getY(), pPoint.getX());
		LatLon endPoint = origin.greatCircleEndPosition(origin, Angle.fromDegrees(pHeading), Angle.fromNmi(pDistance));

		Coordinate[] coordinates = new Coordinate[2];
		double[] orgArray = origin.asDegreesArray();
		double[] endArray = endPoint.asDegreesArray();
		Coordinate coordOrigin = new Coordinate(orgArray[1], orgArray[0]);
		Coordinate coordEnd = new Coordinate(endArray[1], endArray[0]);
		coordinates[0] = coordOrigin;
		coordinates[1] = coordEnd;
		LineString lineString = gf.createLineString(coordinates);

		return new CV_LineString(lineString.toString());
	}

}
