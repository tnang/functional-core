/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * Functional to convert a String to a Polygon.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Polygon</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("String -> Polygon"),
		desc = @CAPCO("Converts a String to a Polygon.  String is in WKT format."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "From String"},
		uuid = "36cf7610-2e85-4ab7-b0d4-abb46108c08c",
		tags = {"string", "polygon", "wkt"}
		)
public class StringToPolygon implements Runnable 
{   
	@Input(name = @CAPCO("Source String"),
			desc = @CAPCO("Source String."),
			required = true
			)
	public CV_String srcString;

	@Output(name = @CAPCO("Destination Polygon"),
			desc = @CAPCO("Destination Polygon."),
			required = true
			)
	public CV_Polygon destPolygon;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(StringToPolygon.class.getName());

	/**
	 * Run functional to perform calculations.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			this.destPolygon = new CV_Polygon(this.srcString.value());
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcString == null)
		{
			String msg = String.format("Source string is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
