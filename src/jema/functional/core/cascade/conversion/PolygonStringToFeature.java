/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_Geometry;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
		creator = "CASCADE",
		desc = @CAPCO("Convert a Polygon String into a Feature Collection"),
		lifecycleState = LifecycleState.TESTING,
		displayPath = { "Conversion", "Data Type", "Features" },
		name = @CAPCO("Polygon String -> Feature Collection"),
		uuid = "1481b0f2-52ab-4f30-a61f-17ee9897383c",
		tags = { "feature collection", "string", "polygon", "feature" })
public class PolygonStringToFeature implements Runnable {

	@Input(
			name = @CAPCO("Polygon String"),
			desc = @CAPCO("Polygon String WKT <LON LAT, LON LAT, LON LAT> formated string. "
					+ "-77.13412 38.1234, -77.2345 38.2345, -77.4567 38.3456"),
			required = true)
	public CV_String input;

	@Input(
			name = @CAPCO("Polygon String"),
			desc = @CAPCO("Name of Feature Collection (optional)"),
			required = false)
	public CV_String name = new CV_String("Polygon");

	@Input(
			name = @CAPCO("Name"),
			desc = @CAPCO("Description of the Feature Collection (optional)"),
			required = false)
	public CV_String desc;

	@Output(
			name = @CAPCO("Polygon Feature Collection"),
			desc = @CAPCO("Feature Collection created from Polygon String"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	/**
	 * Run Method
	 */
	public void run() {
		try {
			result = getFeatureCollectionFromPolygonString(input).toReadable();
			ctx.log(ExecutionContext.MsgLogLevel.INFO, "Polygon String " +
					input.value() + " transformed in to Feature Collection");

		} catch (Exception e) {
			String errorMsg = "PolygonStringToFeature: Fatal Error Processing Results: "+e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
	}

	/**
	 * Method creates a CV_Table Feature Collection from a WKT Polygon String
	 * 
	 * @param pPolygonString
	 *            Either Lat/Lon comma delimited string or proper WKT defined
	 *            string
	 * @return CV_Table
	 * @throws Exception
	 */
	private CV_Table getFeatureCollectionFromPolygonString(
			CV_String pPolygonString) {

		CV_Polygon thePolygon;
		CV_Table polygonFeatures;

		try {
			String featureName = (name == null) ? "Polygon" : name.value();
			String featureDesc = (desc == null) ? "" : desc.value();

			polygonFeatures = ctx.createTable("polygon");

			// Check to see if the input string has correct POLYGON tag, if not
			// add it in
			String polygonString = pPolygonString.value();
			if (!polygonString.startsWith("POLYGON")) {
				StringBuffer sb = new StringBuffer();
				sb.append("POLYGON((");
				sb.append(polygonString);
				sb.append("))");
				polygonString = sb.toString();
			}

			thePolygon = new CV_Polygon(polygonString);

			List<ColumnHeader> columns = new ArrayList<>();
			columns.add(new ColumnHeader("POLYGON", ParameterType.polygon));
			columns.add(new ColumnHeader("name", ParameterType.string));
			columns.add(new ColumnHeader("description", ParameterType.string));
			polygonFeatures.setHeader(new TableHeader(columns));
			polygonFeatures.appendRow(addGeometries(thePolygon, featureName,
					featureDesc));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return polygonFeatures;
	}

	/**
	 * Add a geomtry to a Array List that will be stuffed in to the table
	 * 
	 * @param pGeometry
	 * @param pDesc
	 * @param pName
	 * @return
	 */
	private List<CV_Super> addGeometries(
			CV_Geometry pGeometry,
			String pName,
			String pDesc) {
		List<CV_Super> row = new ArrayList<>();
		row.add(pGeometry);
		row.add(new CV_String(pName));
		row.add(new CV_String(pDesc));
		return row;
	}
}
