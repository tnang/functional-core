// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 23, 2015
 */

package jema.functional.core.cascade.conversion;

import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.common.types.CV_Integer;
import jema.common.types.CV_Decimal;

import java.util.logging.Logger;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "c8a29bed-734d-4c41-8f86-8bde21e98c43",
        name = @CAPCO("Integer -> Decimal"),
        desc = @CAPCO("Takes an integer type and converts it to a decimal (double) type."),

        displayPath = {"Conversion", "Data Type", "Numbers"},
        tags = {"Conversion", "Data Type", "Numbers", "integer", "decimal"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)
public final class IntegerToDecimal implements Runnable {

    private static final Logger log =
            Logger.getLogger(IntegerToDecimal.class.getName());

    @Context
    public ExecutionContext ctx;

    @Input(
      name = @CAPCO("Integer Input"),
      desc = @CAPCO("Integer input")
    )
    public CV_Integer input;

    @Output(
      name = @CAPCO("Decimal Output"),
      desc = @CAPCO("Decimal output")
    )
    public CV_Decimal output;

    @Override
    public void run() {

      output = new CV_Decimal(Double.parseDouble(input.toString()));

    }

}
