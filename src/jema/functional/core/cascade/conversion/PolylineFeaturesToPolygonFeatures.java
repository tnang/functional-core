/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_LineString;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeometryUtil;

import com.vividsolutions.jts.geom.GeometryFactory;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", desc = @CAPCO("Convert a Polyline Feature Collection into a Poluygon Feature Collection"),
	lifecycleState = LifecycleState.TESTING, displayPath = { "Conversion", "Data Type", "Features" },
	name = @CAPCO("Polyline Feature Collection -> Polygon Feature Collection"), uuid = "00467064-40f6-4c5d-a4c9-7eea7fe0250c", tags = { "feature collection",
		"polygon", "polyline" })
public class PolylineFeaturesToPolygonFeatures implements Runnable {

	@Input(name = @CAPCO("Polyline Feature Collection"), desc = @CAPCO("A Feature Collection of Polylines, or Multi-Polylines"), required = true)
	public CV_Table input;

	@Output(name = @CAPCO("Polygon Feature Collection"), desc = @CAPCO("Polygon Feature Collection created from " + " the Polygon Feature Collection"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	/**
	 * Results table
	 */
	private CV_Table resultTable;

	/** Index of the polygon column in the input table */
	protected int polylineIndex;

	/** Column headers */
	protected List<ColumnHeader> columns = new ArrayList<ColumnHeader>();

	/** Geometry Factory */
	protected GeometryFactory gf = new GeometryFactory();

	@Override
	public void run() {
		if (input == null) {
			String errorMsg = "PolylineFeaturesToPolygonFeatures: Input Polyline Feature Colection is a required parameter.";
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
		try {
			polylineIndex = GeometryUtil.findGeometryColumnLineString(input);

			// Build results table
			columns = input.getHeader().asList();
			columns.set(polylineIndex, new ColumnHeader("POLYGON", ParameterType.polygon));
			resultTable = ctx.createTable("POLYGON_FEATURES");
			TableHeader header = new TableHeader(columns);
			resultTable.setHeader(header);

			if (polylineIndex == -1) {
				String errorMsg = "PolylineFeaturesToPolygonFeatures: Polygon Feature not found in input table.";
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
				throw new RuntimeException(errorMsg);
			} else {
				input.forEach(row -> {
					CV_LineString line = (CV_LineString) row.get(polylineIndex);
					try {
						appendPolygon(row, line);
					} catch (TableException e) {
						throw new RuntimeException(e);
					}
				});
			}
			result = resultTable.toReadable();
		} catch (TableException e) {
			String errorMsg = "PolylineFeaturesToPolygonFeatures: Trouble processing Table. " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		} catch (Exception e) {
			String errorMsg = "PolylineFeaturesToPolygonFeatures: Unknown Error has occured. " + e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, errorMsg);
			throw new RuntimeException(errorMsg);
		}
	}

	/**
	 * Append the old row for each line in the Polygon
	 * 
	 * @param pRow
	 * @param pPoly
	 * @throws TableException
	 */
	private void appendPolygon(List<CV_Super> pRow, CV_LineString pLine) throws TableException {

		if (pLine != null) {
			List<CV_LineString> lines = new ArrayList<CV_LineString>();
			lines.add(pLine);
			CV_Polygon poly = new CV_Polygon(lines);

			List<CV_Super> newRow = new ArrayList<CV_Super>();
			newRow.add(poly);

			for (int i = 0; i < pRow.size(); i++) {
				if (i != polylineIndex) {
					newRow.add(pRow.get(i));
				}
			}

			resultTable.appendRow(newRow);

		} else {
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, "PolylineFeaturesToPolygonFeatures: Polyline in table is null: " + pRow.toString());
		}
	}

}
