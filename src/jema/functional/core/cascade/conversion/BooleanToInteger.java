/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conversion;


import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Convert a Boolean to a Intger"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Data Type", "Other Data Types" },
        name = @CAPCO("Boolean -> Integer"),
        uuid = "0c242007-9604-48d1-8cbb-a5364594448a",
        tags = {"Conversion","Integer","Boolean" }        
)

public final class BooleanToInteger implements Runnable {

    
    @Input(name = @CAPCO("Boolean"),
           desc = @CAPCO("A boolean to convert to a integer."),
           required = true
    )
    public CV_Boolean input;
    
    @Output(name = @CAPCO("Output Integer"),
            desc = @CAPCO("Output Integer")
    )
    public CV_Integer result;

    /**
    * Returns a CV_Integer value of the input boolean.
    * @param  val  integer value to convert
    * @return  CV_Integer of the input boolean
    */
    public CV_Integer getIntegerFromBoolean(CV_Boolean  val){
        boolean value = val.value();
        if(value){
            return new CV_Integer(1);  
        }else{
            return new CV_Integer(0);    
        }
    }
    
    @Override
    public void run() {     
            
            result = getIntegerFromBoolean(input);

    }
}

//UNCLASSIFIED