/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conversion;

import jema.functional.api.ExecutionContext;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Integer;
import jema.functional.api.Context;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Convert a Integer to a Boolean"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conversion", "Data Type", "Other Data Types" },
        name = @CAPCO("Integer -> Boolean"),
        uuid = "b181e069-ba6f-49d7-8054-944f3c9e53bf",
        tags = {"Conversion","Integer","Boolean" }        
)

public final class IntegerToBoolean implements Runnable {
    
    @Context
    public ExecutionContext ctx;

    
    @Input(name = @CAPCO("Integer"),
           desc = @CAPCO("A integer to convert to a boolean."),
           required = true
    )
    public CV_Integer input;
    
    @Output(name = @CAPCO("Output Boolean"),
            desc = @CAPCO("Output Boolean")
    )
    public CV_Boolean result;

    /**
    * Returns a CV_Boolean value of the input integer.
    * @param  val  integer value to convert
    * @return  CV_Boolean of the input integer
    * @throws IllegalArgumentException if the integer value is not 0 or 1.
    */
    public CV_Boolean getBooleanFromInteger(CV_Integer  val)throws IllegalArgumentException{
        long value = val.value();
        if(value==0){
            return new CV_Boolean(false);  
        }else if(value==1){
            return new CV_Boolean(true);    
        }else{
            throw new IllegalArgumentException("The input value must be either a 0 or 1.");   
        }
    }
    
    @Override
    public void run() {     
        try {
            
            result = getBooleanFromInteger(input);

        } catch (IllegalArgumentException ex) {

            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);

        }
    }
}

//UNCLASSIFIED