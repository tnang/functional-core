// UNCLASSIFIED
/*
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jema.common.json.JSONUtil;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.TableException;

/**
 *
 * @author cascade
 */
@Functional(
        uuid = "ba01fa08-cf60-45fa-bea6-eded7d1e74ed",
        name = @CAPCO("Table -> JSON"),
        desc = @CAPCO("Takes a table as input and converts it to a JSON string."),
        displayPath = {"Conversion", "Data Type", "From Table"},
        tags = {"Conversion", "Data Type", "Table", "Json"},
        lifecycleState = LifecycleState.TESTING,
        creator = "cascade"
)
public final class TableToJson implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table to convert to JSON.")
    )
    public CV_Table inputTable;

    @Output(
            name = @CAPCO("Json Output"),
            desc = @CAPCO("Json representation of the table")
    )
    public CV_String output;

    @Override
    public void run() {

        List values = new ArrayList();
        values.add(inputTable.getHeader().asList());

        try {
            inputTable.stream().forEach((List<CV_Super> current_row) -> {

                values.add(current_row);

            });

        } catch (TableException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Exception " + ex.getMessage());
        }

        Map mymap = new HashMap();
        mymap.put("table", values);
        // System.out.println(JSONUtil.jsonify(mymap));
        output = new CV_String(JSONUtil.jsonify(mymap));

    }
}
//UNCLASSIFIED
