/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import jema.common.types.CV_Geometry;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

/**
 * @author CASCADE
 */
@Functional(
		creator = "CASCADE",
		desc = @CAPCO("Given an input point feature collection with an optional discriminator "
				+ "and sort column, this componet calculates the polygon feature(s) that are "
				+ "created by joining consecutive points with straight lines and joining "
				+ "back to the first point.  If the optional sort column is used, the order in "
				+ "which the points are added the to the polygon feature(s) is determined by the "
				+ "sorting values, otherwise the order the points are added to the polygon feature(s) "
				+ "is determined by the order in which they appear in the feature collection."),
		lifecycleState = LifecycleState.TESTING,
		displayPath = { "Conversion", "Data Type", "Features" },
		name = @CAPCO("Point Features -> Polygon Feature"),
		uuid = "9eb9a925-64f9-49d9-a14c-c65335c3a857",
		tags = { "features", "points", "polygons", "feature", "point features",
				"polygon features" })
public class PointFeaturesToPolygonFeatures implements Runnable {

	@Input(
			name = @CAPCO("Point Feature(s)"),
			desc = @CAPCO("A point feature collection with an optional discrimintor and an optional sort column."),
			required = true)
	public CV_Table input_points;

	@Input(
			name = @CAPCO("Discriminator Column"),
			desc = @CAPCO("Column that contains values by which to descriminate between points and separate them into "
					+ "groups in order to plot a polygon for each group (optional)"),
			required = false)
	public CV_String input_discriminator = new CV_String("");

	@Input(
			name = @CAPCO("Sort by Column"),
			desc = @CAPCO("Column to order point features by, otherwise Points are added to Polygon Features in the "
					+ "current input order (optional)"),
			required = false)
	public CV_String input_sort = new CV_String("");

	@Output(
			name = @CAPCO("Output Polygon Feature Collection"),
			desc = @CAPCO("Output Polygon Feature Collection"))
	public CV_Table result;

	@Context
	public ExecutionContext ctx;

	@Override
	public void run() {

		final boolean grouped = !input_discriminator.value().isEmpty();
		final boolean sorted = !input_sort.value().isEmpty();
		final String groupedCol = grouped ? input_discriminator.value() : "";
		final String sortedCol = sorted ? input_sort.value() : "";

		// If input_discriminator has not been specified then process table
		// normally

		TableHeader header = input_points.getHeader();
		// Check to make sure the specified columns for discrimator and sort do
		// indeed exist in the input table. If the do not log an error and throw
		// a runtime error
		if (grouped) {
			if (!header.contains(groupedCol)) {
				String errorMsg =
						"PointFeaturesToPolygonFeatures:  Point Features does not contained Discrimator Column " +
								groupedCol + " input column.";
				ctx.log(ExecutionContext.MsgLogLevel.INFO, errorMsg);
				throw new RuntimeException(errorMsg);
			}
		}

		if (sorted) {
			if (!header.contains(sortedCol)) {
				String errorMsg =
						"PointFeaturesToPolygonFeatures:  Point Features does not contained Discrimator Column " +
								groupedCol + " input column.";
				ctx.log(ExecutionContext.MsgLogLevel.INFO, errorMsg);
				throw new RuntimeException(errorMsg);
			}
		}

		// Iterate through the table
		Map<String, SortedMap<Integer, CV_Point>> pointMap =
				new HashMap<String, SortedMap<Integer, CV_Point>>();
		TreeMap<Integer, CV_Point> map = null;
		// No grouping is to be used so we only need one point map
		if (!grouped) {
			map = new TreeMap<Integer, CV_Point>();
		}
		int count = 0;
		Iterator i = input_points.iterator();
		while (i.hasNext()) {
			List<CV_Super> row = (List<CV_Super>) i.next();

			// If this is group get the group string and make it a key
			// if not there will only be one polygon
			String mapKey;
			if (grouped) {
				mapKey = row.get(header.findIndex(groupedCol)).toString();
				// if you can't find the key in the point map that means
				// we have a new key so re init the sorted map for the
				// points
				if (!pointMap.containsKey(mapKey)) {
					map = new TreeMap<Integer, CV_Point>();
				}

			} else { // Not grouped, not key to group by so we use a single key
				mapKey = "MapKeyOne";
			}

			// Get the sorted value if we are using a sort column
			// if not use a one up count.
			Integer sortValue = 0;
			if (sorted) {
				sortValue =
						Integer.parseInt(row.get(header.findIndex(sortedCol))
								.toString());
			} else {
				sortValue = count++;
			}

			// finally we don't know which column contains the point so we need
			// to check and
			// find it, once we do... added it to the sorted map with the
			// sorted value from above
			CV_Point thePoint;
			for (CV_Super rowValue : row) {
				if (rowValue instanceof CV_Point) {
					thePoint = (CV_Point) rowValue;
					map.put(sortValue, thePoint);
				}
			}

			pointMap.put(mapKey, map);

		}

		try {
			result = processPolygons(pointMap).toReadable();
		} catch (Exception e) {
			ctx.log(ExecutionContext.MsgLogLevel.INFO, "Fatal Error ");
			String errorMsg =
					"PointFeaturesToPolygonFeatures: Fatal Error Processing Table Results: " +
							e.getMessage();
			ctx.log(ExecutionContext.MsgLogLevel.INFO, errorMsg);
			throw new RuntimeException(errorMsg);
		}

	}

	private CV_Table processPolygons(
			Map<String, SortedMap<Integer, CV_Point>> pPointMap)
			throws Exception {

		CV_Table polygonFeatures = ctx.createTable("PolygonFeatures");
		List<ColumnHeader> columns = new ArrayList<>();
		columns.add(new ColumnHeader("POLYGON", ParameterType.polygon));
		columns.add(new ColumnHeader("Name", ParameterType.string));
		polygonFeatures.setHeader(new TableHeader(columns));
		int count = 1;
		for (SortedMap<Integer, CV_Point> points : pPointMap.values()) {
			CV_Polygon thePolygon = createPolygon(points);
			polygonFeatures.appendRow(addGeometries(thePolygon, "Polygon " +
					count++));
		}

		return polygonFeatures;
	}

	private List<CV_Super> addGeometries(CV_Geometry pGeometry, String pName) {
		List<CV_Super> row = new ArrayList<>();
		row.add(pGeometry);
		row.add(new CV_String(pName));
		return row;
	}

	private CV_Polygon createPolygon(SortedMap<Integer, CV_Point> pPoints) {

		CV_Polygon result;
		Coordinate[] coordArray = new Coordinate[pPoints.size() + 1];
		Object[] pointArray = pPoints.values().toArray();

		for (int i = 0; i < pointArray.length; i++) {
			coordArray[i] =
					new Coordinate(((CV_Point) pointArray[i]).getX(),
							((CV_Point) pointArray[i]).getY());
		}
		coordArray[pointArray.length] =
				new Coordinate(((CV_Point) pointArray[0]).getX(),
						((CV_Point) pointArray[0]).getY());
		GeometryFactory gf = new GeometryFactory();
		Polygon wktPolygon = gf.createPolygon(coordArray);
		result = new CV_Polygon(wktPolygon.toText());
		return result;

	}
}
