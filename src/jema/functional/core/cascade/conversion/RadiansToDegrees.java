

	// UNCLASSIFIED

	/*
	 * Copyright U.S. Government, all rights reserved.
	 *
	 */

	package jema.functional.core.cascade.conversion;

	import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.Angle;

	/**
	 *
	 * @author cascade
	 */

	@Functional(uuid = "1e35e81d-e2fb-4773-81fa-a230dad042f5", name = @CAPCO("Radians -> Degrees"), desc = @CAPCO("Takes radians and converts it to degrees."),

	displayPath = { "Conversion", "Data Type", "Numbers" }, tags = { "Conversion", "Data Type", "Numbers", "degrees", "radians" },

	lifecycleState = LifecycleState.TESTING,

	creator = "cascade"

	)
	public class RadiansToDegrees implements Runnable {

		private static final Logger log = Logger.getLogger(IntegerToDecimal.class.getName());

		@Context
		public ExecutionContext ctx;

		@Input(name = @CAPCO("Radians Input"), desc = @CAPCO("Radians input"))
		public CV_Decimal input;

		@Output(name = @CAPCO("Degrees Output"), desc = @CAPCO("Degrees output"))
		public CV_Decimal output;

		@Override
		public void run() {
			output = new CV_Decimal(Angle.fromRadians(input.value()).degrees);
		}

	}
