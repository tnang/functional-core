/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.conversion;

import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Polygon;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;

/**
 * Functional to return a CV Circle surrounding a given CV Bounding Box.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Circle</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Box -> Circle"),
		desc = @CAPCO("Return a CV Circle surrounding a given CV Bounding Box"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conversion", "Data Type", "Geometry"},
		uuid = "be0dae8d-2799-4480-a4bb-42d6dcc9b0ac",
		tags = {"box", "circle"}
		)
public class BoxToCircle implements Runnable 
{   
	@Input(name = @CAPCO("Source Box"),
			desc = @CAPCO("Source Box."),
			required = true
			)
	public CV_Box srcBox;

	@Output(name = @CAPCO("Destination Circle"),
			desc = @CAPCO("Destination Circle."),
			required = true
			)
	public CV_Circle destCircle;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(BoxToCircle.class.getName());

	/**
	 * Run functional to perform calculations.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			CV_Polygon cvPolygon = this.srcBox.toPolygon();
			this.destCircle = GeoToolsUtil.instance().polygonToCircle(cvPolygon);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;

		if(this.srcBox == null)
		{
			String msg = String.format("Source box is null");
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
}
