/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.insertion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional that adds a column to a table. The functional also 
 * provides an option to set a default value.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Column name String</li>
 * <li>Column type String</li>
 * <li>Column index number Integer.  If blank, add column to the end of the table</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table with added column</li>
 * </ul>
 * <p>
 * <b>References:</b>
 * <ul>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Add a column to a table"),
		desc = @CAPCO("Add a column to a table and provide option to specify default value."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Insertion"},
		uuid = "73227647-e6b7-4bc0-90ad-aed6799b8095",
		tags = {"table", "column"}
		)
public class AddColumn extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table."),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Column name"),
			desc = @CAPCO("Column name"),
			required = true
			)
	public CV_String columnName;

	@Input(name = @CAPCO("Column type"),
			desc = @CAPCO("Column type"),
			required = true
			)
	public CV_String columnType;

	@Input(name = @CAPCO("Column index number"),
			desc = @CAPCO("Column index number Integer. If blank, add column to the end of the table"),
			required = false
			)
	public CV_Integer columnIndex;

	@SuppressWarnings("rawtypes")
	@Input(name = @CAPCO("Default value"),
			desc = @CAPCO("New column default value. Default: null"),
			required = false
			)
	public CV_Super defaultValue;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table with added column."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(AddColumn.class.getName());

	/**
	 * Create and execute SQL statement.
	 */
	@SuppressWarnings("rawtypes")
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			table = ctx.createTable("AddColumn");			
			this.updateHeader(table);
			List<CV_Super> srcRow = null;
			CV_Table srcTable = this.srcTable.toReadable();
			while((srcRow = srcTable.readRow()) != null)
			{
				List<CV_Super> destRow = new ArrayList<CV_Super>(srcRow);
				int index = (this.columnIndex == null) ? -1 : this.columnIndex.value().intValue();
				if(index < 0) {destRow.add(this.defaultValue);}
				else {destRow.add(index, this.defaultValue);}
				table.appendRow(destRow);
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		this.setDestinationTable(table);
	}
	
	/**
	 * Update the header parameter types if necessary.
	 * 
	 * @param table Destination table
	 */
	@Override
	protected void updateHeader(CV_Table table)
	{
		try
		{
			ParameterType type = ParameterType.valueOf(this.columnType.value());
			ColumnHeader destColHeader = new ColumnHeader(this.columnName.value(), type);
			TableHeader srcHeader = this.srcTable.getHeader();
			List<ColumnHeader> destColHeaderList = new ArrayList<ColumnHeader>(srcHeader.asList());
			if(this.columnIndex == null)
			{
				destColHeaderList.add(destColHeader);
			}
			else
			{
				int index = this.columnIndex.value().intValue();
				destColHeaderList.add(index, destColHeader);
			}
			
			TableHeader destHeader = new TableHeader(destColHeaderList);
			table.setHeader(destHeader);
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
			CV_Table srcTable,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		isValid = this.validateColumnName(
			null, 
			columnName, 
            sb, 
            "Column Name",
            ParameterType.string,
            isValid);
		
		if(this.columnType == null)
		{
			String msg = String.format("Column type is null");
			sb.append(msg).append("\n");
			isValid = false;			
		}
		else
		{
			ParameterType type = ParameterType.valueOf(this.columnType.value());
			if(type == null)
			{
				String msg = String.format("Column type (%s) is invalid", this.columnType.value());
				sb.append(msg).append("\n");
				isValid = false;							
			}
		}
		
		if(this.columnIndex != null)
		{
			Integer maxSize = this.srcTable.getHeader().size()-1;
			isValid = this.validateNumericContent(
				this.columnIndex, 
				sb, 
				"Column Index", 
				ParameterType.integer, 
				0.0, 
				maxSize.doubleValue(), 
				null, 
				isValid);
		}
		
		return isValid;
	}

	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}

	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return this.srcTable;}
}
