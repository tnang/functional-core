/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.insertion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional that appends one table's columns to another table. 
 * Copies columns from the Donor Table and adds it as new columns. 
 * The output Table consists of its original contents plus the donor table columns.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Donor Table</li>
 * <li>List of Strings containing the column names of the donor table to be appended</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table with added columns from donor table</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li></li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Join two tables"),
		desc = @CAPCO("Copies columns from the Donor Table and adds it as new columns to the source table (with DONOR_ prefix."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Insertion"},
		uuid = "73227647-e6b7-4bc0-90ad-aed6799b8095",
		tags = {"table", "column", "join"}
		)
public class AddColumnsFromTable extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table."),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Source donor table"),
			desc = @CAPCO("Source donor table."),
			required = true
			)
	public CV_Table srcDonorTable;

	@Input(name = @CAPCO("Column name list"),
			desc = @CAPCO("Column name list"),
			required = true
			)
	public List<CV_String> columnNameList;

	@Input(name = @CAPCO("Donor column name prefix"),
			desc = @CAPCO("Donor column name prefix to mitigate duplicate " + 
					"column name error with source table. Default prefix: DONOR_"),
			required = false
			)
	public CV_String donorColumnPrefix;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table with joined columns."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(AddColumnsFromTable.class.getName());
	
	/** Donor column prefix */
	private static final String COL_PREFIX = "Donor";
	
	/** Donor column name list with prefix */
	private List<ColumnHeader> colHeaderList;

	/**
	 * Create and execute SQL statement.
	 */
	@SuppressWarnings("rawtypes")
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Create destination table
			table = ctx.createTable("AddColumnsFromTable");			
			this.updateHeader(table);
			
			// Get source tables
			CV_Table srcTable = this.srcTable.toReadable();
			CV_Table srcDonorTable = this.srcDonorTable.toReadable();
			
			TableHeader srcDonorHeader = srcDonorTable.getHeader();
			
			// Create null destination data
			List<CV_Super> nullSrcRow = this.nullRow(srcTable.getHeader().asList());
			List<CV_Super> nullSrcDonorRow = this.nullRow(this.colHeaderList);
			
			// Make donor column index list
			List<Integer> donorIndexList = new ArrayList<Integer>();
			for(CV_String colName : this.columnNameList)
			{
				int iCol = srcDonorHeader.findIndex(colName.value());
				donorIndexList.add(iCol);
			}						
			
			boolean rowsExist = true;
			do
			{
				List<CV_Super> srcRow = srcTable.readRow();
				List<CV_Super> srcDonorRow = srcDonorTable.readRow();
				rowsExist = (srcRow != null) || (srcDonorRow != null);
				if(rowsExist)
				{
					List<CV_Super> destRow = new ArrayList<CV_Super>();
					if(srcRow == null) {destRow.addAll(nullSrcRow);}
					else {destRow.addAll(srcRow);}
					
					if(srcDonorRow == null) {destRow.addAll(nullSrcDonorRow);}
					else 
					{
						for(Integer iCol : donorIndexList)
						{
							CV_Super cell = srcDonorRow.get(iCol);
							destRow.add(cell);							
						}			
					}

					table.appendRow(destRow);
				}
			}
			while(rowsExist);
			
			table = table.toReadable();
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		this.setDestinationTable(table);
	}
	
	/**
	 * Create a row of null data.
	 * @param colHeaderList Source column header list
	 * @return List of null data
	 */
	@SuppressWarnings("rawtypes")
	private List<CV_Super> nullRow(List<ColumnHeader> colHeaderList)
	{
		List<CV_Super> row = new ArrayList<CV_Super>();
		int size = colHeaderList.size();
		for(int iCol = 0; iCol < size; iCol++) {row.add(null);}
		return row;
	}
	
	/**
	 * Update the header parameter types if necessary.
	 * 
	 * @param table Destination table
	 */
	@Override
	protected void updateHeader(CV_Table table)
	{
		try
		{
			if((this.srcTable != null) && (this.srcDonorTable != null) && (table != null))
			{
				TableHeader srcHeader = this.srcTable.getHeader();
				List<ColumnHeader> destHeaderList = new ArrayList<ColumnHeader>(srcHeader.asList());
				
				for(ColumnHeader colHeader : this.colHeaderList)
				{
					if(colHeader != null) {destHeaderList.add(colHeader);}
				}

				TableHeader destHeader = new TableHeader(destHeaderList);			
				table.setHeader(destHeader);
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
			CV_Table srcTable,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		if(this.srcDonorTable == null)
		{
			sb.append("Source Donor table is null").append("\n");
			isValid = false;
		}
		
		isValid = this.validateColumnNameList(
			this.srcDonorTable, 
			this.columnNameList, 
			0, 
			sb, 
			null, 
			isValid);			
		
		// Make donor column list
		if(isValid)
		{
			String prefix = this.donorColumnPrefix == null ? COL_PREFIX : this.donorColumnPrefix.value();

			TableHeader srcDonorHeader = this.srcDonorTable.getHeader();
			this.colHeaderList = new ArrayList<ColumnHeader>();
			for(CV_String colName : this.columnNameList)
			{
				ColumnHeader colHeader = srcDonorHeader.getHeader(colName.value());
				if(colHeader == null)
				{
					sb.append(String.format("Invalid Donor column name (%s)", colName.value())).append("\n");
					isValid = false;					
				}
				else
				{
					String donorName = String.format("%s_%s", prefix, colName.value());
					this.colHeaderList.add(new ColumnHeader(donorName, colHeader.getType()));
				}
			}			
		}
		
		return isValid;
	}

	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}

	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return this.srcTable;}
}
