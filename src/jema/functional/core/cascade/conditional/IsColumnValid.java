/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 22, 2015 7:08:38 PM
 */
package jema.functional.core.cascade.conditional;

import java.util.concurrent.Callable;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.core.cascade.util.TableUtils;

/**
 * Functional to determine if the column exists and is of the correct type.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Column Name String</li>
 * <li>Column Type String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Result Boolean</li>
 * </ul>
 * @author CASCADE
 */
@Conditional(
		creator = "CASCADE",
		desc = @CAPCO("Determine if the column exists and is of the correct type."),
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conditionals"},
		name = @CAPCO("Is Column Valid"),
		uuid = "740cc074-da0f-42cb-9317-221e492d0cff",
		tags = {"conditional", "table", "column", "valid"}
		)
public final class IsColumnValid implements Callable<Boolean> {

	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table containing candiate column."),
			required = true)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Column name"),
			desc = @CAPCO("Column name to be validated."),
			required = true)
	public CV_String columnName;

	@Input(name = @CAPCO("Column type"),
			desc = @CAPCO("Column name must match this column type."),
			required = true)
	public CV_String columnType;

	@Context
	public ExecutionContext ctx;

	/**
	 * Call functional to determine if the column exists and is of the correct type.
	 * 
	 * @return true = For specified input column name, corresponding table column exists
	 * and the column parameter type matches input column type, false = otherwise 
	 */
	@Override
	public Boolean call() 
	{
		// Validate input control parameters
		this.validate();

		// Validate candidate column 
		StringBuilder sb = new StringBuilder();
		ParameterType parameterType = ParameterType.valueOf(this.columnType.value());
		Boolean isValid = true;

		isValid = TableUtils.validateColumnName(
			this.srcTable, 
			this.columnName, 
			sb, 
			"Input", 
			parameterType, 
			isValid);

		if(!isValid)
		{
			ctx.postProgress(-1, sb.toString());
		}

		return isValid;
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");

		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null\n");
			isValid = false;
		}

		if(this.srcTable == null)
		{
			String msg = String.format("Source table is null\n");
			sb.append(msg);
			isValid = false;
		}

		if(this.columnType == null)
		{
			String msg = String.format("Input column type is null\n");
			sb.append(msg);
			isValid = false;
		}
		else
		{
			ParameterType parameterType = null;

			try
			{
				parameterType = ParameterType.valueOf(this.columnType.value());
			}
			catch(Exception e)
			{
				if(parameterType == null)
				{
					StringBuilder validList = new StringBuilder();
					int size = ParameterType.values().length;
					int iValidParam = 0;

					for(int iParam = 0; iParam < size; iParam++)
					{
						ParameterType type = ParameterType.values()[iParam];
						switch(type)
						{
							case any:
							case geometry:
							case table:
								break;
	
							default:
								if((iValidParam++ % 5) == 0) {validList.append("\n\t");}
								validList.append(type.name()).append(",");
								break;
						}
					}
					String msg = String.format(
						"Input column type (%s) is invalid\nValid types are: %s", 
						this.columnType.value(),
						validList.toString());
					sb.append(msg);
					isValid = false;
				}	
			}
		}

		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}

		return isValid;
	}
}

//UNCLASSIFIED
