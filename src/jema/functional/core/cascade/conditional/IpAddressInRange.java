// UNCLASSIFIED

/*
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 28, 2015 3:13:38 PM
 */

package jema.functional.core.cascade.conditional;

import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;

import java.util.logging.Logger;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author cascade
 */
@Functional(
        uuid = "b92652ff-0601-41b8-b879-f47a8f97cfe8",
        name = @CAPCO("Check IP Address"),
        desc = @CAPCO("IpAddressInRange"),

        displayPath = {"Conditionals"},
        tags = {"ip", "ipaddress", "cidr", "cidrnotation", "range", "check"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"
)
public final class IpAddressInRange implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(
            name = @CAPCO("IP Address Input"),
            desc = @CAPCO("String representing an IP address"),
            required = false
    )
    public CV_String inputAddress;

    @Input(
            name = @CAPCO("CIDR Notation Input"),
            desc = @CAPCO("Example: 192.168.0.0/24"),
            required = false
    )
    public CV_String inputCidr;

    @Output(
            name = @CAPCO("Is IP address in CIDR range?"),
            desc = @CAPCO("True if in range, false if not in range.")
    )
    public CV_Boolean outputRangeCheck;

    @Output(
            name = @CAPCO("Error Checking: "),
            desc = @CAPCO("May return error data")
    )
    public CV_String outputValidator;

    @Output(
            name = @CAPCO("CIDR Range: "),
            desc = @CAPCO("Outputs the low end and high end of the ip range")
    )
    public CV_String outputRange;

    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    private InetAddress cidrInetAddress;
    private InetAddress startAddress;
    private InetAddress endAddress;


    private byte[] bigIntToByte(byte[] array, int inputLength) {
        int counter = 0;
        List<Byte> newByteArray = new ArrayList<>();
        while (counter < inputLength && (array.length - 1 - counter >= 0)) {
            newByteArray.add(0, array[array.length - 1 - counter]);
            counter++;
        }

        int size = newByteArray.size();
        for (int i = 0; i < (inputLength - size); i++) {
            newByteArray.add(0, (byte) 0);
        }

        byte[] returnValue = new byte[newByteArray.size()];
        for (int i = 0; i < newByteArray.size(); i++) {
            returnValue[i] = newByteArray.get(i);
        }
        return returnValue;
    }

    private boolean checkRange(String ipAddress) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(ipAddress);
        BigInteger start = new BigInteger(1, this.startAddress.getAddress());
        BigInteger end = new BigInteger(1, this.endAddress.getAddress());
        BigInteger target = new BigInteger(1, address.getAddress());

        int st = start.compareTo(target);
        int te = target.compareTo(end);

        return (st == -1 || st == 0) && (te == -1 || te == 0);
    }

    private static String checkIp(CV_String input) {

        boolean ipCheckFour = checkIpFour(stripCidr(input.toString()));

        if (!ipCheckFour) {
            boolean ipCheckSix = checkIpSix(stripCidr(input.toString()));
            if (ipCheckSix) {
                return "ipv6";
            }
            else return "unknown";
        }

        else return "ipv4";
    }

    private static String stripCidr(String input)  {

        return input.split("/")[0];
    }

    private static boolean checkIpFour(String input) {
        if (input.matches("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$")) {
            String[] byteGroup = input.split("\\.");

            for (int n = 0; n <= 3; n++) {
                String bytePiece = byteGroup[n];
                if (bytePiece == null || bytePiece.length() <= 0) {
                    return false;
                }

                int value;

                try {
                    value = Integer.parseInt(bytePiece);
                } catch (NumberFormatException e) {
                    return false;
                }

                if (value > 255) {
                    return false;
                }
            }
            return true;
        }
        else return false;

    }

    private static boolean checkIpSix(String input) {
        return input.matches("([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])");
    }

    ////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////// 


    @Override
    public void run() throws IllegalArgumentException {

        String ipType = checkIp(inputAddress);
        String cidrType = checkIp(inputCidr);
        outputValidator = new CV_String("IP: " + ipType + " CIDR: " + cidrType);

        String inputIpString;
        inputIpString = inputAddress.toString();

        String inputCidrString;
        inputCidrString = inputCidr.toString();

        int prefixLength;
        if (inputCidrString.contains("/")) {
            int index = inputCidrString.indexOf("/");
            String cidrAddress = inputCidrString.substring(0, index);
            String cidrNetwork = inputCidrString.substring(index + 1);

            try {
                cidrInetAddress = InetAddress.getByName(cidrAddress);
            } catch (UnknownHostException ex) {
                Logger.getLogger(IpAddressInRange.class.getName()).log(Level.SEVERE, null, ex);
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "Processing address part of CIDR failed! Unknown Reason.");
            }
            prefixLength = Integer.parseInt(cidrNetwork);

        } else {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Processing CIDR failed! Not a valid CIDR format.");
            throw new IllegalArgumentException("not a valid CIDR format!");
        }

        ByteBuffer cidrMaskBuffer;
        int addressLength;
        if (cidrInetAddress.getAddress().length == 4) {
            cidrMaskBuffer =
                    ByteBuffer
                            .allocate(4)
                            .putInt(-1);
            addressLength = 4;
        } else {
            cidrMaskBuffer = ByteBuffer.allocate(16)
                    .putLong(-1L)
                    .putLong(-1L);
            addressLength = 16;
        }

        BigInteger mask = (new BigInteger(1, cidrMaskBuffer.array())).not().shiftRight(prefixLength);

        ByteBuffer buffer = ByteBuffer.wrap(cidrInetAddress.getAddress());
        BigInteger ipVal = new BigInteger(1, buffer.array());

        BigInteger lowestIp = ipVal.and(mask);
        BigInteger highestIp = lowestIp.add(mask.not());

        byte[] lowIpByte = bigIntToByte(lowestIp.toByteArray(), addressLength);
        byte[] highIpByte = bigIntToByte(highestIp.toByteArray(), addressLength);

        try {
            startAddress = InetAddress.getByAddress(lowIpByte);
        } catch (UnknownHostException ex) {
            Logger.getLogger(IpAddressInRange.class.getName()).log(Level.SEVERE, null, ex);
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Get low ip in range failed! Unknown Reason.");
        }
        try {
            endAddress = InetAddress.getByAddress(highIpByte);
        } catch (UnknownHostException ex) {
            Logger.getLogger(IpAddressInRange.class.getName()).log(Level.SEVERE, null, ex);
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Get high ip in range failed! Unknown Reason.");
        }

        String lowEnd = startAddress.getHostAddress();
        String highEnd = endAddress.getHostAddress();

        outputRange = new CV_String(lowEnd  + " - " + highEnd);

        if (((cidrType.equals("ipv4")) &&
                (ipType.equals("ipv4"))) ||
                ((cidrType.equals("ipv6")) &&
                        (ipType.equals("ipv6")))) {
            try {
                boolean output = checkRange(inputIpString);
                outputRangeCheck = new CV_Boolean(Boolean.toString(output));
            } catch (UnknownHostException uheinr) {
                Logger.getLogger(IpAddressInRange.class.getName()).log(Level.SEVERE, null, uheinr);
                ctx.log(ExecutionContext.MsgLogLevel.INFO, "Range check failed! Unknown Reason.");
            }
        } else {
            outputRangeCheck = new CV_Boolean("false");
            // else ctx.log(ExecutionContext.MsgLogLevel.INFO, "Range check failed! CIDR-Address IPvType mismatch.");
        }
        
    }
}

