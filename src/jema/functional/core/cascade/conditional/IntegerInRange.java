/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import java.util.concurrent.Callable;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Boolean;



/**
 * @author CASCADE
 */
@Conditional(
        creator = "CASCADE",
        desc = @CAPCO("Determines whether a given integer value is within the specified range"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conditionals"},
        name = @CAPCO("Integer In Range"),
        uuid = "f555d9af-6c55-4c22-92ca-5327a622f8c9",
        tags = {"range", "conditional" }
        
)
public final class IntegerInRange implements Callable<Boolean>{

    @Context
    public ExecutionContext ctx;

    
    @Input(desc = @CAPCO("Upper Bound Integer Value"),
            name = @CAPCO("Upper Bound Integer"),
            required = true
    )
    public CV_Integer upperBoundInteger;
   
    @Input(desc = @CAPCO("Lower Bound Integer Value"),
            name = @CAPCO("Lower Bound Integer"),
            required = true
    )
    public CV_Integer lowerBoundInteger;
    
    @Input(desc = @CAPCO("Integer to check between bounds"),
            name = @CAPCO("Integer To Check"),
            required = true
    )
    public CV_Integer integerToCheck;

    @Input(name = @CAPCO("Open Interval?"),
            desc = @CAPCO("Open = (a, b) interpreted as 'a < x < b'."+
                    " Closed = [a, b] interpreted as 'a <= x <= b'."))
    public CV_Boolean isOpenInterval = new CV_Boolean(false);
    
    @Output(name = @CAPCO("Result"),
            desc = @CAPCO("Boolean result indicating if integer value is within the specified range"),
            required = true
    )
    public CV_Boolean result;
    
    /**
    * Returns an boolean value that indicates whether a integer is within a specified range
    *
    * @param  low  long value of the lower bound of the range
    * @param  high long value of the upper bound of the range
    * @param  n integer value to check if between the lower and higher bounds of the range
    * @return  boolean of is given integer is within the specified range
    */
    public boolean isIntegerInRange(CV_Integer low, CV_Integer high, CV_Integer n)throws IllegalArgumentException { 
        if(low.value() > high.value()){
            throw new IllegalArgumentException("The lower bound integer value must be less than or equal to the upper bound integer value");
        }else{
            if(isOpenInterval.value()){
                //Open Interval:(a, b)  is interpreted as a < x < b  where the endpoints are NOT included.
                return n.value() > low.value() && n.value() < high.value();
            }else{
                //Closed Interval:  [a, b]  is interpreted as a <= x <= b  where the endpoints are included.
                return n.value() >= low.value() && n.value() <= high.value();
            }
        }
    }
    
    
    @Override
    public Boolean call() throws Exception {
        try {
            
            result = new CV_Boolean(isIntegerInRange(lowerBoundInteger, upperBoundInteger, integerToCheck));

        } catch (IllegalArgumentException ex) {

            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);

        }
        return result.value();
    }
}
//UNCLASSIFIED