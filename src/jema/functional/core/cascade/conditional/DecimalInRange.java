/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import java.util.concurrent.Callable;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;


/**
 * @author CASCADE
 */
@Conditional(
        creator = "CASCADE",
        desc = @CAPCO("Determines whether a given Decimal value is within the specified range"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conditionals"},
        name = @CAPCO("Decimal In Range"),
        uuid = "27218e9c-2d48-4ca3-8176-9a34a895c1e",
        tags = {"range", "conditional" }        
)
public final class DecimalInRange implements Callable<Boolean>{

    @Context
    public ExecutionContext ctx;

    
    @Input(desc = @CAPCO("Upper Bound Decimal Value"),
            name = @CAPCO("Upper Bound Decimal"),
            required = true
    )
    public CV_Decimal upperBoundDecimal;
   
    @Input(desc = @CAPCO("Lower Bound Decimal Value"),
            name = @CAPCO("Lower Bound Decimal"),
            required = true
    )
    public CV_Decimal lowerBoundDecimal;
    
    @Input(desc = @CAPCO("Decimal to check between bounds"),
            name = @CAPCO("Decimal To Check"),
            required = true
    )
    public CV_Decimal decimalToCheck;
    
    @Input(name = @CAPCO("Open Interval?"),
            desc = @CAPCO("Open = (a, b) interpreted as 'a < x < b'."+
                    " Closed = [a, b] interpreted as 'a <= x <= b'."))
    public CV_Boolean isOpenInterval = new CV_Boolean(false);


    @Output(name = @CAPCO("Result"),
            desc = @CAPCO("Boolean result indicating if decimal value is within the specified range"),
            required = true
    )
    public CV_Boolean result;
    
    /**
    * Returns an boolean value that indicates whether a decimal is within a specified range
    *
    * @param  low  double value of the lower bound of the range
    * @param  high double value of the upper bound of the range
    * @param  n decimal value to check if between the lower and higher bounds of the range
    * @return  boolean of is given decimal is within the specified range
    */
    public boolean isDecimalInRange(CV_Decimal low, CV_Decimal high, CV_Decimal n)throws IllegalArgumentException { 
        if(low.value() > high.value()){
            throw new IllegalArgumentException("The lower bound decimal value must be less than or equal to the upper bound decimal value");
        }else{
            if(isOpenInterval.value()){
                //Open Interval:(a, b)  is interpreted as a < x < b  where the endpoints are NOT included.
                return n.value() > low.value() && n.value() < high.value();
            }else{
                //Closed Interval:  [a, b]  is interpreted as a <= x <= b  where the endpoints are included.
                return n.value() >= low.value() && n.value() <= high.value();
            }
        }
    }
    
    
    @Override
    public Boolean call() throws Exception {
        try {
            
            result = new CV_Boolean(isDecimalInRange(lowerBoundDecimal, upperBoundDecimal, decimalToCheck));

        } catch (IllegalArgumentException ex) {

            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);

        }
        return result.value();
    }
}
//UNCLASSIFIED