/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 22, 2015 7:08:38 PM
 */
package jema.functional.core.cascade.conditional;

import java.util.concurrent.Callable;
import jema.common.types.CV_Table;
import jema.common.types.CV_WebResource;
import jema.common.types.table.TableException;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * Functional to determine whether a CSV file is empty or not.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>CSV File</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Result Boolean</li>
 * </ul>
 * @author CASCADE
 */
@Conditional(
		creator = "CASCADE",
		desc = @CAPCO("Determine whether a CSV file is empty or not."),
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Conditionals"},
		name = @CAPCO("Is Column Valid"),
		uuid = "8882fea7-3482-49da-9a9e-ba248fe9420e",
		tags = {"conditional", "table", "column", "valid"}
		)
public final class IsCSVEmpty implements Callable<Boolean> {

	@Input(name = @CAPCO("Input CSV"),
			desc = @CAPCO("Input Comma Separated Value file."),
			required = true)
	public CV_WebResource in;

	@Context
	public ExecutionContext ctx;

	/** CV Table obtained from input CSV File */
	private CV_Table csvTable;

	/**
	 * Call functional to determine whether a CSV file is empty or not.
	 * 
	 * @return true = CSV File is not empty, false = otherwise 
	 */
	@Override
	public Boolean call() 
	{
		// Validate input control parameters
		this.validate();

		// Validate CSV content
		Boolean isEmpty = true;
		try {isEmpty = this.csvTable.size() == 0;} 
		catch(RuntimeException e) {throw e;}		
		catch(TableException e) {throw new RuntimeException(e.toString(), e);}		
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		finally{if(this.csvTable != null) {this.csvTable.close();}}

		return isEmpty;
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");

		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null\n");
			isValid = false;
		}

		if(this.in == null)
		{
			String msg = String.format("Input CSV Web Resource is null\n");
			sb.append(msg);
			isValid = false;
		}
		else
		{
			try
			{
				this.csvTable = ctx.createTableFromCSV(in.value().getUrl().openStream(), null);
			}
			catch(Exception e)
			{
				String msg = String.format("Invalid CSV Web Resource [%s]", this.in.toString());
				sb.append(msg);
				isValid = false;
			}
		}

		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}

		return isValid;
	}
}

//UNCLASSIFIED
