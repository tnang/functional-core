/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 22, 2015 7:08:38 PM
 */
package jema.functional.core.cascade.conditional;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Super;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * @author CASCADE
 */
@Conditional(
        creator = "CASCADE",
        desc = @CAPCO("Determine whether the input list is empty."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conditionals"},
        name = @CAPCO("Is List Empty"),
        uuid = "16e76fd9-0698-4ced-958f-4b0edc7b025f",
        tags = {"conditional", "list", "empty"}
)
public final class IsListEmpty implements Callable<Boolean> {

    @Input(name = @CAPCO("Input List"),
            desc = @CAPCO("A list to check if empty."),
            required = true)
    public List<CV_Super> inputList;

    @Input(name = @CAPCO("Ignore Empties"),
            desc = @CAPCO("If checked ignore empty values (e.g. null and empty string)."),
            required = true)
    public CV_Boolean ignoreEmpties = new CV_Boolean(false);

    @Override
    public Boolean call() {
        if (!ignoreEmpties.value()) {
            return inputList.isEmpty();
        } else {
            // Collect non-null values
            List<CV_Super> nonNullValues = inputList
                    .stream()
                    .filter(f -> f != null && f.value() != null)
                    .collect(Collectors.toList());

            if (nonNullValues.isEmpty()) {
                return true;
            } else { // Collect non-empty values
                List<CV_Super> nonEmptyValues = nonNullValues
                        .stream()
                        .filter(f -> !(f.toString().isEmpty()))
                        .collect(Collectors.toList());

                return nonEmptyValues.isEmpty();
            }
        }
    }
}

//UNCLASSIFIED
