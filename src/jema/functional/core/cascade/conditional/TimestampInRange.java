/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.conditional;

import java.util.concurrent.Callable;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Conditional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Timestamp;


/**
 * @author CASCADE
 */
@Conditional(
        creator = "CASCADE",
        desc = @CAPCO("Determines whether a given Timestamp value is within the specified range"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Conditionals"},
        name = @CAPCO("Timestamp In Range"),
        uuid = "d4a6beba-48bc-4782-bff0-5b3877de48cd",
        tags = {"range", "conditional","timestamp" }        
)
public final class TimestampInRange implements Callable<Boolean>{

    @Context
    public ExecutionContext ctx;

    
    @Input(desc = @CAPCO("Upper Bound Timestamp Value"),
            name = @CAPCO("Upper Bound Timestamp"),
            required = true
    )
    public CV_Timestamp upperBoundTimestamp;
   
    @Input(desc = @CAPCO("Lower Bound Timestamp Value"),
            name = @CAPCO("Lower Bound Timestamp"),
            required = true
    )
    public CV_Timestamp lowerBoundTimestamp;
    
    @Input(desc = @CAPCO("Timestamp to check between bounds"),
            name = @CAPCO("Timestamp To Check"),
            required = true
    )
    public CV_Timestamp timestampToCheck;

    @Output(name = @CAPCO("Result"),
            desc = @CAPCO("Boolean result indicating if timestamp value is within the specified range"),
            required = true
    )
    public CV_Boolean result;
    
    /**
    * @param  low  CV_Timestamp value of the lower bound of the range
    * @param  high CV_Timestamp value of the upper bound of the range
    * @param  n CV_Timestamp value to check if between the lower and higher bounds of the range
    * Returns an boolean value that indicates whether a decimal is within a specified range
    * @return  boolean of is given CV_Timestamp is within the specified range
    */
    public boolean isTimestampInRange(CV_Timestamp low, CV_Timestamp high, CV_Timestamp n)throws IllegalArgumentException { 
        if(low.value().isAfter(high.value())){
            throw new IllegalArgumentException("The lower bound timestamp value must be less than or equal to the upper bound timestamp value");
        }else{
            
                return low.value().isBefore(n.value())  && high.value().isAfter(n.value());
        }
    }
    
    
    @Override
    public Boolean call() throws Exception {
        try {
            
            result = new CV_Boolean(isTimestampInRange(lowerBoundTimestamp, upperBoundTimestamp, timestampToCheck));

        } catch (IllegalArgumentException ex) {

            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);

        }
        return result.value();
    }
}
//UNCLASSIFIED
