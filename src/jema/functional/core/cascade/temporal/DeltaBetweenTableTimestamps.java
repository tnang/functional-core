/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.temporal;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * Functional to calculate the difference between two table column timestamps and 
 * and append resultant columns for seconds, minutes, hours, and day differences to output table.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Date1 column name String</li>
 * <li>Date1 column format String</li>
 * <li>Date2 column name String</li>
 * <li>Date2 column format String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing extra columns for seconds, minutes, hours, and day differences between the 2 columns</li>
 * </ul>
 * <p>
 * <b>References</b>
 * <ul>
 * <li>Algorithm based upon jema.functional.core.cascade.temporal.DeltaBetweenTimestamps</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculate the difference between two table column timestamps"),
        desc = @CAPCO("Calculate the difference between two table column timestamps " + 
        		"and append resultant columns for seconds, minutes, hours, and day differences to output table."),
        creator = "CASCADE",
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Filter"},
        uuid = "98c7f81a-44a8-405b-a9b6-c3d4d6a26486",
        tags = {"table", "temporal", "delta", "timestamps"}
)
public class DeltaBetweenTableTimestamps implements Runnable {

    @Input(name = @CAPCO("Source table"),
            desc = @CAPCO("Source table containing timestamp columns."),
            required = true
    )
    public CV_Table srcTable;
    
    @Input(name = @CAPCO("Date column name 1"),
            desc = @CAPCO("Column name 1 with CV_Timestamp format."),
            required = true
    )
    public CV_String dateColumnName1;
    
    @Input(name = @CAPCO("Date column name 2"),
            desc = @CAPCO("Column name 2 with CV_Timestamp format."),
            required = true
    )
    public CV_String dateColumnName2;

    @Output(name = @CAPCO("Destination table"),
            desc = @CAPCO("Table containing extra columns for seconds, minutes, hours, " + 
            		"and day differences between the 2 columns."),
            required = true
    )
    public CV_Table destTable;

    @Context
    ExecutionContext ctx;
    
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(DeltaBetweenTableTimestamps.class.getName());
    
    /** Column header labels */
    private final String[] deltaHeaderLabels = {"DeltaDay", "DeltaHour", "DeltaMinute", "DeltaSecond"};

    /**
     * Run functional to calculate the difference between two table column timestamps and 
     * and append resultant columns for seconds, minutes, hours, and day differences to output table.
     */
    @Override
    public void run() 
    {
		try 
		{
			// Validate input parameters
			this.validate();
			
			// Calculate timestamp differences and put results in destination table
			this.destTable = this.makeDestinationTable();
		} 
		catch(IllegalArgumentException e) {throw e;}
        catch(RuntimeException e) {throw e;}
        catch(Exception e) {throw new RuntimeException(e.toString(), e);}
    }

    /**
     * Calculate timestamp differences and put results in destination table.
     * <p>
     * <b>Notes:</b>
     * <ul>
     * <li>Create destination table from context</li>
     * <li>Traverse source table, perform delta calculations, and put results in destination table</li>
     * <li>Update destination table header with timestamp columns</li>
     * <li>Change writable destination table to readable destination table</li>
     * </ul>
     * @return
     */
    @SuppressWarnings("rawtypes")
	private CV_Table makeDestinationTable()
    {
    	CV_Table destTableReadable = null;
    	
    	try
    	{
    		// Create destination table
			String label = this.srcTable.getMetadata().getLabel().getValue() + "_DeltaBetweenTableTimestamps";
			CV_Table destTableWritable = ctx.createTable(label);
			
			// Make destination table header
			this.makeDestinationTableHeader(destTableWritable);
			
			// Column indices
			TableHeader srcTableHeader = this.srcTable.getHeader();
			int iCol1 = srcTableHeader.findIndex(this.dateColumnName1.value());
			int iCol2 = srcTableHeader.findIndex(this.dateColumnName2.value());
			
			CV_Table srcTableReadable = this.srcTable.toReadable();
			
			// Add computational results to table
			List<CV_Super> srcRow = null;
			while((srcRow = srcTableReadable.readRow()) != null)
			{
				List<CV_Super> destRow = doCalculation(iCol1, iCol2, srcRow);
				destTableWritable.appendRow(destRow);
			}
			
			srcTableReadable.close();
			
			destTableReadable = destTableWritable.toReadable();
    	}
    	catch(Exception e) {throw new RuntimeException(e.toString(), e);}
    	
    	return destTableReadable;
    }
    
    /**
     * Make destination table header.
     * <p>
     * <b>Notes:</b>
     * <ul>
     * <li>Create destination column header list from source table header </li>
     * <li>Append delta time labels to destination header</li>
     * <li>Update destination table with new header list</li>
     * </ul>
     * 
     * @param destTable Destination table to be updated with modified header
     */
	private void makeDestinationTableHeader(
		CV_Table destTable)
    {
		try
		{
			List<ColumnHeader> destColumnHeaderList = 
				new ArrayList<ColumnHeader>(this.srcTable.getHeader().asList());
			
			for(String label : this.deltaHeaderLabels)
			{
				String colName = String.format("%s_%s_%s", label, this.dateColumnName1, this.dateColumnName2);				
				ColumnHeader columnHeader = new ColumnHeader(colName, ParameterType.decimal);
				destColumnHeaderList.add(columnHeader);
			}
			TableHeader destTableHeader = new TableHeader(destColumnHeaderList);		
			destTable.setHeader(destTableHeader);
    	}
    	catch(Exception e) {throw new RuntimeException(e.toString(), e);}
    }
    
    /**
     * Perform delta time calculation per designated column pair.
     * 
     * @param iCol1 Column index to allow extraction of data from column 1
     * @param iCol2 Column index to allow extraction of data from column 2
     * @param srcRow Source table row
     * @return Destination row with additional delta time columns
     */
    @SuppressWarnings("rawtypes")
	private List<CV_Super> doCalculation(
		int iCol1,
		int iCol2,
		List<CV_Super> srcRow)
    {
    	List<CV_Super> destRow = new ArrayList<CV_Super>(srcRow);
		try
		{
			CV_Timestamp date1 = (CV_Timestamp) srcRow.get(iCol1);
			CV_Timestamp date2 = (CV_Timestamp) srcRow.get(iCol2);
			
	        Duration duration = Duration.between(date1.getZonedDateTime(), date2.getZonedDateTime());
	        long nanos = duration.toNanos();
	        double seconds = ((double) nanos) * 1.0E-9;
	        double minutes = seconds / 60;
	        double hours = minutes / 60;
	        double days = hours / 24;
	        
	        destRow.add(CV_Decimal.valueOf(days));
	        destRow.add(CV_Decimal.valueOf(hours));
	        destRow.add(CV_Decimal.valueOf(minutes));
	        destRow.add(CV_Decimal.valueOf(seconds));
   	    }
    	catch(Exception e) {throw new RuntimeException(e.toString(), e);}
		
    	return destRow;
    }
    
	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	private Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");
		
		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null").append("\n");
			isValid = false;
		}
		
		// Source table
		if(this.srcTable == null)
		{
			sb.append("Source table is null").append("\n");
			isValid = false;
		}
		
		// Validate column name 1 and associated parameter type
		isValid = this.validateColumnName(this.dateColumnName1, sb, "1", isValid);
		
		// Validate column name 2 and associated parameter type
		isValid = this.validateColumnName(this.dateColumnName2, sb, "2", isValid);
		
		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}
		
		return isValid;
	}
    
	/**
	 * Validate column name existence and parameter type.
	 * 
	 * @param colName Candidate column name
	 * @param sb String builder containing list of error messages
	 * @param msgText Text to add to error message to distinguish specific column
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name is valid, false=otherwise
	 */
	private Boolean validateColumnName(
		CV_String colName,
		StringBuilder sb,
		String msgText,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		if(colName == null)
		{
			String msg = String.format("Date column name %s is null", msgText);
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(this.srcTable != null)
		{
			TableHeader tableHeader = this.srcTable.getHeader();

			String name = (colName == null) ? null : colName.value();
			int iCol = (name == null) ? -1 : tableHeader.findIndex(name);

			if(iCol == -1)
			{
				sb.append(String.format("Invalid table column %s name, name = %s", msgText, name)).append("\n");
				isValid = false;									
			}
			else
			{
				ColumnHeader columnHeader = tableHeader.getHeader(iCol);
				ParameterType parameterType = columnHeader.getType();
				if(!parameterType.equals(ParameterType.timestamp))
				{
					String msg = String.format(
						"Invalid table column %s data type, name = %s, type = %s)", 
						msgText, 
						name, 
						parameterType);
					sb.append(msg).append("\n");
					isValid = false;																
				}
			}
		}
		
		return isValid;
	}
}
