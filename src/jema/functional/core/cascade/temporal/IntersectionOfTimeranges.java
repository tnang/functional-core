/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 16, 2015 8:31:03 AM
 */
package jema.functional.core.cascade.temporal;

import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;
import jema.common.types.CV_TemporalRange;
import jema.common.types.CV_Timestamp;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Calculate the intersection of timerange inputs, resulting in a single timerange output "
                + "representing the intersection plus the start and end timestamps of the intersection. "
                + "If no timerange exists that is common to all of the timerange inputs, "
                + "the resulting timerange will be null."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Temporal"},
        name = @CAPCO("Calculate Intersection of Timeranges"),
        uuid = "e507105c-71a9-4243-b442-d71b0e001d6b",
        tags = {"temporal", "intersection", "timerange"}
)
public final class IntersectionOfTimeranges implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(IntersectionOfTimeranges.class.getName());

    @Input(name = @CAPCO("Time Ranges"),
            desc = @CAPCO("The time ranges to calculate the intersection of."),
            required = true
    )
    public List<CV_TemporalRange> ranges;

    @Output(name = @CAPCO("Intersection Time Range"),
            desc = @CAPCO("The intersection of input time ranges.")
    )
    public CV_TemporalRange intersection;

    @Output(name = @CAPCO("Start Timestamp"),
            desc = @CAPCO("Earliest timestamp of the intersection time range.")
    )
    public CV_Timestamp startTime;

    @Output(name = @CAPCO("Stop Timestamp"),
            desc = @CAPCO("Latest timestamp of the intersection time range.")
    )
    public CV_Timestamp stopTime;

    @Override
    public void run() {

        try {
            if (ranges == null) {
                throw new RuntimeException("The Time Ranges cannot be null.");
            } else {
                //Validate time ranges
                ranges.stream().forEach(range -> {
                    if (range == null) {
                        throw new RuntimeException("An input Time Range cannot be null.");
                    }
                    if (range.getDuration().isZero()) {
                        ctx.log(ExecutionContext.MsgLogLevel.WARNING, "An input has a duration of 0");
                    }
                });

                if (ranges.size() == 1) {
                    intersection = ranges.get(0);
                    startTime = new CV_Timestamp(ranges.get(0).getBegin());
                    stopTime = new CV_Timestamp(ranges.get(0).getEnd());
                } else {
                    calculateIntersection();
                }
            }
        } catch (RuntimeException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private void calculateIntersection() {
        Boolean allOverlap = true;

        if (ranges.size() > 1) {
            Instant begin = ranges.get(0).getBegin();
            Instant end = ranges.get(0).getEnd();

            for (int i = 1; i < ranges.size(); i++) {
                CV_TemporalRange range = ranges.get(i);

                Instant nextBegin = range.getBegin();
                Instant nextEnd = range.getEnd();
                if (begin.isBefore(nextEnd) && nextBegin.isBefore(end)) {
                    if (begin.isBefore(nextBegin)) {
                        begin = nextBegin;
                    }

                    if (end.isAfter(nextEnd)) {
                        end = nextEnd;
                    }
                } else { //no intersection between current time ranges
                    allOverlap = false;
                }
            }

            if (allOverlap) {
                intersection = new CV_TemporalRange(begin, end);
                startTime = new CV_Timestamp(begin);
                stopTime = new CV_Timestamp(end);
            } else {
                intersection = null;
                startTime = null;
                stopTime = null;
            }
        }
    }
}

//UNCLASSIFIED
