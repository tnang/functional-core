/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.temporal;


import java.time.Duration;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Timestamp;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Calculates the difference between two timestamps and return values for seconds, minutes, hours, and day differences."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Temporal"},
        name = @CAPCO("Delta Between Timestamps"),
        uuid = "1583417b-49fb-4720-bf7c-2b3bcdbc48e3",
        tags = {"temporal", "delta", "timestamps" }
        
)
public final class DeltaBetweenTimestamps implements Runnable {


   @Context
    public ExecutionContext ctx;

    
    @Input(desc = @CAPCO("Start Timestamp Value"),
            name = @CAPCO("Start Timestamp Value"),
            required = true
    )
    public CV_Timestamp startTimestamp;
   
    @Input(desc = @CAPCO("End Timestamp Value"),
            name = @CAPCO("End Timestamp Value"),
            required = true
    )
    public CV_Timestamp endTimestamp;

    @Output(name = @CAPCO("Seconds Count"),
            desc = @CAPCO("A count of how many whole seconds there are between the two timestamps.")
    )
    public CV_Integer seconds;
    
    @Output(name = @CAPCO("Minutes Count"),
            desc = @CAPCO("A count of how many whole minutes there are between the two timestamps.")
    )
    public CV_Integer minutes;
    
    @Output(name = @CAPCO("Hours Count"),
            desc = @CAPCO("A count of how many whole hoursthere are between the two timestamps.")
    )
    public CV_Integer hours;
    
    @Output(name = @CAPCO("Day Count"),
            desc = @CAPCO("A count of how many whole days (in 24 hour increments) there are between the two timestamps.")
    )
    public CV_Integer days;  
    

    @Override
    public void run() {
        
        Duration duration = Duration.between(startTimestamp.getZonedDateTime(), endTimestamp.getZonedDateTime());
        seconds = new CV_Integer(duration.toMinutes()*60);
        minutes = new CV_Integer(duration.toMinutes());
        hours = new CV_Integer(duration.toHours());
        days = new CV_Integer(duration.toDays());

    }
}
//UNCLASSIFIED