/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 27, 2015
 */

package jema.functional.core.cascade.datamanipulation;


import jema.common.types.CV_Boolean;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.common.types.CV_String;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "b9bb9a82-3c28-475f-a0e7-817e9f0306b7",
        name = @CAPCO("Replace Substring"),
        desc = @CAPCO("Searches a string for a substring and replaces it with a third string"),

        displayPath = {"Data Manipulation", "Strings"},
        tags = {"Data Manipulation", "Strings", "substring", "string", "replace"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public class ReplaceSubstring implements Runnable {

    @Input(
            name = @CAPCO("Input String"),
            desc = @CAPCO("Input outter string."),
            required = true
    )
    public CV_String input_string; 

    @Input(
            name = @CAPCO("Input Search String"),
            desc = @CAPCO("Input string to search for within the input string"),
            required = true
    )
    public CV_String input_subString; 
    
    @Input(
            name = @CAPCO("Replacement String"),
            desc = @CAPCO("Input string to replace the search string with"),
            required = true
    )
    public CV_String input_replacementString; 
    
    @Input(
            name = @CAPCO("Case Sensitivity"),
            desc = @CAPCO("True :: search string is case sensitive, False :: search string is not case sensitive."),
            required = true
    )
    public CV_Boolean input_caseSensitive = new CV_Boolean(true); 
    
    @Output(
            name = @CAPCO("Output String"),
            desc = @CAPCO("Output string.")
    )
    public CV_String output;   
    
    @Override
    public void run() {
        
        String initialText = input_string.value();
        String searchText = input_subString.value();
        String replaceText = input_replacementString.value();
        Boolean sensitive = input_caseSensitive.value();
        
        if (!sensitive) {
            searchText = "(?i)" + searchText;
        }
        
        String out = initialText.replaceAll(searchText, replaceText);
        
        output = new CV_String(out);        
        
    }

}

// UNCLASSIFIED
