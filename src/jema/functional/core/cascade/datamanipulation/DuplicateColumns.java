/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional that duplicates specified columns in the input table.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>List of Strings containing the column headers to duplicate</li>
 * <li>List of Strings containing the new duplicate column names</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing duplicates of columns listed</li>
 * </ul>
 * <p>
 * <b>References:</b>
 * <ul>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Duplicate specified columns in the input table"),
		desc = @CAPCO("Duplicate specified columns in the input table."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Data Manipulation", "Tables"},
		uuid = "dd302734-d38a-4b32-99a7-d130c0d18bfb",
		tags = {"table", "duplicate", "columns"}
		)
public class DuplicateColumns extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table."),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Origial column header name list"),
			desc = @CAPCO("List of Strings containing the column headers to duplicate"),
			required = false
			)
	public List<CV_String> origColumnHeaders;

	@Input(name = @CAPCO("Duplicate column header name list"),
			desc = @CAPCO("List of Strings containing the new duplicate column names"),
			required = false
			)
	public List<CV_String> duplicateColumnHeaders;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table containing duplicates of columns listed."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(DuplicateColumns.class.getName());

	/**
	 * Perform calculations.
	 */
    @Override
	protected String performCalculation()
	{
		String calcColumn = null;

		try
		{
			StringBuilder sb = new StringBuilder();
			int origSize = this.origColumnHeaders.size();
			for(int iCol = 0; iCol < origSize; iCol++)
			{
				CV_String origColName = this.origColumnHeaders.get(iCol);
				CV_String dupColName = this.duplicateColumnHeaders.get(iCol);
				sb.append(",").append(String.format("%s AS %s", origColName, dupColName));
			}
			
			if(sb.length() != 0) {calcColumn = sb.substring(1);}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return calcColumn;
	}
	
	/**
	 * Update the header parameter types if necessary.
	 * 
	 * @param table Destination table
	 */
	@Override
	protected void updateHeader(CV_Table table)
	{
		try
		{
			if((this.srcTable != null) && (table != null))
			{
				TableHeader srcHeader = this.srcTable.getHeader();
				TableHeader destHeader = table.getHeader();			
				List<ColumnHeader> colHeaderList = new ArrayList<ColumnHeader>();

				int iDup = 0;
				CV_String dupColName = this.duplicateColumnHeaders.get(iDup);
				String dupName = (dupColName == null) ? null : dupColName.value().toUpperCase();

				int size = destHeader.size();
				for(int iCol = 0; iCol < size; iCol++)
				{
					ColumnHeader destColHeader = destHeader.getHeader(iCol);
					String destName = destColHeader.getName().toUpperCase();
					if((dupName != null) && destName.equals(dupName))
					{
						CV_String origColName = this.origColumnHeaders.get(iDup);
						ColumnHeader srcColHeader = srcHeader.getHeader(origColName.value());
						ColumnHeader colHeader = new ColumnHeader(dupColName.value(), srcColHeader.getType());
						colHeaderList.add(colHeader);
						dupColName = this.duplicateColumnHeaders.get(++iDup);
						dupName = (dupColName == null) ? null : dupColName.value().toUpperCase();
					}
					else
					{
						colHeaderList.add(destColHeader);
					}
				}

				TableHeader modifiedHeader = new TableHeader(colHeaderList);
				table.setHeader(modifiedHeader);
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
			CV_Table srcTable,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		isValid = this.validateColumnNameList(
			this.srcTable, 
			this.origColumnHeaders, 
            0, 
            sb, 
            null,
            isValidCur);
		
		if(this.duplicateColumnHeaders == null)
		{
			String msg = String.format("Duplicate column names list is null!");
			sb.append(msg).append("\n");
			isValid = false;				
		}
		
		if(this.origColumnHeaders != null && this.duplicateColumnHeaders != null)
		{
			// Validate size
			int sizeOrig = this.origColumnHeaders.size();
			int sizeDup = this.duplicateColumnHeaders.size();
			if(sizeOrig != sizeDup)
			{
				String msg = String.format(
					"Original column name list size (%d) does not match duplicate column name size (%d)!",
					sizeOrig,
					sizeDup);
				sb.append(msg).append("\n");
				isValid = false;				
			}
			
			// Find duplicate column names
			if(this.srcTable != null)
			{
				for(CV_String name : this.duplicateColumnHeaders)
				{
					ColumnHeader colHeader = this.srcTable.getHeader().getHeader(name.value());
					if(colHeader != null)
					{
						String msg = String.format(
							"Duplicate column name (%s) matches existing column name!",
							name.value());
						sb.append(msg).append("\n");
						isValid = false;										
					}
				}
			}
		}

		return isValid;
	}

	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}

	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return this.srcTable;}
}
