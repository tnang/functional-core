/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Aug 31, 2015 6:48:15 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.measures.length.LengthUnit;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Converts a column of distances in a table to a new column of distances in a new unit of measure."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Spatial Analysis", "Conversion"},
        name = @CAPCO("Convert Distance in Table"),
        uuid = "cdd49058-dbcd-4197-be51-e465ea58ac57",
        tags = {"conversion", "distance"}
)
public final class ConvertDistanceInTable implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(ConvertDistanceInTable.class.getName());

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table containing the column of distances for conversion."),
            required = true
    )
    public CV_Table inputTable;

    @Input(name = @CAPCO("Distance Column Name"),
            desc = @CAPCO("The name of the column containing the distances for conversion."),
            required = true
    )
    public CV_String columnNameDist;

    @Input(name = @CAPCO("Input Distance Unit"),
            desc = @CAPCO("The current unit of measure."),
            required = true
    )
    public CV_String unitIn;

    @Input(name = @CAPCO("Distance Conversion Column Name"),
            desc = @CAPCO("The name of the new column containing the converted distance."
                    + "If the supplied column exists in the table, column data will be overwritten. "
                    + "Default Column Name: Distance Conversion"),
            required = true
    )
    public CV_String columnNameDistOut = new CV_String("Distance Conversion");

    @Input(name = @CAPCO("Output Distance Unit"),
            desc = @CAPCO("The new unit of measure."),
            required = true
    )
    public CV_String unitOut;

    @Output(name = @CAPCO("Output Table"),
            desc = @CAPCO("The original table with an additional column containing the converted distances.")
    )
    public CV_Table result;

    @Override
    public void run() {

        try {
            //Get index of distance column
            int columnIndex = inputTable.findIndex(columnNameDist.value());
            if (columnIndex == -1) {
                throw new RuntimeException("The Distance Column: " + columnNameDist + " is not in the table.");
            }

            //Get index of distance conversion column and check parameter type
            int columnOutIndex = inputTable.findIndex(columnNameDistOut.value());
            List<ColumnHeader> headers = inputTable.getHeader().asList();
            if (columnOutIndex != -1) {
                ParameterType type = headers.get(columnOutIndex).getType();
                if (!type.equals(ParameterType.length)) {
                    throw new RuntimeException("The Distance Conversion Column exists, but has an incorrect type: " + type.toString() + ". Expected type: length");
                }
            }

            //For each row in the table add the elements from the column to a list
            List<String> columnValues = new ArrayList<>();
            inputTable.stream().forEach(row -> {
                if (row.get(columnIndex) != null) {
                    columnValues.add(row.get(columnIndex).toString());
                } else {
                    columnValues.add(null);
                }
            });

            //Convert each distance
            List<CV_Length> convertedValues = new ArrayList<>();
            columnValues.stream().forEach(distanceString -> {
                if (distanceString != null) {
                    CV_Length distanceMETER = CV_Length.fromLength(LengthUnit.valueOf(unitIn.value()), Double.parseDouble(distanceString));
                    Double newDistance = distanceMETER.toLength(LengthUnit.valueOf(unitOut.value()));
                    convertedValues.add(new CV_Length(newDistance));
                } else {
                    convertedValues.add(null);
                }
            });

            //Add new distance column if it does not exist
            if (columnOutIndex == -1) {
                headers.add(new ColumnHeader(columnNameDistOut.value(), ParameterType.length));
            }

            //Create the new writable table
            CV_Table newTable = ctx.createTable(inputTable.getMetadata().getLabel().getValue() + "_ConvertDist");
            newTable.setHeader(new TableHeader(headers));

            //Insert the converted distance into the appropriate column
            inputTable.stream().forEach(row -> {
                try {
                    CV_Length convertedVal = convertedValues.get(inputTable.getCurrentRowNum());
                    if (columnOutIndex == -1) {
                        row.add(convertedVal);
                    } else {
                        row.set(columnOutIndex, convertedVal);
                    }

                    newTable.appendRow(row);
                    inputTable.readRow();
                } catch (TableException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            });

            result = newTable.toReadable();
        } catch (RuntimeException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            String message = "Error converting distance: ";
            logger.log(Level.SEVERE, message + ex.getMessage(), ex);
            throw new RuntimeException(message + ex.getMessage(), ex);
        }
    }
}

//UNCLASSIFIED
