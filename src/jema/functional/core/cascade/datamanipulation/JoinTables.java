/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Sep 23, 2015 10:11:48 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Performs a table join based on matching identifiers in specified column."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables", "Combine"},
        name = @CAPCO("Join Tables"),
        uuid = "7485370c-561e-461f-b9e7-463c3cecf1d5",
        tags = {"data manipulation", "table", "join"}
)
public final class JoinTables implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(JoinTables.class.getName());

    @Input(name = @CAPCO("Base Table"),
            desc = @CAPCO("The base table."),
            required = true
    )
    public CV_Table baseTable;

    @Input(name = @CAPCO("Join Field 1"),
            desc = @CAPCO("The name of the column containing the join field from the Base Table"),
            required = true
    )
    public CV_String baseKey;

    @Input(name = @CAPCO("Join Table"),
            desc = @CAPCO("The join table."),
            required = true
    )
    public CV_Table joinTable;

    @Input(name = @CAPCO("Join Field 2"),
            desc = @CAPCO("The name of the column containing the join field from the Join Table"),
            required = true
    )
    public CV_String joinKey;

    @Input(name = @CAPCO("Join Type"),
            desc = @CAPCO("Select the type of join."),
            required = true
    )
    public CV_String joinType;

    @Input(name = @CAPCO("Include Join Field 2"),
            desc = @CAPCO("Include Join Field 2 in the output table. "
                    + "If not checked, the field will be omitted"),
            required = false
    )
    public CV_Boolean includeField2 = new CV_Boolean(false);

    @Output(name = @CAPCO("Output Table"),
            desc = @CAPCO("The joined tables.")
    )
    public CV_Table result;

    @Override
    public void run() {
        try {
            TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
            SelectBuilder selectBuilder = factory.selectBuilder();
            String baseAlias = "cvt0";
            String joinAlias = "cvt1";

            //Get header of table keys
            TableHeader baseTableHeader = baseTable.getHeader();
            ColumnHeader baseKeyHeader = baseTableHeader.getHeader(baseKey.value());
            if (baseKeyHeader == null) {
                throw new RuntimeException("Join Field 1 column: " + baseKey + " is not in the Base Table.");
            }

            TableHeader joinTableHeader = joinTable.getHeader();
            ColumnHeader joinKeyHeader = joinTableHeader.getHeader(joinKey.value());
            if (joinKeyHeader == null) {
                throw new RuntimeException("Join Field 2 column: " + joinKey + " is not in the Join Table.");
            }

            //Get Base Table column headers to select
            ArrayList<ColumnHeader> selectHeaders = new ArrayList<>();
            baseTableHeader.asList().stream().forEach(columnHeader -> {
                if (!selectHeaders.contains(columnHeader)) {
                    selectHeaders.add(columnHeader);
                }
            });

            //Convert headers to strings for select statement
            ArrayList<String> selectHeaderStrings = new ArrayList<>();
            selectHeaders.stream().forEach((columnHeader) -> {
                selectHeaderStrings.add(baseAlias + "." + columnHeader.getName());
            });

            //Get Join Table column headers to select.
            //Set alias if duplicate column.
            joinTableHeader.asList().stream().forEach(columnHeader -> {
                String column = joinAlias + "." + columnHeader.getName();
                String columnAlias = column + " as " + columnHeader.getName() + "_2";
                if (!columnHeader.equals(baseKeyHeader)) {
                    if (!selectHeaders.contains(columnHeader)) {
                        int colIndex = selectHeaderStrings.indexOf(baseAlias + "." + columnHeader.getName());
                        if (colIndex != -1) { //If column name exists but is of different type
                            ParameterType type = selectHeaders.get(colIndex).getType();
                            if (!type.equals(columnHeader.getType())) {
                                selectHeaderStrings.add(columnAlias);
                            }
                        } else {
                            selectHeaderStrings.add(column);
                        }
                    } else {
                        selectHeaderStrings.add(columnAlias);
                    }
                } else {
                    if (includeField2.value()) {
                        selectHeaderStrings.add(columnAlias);
                    }
                }
            });

            String selectStatement = "SELECT " + String.join(",", selectHeaderStrings)
                    + " FROM !Table1 AS " + baseAlias
                    + " " + joinType.value() + " !Table2 AS " + joinAlias
                    + " ON " + baseAlias + "." + baseKey + "=" + joinAlias + "." + joinKey;

            // Put SELECT statement into Select Builder, associate table alias with table name, and set select statement as a CV_String
            selectBuilder.tableAs(baseTable, "!Table1").tableAs(joinTable, "!Table2").spec(CV_String.valueOf(selectStatement));

            // Execute query
            result = TableQueryExecutor.executeQuery(selectBuilder);
        } catch (RuntimeException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            String message = "Error joining tables: ";
            logger.log(Level.SEVERE, message + ex.getMessage(), ex);
            throw new RuntimeException(message + ex.getMessage(), ex);
        }
    }
}

//UNCLASSIFIED
