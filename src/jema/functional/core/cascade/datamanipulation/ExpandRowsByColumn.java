/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 14, 2015 10:28:23 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Generate an expanded version of the input table that contains "
                + "a row for each of the values found in a column, assuming the column contains "
                + "one-to-many values separated by a user-defined character."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Expand Rows By Column"),
        uuid = "6c78f8bd-ca7d-4c4a-b832-e54802654d4b",
        tags = {"table", "expand", "rows", "column"}
)
public final class ExpandRowsByColumn implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(ExpandRowsByColumn.class.getName());

    @Input(name = @CAPCO("Table"),
            desc = @CAPCO("The table containing the column to expand."),
            required = true
    )
    public CV_Table inputTable;

    @Input(name = @CAPCO("Expand Column Name"),
            desc = @CAPCO("Column name to expand. The column must be of type string "
                    + "and values must be separated by a delimiter."),
            required = true
    )
    public CV_String columnName;

    @Input(name = @CAPCO("Separator"),
            desc = @CAPCO("Separator for values in the column to expand. "
                    + "Default is semicolon."),
            required = false
    )
    public CV_String separator = new CV_String(":");

    @Output(name = @CAPCO("Output Table"),
            desc = @CAPCO("A new table that contains the expanded data.")
    )
    public CV_Table result;

    @Override
    public void run() {

        try {
            //Get index of expand column and check the type
            int columnIndex = inputTable.findIndex(columnName.value());
            List<ColumnHeader> headers = inputTable.getHeader().asList();
            if (columnIndex == -1) {
                throw new RuntimeException("The Expand Column: " + columnName + " is not in the table.");
            } else {
                ParameterType type = headers.get(columnIndex).getType();
                if (!type.equals(ParameterType.string)) {
                    throw new RuntimeException("The Expand Column exists, but has an incorrect type: " + type.toString() + ". Expected type: string");
                }
            }

            //Use input table header on output table
            result = ctx.createTable(inputTable.getMetadata().getLabel());
            TableHeader tableHeader = inputTable.getHeader();
            result.setHeader(tableHeader);

            inputTable.stream().forEach(row -> {
                try {
                    if (row.get(columnIndex) != null) {
                        String[] expandValues = row.get(columnIndex).toString().split(separator.value());
                        if (expandValues.length <= 0) {// if no value, add empty string
                            row.set(columnIndex, new CV_String(""));
                            result.appendRow(row);
                        } else {
                            for (String value : expandValues) { //Add separate row for each value
                                row.set(columnIndex, new CV_String(value));
                                result.appendRow(row);
                            }
                        }
                    } else { //if value is null, add empty string
                        row.set(columnIndex, new CV_String(""));
                        result.appendRow(row);
                    }
                    inputTable.readRow();
                } catch (TableException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            });
            result = result.toReadable();
        } catch (RuntimeException ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            String message = "Error expanding rows: ";
            logger.log(Level.SEVERE, message + ex.getMessage(), ex);
            throw new RuntimeException(message + ex.getMessage(), ex);
        }
    }
}

//UNCLASSIFIED
