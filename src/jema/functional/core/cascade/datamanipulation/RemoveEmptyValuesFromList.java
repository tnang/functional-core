/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 30, 2015 9:45:33 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.stream.Collectors;
import jema.common.types.CV_Super;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Removes empty and null values from a list."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Lists"},
        name = @CAPCO("Remove Empty Values From List"),
        uuid = "76d551ab-c126-449c-ac73-28529b5844ad",
        tags = {"list", "remove", "empty"}
)
public final class RemoveEmptyValuesFromList implements Runnable {

    @Input(name = @CAPCO("Input List"),
            desc = @CAPCO("A list to remove empty and null values from."),
            required = true
    )
    public List<CV_Super> inputList;

    @Output(name = @CAPCO("Output List"),
            desc = @CAPCO("A list with the empty and null values removed.")
    )
    public List<CV_Super> result;

    @Override
    public void run() {
        // Collect non-null values
        result = inputList
                .stream()
                .filter(f -> f != null && f.value() != null)
                .collect(Collectors.toList());

        // Collect non-empty strings
        result = result
                .stream()
                .filter(f -> !(f.toString().isEmpty()))
                .collect(Collectors.toList());
    }
}

//UNCLASSIFIED
