/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 8, 2015 8:01:30 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Given a Target Table, append columns to it from the Append Table. "
                + "User can specify which columns to append. Default: append all columns from the Append Table. "
                + "Columns are appended by adding the columns, side-by-side, to the end of the Target Table in "
                + "the same order that they appear in the Append Table. No joining or matching of data is done. "
                + "If a column to be appended already exists in the Target Table, "
                + "then the column from the Append Table will be discarded and the column in the Target Table will be retained."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Append Columns From Table"),
        uuid = "94ffe4b9-8d0b-413e-8020-1035f9678946",
        tags = {"data manipulation", "table", "append", "columns"}
)
public final class AppendColumnsFromTable implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(AppendColumnsFromTable.class.getName());

    @Input(name = @CAPCO("Target Table"),
            desc = @CAPCO("The table to append columns from the Append Table to."),
            required = true
    )
    public CV_Table targetTable;

    @Input(name = @CAPCO("Append Table"),
            desc = @CAPCO("The table containing the columns to append to the Target Table."),
            required = true
    )
    public CV_Table appendTable;

    @Input(name = @CAPCO("Append Column Names"),
            desc = @CAPCO("List of column names from Append Table to be added to the Target Table. "
                    + "Default: all columns will be appended."),
            required = false
    )
    public List<CV_String> appendColumnNames;

    @Output(name = @CAPCO("Appended Table"),
            desc = @CAPCO("A new table with the appended columns.")
    )
    public CV_Table result;

    private List<ColumnHeader> resultHeader = new ArrayList<>();
    private CV_Table appendTableMod;

    @Override
    public void run() {

        // If only one appendColumn and it exists in targetTable
        if (appendColumnNames != null && appendColumnNames.size() == 1
                && targetTable.getHeader().contains(appendColumnNames.get(0).value())) {
            try {
                result = ctx.createWritableTableCopy(targetTable).toReadable();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        } else {

            try {
                // Get Append Table
                if (appendColumnNames != null && !appendColumnNames.isEmpty()) {
                    List<ColumnHeader> appendHeaders = new ArrayList<>();
                    appendColumnNames.stream().forEach(colName -> {
                        appendHeaders.add(appendTable.getHeader().getHeader(colName.value()));
                    });

                    TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
                    SelectBuilder selectBuilder = factory.selectBuilder();
                    for (ColumnHeader input_header : appendHeaders) {
                        selectBuilder.selectColumn(input_header);
                    }
                    selectBuilder.from(appendTable);
                    appendTableMod = TableQueryExecutor.executeQuery(selectBuilder).toReadable();
                } else {
                    appendTableMod = ctx.createWritableTableCopy(appendTable).toReadable();
                }

                //check if column names exist in targetTable
                appendTableMod.getHeader().asList().stream().forEach(colHeader -> {
                    if (targetTable.getHeader().contains(colHeader.getName())) {
                        try {
                            appendTableMod = appendTableMod.dropColumn(colHeader.getName());
                        } catch (Exception ex) {
                            Logger.getLogger(AppendColumnsFromTable.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                //Set result table header
                result = ctx.createTable("AppendColumnsFromTable");
                resultHeader = getResultHeader();
                result.setHeader(new TableHeader(resultHeader));

                int resultSize = Integer.max(targetTable.size(), appendTableMod.size());
                List<CV_Super> newRow;
                for (int i = 0; i < resultSize; i++) {
                    newRow = new ArrayList<>();

                    List<CV_Super> targetRow;
                    if (i < targetTable.size()) {
                        targetRow = targetTable.readRow();
                    } else {
                        targetRow = Arrays.asList(new CV_Super[targetTable.getHeader().size()]);
                    }

                    List<CV_Super> appendRow;
                    if (i < appendTableMod.size()) {
                        appendRow = appendTableMod.readRow();
                    } else {
                        appendRow = Arrays.asList(new CV_Super[appendTableMod.getHeader().size()]);
                    }

                    newRow.addAll(targetRow);
                    newRow.addAll(appendRow);

                    result.appendRow(newRow);
                }

                result.toReadable();
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
    }

    private List<ColumnHeader> getResultHeader() {
        List<CV_Table> inputTables = new ArrayList<>();
        inputTables.add(targetTable);
        inputTables.add(appendTableMod);

        List<ColumnHeader> resultHeaders = new ArrayList<>();

        // Get the table headers
        inputTables.stream().forEach(table -> {
            table.getHeader().asList().stream().forEach(columnHeader -> {
                if (!resultHeaders.contains(columnHeader)) {
                    resultHeaders.add(columnHeader);
                }
            });
        });

        return resultHeaders;
    }
}

//UNCLASSIFIED
