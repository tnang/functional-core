/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 27, 2015
 */

package jema.functional.core.cascade.datamanipulation;


import jema.common.types.CV_Integer;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.common.types.CV_String;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "9f5f08f9-a651-407a-abde-c27991ffdc36",
        name = @CAPCO("Extract Substring"),
        desc = @CAPCO("Extracts a substring from the input string. Assumings 1 based index."),

        displayPath = {"Data Manipulation", "Strings"},
        tags = {"Data Manipulation", "Strings", "substring", "string", "extract"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public class ExtractSubstring implements Runnable {

    @Input(
            name = @CAPCO("Input String"),
            desc = @CAPCO("Input string."),
            required = true
    )
    public CV_String input_string; 
    
    @Input(
            name = @CAPCO("Start Index (inclusive)"),
            desc = @CAPCO("Input start index, index starts at 1 not 0 and is inclusive."),
            required = true
    )
    public CV_Integer input_startIndex; 
    
    @Input(
            name = @CAPCO("Stop Index (inclusive)"),
            desc = @CAPCO("Input stop index, index starts at 1 not 0 and is inclusive."),
            required = true
    )
    public CV_Integer input_stopIndex; 

    @Output(
            name = @CAPCO("Output"),
            desc = @CAPCO("Output")
    )
    public CV_String output;   

    @Override
    public void run() {
        
        if (input_startIndex.value() > 2147483646 || input_stopIndex.value() > 2147483646) {
            throw new IllegalStateException("Index is too large!");
        }
        
        int start = Integer.valueOf(String.valueOf(input_startIndex.value() - 1));
        int stop = input_stopIndex.value().intValue();
        
        if (start >= stop) {
            throw new IllegalStateException("Stop Index must be larger than Start Index!");
        }
        
        if (start == -1) {
            throw new IllegalStateException("Index starts at 1, not 0!");
        }
        
        String input = input_string.value();
        
        output = new CV_String(input.substring(start, stop));
        

    }

}

// UNCLASSIFIED
