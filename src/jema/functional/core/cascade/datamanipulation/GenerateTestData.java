/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.datamanipulation;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_TemporalRange;


import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional to generate a table with a single column of randomly generated data.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Column Name String</li>
 * <li>CV Data Type String</li>
 * <li>Regular Expression Format String</li>
 * <li>Number of Rows Integer</li>
 * <li>Percentage of Blank Values inserted Intege</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing a column of randomly generated data</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li>Column Name modified to support list of column names</li>
 * <li>Column Data Type modified to support list of column data types</li>
 * <li>Regular expression not currently implemented</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Generate random test data"),
		desc = @CAPCO("Generate a table with a single column of randomly generated data."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Data Manipulation", "Tables"},
		uuid = "601c9-9713-47c5-8571-df71b55eda33",
		tags = {"table", "test", "random"}
		)
public class GenerateTestData extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Column Name List"),
			desc = @CAPCO("List of destination table column names."),
			required = true
			)
	public List<CV_String> columnNames;

	@Input(name = @CAPCO("Column Data Type List"),
			desc = @CAPCO("Destination table column data type list that corresponds to column name list."),
			required = true
			)
	public List<CV_String> columnDataTypes;

	@Input(name = @CAPCO("Regular Expression Format"),
			desc = @CAPCO("Regular Expression Format (Not implemented)."),
			required = true
			)
	public CV_String regexFormat;

	@Input(name = @CAPCO("Number of Rows"),
			desc = @CAPCO("Number of randomly generated rows."),
			required = true
			)
	public CV_Integer numberOfRows;

	@Input(name = @CAPCO("Percentage of Blank (null) Values inserted"),
			desc = @CAPCO("Number of rows with blank or null value, Default is 0.  "),
			required = false
			)
	public CV_Integer pctBlankValues;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table containing rows and columns of randomly generated data."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(GenerateTestData.class.getName());
	
	/** Random data generator */
	private CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();
	
	/** Timestamp offset (seconds) */
	private final static int TIMESTAMP_OFFSET = (60*60*24*365*10);
	
	/** Temporal Range offset (seconds) */
	private final static int TEMPORAL_RANGE_OFFSET = (60*60*48);
	
	/**
	 * Generate random data.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Create table
			table = ctx.createTable("RandomDataTable");
			
			// Create header
			List<ColumnHeader> columnHeaderList = new ArrayList<ColumnHeader>();
        	for(int iCol = 0; iCol < this.columnDataTypes.size(); iCol++)
        	{
        		CV_String columnName = this.columnNames.get(iCol);
        		CV_String columnDataType = this.columnDataTypes.get(iCol);
    			ParameterType parameterType = ParameterType.valueOf(columnDataType.value());
    			ColumnHeader columnHeader = new ColumnHeader(
    				columnName.value(), 
    				parameterType);
    			columnHeaderList.add(columnHeader);
        	}			
			TableHeader tableHeader = new TableHeader(columnHeaderList);
			table.setHeader(tableHeader);
			
			// Setup random null rows
			Random rand = new Random(System.currentTimeMillis());
			
			// generate random data
			long numRow = this.numberOfRows.value();
			long pctNull = (this.pctBlankValues == null) ? 0l : this.pctBlankValues.value();
			
			List<CV_Super> row = null;
			for(long iRow = 0; iRow < numRow; iRow++)
			{
				row = this.genRow(rand, tableHeader, pctNull);
				table.appendRow(row);
			}
						
			// Close table, then reopen which makes table readable
			table = table.toReadable();
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		this.setDestinationTable(table);
	}
	
	/**
	 * Generate row of random values.
	 * 
	 * @param rand Random number generator
	 * @param tableHeader Table header containing applicable parameter types
	 * @param pctNull Percentage of rows containing null values.
	 * @return Row of randomly generated values
	 */
	@SuppressWarnings("rawtypes")
	protected List<CV_Super> genRow(
		Random rand,
		TableHeader tableHeader,
		long pctNull)
	{
		List<CV_Super> row = new ArrayList<CV_Super>();
		CV_Super cell = null;
		
        for (ColumnHeader columnHeader : tableHeader.asList()) 
        {
			int iNull = rand.nextInt(100);
			if(iNull < pctNull)
			{
				cell = null;
			}
			else 
			{
				switch(columnHeader.getType())
				{
					case string:
						cell = this.cvDataGenerator.genString();
						break;
					case temporalRange:
						{
			                long secondsToAdd = rand.nextLong();
			                secondsToAdd = (secondsToAdd < 0) ? -1 * secondsToAdd : secondsToAdd;
			                Duration offset = Duration.ofSeconds(secondsToAdd % TEMPORAL_RANGE_OFFSET);
							secondsToAdd = rand.nextInt(TIMESTAMP_OFFSET);
							Instant instantCur = Instant.now().plusSeconds(secondsToAdd);
			                cell = new CV_TemporalRange(instantCur, offset);
						}
						break;
					case timestamp:
						{
							long secondsToAdd = rand.nextInt(TIMESTAMP_OFFSET);
							Instant instantCur = Instant.now().plusSeconds(secondsToAdd);
					        cell = this.cvDataGenerator.genTimestamp(instantCur);
						}
						break;
						
				    default:
					   cell = this.cvDataGenerator.genSuperTest(columnHeader.getType());
					   break;
				}
			}
	        
	        row.add(cell);
	    }
        
		return row;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		ParameterType[] parameterTypeList = {ParameterType.string};
		
        if(this.columnNames == null)
        {
			String msg = String.format("Column name list is null.");
			sb.append(msg).append("\n");
			isValid = false;        	
        }
		
        if(this.columnDataTypes == null)
        {
			String msg = String.format("Column data type list is null.");
			sb.append(msg).append("\n");
			isValid = false;        	
        }
        
        if((this.columnDataTypes != null) && (this.columnNames != null))
        {
        	int columnNamesSize = this.columnNames.size();
        	int columnDataTypesSize = this.columnDataTypes.size();
            if(columnNamesSize != columnDataTypesSize)
            {
				String msg = String.format(
					"Column name list size (%d) does not match column data type list size (%d).", 
					columnNamesSize, 
					columnDataTypesSize);
				sb.append(msg).append("\n");
				isValid = false; 
            }
        }        

        if(this.columnNames != null)
        {
        	for(CV_String columnName : this.columnNames)
        	{
				isValid = this.validateColumnName(
					null, 
					columnName, 
		            sb, 
		            "Column Name",
		            parameterTypeList,
		            isValid);
        	}
        }

        if(this.columnDataTypes != null)
        {
        	for(CV_String columnDataType : this.columnDataTypes)
        	{
	    		isValid = this.validateParameterType(
	    				columnDataType, 
	    	            sb, 
	    	            isValid);
        	}
        }
		
		if(this.pctBlankValues == null) {this.pctBlankValues = CV_Integer.valueOf(0);}		
		isValid = this.validateNumericContent(
				this.pctBlankValues, 
	            sb, 
	            "Percentage blank values",
	            ParameterType.integer,
	            0.0,
	            100.0,
	            null,
	            isValid);
		
		isValid = this.validateNumericContent(
				this.numberOfRows, 
	            sb, 
	            "Number of Rows",
	            ParameterType.integer,
	            1.0,
	            null,
	            null,
	            isValid);
		
		return isValid;
	}

	/**
	 * Validate the existence of the input source table.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param srcTableRequired true=source table is required input, false=otherwise
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Source table exists, false=otherwise
	 */
	protected Boolean validateParameterType(
			CV_String parameterTypeStr,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		if(parameterTypeStr == null)
		{
			String msg = String.format("Column data type is null.", parameterTypeStr);
			sb.append(msg).append("\n");
			isValid = false;
			
		}
		else
		{	
			ParameterType parameterType = ParameterType.valueOf(parameterTypeStr.value());
	
			if(parameterType == null)
			{
				String msg = String.format("Parameter type (%s) is invalid.", parameterTypeStr);
				sb.append(msg).append("\n");
				isValid = false;
			}
		}

		return isValid;
	}
	
	/**
	 * Validate the existence of the input source table.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param srcTableRequired true=source table is required input, false=otherwise
	 * @param sb String builder containing list of error messages
     * @param parameterType Column parameter type
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Source table exists, false=otherwise
	 */
	@Override
	protected Boolean validateSourceTable(
		CV_Table srcTable,
		Boolean srcTableRequired,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		return isValid;
	}
 	
	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}
	
	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return null;}
}
