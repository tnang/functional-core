/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Nov 20, 2015 8:24:27 PM
 */
package jema.functional.core.cascade.datamanipulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Combine unbounded JSONs into a single output, coalescing objects on a designated key value. "
                + "Any JSON that does not contain the designated key will not be processed "
                + "and will not appear in the combined output."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "JSON"},
        name = @CAPCO("Combine JSONs"),
        uuid = "fb26e51e-4851-447e-bca4-2f97c8cf1ba5",
        tags = {"combine", "json"}
)
public final class CombineJSON implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(CombineJSON.class.getName());

    @Input(name = @CAPCO("Input JSONs"),
            desc = @CAPCO("JSON files."),
            required = true
    )
    public List<CV_WebResource> files;

    @Input(name = @CAPCO("Combine Key"),
            desc = @CAPCO("The key of the key / value pair used to combine JSON objects. "
                    + "Key must be present at the top level of the JSON object."),
            required = true
    )
    public CV_String combineKey;

    @Output(name = @CAPCO("Combined JSON"),
            desc = @CAPCO("Combined JSON file containing all objects with the combine key."),
            metadata = "application/json")
    public CV_WebResource output;

    /**
     * Map used to merge JSONs
     */
    Map<String, JSONObject> combinedMap = new HashMap<>();

    @Override
    public void run() {
        try {
            processJSONs();
            writeOutput();
        } catch (IOException e) {
            String message = "Issue creating output file : " + e.getMessage();
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, message);
            throw new RuntimeException(message, e);
        } catch (RuntimeException e) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, e.getMessage());
            throw new RuntimeException(e);
        } catch (Exception e) {
            String message = "Unexpected Error has occured : " + e.getMessage();
            logger.log(Level.SEVERE, message, e);
            throw new RuntimeException(message, e);
        }
    }

    /**
     * Read and combine the input JSONs.
     *
     * @throws IOException
     */
    private void processJSONs() throws IOException {
        JSONArray baseArray = null;
        JSONObject baseObject = null;
        boolean firstFile = true;
        Class firstFileClass = null;

        for (CV_WebResource jsonFile : files) {
            Object json = new JSONTokener(jsonFile.getURL().openStream()).nextValue();
            Class fileClass = json.getClass();

            // Determine class of first file
            if (firstFile) {
                firstFileClass = fileClass;
                firstFile = false;
            }

            // If file class matches class of first file
            if (fileClass.equals(firstFileClass)) {
                // Determine type of file
                if (json instanceof JSONObject) {
                    baseObject = (JSONObject) json;
                } else if (json instanceof JSONArray) {
                    baseArray = (JSONArray) json;
                }

                // Process file based on type
                if (baseArray != null) {
                    processArray(baseArray);
                } else if (baseObject != null) {
                    processObject(baseObject);
                } else {
                    ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Empty JSON file.");
                }
            } else {
                ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Ignored JSON file - type mismatch.");
            }
        }

        if (combinedMap.isEmpty()) {
            throw new RuntimeException("Input JSONs could not be processed. "
                    + "They may be empty or improperly formatted.");
        }
    }

    /**
     * Process each JSONObject in the array.
     *
     * @param array
     */
    private void processArray(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            JSONObject obj = array.getJSONObject(i);
            processObject(obj);
        }
    }

    /**
     * Add JSONObject to the combinedMap, taking the provided key into account.
     *
     * @param object
     */
    private void processObject(JSONObject object) {
        String key = combineKey.value();

        if (object.has(key)) {
            String value = object.get(key).toString();

            if (!combinedMap.containsKey(value)) {
                combinedMap.put(value, object);
            } else { //object exists for key
                for (String name : JSONObject.getNames(object)) {
                    if (!name.equalsIgnoreCase(key)) {
                        if (!combinedMap.get(value).has(name)) {
                            combinedMap.get(value).put(name, object.get(name));
                        } else { //object contains object with this name
                            Object existing = combinedMap.get(value).get(name);
                            //If existing object is JSONArray, concatenate
                            if (existing instanceof JSONArray && object.get(name) instanceof JSONArray) {
                                JSONArray existingArray = (JSONArray) existing;
                                JSONArray newArray = object.getJSONArray(name);

                                for (int i = 0; i < newArray.length(); i++) {
                                    existingArray.put(newArray.get(i));
                                }
                                combinedMap.get(value).put(name, existingArray);

                            } else { //Create new array
                                JSONArray newArray = new JSONArray();
                                newArray.put(existing);
                                newArray.put(object.get(name));
                                combinedMap.get(value).put(name, newArray);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Write the combined JSON.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void writeOutput() throws FileNotFoundException, IOException {
        List<JSONObject> outArray = new ArrayList<>(combinedMap.size());

        combinedMap.keySet().stream().forEach((key) -> {
            outArray.add(combinedMap.get(key));
        });

        JSONArray mergedJson = new JSONArray(outArray.toArray());
        File combinedJSON = ctx.createTempFileWithExtension(".json");
        try (OutputStream os = new FileOutputStream(combinedJSON)) {
            IOUtils.write(mergedJson.toString(), os);
        }

        output = new CV_WebResource(combinedJSON.toURI().toURL());
    }
}

//UNCLASSIFIED
