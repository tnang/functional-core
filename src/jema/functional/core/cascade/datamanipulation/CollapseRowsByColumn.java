/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;

import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.functional.core.cascade.util.SHA1;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Collapses rows that have duplicate values in all columns except a single user defined collapse column."+
                "Values in the collapse column will be concatenated together in the resulting row of the output table, separated by a user defined separator."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Collapse Rows By Column"),
        uuid = "55a37299-b6b4-4f14-89f7-9f73d1c17d4d",
        tags = {"collapse", "rows", "subset","table"}
)

public final class CollapseRowsByColumn implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The input table, rows with duplicates will be collapsed."),
            required = true
    )
    public CV_Table inputTable;

    @Input(desc = @CAPCO("Column Name"),
            name = @CAPCO("Column in the Input Table that contains the data to be collapsed into one value for the duplicate rows"),
            required = true
    )
    public CV_String columnName ;

    @Input(desc = @CAPCO("Seperator"),
            name = @CAPCO("A optional single character that values in the resulting collapsed column will be seperated by (e.g. a comma, pipe, semi-colon)."+
                    "If left blank, default value is assumed to be a space."),
            required = false
    )
    public CV_String deliminator;
    
    @Output(name = @CAPCO("Result Table"),
            desc = @CAPCO("A new table containing the collapsed data.")
    )
    public CV_Table outputTable;



    @Override
    public void run() {

        //In memory store of row hashes.
        Map<String, List<CV_Super>> row_map = new HashMap<>();

        if (deliminator == null) {

            deliminator = new CV_String(" ");
        }

        try {
            outputTable = ctx.createTable(inputTable.getMetadata().getLabel());
            List<ColumnHeader> columns = new ArrayList<>();
            inputTable.getHeader().forEach(columnheader -> {
                columns.add(columnheader);
            });

            int columnheaderindex = inputTable.getHeader().findIndex(columnName.value());
            
            
            if (columnheaderindex == -1) {

                String message = "The column name can not be found within the table, please check the spelling of the 'Column Name' variable";
                ctx.log(ExecutionContext.MsgLogLevel.WARNING, message);
                throw new IllegalArgumentException(message);
            }
            
            

            outputTable.setHeader(new TableHeader(columns));

            //Iterate over each row in the input table
            inputTable.stream().forEach((List<CV_Super> current_row) -> {

                try {

                    //Generate a row hash.
                    String hashed_row = createRowHash(current_row, columnheaderindex);
                    //check to see if the hashedrow is unique or not.
                    if (!row_map.containsKey(hashed_row)) {
                        //hashrow is unique and entered in the outputtable and row_map for future lookup
                        row_map.put(hashed_row, current_row);

                    } else {//hashed_row is NOT unique
                        List<CV_Super> row = row_map.get(hashed_row);
                        CV_Super column = row.get(columnheaderindex);

                        StringBuilder sb = new StringBuilder();
                        sb.append(column.value());
                        sb.append(deliminator.value());
                        sb.append(current_row.get(columnheaderindex).value());
                        row.set(columnheaderindex, new CV_String(sb.toString()));
                        row_map.put(hashed_row, row);

                    }

                    inputTable.readRow();
                } catch (TableException ex) {
                    ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception " + ex.getMessage());
                    throw new RuntimeException(ex);
                }
            });
            
            for (String val : row_map.keySet()) {
                outputTable.appendRow(row_map.get(val));
                //System.out.println(row_map.get(val));
            }

        } catch (Exception ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception " + ex.getMessage());
            throw new RuntimeException(ex);
        }

    }
    
    
    /**
    * Returns a String SHA1 has value of the input row
    * @param  row  value of the row to hash
    * @param  columnheaderindex  the column to collapse on
    * @return  String SHA1 hashed row value
    */
    private String createRowHash(List<CV_Super> row, int columnheaderindex) {
        //get the collapsable column name field 
        CV_Super temp = row.get(columnheaderindex);
        //set the field to blank as it should NOT be a part of the row hash. 
        row.set(columnheaderindex, new CV_String(""));
        
        StringBuilder rowhash = new StringBuilder();
        
        for(CV_Super s:row){
            
            rowhash.append(s.value());
        }
        //reset the collapsable column name field that is NOT part of the row hash
        row.set(columnheaderindex, temp);
        SHA1 md = new SHA1();
        md.reset();
        md.update(rowhash.toString().getBytes());
        md.finish();
        
        return  md.digout();
    }
    
}//UNCLASSIFIED