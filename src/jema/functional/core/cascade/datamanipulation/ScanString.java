/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Aug 21, 2015
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
/**
*
* @author cascade
*/

@Functional(
       uuid = "b186ee9e-50ee-4843-9276-dab759a819df",
       name = @CAPCO("Scan String"),
       desc = @CAPCO("Applies a regular expression to an input string and generates a " +
    		   "list of output strings for substring matching the expression. " +
    		   "Analagous to the C/C++ 'scanf()' function. Users Java regular " +
    		   "expression syntax."),
       displayPath = {"Data Manipulation", "Strings"},
       tags = {"Data Manipulation", "Strings", "scan", "string", "regex"},
       lifecycleState = LifecycleState.TESTING,
       creator = "cascade"
)

public class ScanString implements Runnable {

    @Input(
            name = @CAPCO("Input String"),
            desc = @CAPCO("Input string value."),
            required = true
    )
    public CV_String input_string; 

    @Input(
            name = @CAPCO("Regular Expression"),
            desc = @CAPCO("Regular express that will be applied to match against the input string"),
            required = true
    )
    public CV_String input_regex; 
       
    @Output(
            name = @CAPCO("Matches"),
            desc = @CAPCO("Segement(s) of the input string that match the expression")
    )
    public List<CV_String> output;   
    
    @Context
    ExecutionContext ctx;
    
    @Override
    public void run() {
        
    	List<CV_String> results = new ArrayList<CV_String>();
        Pattern pattern = Pattern.compile(input_regex.value());
        Matcher matcher = pattern.matcher(input_string.value());
        
        while (matcher.find()) {
        	results.add(new CV_String(matcher.group()));
        }
    	
        output = results;
    }

}

// UNCLASSIFIED
