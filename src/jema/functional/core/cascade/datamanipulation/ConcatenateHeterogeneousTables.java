/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Oct 7, 2015 8:16:42 AM
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Concatenate tables with different columns into one table."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Concatenate Heterogeneous Tables"),
        uuid = "391fb389-f0b3-41cd-a54f-2b9e0b08029a",
        tags = {"data manipulation", "table", "concatenate", "heterogeneous"}
)
public final class ConcatenateHeterogeneousTables implements Runnable {

    @Context
    public ExecutionContext ctx;

    private static final Logger logger = Logger.getLogger(ConvertDistanceInTable.class.getName());

    @Input(name = @CAPCO("Input Table(s)"),
            desc = @CAPCO("The table(s) to concatenate together."),
            required = true
    )
    public List<CV_Table> inputTables;

    @Input(name = @CAPCO("Ignore Column Type"),
            desc = @CAPCO("If selected, columns with the same name will be concatenated regardless of type."
                    + " Otherwise, they will be separate columns."),
            required = false
    )
    public CV_Boolean ignoreType = new CV_Boolean(false);

    @Output(name = @CAPCO("Concatenated Table"),
            desc = @CAPCO("A new table with the concatenated results.")
    )
    public CV_Table result;

    private List<ColumnHeader> resultHeader = new ArrayList<>();

    @Override
    public void run() {
        // If only one input table, output the table
        if (inputTables.size() == 1) {
            try {
                result = ctx.createWritableTableCopy(inputTables.get(0)).toReadable();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        } else {
            doHeterogeneousConcatenate();
        }
    }

    private void doHeterogeneousConcatenate() {
        try {
            //Set result table header
            result = ctx.createTable("HeterogeneousConcatenateTables");
            resultHeader = getResultHeader();
            result.setHeader(new TableHeader(resultHeader));

            //Concatenate tables
            inputTables.stream().forEach(table -> {
                //Get result table indicies for each column in current table
                TableHeader tableHeader = table.getHeader();
                List<Integer> resultIndicies = getResultIndicies(tableHeader);
                List<CV_Super> newRow = Arrays.asList(new CV_Super[resultHeader.size()]);

                //Write to output table
                try {
                    table.stream().forEach(row -> {
                        try {
                            for (int i = 0; i < row.size(); i++) {
                                newRow.set(resultIndicies.get(i), row.get(i));
                            }
                            result.appendRow(newRow);
                            table.readRow();
                        } catch (TableException ex) {
                            throw new RuntimeException(ex.getMessage(), ex);
                        }
                    });
                } catch (TableException ex) {
                    ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Unable to read table... Skipping.");
                }
            });

            result.toReadable();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    /**
     *
     * @return
     */
    private List<ColumnHeader> getResultHeader() {
        List<ColumnHeader> resultHeaders = new ArrayList<>();
        List<String> resultHeadersStr = new ArrayList<>();
        // Get the table headers
        inputTables.stream().forEach(table -> {
            List<ColumnHeader> tableHeader = table.getHeader().asList();
            tableHeader.stream().forEach(columnHeader -> {
                if (!resultHeaders.contains(columnHeader)) {
                    int colIndex = resultHeadersStr.indexOf(columnHeader.getName().toLowerCase());
                    if (colIndex != -1) { //If column name exists but is of different type
                        ParameterType type = resultHeaders.get(colIndex).getType();
                        if (!type.equals(columnHeader.getType())) {
                            if (!ignoreType.value()) { //create 2nd column
                                ColumnHeader newHeader = new ColumnHeader(columnHeader.getName() + "_2", columnHeader.getType());
                                resultHeaders.add(newHeader);
                                resultHeadersStr.add(newHeader.getName().toLowerCase());
                            } else { //change column type
                                ColumnHeader newHeader = new ColumnHeader(columnHeader.getName(), ParameterType.string);
                                resultHeaders.set(colIndex, newHeader);
                            }
                        }
                    } else {
                        resultHeaders.add(columnHeader);
                        resultHeadersStr.add(columnHeader.getName().toLowerCase());
                    }
                }
            });
        });

        return resultHeaders;
    }

    /**
     * Get list of indices for the input table header from the result table
     * header.
     *
     * @param header
     * @return
     */
    private List<Integer> getResultIndicies(TableHeader header) {
        TableHeader resultTableHeader = new TableHeader(resultHeader);
        List<Integer> resultIndicies = new ArrayList<>();

        header.asList().stream().forEach(columnHeader -> {
            int index = resultTableHeader.findIndex(columnHeader);
            if (index != -1) {
                resultIndicies.add(index);
            } else {
                index = resultTableHeader.findIndex(columnHeader.getName() + "_2");
                if (index != -1) {
                    resultIndicies.add(index);
                } else {
                    index = resultTableHeader.findIndex(columnHeader.getName());
                    if (index != -1) {
                        resultIndicies.add(index);
                    }
                }
            }
        });
        return resultIndicies;
    }
}

//UNCLASSIFIED
