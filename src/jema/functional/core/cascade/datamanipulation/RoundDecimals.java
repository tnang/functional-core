/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Sep 26, 2015
 */

package jema.functional.core.cascade.datamanipulation;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;

import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

/**
 *
 * @author cascade
 */

@Functional(
        uuid = "c5d21dfd-8c4f-49c9-936b-55b50e5b8b51",
        name = @CAPCO("Round Decimal (Column or List)"),
        desc = @CAPCO(  "in a column (table or list) to the nth decimal place. "
                      + "Input column on table can be of type {string} or {decimal}. Output will be {decimal}."),

        displayPath = {"Data Manipulation", "Tables"},
        tags = {"round", "rounding", "decimal", "column", "double", "table", "list"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)

public class RoundDecimals implements Runnable {

    @Input(
            name = @CAPCO("Input Table"),
            desc = @CAPCO("Input a table with your column of data to round."),
            required = false
    )
    public CV_Table inputTable; 
    
    @Input(
            name = @CAPCO("Input Column Name of Table"),
            desc = @CAPCO("Input the name of the column of the table."),
            required = false
    )
    public CV_String inputColumnName; 
    
    @Input(
            name = @CAPCO("Input List"),
            desc = @CAPCO("Input a list with your column of data to round."),
            required = false
    )
    public List<CV_String> inputList; 
    
    @Input(
            name = @CAPCO("Select method"),
            desc = @CAPCO("Select method"),
            required = true
    )
    public CV_String input_roundingMethod;
    
    @Input(
            name = @CAPCO("Input Decimal's place"),
            desc = @CAPCO("Input number of decimals to round to. No fewer than 0."),
            required = true
    )
    public CV_Integer inputRoundingPlace; 

    @Output(
            name = @CAPCO("Output List"),
            desc = @CAPCO("Output List")
    )
    public List<CV_Decimal> outputList;   
    
    @Output(
            name = @CAPCO("Output Table"),
            desc = @CAPCO("Output Table")
    )
    public CV_Table outputTable;   

    @Context
    public ExecutionContext ctx;
    
    // make header value for new table, check if columns labeled Latitude and Longitude already exist and throw error if they do
    public TableHeader fixHeaders() {
        TableHeader tempHeader = inputTable.getHeader();
        List<ColumnHeader> tempColHeader = tempHeader.asList();
        tempColHeader.add(new ColumnHeader(inputColumnName.value()+ "_ROUNDED", ParameterType.decimal));
        return new TableHeader(tempColHeader);
    }
    
    public String fixString(double inputDouble, int roundingPlace, String roundMethod) {
        String outString = "";
        if (roundMethod.equals("halfUp")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_HALF_UP).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        if (roundMethod.equals("halfDown")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_HALF_DOWN).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        if (roundMethod.equals("ceiling")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_CEILING).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        if (roundMethod.equals("floor")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_FLOOR).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        if (roundMethod.equals("up")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_UP).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        if (roundMethod.equals("down")) {
            outString = BigDecimal.valueOf(inputDouble).setScale(roundingPlace, BigDecimal.ROUND_DOWN).toPlainString().replaceAll("0*$", "").replaceAll("\\.$", ".0");
        }
        return outString;
    }
    
    
    
    
    @Override
    public void run() throws RuntimeException {
        
        int placeToRoundTo = Integer.parseInt(inputRoundingPlace.toString());
        String roundingMethod = input_roundingMethod.value();

        //check for input values
        if ((inputTable == null) && (inputList.isEmpty())) {
            throw new RuntimeException("You must input either a table or a list or both a table and a list!");
        }
        
        //check for table + column
        if ((null != inputTable) && (null == inputColumnName)) {
            throw new RuntimeException("You must input a column name if you input a table!");
        }
                
        if (null!=inputList) {
            List<CV_Decimal> tempList = new ArrayList<>();
            int listSize = inputList.size();
            double tempDouble;
            String outString = "";
            
            for (int i=0;i<listSize;i++) {
                try {
                    tempDouble = Double.valueOf(inputList.get(i).value());
                    outString = fixString(tempDouble, placeToRoundTo, roundingMethod);
                } catch (NullPointerException npe) {
                    throw new RuntimeException(npe);
                }
              
                CV_Decimal outDecimal = new CV_Decimal(outString);
                tempList.add(outDecimal);
            }
            outputList = tempList; 
        }
        
        
        if (null!=inputTable) {
            TableHeader tableHeader;
            int rowCount;
            List<CV_Decimal> tempList = new ArrayList<>();
           
            try {
                CV_Table newTable = ctx.createTable(inputTable.getMetadata().getLabel().getValue());
                tableHeader = fixHeaders();
                newTable.setHeader(tableHeader);
                int columnCount = tableHeader.size();
                int colPlacement = tableHeader.findIndex(inputColumnName.value());
                rowCount = inputTable.size();
                double tempDouble;
                
                // start looping through rows
                for (int j=1; j<=rowCount; j++) {
                    List<String> tempStringArray = new ArrayList<>();
                    List<CV_Super> tempSuperArray;
                    tempSuperArray = inputTable.readRow();
                    
                    try {
                    // start looping through columns in row
                    for (int k=0; k<columnCount-1; k++) {
                        if (null != tempSuperArray.get(k)) {
                            // add to new generated row
                            tempStringArray.add(tempSuperArray.get(k).toString());
                        } else {
                            tempStringArray.add("");
                        }
                    }
                    } catch (Exception e) {
                        ctx.log(ExecutionContext.MsgLogLevel.INFO, e.toString());
                    }
                    
                    tempDouble = Double.valueOf(tempSuperArray.get(colPlacement).toString());
                    String outTableString = fixString(tempDouble, placeToRoundTo, roundingMethod);
                            
                    /*        BigDecimal.valueOf(tempDouble)
                            .setScale(placeToRoundTo, BigDecimal.ROUND_HALF_UP)
                            .toPlainString()
                            .replaceAll("0*$", "")
                            .replaceAll("\\.$", ".0");*/
                    tempStringArray.add(outTableString);
                    
                    newTable.appendRow(tempStringArray);
                }
            outputTable = newTable.toReadable();
                
            } catch (Exception e) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, e.toString());
            }
            
            
        }
        
        
        
    }

}

// UNCLASSIFIED
