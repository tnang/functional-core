/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.logging.Logger;

import jema.common.types.CV_Integer;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.common.types.table.query.builder.WhereBuilder;
import jema.common.types.table.sql.WriteableSQLTableSPI;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional that copies rows in the specified index range from the table 
 * to a new table. The start or stop index may be omitted to copy all 
 * elements before or after the index.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Start Index Integer</li>
 * <li>Stop Index Integer</li>
 * <li>Length Integer</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing the rows [Start index-Stop index], 
 * [Start index - (Start Index + length)], [0 - Stop Index], 
 * depending on the user's input parameters</li>
 * </ul>
 * <p>
 * 
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://pgxn.org/dist/kmeans/">PGXN PostgreSQL Extension Network - kmeans</a></li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Copy table rows to destination table"),
		desc = @CAPCO("Copy rows in the specified index range from the table to a new table. " +
				"The start or stop index may be omitted to copy all elements before or after the index."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Data Manipulation", "Tables"},
		uuid = "7dbb735f-0c83-4f14-9c4c-94df7b802241",
		tags = {"table", "copy"}
		)
public class CopyTableRows extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table."),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Start Index"),
			desc = @CAPCO("Start Index. Index range from 0 thru end of table." + 
					"Default is 0"),
			required = false
			)
	public CV_Integer startIndex;

	@Input(name = @CAPCO("Stop Index"),
			desc = @CAPCO("Stop Index. Index range from 0 thru end of table." + 
					"Default is end of table"),
			required = false
			)
	public CV_Integer stopIndex;

	@Input(name = @CAPCO("Length"),
			desc = @CAPCO("Number of records to copy from start index."),
			required = false
			)
	public CV_Integer length;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table containing the rows [Start index-Stop index], " +
					"[Start index - (Start Index + length)], [0 - Stop Index], " + 
					"depending on the user's input parameters."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(CopyTableRows.class.getName());

	/**
	 * Make where statement.
	 * 
	 * @param selectBuilder selectBuilder
	 */
	protected WhereBuilder makeWhereStatement(
		TableQueryBuilderFactory factory)
	{
		WhereBuilder wb = null;

		try
		{
			ColumnHeader rowId = new ColumnHeader(WriteableSQLTableSPI.PRIMARY_KEY, ParameterType.integer);
			
			// Start Index to Stop Index
			if((this.startIndex != null) && (this.stopIndex != null))
			{
				wb = factory.whereBuilder();
				WhereBuilder wbStart = factory.whereBuilder();
				wbStart.gte(rowId, this.startIndex);
				WhereBuilder wbStop = factory.whereBuilder();
				wbStop.lte(rowId, this.stopIndex);
				wb.and(wbStart, wbStop);
			}

			// Only Start Index 
			else if(this.startIndex != null)
			{
				wb = factory.whereBuilder();
				wb.gte(rowId, this.startIndex);
			}

			// Only Stop Index
			else if(this.stopIndex != null)
			{
				wb = factory.whereBuilder();
				wb.lte(rowId, this.stopIndex);
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return wb;
	}

	/**
	 * Create and execute SQL statement.
	 */
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Build query 
			TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
			SelectBuilder selectBuilder = factory.selectBuilder();
			selectBuilder.from(this.srcTable);
			
			WhereBuilder whereBuilder = this.makeWhereStatement(factory);
			if(whereBuilder != null) {selectBuilder.where(whereBuilder);}

			// Execute query
			table = TableQueryExecutor.executeQuery(selectBuilder);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		this.setDestinationTable(table);
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
			CV_Table srcTable,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		if(this.startIndex != null)
		{
			isValid = this.validateNumericContent(
				this.startIndex, 
				sb, 
				"Start Index",
				ParameterType.integer,
				0.0,
				null,
				null,
				isValid);
		}
		else
		{
			this.startIndex = CV_Integer.valueOf(0);
		}

		if(this.stopIndex != null)
		{
			Double minValue = (this.startIndex.value() < 0) ? 0 : this.startIndex.value().doubleValue();
			
			isValid = this.validateNumericContent(
				this.stopIndex, 
				sb, 
				"Stop Index",
				ParameterType.integer,
				minValue,
				null,
				null,
				isValid);
		}

		if(this.length != null)
		{
			isValid = this.validateNumericContent(
				this.length, 
				sb, 
				"Length",
				ParameterType.integer,
				0.0,
				null,
				null,
				isValid);
			
			if(this.stopIndex == null)
			{
				this.stopIndex = CV_Integer.valueOf(this.startIndex.value() + this.length.value());
			}
			else
			{				
				String msg = String.format(
					"Cannot set both Stop Index (%d) and Length (%d)!", 
					this.stopIndex.value(),
					this.length.value());
				sb.append(msg).append("\n");
				isValid = false;
			}
		}

		return isValid;
	}

	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}

	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return this.srcTable;}
}
