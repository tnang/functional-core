/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.datamanipulation;

import java.util.ArrayList;
import java.util.List;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Reorders the tables's column to the specified input."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables" },
        name = @CAPCO("Reorder Columns"),
        uuid = "67c7a388-2f4d-4c15-b99a-eee9039bda95",
        tags = {"table","column","sort"}        
)

public final class ReOrderColumns implements Runnable {

    @Context
    public ExecutionContext ctx;
    
    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The table from which to reorder columns."),
            required = true
    )
    public CV_Table inputTable;
    
    @Input(name = @CAPCO("Input List"),
           desc = @CAPCO("An input list to deduplicate."),
           required = true
    )
    public List<CV_String> input;
    
    @Output(name = @CAPCO("Reordered Column Table"),
            desc = @CAPCO("A new table with columns reordered.")
    )
    public CV_Table outputTable;
    
    @Input(name = @CAPCO("Append Remaining?"),
            desc = @CAPCO("Append remaining fields to the end of the data table"))
    public CV_Boolean isAppending = new CV_Boolean(false);

    
    @Override
    public void run() {
        
        List<ColumnHeader> listofheaders = new ArrayList<>();
        for (CV_String input_header : input) {
            listofheaders.add(inputTable.getHeader().getHeader(input_header.value()));
        }
        if (isAppending.value()) {
                
            for(ColumnHeader header:inputTable.getHeader().asList()){
                if(!listofheaders.contains(header)){
                     listofheaders.add(header);
                }
            } 
        }

        try {

            TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
            SelectBuilder selectBuilder = factory.selectBuilder();
            
            //if listofheaders is empty...than the original table will be the output
            for (ColumnHeader input_header : listofheaders) {
              
                selectBuilder.selectColumn(input_header);

            }

            selectBuilder.from(inputTable);
            outputTable = TableQueryExecutor.executeQuery(selectBuilder);

        } catch (Exception ex) {
            ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Exception " + ex.getMessage());
        }

    }
}
//UNCLASSIFIED