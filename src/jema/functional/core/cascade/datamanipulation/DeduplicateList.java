/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.datamanipulation;

import java.util.List;
import java.util.stream.Collectors;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns a deduplicated list of values"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Lists" },
        name = @CAPCO("Deduplicate List"),
        uuid = "0d8c2053-d339-4f1f-bfd3-e1f5e014b360",
        tags = {"list","deduplicate"}        
)

public final class DeduplicateList implements Runnable {

    
    @Input(name = @CAPCO("Input List"),
           desc = @CAPCO("An input list to deduplicate."),
           required = true
    )
    public List<CV_String> input;
    
    @Output(name = @CAPCO("Output List"),
            desc = @CAPCO("A deduplicated output list")
    )
    public List<CV_String> result;


    
    @Override
    public void run() {
        
     result = input.stream().distinct().collect(Collectors.toList());
        
    }
}
//UNCLASSIFIED