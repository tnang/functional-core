/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.datamanipulation;

import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Functional to calculate clusters for Geos based on user defined distance and 
 * minimum Geos per cluster inputs.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Feature Class Table</li>
 * <li>Latitude Column Name String</li>
 * <li>Longitude Column Name String</li>
 * <li>Discriminator Column Name String</li>
 * <li>Max Separation Decimal</li>
 * <li>Minimum Geos Integer</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Feature Class Table containing Geos</li>
 * <li>Feature Class Table containing Clusters</li>
 * </ul>
 * <p>
 * 
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://pgxn.org/dist/kmeans/">PGXN PostgreSQL Extension Network - kmeans</a></li>
 * <li><a href="iki.osgeo.org/wiki/Point_Clustering">Point Clustering</a></li>
 * <li><a href="http://gis.stackexchange.com/questions/11567/spatial-clustering-with-postgis">Spatial clustering with PostGIS</a></li>
 * <li><a href="ww-users.cs.umn.edu/~kumar/dmbook/ch8.pdf">Cluster Analysis: Basic Concepts and Algorithms</a></li>
 * <li><a href="http://stats.stackexchange.com/search?q=%5Bclustering%5D+%2Bdistance">Stack Exchange: Cross Validated - [clustering] +distance</a></li>
 * <li><a href="http://jonisalonen.com/2012/k-means-clustering-in-mysql/">K-means Clustering in MySQL</a></li>
 * <li><a href="http://www.postgresonline.com/journal/archives/216-PostgreSQL-9.1-Exploring-Extensions.html">
 * POSTGRESQL 9.1 EXPLORING EXTENSIONS</a></li>
 * <li><a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.109.5005&rep=rep1&type=pdf">
 * Programming the K-means Clustering Algorithm in SQL</a></li>
 * <li><a href="https://gist.github.com/mgiraldo/3f810f49c09d3311e964">postgis-cluster-lat-lon-with-filters.sql</a></li>
 * 
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculate Geo Clusters"),
		desc = @CAPCO("Calculates clusters for Geos based on user defined distance and " +
		"minimum Geos per cluster inputs."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Data Manipulation", "Tables"},
		uuid = "3ee661d5-8d9b-4170-a96c-31f6cdcf6bc0",
		tags = {"table", "geocluster", "k-means"}
		)
public class GeoClusterRows extends TableFunctionalBase 
{   
	@Input(name = @CAPCO("Source feature class table"),
			desc = @CAPCO("Source table containing geospatial data."),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Latitude Column Name"),
			desc = @CAPCO("Latitude Column Name."),
			required = true
			)
	public CV_String latitudeColumnName;

	@Input(name = @CAPCO("Longitude Column Name"),
			desc = @CAPCO("Longitude Column Name."),
			required = true
			)
	public CV_String longitudeColumnName;

	@Input(name = @CAPCO("Discriminator Column Name"),
			desc = @CAPCO("Discriminator Column Name."),
			required = true
			)
	public CV_String discriminatorColumnName;

	@Input(name = @CAPCO("Maximum Separation"),
			desc = @CAPCO("Maximum Separation"),
			required = true
			)
	public CV_Decimal maxSeparation;

	@Input(name = @CAPCO("Minimum Geos"),
			desc = @CAPCO("Minimum Geos."),
			required = true
			)
	public CV_Integer minGeos;

	@Output(name = @CAPCO("Feature Class Table containing Geos"),
			desc = @CAPCO("Feature Class Table containing Geos."),
			required = true
			)
	public CV_Table destGeosTable;

	@Output(name = @CAPCO("Feature Class Table containing Clusters"),
			desc = @CAPCO("Feature Class Table containing Clusters."),
			required = true
			)
	public CV_Table destClusterTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(GeoClusterRows.class.getName());
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		ParameterType[] parameterTypeList = {ParameterType.string};

		Boolean isValid = this.validateColumnName(
			this.srcTable, 
			this.latitudeColumnName, 
            sb, 
            "Latitude Column Name",
            parameterTypeList,
            isValidCur);

		isValid = this.validateColumnName(
			this.srcTable, 
			this.longitudeColumnName, 
            sb, 
            "Longitude Column Name",
            parameterTypeList,
            isValid);

		if(this.discriminatorColumnName != null)
		{
			isValid = this.validateColumnName(
				this.srcTable, 
				this.discriminatorColumnName, 
	            sb, 
	            "Disciminator Column Name",
	            parameterTypeList,
	            isValid);
		}
		
		isValid = this.validateNumericContent(
				this.maxSeparation, 
	            sb, 
	            "Maximum Separation",
	            ParameterType.decimal,
	            0.0,
	            null,
	            null,
	            isValid);
		
		isValid = this.validateNumericContent(
				this.minGeos, 
	            sb, 
	            "Minimum Geos",
	            ParameterType.integer,
	            0.0,
	            null,
	            null,
	            isValid);
		
		return isValid;
	}
	
	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	@Override
	protected void setDestinationTable(CV_Table destTable) {}
	
	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	@Override
	protected CV_Table getSourceTable() {return null;}
}
