/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.retrieval;

import jema.functional.api.ExecutionContext;
import jema.functional.api.Context;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.common.security.DN;
import jema.common.types.CV_String;
import jema.functional.api.Output;


/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the user's PKI Distinguished Name"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Security"},
        name = @CAPCO("Get My DN"),
        uuid = "3282e15c-8e13-47a7-a06a-99b67e142328",
        tags = {"dn", "pki" }
        
)
public final class GetMyDN implements Runnable {


    @Context
    public ExecutionContext ctx;
    
    @Output(name = @CAPCO("DN Result"),
            desc = @CAPCO("The DN of the current user"),
            required = true
    )
    public CV_String result;

    /**
    * Returns a String value of the PrincipalDN as provided by the Execution Context
    * @return  String of the PrincipalDN
    */
    public String getDN(){
        DN dn =ctx.getPrincipalDN();
        return  dn.getAsProvided();
    }
    
    @Override
    public void run() {
        
        result =new CV_String(getDN());
        ctx.log(ExecutionContext.MsgLogLevel.INFO, "DN Principal = "+result.value());
    
    }
}

//UNCLASSIFIED