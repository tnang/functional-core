/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.spatialanalysis;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;
import jema.functional.core.cascade.util.geo.LatLon;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", name = @CAPCO("Calculate Distance Between Coordinates"),
	desc = @CAPCO("Calculates the distance between two latitude and longitude coordinates." + "Outputs distance in Nautical Miles and Azmuth in Degrees."),
	lifecycleState = LifecycleState.TESTING, displayPath = { "Spatial Analysis", "Calculation" }, uuid = "e034b318-a94c-4f5b-8c8d-ba4ae7110252", tags = {
		"Calculation", "Points", "Distance", "spatial", "analysis" })
public class CalculateDistanceBetweenCoordinates implements Runnable {

	@Input(name = @CAPCO("Latitude One"), desc = @CAPCO("First Latitude Coordinate (DD)"), required = true)
	public CV_Decimal inputLatitudeOne;

	@Input(name = @CAPCO("Longitude One"), desc = @CAPCO("First Longitude Coordinate (DD)"), required = true)
	public CV_Decimal inputLongitudeOne;

	@Input(name = @CAPCO("Latitude Two"), desc = @CAPCO("Second Latitude Coordinate (DD)"), required = true)
	public CV_Decimal inputLatitudeTwo;

	@Input(name = @CAPCO("Longitude Two"), desc = @CAPCO("Second Longitude Coordinate (DD)"), required = true)
	public CV_Decimal inputLongitudeTwo;

	@Output(name = @CAPCO("Distance"), desc = @CAPCO("The distance between the input points in Nautical Miles"))
	public CV_Decimal outputDistance;

	@Output(name = @CAPCO("Azmuth"), desc = @CAPCO("The Azmuth between the input points in Degrees"))
	public CV_Decimal outputAzmuth;

	@Context
	ExecutionContext ctx;

	@Override
	public void run() {
		try {

			if (inputLatitudeOne == null || inputLongitudeOne == null || inputLatitudeTwo == null || inputLongitudeTwo == null) {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "One or more input coordinates were null");
				throw new RuntimeException("One or more input points were null");
			}
			CoordinateConversion convert = new CoordinateConversion();
			LatLon point1, point2;
			if (convert.isDDLatLonValid(inputLatitudeOne.value(), inputLongitudeOne.value())) {
				point1 = LatLon.fromDegrees(inputLatitudeOne.value(), inputLongitudeOne.value());
			} else {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Input Point #1 was not a valid Latitude/Longitude Coordinate");
				throw new RuntimeException("Input Point #1 was not a valid Latitude/Longitude Coordinate");
			}

			if (convert.isDDLatLonValid(inputLatitudeTwo.value(), inputLongitudeTwo.value())) {
				point2 = LatLon.fromDegrees(inputLatitudeTwo.value(), inputLongitudeTwo.value());
			} else {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Input Point #2 was not a valid Latitude/Longitude Coordinate");
				throw new RuntimeException("Input Point #2 was not a valid Latitude/Longitude Coordinate");
			}

			double distance = LatLon.greatCircleDistanceNmi(point1, point2);
			double azmuth = (LatLon.greatCircleAzimuth(point1, point2).getDegrees() + 360) % 360;

			outputDistance = new CV_Decimal(distance);
			outputAzmuth = new CV_Decimal(azmuth);

		} catch (Exception e) {
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Unexpected Error: " + e.getMessage());
			throw new RuntimeException("Unexpected Error: " + e.getMessage());
		}

	}

}
