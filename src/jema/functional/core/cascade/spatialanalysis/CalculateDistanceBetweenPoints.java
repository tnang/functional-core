/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.spatialanalysis;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Point;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.CoordinateConversion;
import jema.functional.core.cascade.util.geo.Angle;
import jema.functional.core.cascade.util.geo.LatLon;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", name = @CAPCO("Calculate Distance Between Points"), desc = @CAPCO("Calculates the distance between two point values. " +
	"Outputs distance in Nautical Miles and Azmuth in Degrees"),
	lifecycleState = LifecycleState.TESTING, displayPath = { "Spatial Analysis", "Calculation" }, uuid = "2d117604-2cd0-4898-8c9b-e481e103ed18", tags = {
		"Calculation", "Points", "Distance", "spatial", "analysis" })
public class CalculateDistanceBetweenPoints implements Runnable {

	@Input(name = @CAPCO("Point One"), desc = @CAPCO("First Point"), required = true)
	public CV_Point inputPointOne;

	@Input(name = @CAPCO("Point Two"), desc = @CAPCO("Second Point"), required = true)
	public CV_Point inputPointTwo;

	@Output(name = @CAPCO("Distance"), desc = @CAPCO("The distance between the input points in Nautical Miles"))
	public CV_Decimal outputDistance;

	@Output(name = @CAPCO("Azmuth"), desc = @CAPCO("The Azmuth between the input points in Degrees"))
	public CV_Decimal outputAzmuth;
	
	@Context
	ExecutionContext ctx;

	@Override
	public void run() {
		try {
			
			if (inputPointOne == null || inputPointTwo == null) {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "One or more input points were null");
				throw new RuntimeException("One or more input points were null");
			} 
			CoordinateConversion convert = new CoordinateConversion();
			LatLon point1,point2;
			if (convert.isDDLatLonValid(inputPointOne.getY(), inputPointOne.getX())) {
				point1 = LatLon.fromDegrees(inputPointOne.getY(), inputPointOne.getX()); 
			} else {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Input Point #1 was not a valid Latitude/Longitude Coordinate");
				throw new RuntimeException("Input Point #1 was not a valid Latitude/Longitude Coordinate");
			}
			
			if (convert.isDDLatLonValid(inputPointTwo.getY(), inputPointTwo.getX())) {
				point2  = LatLon.fromDegrees(inputPointTwo.getY(), inputPointTwo.getX());
			} else {
				ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Input Point #2 was not a valid Latitude/Longitude Coordinate");
				throw new RuntimeException("Input Point #2 was not a valid Latitude/Longitude Coordinate");
			}
			
			
			double distance = LatLon.greatCircleDistanceNmi(point1, point2);
			double azmuth = (LatLon.greatCircleAzimuth(point1, point2).getDegrees() + 360) % 360;
			
			outputDistance = new CV_Decimal(distance);
			outputAzmuth = new CV_Decimal(azmuth);
			
		} catch (Exception e) {
			ctx.log(ExecutionContext.MsgLogLevel.WARNING, "Unexpected Error: "+e.getMessage());
			throw new RuntimeException("Unexpected Error: "+e.getMessage());
		}

	}
}
