/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.spatialanalysis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.KmlFactory;
import de.micromata.opengis.kml.v_2_2_0.StyleSelector;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", name = @CAPCO("Merge KMLs"), desc = @CAPCO("Merge one or more KML documents into a single KML document "), lifecycleState = LifecycleState.TESTING, displayPath = {
	"Spatial Analysis", "Merge" }, uuid = "fb69f3ca-7c2d-4f70-90b0-bd5d9cecd588", tags = {
	"merge", "KML", "kml", "spatial", "analysis" })
public class MergeKML implements Runnable {

    @Input(name = @CAPCO("KML List"), desc = @CAPCO("List containing KML Resources"), required = true)
    public List<CV_WebResource> input;

    @Output(name = @CAPCO("KML"), desc = @CAPCO("Merged KML Document"), metadata = "application/vnd.google-earth.kml+xml")
    public CV_WebResource output;

    @Context
    ExecutionContext ctx;

    @Override
    public void run() {
	try {
	    // Create KML objects from URL/CV_WebResources
	    List<Kml> theList = generateKmlObject(input);
	    ctx.log(ExecutionContext.MsgLogLevel.INFO, "MergeKML:  Merging "
		    + theList.size() + " KML Documents");
	    // Merge those KML objects into single KML object
	    Kml outputKml = mergeKml(theList);

	    // Create a KML tmp file and marshall the KML object out
	    // then create the output from the stream
	    File kmlOut = ctx.createTempFileWithExtension(".kml");

	    JAXBContext jc = null;
	    try {
		// works locally, fails on devOps
		jc = JAXBContext.newInstance(Kml.class);
	    } catch (Exception je) {
		// works on devOps, fails locally
		jc = JAXBContext.newInstance(
			"de.micromata.opengis.kml.v_2_2_0",
			Kml.class.getClassLoader());
	    }

	    Marshaller m = jc.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    OutputStream outputStream = new FileOutputStream(kmlOut);
	    m.marshal(outputKml, outputStream);
	    outputStream.flush();
	    outputStream.close();
	    output = new CV_WebResource(kmlOut.toURI().toURL());
	    
	    ctx.log(ExecutionContext.MsgLogLevel.INFO, "MergeKML:  Completed");

	} catch (JAXBException e) {
	    ctx.log(ExecutionContext.MsgLogLevel.WARNING,
		    "There was a problem processing KML Objects : "
			    + e.getMessage());
	    throw new RuntimeException(
		    "There was a problem processing KML Objects : "
			    + e.getMessage(), e);
	} catch (IOException e) {
	    ctx.log(ExecutionContext.MsgLogLevel.WARNING,
		    "Issue creating output file : " + e.getMessage());
	    throw new RuntimeException("Issue creating output file : "
		    + e.getMessage(), e);
	} catch (RuntimeException e) {
	    ctx.log(ExecutionContext.MsgLogLevel.WARNING,
		    "Unexpected Error has occured : " + e.getMessage());
	    throw new RuntimeException("Unexpected Error has occured : "
		    + e.getMessage(), e);
	}
    }

    /**
     * Take a list of CV_WebResources and unmarshall them in to Kml objects. No
     * error handling at this level it will be handled up stream.
     * 
     * @param pKmlResourceList
     * @return
     * @throws JAXBException
     * @throws IOException
     * @throws RuntimeException
     */
    private List<Kml> generateKmlObject(List<CV_WebResource> pKmlResourceList)
	    throws JAXBException, IOException, RuntimeException {

	JAXBContext jc = JAXBContext.newInstance(Kml.class);
	Unmarshaller u = jc.createUnmarshaller();

	List<Kml> kmlList = new ArrayList<Kml>();

	for (CV_WebResource item : pKmlResourceList) {
	    Kml kmlItem = (Kml) u.unmarshal(item.getURL().openStream());
	    kmlList.add(kmlItem);
	}

	return kmlList;

    }

    /**
     * Take a list of Kml objects and combine them in to a single KML object,
     * You need to make sure you maintain the styles of each seperate document
     * 
     * @param pKmlList
     * @return
     */
    private Kml mergeKml(List<Kml> pKmlList) {
	// Count
	int count = 0;
	// This will be the new MERGED KML File
	Kml theKml = KmlFactory.createKml();
	// This is root document for the MERGED KML
	Document mergedKmlDoc = theKml.createAndSetDocument();
	// Loop through all of the KML Files and combine the features and styles
	// into
	// one document
	for (Kml kmlItem : pKmlList) {
	    // Root doc
	    Document rootDoc = (Document) kmlItem.getFeature();
	    // Get all the styles for the doc
	    List<StyleSelector> styles = rootDoc.getStyleSelector();
	    // Loop through the sub features and add them to the merged doc
	    for (StyleSelector subStyle : styles) {
		mergedKmlDoc.addToStyleSelector(subStyle);
	    }

	    // Sub-features of the root doc
	    List<Feature> features = rootDoc.getFeature();
	    // Loop through the sub features and add them to the merged doc
	    for (Feature subFeature : features) {
		mergedKmlDoc.addToFeature(subFeature);
	    }
	    count += features.size();
	}
	ctx.log(ExecutionContext.MsgLogLevel.INFO, "MergeKML:  Merged " + count
		+ " KML Features");
	return theKml;
    }

}
