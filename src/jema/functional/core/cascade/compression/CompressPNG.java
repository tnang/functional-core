/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.compression;

import java.net.URL;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;


/**
 *
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Compresses a PNG file into a new or "
                + "existing zip file with an optional name."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Compression"},
        name = @CAPCO("Compress PNG File"),
        uuid = "75903fad-9515-468e-b937-e50536cd2882",
        tags = {"compression", "compress", "png",".png"})

public class CompressPNG extends CompressionBase {
    
    @Input(desc = @CAPCO("Input a PNG file (.png)."),
            name = @CAPCO("Input .png file"),
            metadata = {"image/png"})
    public CV_WebResource in;    

        @Override
    protected URL getResource() {
        return in.getURL();
    }

    @Override
    protected String getExtension() {
        return ".png";
    }

}