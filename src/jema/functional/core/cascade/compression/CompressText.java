/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.compression;

import java.net.URL;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 *
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Compresses a text file into a new or "
                + "existing zip file with an optional name."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Compression"},
        name = @CAPCO("Compress Text File"),
        uuid = "4ba77ffd-5efd-40e2-8c1a-40c97f580b2b",
        tags = {"compression", "compress", "text",".txt"})

public class CompressText extends CompressionBase {
    
    @Input(desc = @CAPCO("Input a text file (.txt)."),
            name = @CAPCO("Input .txt file"),
            metadata = {"text/plain", "text/*"})
    public CV_WebResource in;    

        @Override
    protected URL getResource() {
        return in.getURL();
    }

    @Override
    protected String getExtension() {
        return ".txt";
    }

}