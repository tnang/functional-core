/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.compression;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.attribute.FileTime;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import jema.common.types.CV_String;
import jema.common.types.CV_WebResource;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.Output;

/**
 *
 * @author Brian Kalb
 */
public abstract class CompressionBase implements Runnable {
    @Input(desc = @CAPCO("Optional input ZIP archive file to add the resource to."),
            name = @CAPCO("Input ZIP"),
            metadata = "application/zip",
            required = false)
    public CV_WebResource inputZip;
    
    @Input(
            name = @CAPCO("Name"),
            desc = @CAPCO("Optional name for the resource"),
            required = false
    )
    CV_String name;        

    @Output(desc = @CAPCO("Output ZIP archive file."),
            name = @CAPCO("Output ZIP"),
            metadata = "application/zip")
    public CV_WebResource out;   
    
    @Context
    ExecutionContext ctx;    
    
    abstract protected URL getResource();
    abstract protected String getExtension();
    
    // 4MB buffer
    private static final byte[] BUFFER = new byte[4096 * 1024];    
    
    @Override
    public void run() {
        URL in = getResource();
        String ext = getExtension();
        File zipFile = ctx.createTempFileWithExtension(".zip");
        
        try {
            ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(zipFile));
            File inputFile = new File(in.getPath());
            
            // If the optional zip file is empty just add the file
            if(inputZip == null) {
                addToZipFile(inputFile, zip, ext);
                zip.close();
            } 
            // else copy the entire archive into a new zip and add the new file
            // TODO: discover if java has a better way to do this
            else {
                ZipFile archive = new ZipFile(new File(inputZip.getURL().toURI()));
                Enumeration<? extends ZipEntry> entries = archive.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry e = entries.nextElement();
                    zip.putNextEntry(e);
                    if (!e.isDirectory()) {
                        copy(archive.getInputStream(e), zip);
                    }
                    zip.closeEntry();                    
                }
                addToZipFile(inputFile, zip, ext);
                zip.close();
            }
            
            out = new CV_WebResource(zipFile.toURI().toURL());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex); 
        }    
    }
    
    
    /**
     * Adds an extra file to the zip archive, copying in the created
     * date and a comment.
     * @param file file to be archived
     * @param zipStream archive to contain the file.
     * @param ext extension to use if no file name is specified
     */
    private void addToZipFile(File file, ZipOutputStream zipStream, String ext) {
        String inputFileName = file.getPath();
        try (FileInputStream inputStream = new FileInputStream(inputFileName)) {

            // create a new ZipEntry, which is basically another file
            // within the archive. We omit the path from the filename
            ZipEntry entry = null;
            if(name == null || name.equals("")) {
                entry = new ZipEntry("JEMA-" + String.valueOf(FileTime.fromMillis(file.lastModified())) + ext);
            } else {
                entry = new ZipEntry(name.value());
            }
            
            entry.setCreationTime(FileTime.fromMillis(file.lastModified()));
            entry.setComment("Created by JEMA");
            zipStream.putNextEntry(entry);

            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Generated new entry for: " + inputFileName);

            // Now we copy the existing file into the zip archive. To do
            // this we write into the zip stream, the call to putNextEntry
            // above prepared the stream, we now write the bytes for this
            // entry. 
            byte[] readBuffer = new byte[2048];
            int amountRead;
            int written = 0;

            while ((amountRead = inputStream.read(readBuffer)) > 0) {
                zipStream.write(readBuffer, 0, amountRead);
                written += amountRead;
            }
            zipStream.closeEntry();
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Stored " + written + " bytes to " + inputFileName);


        }
        catch(IOException e) {
            throw new RuntimeException("Unable to process " + inputFileName, e);
        }
    }

    private void copy(InputStream input, ZipOutputStream output) throws IOException {
        int bytesRead;
        while ((bytesRead = input.read(BUFFER))!= -1) {
            output.write(BUFFER, 0, bytesRead);
        }
    }
}