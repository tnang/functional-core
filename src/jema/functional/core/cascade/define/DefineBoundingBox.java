/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.define;

import java.util.logging.Logger;

import jema.common.measures.length.LengthUnit;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Length;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableUtils;
import jema.functional.core.cascade.util.geo.GeoToolsUtil;
import jema.functional.core.cascade.util.geo.GeometryUtil;

/**
 * Functional to define a bounding box based on a latitude, longitude and radius.
 * 
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Latitude Decimal Number</li>
 * <li>Longitude Decimal Number</li>
 * <li>Radius Decimal Number</li>
 * <li>Radius Measurement String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Box</li>
 * </ul>
 * <p>
 * <b>Notes</b>
 * <ul>
 * <li>If crossDateline is true, then all negative lon are shifted 360 degrees for calculations</li>
 * <li>Valid measurement units: NM,MILE,KM,YARD,FOOT,INCH,METER</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Define a bounding box"),
		desc = @CAPCO("Define a bounding box based on a longitude, latitude and radius"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Define"},
		uuid = "ef8c897e-8d0c-4c90-8377-0c6a29ca2366",
		tags = {"box", "latitude", "longitude", "radius", "units"}
		)
public class DefineBoundingBox implements Runnable 
{   
	@Input(name = @CAPCO("Longitude"),
			desc = @CAPCO("Longitude (deg)."),
			required = true
			)
	public CV_Decimal longitude;

	@Input(name = @CAPCO("Latitude"),
			desc = @CAPCO("Latitude (deg)."),
			required = true
			)
	public CV_Decimal latitude;

	@Input(name = @CAPCO("Radius"),
			desc = @CAPCO("Radius (units specified by user)."),
			required = true
			)
	public CV_Decimal radius;

	@Input(name = @CAPCO("Radius Measurement.  Default: METER"),
			desc = @CAPCO("Radius Measurement Units (NM,MILE,KM,YARD,FOOT,INCH,METER)"),
			required = false
			)
	public CV_String radiusUnits;

	@Input(name = @CAPCO("Polygon in both hemispheres. Default is false."),
			desc = @CAPCO("true=polygon points go from E Hemisphere to W Hemisphere across " +
					      "dateline, false=polygon lines do not cross dateline"),
			required = false
			)
	public CV_Boolean crossDateline;

	@Output(name = @CAPCO("Destination Box"),
			desc = @CAPCO("Destination Box."),
			required = true
			)
	public CV_Box destBox;

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(DefineBoundingBox.class.getName());

	/** Circle containing the input lon/lat and radius */
	private CV_Circle circle;

	/** Radius in meters converted from input measurement units */
	private CV_Length radiusMeters;

	/**
	 * Run functional to define a bounding box based on a latitude, longitude and radius.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Perform transformation
	 */
	protected void doCalculation()
	{
		try
		{
			CV_Polygon cvPolygon = this.circle.generatePolygon();
			Boolean crossDateline = this.crossDateline == null ? false : this.crossDateline.value();
			if(crossDateline) {this.destBox = GeometryUtil.polygonToBox(cvPolygon, crossDateline);}
			else {this.destBox = GeoToolsUtil.instance().polygonToBox(cvPolygon);}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");

		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null").append("\n");
			isValid = false;
		}

		isValid = TableUtils.validateNumericContent(
				this.longitude, 
				sb, 
				"Longitude", 
				ParameterType.decimal, 
				-180.0, 
				180.0, 
				null, 
				isValid);

		isValid = TableUtils.validateNumericContent(
				this.latitude, 
				sb, 
				"Latitude", 
				ParameterType.decimal, 
				-90.0, 
				90.0, 
				null, 
				isValid);

		isValid = TableUtils.validateNumericContent(
				this.radius, 
				sb, 
				"Radius", 
				ParameterType.decimal, 
				1.0e-1, 
				null, 
				null, 
				isValid);
		
		isValid = this.validateMeasurementUnits(
			this.radiusUnits, 
			sb, 
			"Radius Measurement Units", 
			isValid);

		if(isValid)
		{
			this.circle = new CV_Circle(
				this.longitude.value(),
				this.latitude.value(),
				this.radiusMeters.value());
		}
		else
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}

		return isValid;
	}
	/**
	 * Validate measurement units.
	 * 
	 * @param value Source parameter
	 * @param sb String builder containing list of error messages
	 * @param valName Input value name
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Parameter value is valid, false=otherwise
	 */
	@SuppressWarnings("rawtypes")
	protected Boolean validateMeasurementUnits(
			CV_Super value,
			StringBuilder sb,
			String valName,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		LengthUnit lu = null;

		if(value == null)
		{
			this.radiusMeters = !isValid ? null : new CV_Length(this.radius.value());
		}
		else
		{
			try
			{
				lu = LengthUnit.valueOf(value.toString());
				this.radiusMeters = CV_Length.fromLength(lu, this.radius.value());
			}
			catch(Exception e)
			{
				StringBuilder validList = new StringBuilder();
				int size = LengthUnit.values().length;
				for(int iUnit = 0; iUnit < size; iUnit++)
				{
					LengthUnit unit = LengthUnit.values()[iUnit];
					validList.append(",").append(unit.name());
				}
				String msg = String.format(
					"Measurement unit (%s) is invalid\nValid types are: %s", 
					this.radiusUnits.value(),
					validList.substring(1));
				sb.append(msg);
				isValid = false;
			}
		}

		return isValid;
	}
}
