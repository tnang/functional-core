/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.statistics;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import jema.functional.core.cascade.util.PivotColumns;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableUtils;

/**
 * @author CASCADE
 */
@Functional(creator = "CASCADE", name = @CAPCO("Pivot Summary"),
	desc = @CAPCO("Given an input set of data in a table, pivot the data to summarize by user defined rows "
		+ "and columns using a user defined summarizaion operation (one of count, sum, average, min" + ", or max)."), lifecycleState = LifecycleState.TESTING,
	displayPath = { "Statistics" }, uuid = "9a175e63-d113-43ee-931d-2d144dff6a37", tags = { "pivot", "summary", "count", "sum", "average", "table" })
public class PivotSummary implements Runnable {

	@Input(name = @CAPCO("Input Table"), desc = @CAPCO("Table of data to be summarized.(Required)"), required = true)
	public CV_Table input_table;

	@Input(name = @CAPCO("Row Data"), desc = @CAPCO("Columns in the Input Table that contain the data to differeniate rows by.(Required)"), required = true)
	public List<CV_String> input_rowdata;

	@Input(name = @CAPCO("Column Data"), desc = @CAPCO("Column in the Input Table that contains the data to differentiate columns by.(Required)"),
		required = true)
	public CV_String input_columnname;

	@Input(name = @CAPCO("Summarized Data"), desc = @CAPCO("If no summarized data is set, simply return the count of the records for each set of "
		+ "row discriminators, that map to each column.  Ignore anything populated in the 'Data Operation'" + "parameter. (Optional)"), required = false)
	public CV_String input_summarizedColumn = new CV_String("");

	@Input(name = @CAPCO("Data Operation"), desc = @CAPCO("How to summarized the data. COUNT, SUM, MIN, MAX, AVERAGE (Required)"), required = true,
		inputControlRef = "statistics/pivotsummarydataop.json")
	public CV_String input_dataOperation = new CV_String("COUNT");

	@Input(name = @CAPCO("Include \"Total Column\""), desc = @CAPCO("Add a column to the right hand side of the output table"
		+ ",containing the summarized value across each row. (Optional)"), required = false)
	public CV_Boolean input_totalColumn = new CV_Boolean(false);

	@Input(name = @CAPCO("Include \"Total Row\""), desc = @CAPCO("Add a row to the bottom of the output table containing the "
		+ "summarized value over each column (Optional)"), required = false)
	public CV_Boolean input_totalRow = new CV_Boolean(false);

	@Output(name = @CAPCO("Pivot Summary Table"), desc = @CAPCO("Pivot Summary Table"))
	public CV_Table result;

	@Context
	ExecutionContext myCtx;

	/**
	 * Input Column headers
	 */
	private List<ColumnHeader> inputColumns = new ArrayList<ColumnHeader>();

	/**
	 * Boolean variables for the total columns
	 */
	private boolean inputTotalColumn = false;
	private boolean inputTotalRow = false;
	
	/**
	 * enum for the data op method
	 */
	private enum DataOperation {
		COUNT, SUM, MIN, MAX, AVERAGE
	};

	/**
	 * indexes for all of the columns was can pivot on
	 */
	private int colIdx;
	private int valIdx;
	private int[] rowIdx;

	@Override
	public void run() {

		try {
			// Get totals booleans
			inputTotalColumn = input_totalColumn.value();
			inputTotalRow = input_totalRow.value();
			// get the header columns for the input table
			inputColumns = input_table.getHeader().asList();

			// get the data op
			DataOperation dataOp = DataOperation.valueOf(input_dataOperation.value());

			// get the column index
			colIdx = input_table.findIndex(input_columnname.value());
			if (colIdx == -1) {
				throw new RuntimeException("Pivot Column " + input_columnname.value() + " does not exist in input table.");
			}

			// If no value/summaried column was inputed then default to the
			// pivot column
			if (input_summarizedColumn == null || input_summarizedColumn.value().isEmpty()) {
				valIdx = colIdx;
			} else {
				// get the value index
				valIdx = input_table.findIndex(input_summarizedColumn.value());
				if (valIdx == -1) {
					throw new RuntimeException("Summarizing Column " + input_summarizedColumn.value() + " does not exist in input table.");
				}
			}

			// get the row pivot columns
			// input_rowdata
			rowIdx = new int[input_rowdata.size()];
			for (int i = 0; i < input_rowdata.size(); i++) {
				rowIdx[i] = input_table.findIndex(input_rowdata.get(i).value());
				if (rowIdx[i] == -1) {
					throw new RuntimeException("Row Pivot Column " + input_rowdata.get(i).value() + " does not exist in input table.");
				}
			}
			CV_Super valueObj = TableUtils.getCV_ColumnDataType(valIdx, input_table);
			boolean invalidDataType = false;

			switch (dataOp) {
			case COUNT:
				result = CountTable(input_table, colIdx, valIdx, rowIdx);
				break;
			case SUM:
				if (valueObj instanceof CV_Decimal || valueObj instanceof CV_Integer) {
					result = SumTable(input_table, colIdx, valIdx, rowIdx);
				} else {
					invalidDataType = true;
				}
				break;
			case MIN:
				if (valueObj instanceof CV_Decimal || valueObj instanceof CV_Integer || valueObj instanceof CV_Timestamp) {
					result = MinTable(input_table, colIdx, valIdx, rowIdx);
				} else {
					invalidDataType = true;
				}
				break;
			case MAX:
				if (valueObj instanceof CV_Decimal || valueObj instanceof CV_Integer || valueObj instanceof CV_Timestamp) {
					result = MaxTable(input_table, colIdx, valIdx, rowIdx);
				} else {
					invalidDataType = true;
				}
				break;
			case AVERAGE:
				if (valueObj instanceof CV_Decimal || valueObj instanceof CV_Integer) {
					result = AverageTable(input_table, colIdx, valIdx, rowIdx);
				} else {
					invalidDataType = true;
				}
				break;
			default:
				throw new RuntimeException("Pivot Data operation " + this.input_dataOperation.value() + " is not supported.");
			}

			if (result == null) {
				myCtx.log(ExecutionContext.MsgLogLevel.INFO, "Pivot Summary Table has return a empty table.");
				result = myCtx.createTable("Results");
			}

			if (valueObj == null || invalidDataType) {
				throw new RuntimeException("Pivot Data Data Operation: " + dataOp.toString() + " on DateType :" + valueObj.getClass() + " is not allowed.");
			}

			myCtx.log(ExecutionContext.MsgLogLevel.INFO, "Pivot Summery " + input_dataOperation.value() + " processed " + result.size() + " rows of data.");

		} catch (RuntimeException ex) {
                        myCtx.log(ExecutionContext.MsgLogLevel.WARNING, ex.getMessage());
                        throw new RuntimeException(ex);
                } catch (Exception e) {
			e.printStackTrace();
			myCtx.log(ExecutionContext.MsgLogLevel.WARNING, "Pivot Summary Error::" + e.getLocalizedMessage());
			throw new RuntimeException("Pivot Summary Error::" + e.getMessage());
		}

	}

	/**
	 * Sum for Pivot table. This will perform a sum pivot on the Column, Rows
	 * and Value. This function will determine the column type and them route to
	 * the correct sum, either integer or double
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table SumTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// Determine data type of Value Pivot, only support integer and double
		CV_Super dataObj = pData.stream().findFirst().get().get(pValueIdx);
		if (dataObj instanceof CV_Integer) {
			return SumIntTable(pData, pColIdx, pValueIdx, pRowIdx);
		} else if (dataObj instanceof CV_Decimal) {
			return SumDoubleTable(pData, pColIdx, pValueIdx, pRowIdx);
		} else {
			myCtx.log(ExecutionContext.MsgLogLevel.WARNING, "Pivot Data Sum Operation on non numeric data is not allowed.  DataType: " + dataObj.getClass());
			throw new RuntimeException("Pivot Data Sum Operation on non numeric data is not allowed.  DataType: " + dataObj.getClass());
		}
	}

	/**
	 * Sum for Pivot table. This will perform a double sum pivot on the Column,
	 * Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table SumDoubleTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {

		// Create a Pivot map using nested collectors with
		// the final step being sum
		Map<Object, Map<Object, Double>> myList =
			pData.stream().collect(
				Collectors.groupingBy(
					r -> new PivotColumns(r, pRowIdx),
					Collectors.groupingBy(r -> ((List<CV_Super>) r).get(pColIdx),
						Collectors.reducing(0.0, r -> ((CV_Decimal) ((List<CV_Super>) r).get(pValueIdx)).value(), Double::sum))));

		// Turn Grouped Map in to a flattened List
		List<List<Object>> flattenedResults = buildDoubleResults(myList, pRowIdx);
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Sum");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i)
						.add(
							flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream()
								.reduce(0.0, (a, b) -> (Double) a + (Double) b));
				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Sum");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i))
						.reduce(0.0, (a, b) -> (Double) a + (Double) b));

				});
			flattenedResults.add(totalRow);

		}

		return convertListToCvTable(flattenedResults);

	}

	/**
	 * Sum for Pivot table. This will perform a integer sum pivot on the Column,
	 * Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table SumIntTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// Perform the pivot using streams
		Map<Object, Map<Object, Integer>> myList =
			pData.stream().collect(
				Collectors.groupingBy(
					r -> new PivotColumns(r, pRowIdx),
					Collectors.groupingBy(r -> ((List<CV_Super>) r).get(pColIdx),
						Collectors.reducing(0, r -> ((CV_Integer) ((List<CV_Super>) r).get(pValueIdx)).value().intValue(), Integer::sum))));
		// Flattened the Map out to CV results
		List<List<Object>> flattenedResults = buildIntResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Sum");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i)
						.add(
							flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream()
								.reduce(0, (a, b) -> (Integer) a + (Integer) b));
				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Sum");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i))
						.reduce(0, (a, b) -> (Integer) a + (Integer) b));
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);

	}

	/**
	 * Count Pivot table. This performs a count pivot on the Column, Rows and
	 * Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table CountTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// Perform the Pivot Count
		Map<Object, Map<Object, Integer>> myList =
			pData.stream().collect(
				Collectors.groupingBy(r -> new PivotColumns((List<CV_Super>) r, pRowIdx),
					Collectors.groupingBy(r -> ((List<CV_Super>) r).get(pColIdx), Collectors.reducing(0, e -> 1, Integer::sum))));
		// Flattened the map
		List<List<Object>> flattenedResults = buildIntResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Count");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i)
						.add(
							flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream()
								.reduce(0, (a, b) -> (Integer) a + (Integer) b));
				});
		}

		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Count");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i))
						.reduce(0, (a, b) -> (Integer) a + (Integer) b));
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);

	}

	/**
	 * Max Pivot table. This performs a max pivot on the Column, Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table MaxTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// Perform the Max pivot
		Map<Object, Map<Object, Object>> myList =
			pData.stream()
				.collect(
					Collectors.groupingBy(
						r -> new PivotColumns((List<CV_Super>) r, pRowIdx),
						Collectors.groupingBy(
							r -> r.get(pColIdx),
							Collectors.collectingAndThen(
								Collectors.maxBy(Comparator.comparing(r -> (Comparable<Object>) (((List<CV_Super>) r).get(pValueIdx)))),
								r -> r.get().get(pValueIdx)))));
		// Flatten the data
		List<List<Object>> flattenedResults = buildResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Max");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i).add(
						flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream()
							.max(Comparator.comparing(r -> (Comparable<Object>) r)).get());
				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Max");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i))
						.max(Comparator.comparing(r -> (Comparable<Object>) r)).get());
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);
	}

	/**
	 * Min Pivot table. This performs a min pivot on the Column, Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table MinTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// Perform Min Pivot
		Map<Object, Map<Object, Object>> myList =
			pData.stream()
				.collect(
					Collectors.groupingBy(
						r -> new PivotColumns((List<CV_Super>) r, pRowIdx),
						Collectors.groupingBy(
							r -> r.get(pColIdx),
							Collectors.collectingAndThen(
								Collectors.minBy(Comparator.comparing(r -> (Comparable<Object>) (((List<CV_Super>) r).get(pValueIdx)))),
								r -> r.get().get(pValueIdx)))));
		// Flatten results
		List<List<Object>> flattenedResults = buildResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Min");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i).add(
						flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream()
							.min(Comparator.comparing(r -> (Comparable<Object>)r)).get());
				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Min");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i))
						.min(Comparator.comparing(r -> (Comparable<Object>) r)).get());
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);

	}

	/**
	 * Average Pivot table. This performs an average pivot on the Column, Rows
	 * and Value. it will route the incoming call to the correct method, integer
	 * or double
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table AverageTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {

		// Determine data type of Value Pivot, only suport integer and double
		CV_Super dataObj = pData.stream().findFirst().get().get(pValueIdx);
		if (dataObj instanceof CV_Integer) {
			return AverageIntTable(pData, pColIdx, pValueIdx, pRowIdx);
		} else if (dataObj instanceof CV_Decimal) {
			return AverageDoubleTable(pData, pColIdx, pValueIdx, pRowIdx);
		} else {
			myCtx.log(ExecutionContext.MsgLogLevel.WARNING, "Pivot Data Averge Operation on non numeric data is not allowed.  DataType: " + dataObj.getClass());
			throw new RuntimeException("Pivot Data Sum Operation on non numeric data is not allowed.  DataType: " + dataObj.getClass());
		}

	}

	/**
	 * Integer Average Pivot table. This performs an average pivot on the
	 * Column, Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table AverageIntTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {
		// perform the pivot
		Map<Object, Map<Object, Double>> myList =
			pData.stream().collect(
				Collectors.groupingBy(
					r -> new PivotColumns(r, pRowIdx),
					Collectors.groupingBy(r -> r.get(pColIdx),
						Collectors.averagingInt(r -> ((CV_Integer) ((List<CV_Super>) r).get(pValueIdx)).value().intValue()))));
		// flatten the table
		List<List<Object>> flattenedResults = buildDoubleResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Average");

			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i).add(
						flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream().mapToDouble(a -> (Double) a).average()
							.getAsDouble());
				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Average");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i)).mapToDouble(a -> (Double) a)
						.average().getAsDouble());
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);
	}

	/**
	 * Double Average Pivot table. This performs an average pivot on the Column,
	 * Rows and Value.
	 * 
	 * @param pData
	 *            Input CV_Table
	 * @param pColIdx
	 *            Index for the Pivot Column
	 * @param pValueIdx
	 *            Index for the Column Value of the Pivot
	 * @param pRowIdx
	 *            Indexes for the Columns to be used by Pivot
	 * @throws Exception
	 */
	private CV_Table AverageDoubleTable(CV_Table pData, int pColIdx, int pValueIdx, int... pRowIdx) throws Exception {

		Map<Object, Map<Object, Double>> myList =
			pData.stream().collect(
				Collectors.groupingBy(r -> new PivotColumns(r, pRowIdx),
					Collectors.groupingBy(r -> r.get(pColIdx), Collectors.averagingDouble(r -> ((CV_Decimal) ((List<CV_Super>) r).get(pValueIdx)).value()))));

		// flatten the table
		List<List<Object>> flattenedResults = buildDoubleResults(myList, pRowIdx);
		// add in the total column count and row count if they have been
		// provided
		// If include Total Row Has been Selected
		if (inputTotalColumn) {
			flattenedResults.get(0).add("Row Total Average");
			IntStream.range(1, flattenedResults.size()).forEach(
				i -> {
					flattenedResults.get(i).add(
						flattenedResults.get(i).subList(pRowIdx.length, flattenedResults.get(i).size()).stream().mapToDouble(a -> (Double) a).average()
							.getAsDouble());

				});
		}
		// add in the total column count and row count if they have been
		// provided
		// If include total column has been selected
		if (inputTotalRow) {
			List<Object> totalRow = new ArrayList<Object>();
			totalRow.add("Count Total Average");
			IntStream.range(1, pRowIdx.length).forEach(i -> totalRow.add(""));
			IntStream.range(pRowIdx.length, flattenedResults.get(0).size()).forEach(
				i -> {
					totalRow.add(flattenedResults.subList(1, flattenedResults.size()).stream().map(r -> ((List<Object>) r).get(i)).mapToDouble(a -> (Double) a)
						.average().getAsDouble());
				});
			flattenedResults.add(totalRow);
		}

		return convertListToCvTable(flattenedResults);
	}

	/**
	 * This function takes in the Pivot Map that was generated and flattens it
	 * in to a List List
	 * 
	 * @param pDataMap
	 * @param pRowIdx
	 * @return
	 */
	private List<List<Object>> buildResults(Map<Object, Map<Object, Object>> pDataMap, int... pRowIdx) {

		List<List<Object>> resultSet = new ArrayList<>();

		List<Object> tableHeader = new ArrayList<>();

		for (int i = 0; i < pRowIdx.length; i++) {
			tableHeader.add(inputColumns.get(pRowIdx[i]).getName());
		}

		pDataMap.values().stream().forEach(k -> {
			tableHeader.addAll(k.keySet().stream().map(c -> ((CV_Super) c).value().toString()).collect(Collectors.toList()));
		});
		
		List<Object> distinctTableHeader = tableHeader.stream().distinct().collect(Collectors.toList());
		int tableSize = distinctTableHeader.size();

		for (Map.Entry<Object, Map<Object, Object>> entry : pDataMap.entrySet()) {
			Object obj = entry.getValue().entrySet().stream().findFirst().get().getValue();
			Object[] row = new Object[tableSize];
			for (int i = 0; i < tableSize; i++) {
				row[i] = getInitValueForType(obj);
			}

			for (int i = 0; i < ((PivotColumns) entry.getKey()).columns.size(); i++) {
				row[i] = ""+((PivotColumns) entry.getKey()).columns.get(i);
			}
			entry.getValue().entrySet().stream().forEach(e -> {
				int idx = distinctTableHeader.indexOf(e.getKey().toString());
				row[idx] = e.getValue();
			});

			resultSet.add(new ArrayList<Object>(Arrays.asList(row)));
		}
		// Now prepend Column_
		List<Object> newHeaders = new ArrayList<>();
		for (Object obj : distinctTableHeader) {
			newHeaders.add("pvt_" + (obj.toString()));
		}
		resultSet.add(0, newHeaders);
		return resultSet;
	}

	/**
	 * The flattens map for Integer Results
	 * 
	 * @param pDataMap
	 * @param pRowIdx
	 * @return
	 */
	private List<List<Object>> buildIntResults(Map<Object, Map<Object, Integer>> pDataMap, int... pRowIdx) {

		List<List<Object>> resultSet;
		Map<Object, Map<Object, Object>> outerGenericMap = new HashMap<>();
		Map<Object, Object> innerGenericMap;

		for (Map.Entry<Object, Map<Object, Integer>> outEntry : pDataMap.entrySet()) {
			innerGenericMap = new HashMap<>();
			for (Map.Entry<Object, Integer> inEntry : outEntry.getValue().entrySet()) {
				innerGenericMap.put(inEntry.getKey(), (Object) inEntry.getValue());
			}
			outerGenericMap.put(outEntry.getKey(), innerGenericMap);
		}
		resultSet = buildResults(outerGenericMap, pRowIdx);

		return resultSet;
	}

	/**
	 * The flattens map for Double Results
	 * 
	 * @param pDataMap
	 * @param pRowIdx
	 * @return
	 */
	private List<List<Object>> buildDoubleResults(Map<Object, Map<Object, Double>> pDataMap, int... pRowIdx) {

		List<List<Object>> resultSet;
		Map<Object, Map<Object, Object>> outerGenericMap = new HashMap<>();
		Map<Object, Object> innerGenericMap;

		for (Map.Entry<Object, Map<Object, Double>> outEntry : pDataMap.entrySet()) {
			innerGenericMap = new HashMap<>();
			for (Map.Entry<Object, Double> inEntry : outEntry.getValue().entrySet()) {
				innerGenericMap.put(inEntry.getKey(), (Object) inEntry.getValue());
			}
			outerGenericMap.put(outEntry.getKey(), innerGenericMap);
		}
		resultSet = buildResults(outerGenericMap, pRowIdx);

		return resultSet;
	}

	private CV_Table convertListToCvTable(List<List<Object>> pInputList) throws Exception {
		CV_Table results = myCtx.createTable("PivotSummaryTable");
		// Check to see there is atleast 2 rows.. 1 hdr row and 1 row of
		// results.
		if (pInputList.size() > 1) {
			// Get the first row which are the headers
			List<Object> headerList = pInputList.get(0);
			// Get the second row, so we can get the types
			List<Object> row = pInputList.get(1);
			// Now create the CV_Table Column headers
			List<ColumnHeader> resultColumns = new ArrayList<ColumnHeader>();
			// Get the first column this should be a string because of the total
			// column
			String columnName = (String) headerList.get(0);
			resultColumns.add(new ColumnHeader(columnName, ParameterType.string));
			myCtx.log(ExecutionContext.MsgLogLevel.INFO, "Pivot Summary convertListToCvTable: Header Size " + headerList.size());
			myCtx.log(ExecutionContext.MsgLogLevel.INFO, "Pivot Summary convertListToCvTable: Adding Column " + columnName + " with Class Type: CV_String");
			for (int i = 1; i < headerList.size(); i++) {
				columnName = (String) headerList.get(i);
				//myCtx.log(ExecutionContext.MsgLogLevel.INFO, "Pivot Summary convertListToCvTable: Adding Column " + columnName + " with Class Type: " +
				//	TableUtils.getCvTypeForJavaType(row.get(i)));
				resultColumns.add(new ColumnHeader(columnName, ParameterType.valueOf(TableUtils.getCvTypeForJavaType(row.get(i)))));
			}

			TableHeader h = new TableHeader(resultColumns);
			results.setHeader(h);

			pInputList.subList(1, pInputList.size()).stream().forEach(r -> {

				List<CV_Super> cv_row = r.stream().map(cv -> {

					CV_Super retValue;

					if (cv instanceof String)
						retValue = new CV_String((String) cv);
					else if (cv instanceof Integer)
						retValue = new CV_Integer((Integer) cv);
					else if (cv instanceof Double)
						retValue = new CV_Decimal((Double) cv);
					else if (cv instanceof CV_Super) {
						retValue = (CV_Super) cv;
					} else {
						retValue = new CV_String((String) cv);
					}

					return retValue;
				}).collect(Collectors.toList());

				try {
					results.appendRow(cv_row);
				} catch (Exception e) {
					myCtx.log(ExecutionContext.MsgLogLevel.WARNING, "Pivot Summary Error: convertListToCvTable:" + e.getMessage());
					throw new RuntimeException("Pivot Summary Error: convertListToCvTable:" + e.getMessage());
				}
			});
		} else {
			// Something went wrong so return a empty table.
			return results.toReadable();
		}
		return results.toReadable();
	}
	
	private Object getInitValueForType(Object obj) {
		if (obj instanceof CV_Integer) 
			return new CV_Integer(0);
		else if (obj instanceof Integer) 
			return new Integer(0);
		else if (obj instanceof CV_Decimal) 
			return new CV_Decimal(0.0);
		else if (obj instanceof Double)
			return new Double(0.0);
		else if (obj instanceof CV_Timestamp)
			return new CV_Timestamp(Instant.EPOCH);
		else return 0;
	}
	
	private boolean equalZero(Object pObj) {
		if (pObj instanceof CV_Integer) 
			return (((CV_Integer)pObj).value() != 0);
		else if (pObj instanceof Integer) 
			return ((Integer)pObj != 0);
		else if (pObj instanceof CV_Decimal) 
			return(((CV_Decimal)pObj).value() != 0);
		else if (pObj instanceof Double)
			return ((Double)pObj != 0);
		else if (pObj instanceof CV_Timestamp)
			return  ((CV_Timestamp)pObj).value().equals(Instant.EPOCH);
		else return false;	
	}

}
