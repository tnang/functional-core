/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Input;
import jema.functional.api.Output;

/**
 * Functional to calculate summary statistics against a table based on discriminator fields.
 *
 * @author CASCADE
 */
public abstract class SummaryStatisticsBase implements Runnable
{
	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(SummaryStatisticsSQL.class.getName());

	/**
	 * Enumerated list of statistical calculation types.
	 * @author CASCADE
	 */
	protected enum StatCalcType
	{
		COUNT,
		FIRST,
		LAST,
		MAD,
		MAX,
		MEAN,
		MEDIAN,
		MIN,
		MODE,
		RANGE,
		SDEV,
		SUM,
		UNIQUE;	
	}

	/** Map of statistical calculation type against enabled calculation to indicate valid calculation */
	protected final Map<StatCalcType, Boolean> statCalcMap = new HashMap<StatCalcType, Boolean>();

	@Input(
			name = @CAPCO("Source table"),
			desc = @CAPCO("Source table to calculate summary statistics against."),
			required = true
			)
	public CV_Table srcTable;

	@Input(
			name = @CAPCO("Summary statistics columns list"),
			desc = @CAPCO("List of Columns to calculate summary statistics against. " +
			"Default: if null then calculate statistics against entire data set."),
			required = false
			)
	public List<CV_String> statisticsColumnsList;

	@Input(
			name = @CAPCO("Discriminator columns list"),
			desc = @CAPCO("List of Columns for discriminator."),
			required = false
			)
	public List<CV_String> discriminatorColumnsList;

	@Input(
			name = @CAPCO("Count number of rows in table"),
			desc = @CAPCO("Optional - count rows" + 
					" true = Count rows, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doCountCalc;

	@Input(
			name = @CAPCO("Retrieve first row"),
			desc = @CAPCO("Optional - retrieve first row" + 
					" true = Retrieve first row, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doFirstCalc;

	@Input(
			name = @CAPCO("Retrieve last row"),
			desc = @CAPCO("Optional - retrieve last row" + 
					" true = Retrieve last row, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doLastCalc;

	@Input(
			name = @CAPCO("Calculate Mean Absolute Deviation (MAD) value"),
			desc = @CAPCO("Optional - calculate MAD" + 
					" true = Calculate MAD value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doMadCalc;

	@Input(
			name = @CAPCO("Calculate maximum value"),
			desc = @CAPCO("Optional - calculate maximum value" + 
					" true = Calculate maximum value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doMaxCalc;

	@Input(
			name = @CAPCO("Calculate mean value"),
			desc = @CAPCO("Optional - calculate mean value" + 
					" true = Calculate mean value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doMeanCalc;

	@Input(
			name = @CAPCO("Calculate median value"),
			desc = @CAPCO("Optional - calculate median value" + 
					" true = Calculate median value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doMedianCalc;

	@Input(
			name = @CAPCO("Calculate minimum value"),
			desc = @CAPCO("Optional - calculate minimum value" + 
					" true = Calculate minimum value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doMinCalc;

	@Input(
			name = @CAPCO("Calculate mode value"),
			desc = @CAPCO("Optional - calculate mode value" + 
					" true = Calculate mode value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doModeCalc;

	@Input(
			name = @CAPCO("Calculate range of values"),
			desc = @CAPCO("Optional - calculate range of values" + 
					" true = Calculate value range, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doRangeCalc;

	@Input(
			name = @CAPCO("Calculate standard deviation (SDEV) value"),
			desc = @CAPCO("Optional - calculate standard deviation" + 
					" true = Calculate SDEV, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doSDevCalc;

	@Input(
			name = @CAPCO("Calculate summation value"),
			desc = @CAPCO("Optional - calculate sum value" + 
					" true = Calculate sum value, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doSumCalc;

	@Input(
			name = @CAPCO("Select unique rows"),
			desc = @CAPCO("Optional - select unique rows" + 
					" true = Select unique rows, false or null = otherwise"),
			required = false
			)
	public CV_Boolean doUniqueCalc;

	@Output(
			name = @CAPCO("Destination table"),
			desc = @CAPCO("Destination table containing columns with user specified statistics."),
			required = true
			)
	public CV_Table destTable;

	/** Job execution context used by the Functional */
	@Context
	public ExecutionContext ctx;

	/**
	 * Initialize statistical calculation map.
	 */
	protected void initMap()
	{
		this.statCalcMap.put(StatCalcType.COUNT, (this.doCountCalc == null) ? false : this.doCountCalc.value());
		this.statCalcMap.put(StatCalcType.FIRST, (this.doFirstCalc == null) ? false : this.doFirstCalc.value());
		this.statCalcMap.put(StatCalcType.LAST, (this.doLastCalc == null) ? false : this.doLastCalc.value());
		this.statCalcMap.put(StatCalcType.MAD, (this.doMadCalc == null) ? false : this.doMadCalc.value());
		this.statCalcMap.put(StatCalcType.MAX, (this.doMaxCalc == null) ? false : this.doMaxCalc.value());
		this.statCalcMap.put(StatCalcType.MEAN, (this.doMeanCalc == null) ? false : this.doMeanCalc.value());
		this.statCalcMap.put(StatCalcType.MEDIAN, (this.doMedianCalc == null) ? false : this.doMedianCalc.value());
		this.statCalcMap.put(StatCalcType.MIN, (this.doMinCalc == null) ? false : this.doMinCalc.value());
		this.statCalcMap.put(StatCalcType.MODE, (this.doModeCalc == null) ? false : this.doModeCalc.value());
		this.statCalcMap.put(StatCalcType.RANGE, (this.doRangeCalc == null) ? false : this.doRangeCalc.value());
		this.statCalcMap.put(StatCalcType.SDEV, (this.doSDevCalc == null) ? false : this.doSDevCalc.value());
		this.statCalcMap.put(StatCalcType.SUM, (this.doSumCalc == null) ? false : this.doSumCalc.value());
		this.statCalcMap.put(StatCalcType.UNIQUE, (this.doUniqueCalc == null) ? false : this.doUniqueCalc.value());
	}

	/**
	 * Return string representation of this Functional.
	 * 
	 * @return String representation of this Functional
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		try
		{
			sb.append("\n").append("srcTable = ").append(this.srcTable == null ? null : this.srcTable.getUri());
			sb.append("\n").append("statisticsColumnsList = ").append(this.statisticsColumnsList == null ? null : this.statisticsColumnsList);
			sb.append("\n").append("discriminatorColumnsList = ").append(this.discriminatorColumnsList == null ? null : this.discriminatorColumnsList);
			for(StatCalcType statCalcType : this.statCalcMap.keySet())
			{
				Boolean doCalc = this.statCalcMap.get(statCalcType);
				sb.append(String.format("\n%s = %s", statCalcType.name(), doCalc));
			}
		}
		catch(Exception e)
		{
			sb = new StringBuilder();
		}
		return sb.toString();
	}
	
	/**
	 * Validate required input parameters.
	 * <p>
	 * </b>Notes:</b>
	 * <ul>
	 * <li>GeometryCollection parameter type not supported due to comparison bug</li>
	 * </ul>
	 * @return true=valid required input parameters, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder();
		
		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null").append("\n");
			isValid = false;
		}
		
		// Source Table
		if(this.srcTable == null)
		{
			sb.append("Source table is null").append("\n");
			isValid = false;
		}

		// Statistics column list
		isValid = this.validateStatisticsColumnsList(sb, isValid);

		// Discriminator column list
		isValid = this.validateDiscriminatorColumnsList(sb, isValid);
		
		// Throw exception if invalid required parameters
		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s%s", sb.toString(), this.toString());
			SummaryStatisticsException.throwIllegalArgumentException(msg, null);
		}
		
		return isValid;
	}
	
	/**
	 * Validate the statistics column list.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>If list is null, then create list and populate with entire source table header column names</li>
	 * <li>If list is empty, throw exception</li>
	 * <li>If list contains invalid column names, throw exception</li>
	 * </ul>
	 * 
	 * @param sb List of error messages
	 * @param isValidCur Current validation state of required input parameters
	 * 
	 * @return true=statistics column list is valid, false=otherwise
	 */
	private Boolean validateStatisticsColumnsList(
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		// Create and populate statistics column list if null
		if(this.statisticsColumnsList == null)
		{
			this.statisticsColumnsList = new ArrayList<CV_String>();
			
			if(this.srcTable != null)
			{
				TableHeader tableHeader = this.srcTable.getHeader();
				for(ColumnHeader columnHeader : tableHeader.asList())
				{
					this.statisticsColumnsList.add(CV_String.valueOf(columnHeader.getName()));
				}
			}
		}

		// Validate list
		else
		{
			if(this.statisticsColumnsList.isEmpty())
			{
				sb.append("Statistics column list is invalid").append("\n");
				isValid = false;
			}
			
			// Validate Statistics column list content
			else if(this.srcTable != null)
			{
				TableHeader tableHeader = this.srcTable.getHeader();
				for(CV_String colName : this.statisticsColumnsList)
				{
					String name = colName.value();
					int iCol = tableHeader.findIndex(name);
	
					// Validate table column name
					if(iCol == -1)
					{
						sb.append(String.format("Statistics Column List: Invalid name (%s)", name)).append("\n");
						isValid = false;									
					}
				}
			}
		}

		return isValid;
	}
	
	/**
	 * Validate the Discriminator columns list.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>If list is null, then create empty list which implies process entire data set as one entity</li>
	 * <li>If list contains invalid column names, throw exception</li>
	 * </ul>
	 * 
	 * @param sb List of error messages
	 * @param isValidCur Current validation state of required input parameters
	 * 
	 * @return true=statistics column list is valid, false=otherwise
	 */
	private Boolean validateDiscriminatorColumnsList(
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		
		// Create empty list which implies process entire data set as one entity
		if(this.discriminatorColumnsList == null)
		{
			this.discriminatorColumnsList = new ArrayList<CV_String>();
		}

		// Validate discriminator column list
		else
		{
			// Validate discriminator column list content
			if(this.srcTable != null)
			{
				TableHeader tableHeader = this.srcTable.getHeader();
				for(CV_String colName : this.discriminatorColumnsList)
				{
					String name = colName.value();
					int iCol = tableHeader.findIndex(name);
	
					// Validate table column name
					if(iCol == -1)
					{
						sb.append(String.format("Discriminator Column List: Invalid name (%s)", name)).append("\n");
						isValid = false;									
					}
				}
			}
		}

		return isValid;
	}
}
// UNCLASSIFIED
