/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_String;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.build.QueryBuild;
import jema.common.types.table.query.builder.FunctionBuilder;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * Functional to calculate summary statistics against a table based on SQL queries.
 *
 * @author CASCADE
 */
@Functional(
		creator = "CASCADE",
		name = @CAPCO("SQL summary statistics calculations"),
		desc = @CAPCO("Calculate summary statistics against a table based on SQL queries."),
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Statistics"},
		uuid = "093df505-850e-481c-8f38-f0f5e832f9d1",
		tags = {
				"Count", "First", "Last", "MAD", "Max", "Mean", "Median", 
				"Min", "Mode", "Range", "SDEV", "Sum", "Unique", "SQL"})

public class SummaryStatisticsSQL extends SummaryStatisticsBase
{
	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(SummaryStatisticsSQL.class.getName());

	/** SELECT statement table alias */
	protected final static String TABLE_ALIAS1 = SelectBuilder.QB_TABLE_ALIAS+"1";

	@Input(
			name = @CAPCO("Discriminator WHERE clause"),
			desc = @CAPCO("SQL WHERE clause"),
			required = false
			)
	public CV_String discriminatorClause;

	/**
	 * Run functional to calculate summary statistics against a table based on SQL queries. 
	 * The result is destination table containing columns with user specified statistics.
	 */
	public void run() 
	{
		try 
		{
			// Validate required input parameters
			this.validate();

			// Setup map
			this.initMap();

			// List of column aliases to use in GROUP BY clause
			List<String> columnAliasList = new ArrayList<String>();

			// SELECT specification list
			StringBuilder sbSelectList = new StringBuilder();

			// WHERE Clause
			String whereClause = this.makeWhereClause();

			// Table headers
			TableHeader srcTableHeader = this.srcTable.getHeader();

			// Traverse columns and create SELECT specifications per column
			for(CV_String colName : this.statisticsColumnsList)
			{
				// Get Column
				ColumnHeader columnHeader = srcTableHeader.getHeader(colName.value());

				// Aggregate Functions

				// Count number of rows in table
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.COUNT, StatCalcType.COUNT);

				// Calculate maximum value
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.MAX, StatCalcType.MAX);

				// Calculate mean value
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.AVG, StatCalcType.MEAN);

				// Calculate minimum value
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.MIN, StatCalcType.MIN);

				// Calculate standard deviation (SDEV) value
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.STDDEV_POP, StatCalcType.SDEV);

				// Calculate sum value
				this.calcStatisticalValue(sbSelectList, whereClause, null, columnHeader, FunctionBuilder.Functions.SUM, StatCalcType.SUM);

				// Custom Functions

				// Retrieve first row
				this.calcFirstValue(sbSelectList, whereClause, columnAliasList, columnHeader);

				// Retrieve last row
				this.calcLastValue(sbSelectList, whereClause, columnAliasList, columnHeader);

				// Calculate Mean Absolute Deviation (MAD) value
				this.calcMadValue(sbSelectList, whereClause, columnAliasList, columnHeader);

				// Calculate median value
				this.calcMedianValue(sbSelectList, whereClause, columnAliasList, columnHeader);

				// Calculate mode value
				this.calcModeValue(sbSelectList, whereClause, columnAliasList, columnHeader);

				// Calculate range of values
				this.calcRangeValue(sbSelectList, whereClause, null, columnHeader);							  

				// Count unique rows	
				this.calcUniqueValue(sbSelectList, whereClause, null, columnHeader);							  
			}

			if(sbSelectList.length() > 0)
			{
				// Create select statement
				SelectBuilder selectBuilder = this.createSelectStatement(sbSelectList.substring(1), whereClause, columnAliasList);

				// Execute query
				this.destTable = TableQueryExecutor.executeQuery(selectBuilder);
				ctx.postProgress(
					-1, 
					String.format("Performed %d statistical calculations.\n", this.statisticsColumnsList.size()));
			}
			else
			{
				String msg = String.format("Resultant table is empty: %s", this.toString());
				SummaryStatisticsException.throwRuntimeException(null, null, msg, null);								
			}
		} 
		catch (IllegalArgumentException e) {throw e;}
		catch (RuntimeException e) {throw e;}
		catch (Exception e) {SummaryStatisticsException.throwRuntimeException(null, null, this.toString(), e);}
	}

	/**
	 * Calculate statistical value.
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param columnHeader Column to calculate summary statistics against
	 * @param ftn Statistical function
	 * @param statCalcType Statistical calculation type used as key to statistical calculation map
	 */
	private void calcStatisticalValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader,
			FunctionBuilder.Functions ftn,
			StatCalcType statCalcType)
	{
		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
							"%s(%s) AS %s", 
							ftn.getFunction(),
							colName,
							alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Retrieve first cell.
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcFirstValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.FIRST;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
							"(SELECT %s FROM %s %s ORDER BY %s ASC LIMIT 1) AS %s", 
							colName,
							TABLE_ALIAS1,
							whereClause,
//							this.groupByList(),
							QueryBuild.PRIMARY_KEY,
							alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Retrieve last row/cell.
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcLastValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.LAST;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
							"(SELECT %s FROM %s %s ORDER BY %s DESC LIMIT 1) AS %s", 
							colName,
							TABLE_ALIAS1,
							whereClause,
							QueryBuild.PRIMARY_KEY,
							alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Calculate Mean Absolute Deviation (MAD) value.
	 * <p>
	 * <b>Algorithm</b>
	 * <pre>
	 * BEGIN
	 * SELECT AVG(x) INTO :in_average FROM Samples;
	 * SELECT SUM(ABS(x - :in_average)) / COUNT(x) AS AverageDeviation FROM Samples;
	 * END;
	 * </pre>
	 * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li>SQL For Smarties, Joe Celko, Average Deviation</li>
	 * </ul>
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcMadValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.MAD;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
						"(SELECT " + 
						"AVG(ABS(%s - (SELECT AVG(%s) FROM %s %s))) " +
						"FROM %s %s) " +
						"AS %s",
						colName,
						colName,
						TABLE_ALIAS1,
						whereClause,
						TABLE_ALIAS1,
						whereClause,
						alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Calculate median value.
	 * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li><a href="https://www.simple-talk.com/sql/t-sql-programming/median-workbench/">
	 * Median Workbench, 05 April 2009, Joe Celko</a></li>
	 * </ul>
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcMedianValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.MEDIAN;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
						"(SELECT AVG(DISTINCT %s) " +
						"FROM (SELECT P1.%s " +
						"FROM %s AS P1, %s AS P2 %s " +
						"GROUP BY P1.%s " +
						"HAVING SUM(CASE WHEN P2.%s = P1.%s " +
						"THEN 1 ELSE 0 END) " +
						">= ABS(SUM(CASE WHEN P2.%s < P1.%s THEN 1 " +
						"WHEN P2.%s > P1.%s THEN -1 " +
						"ELSE 0 END))) " +
						") AS %s",
						colName,
						colName,
						TABLE_ALIAS1,
						TABLE_ALIAS1,
						whereClause,
						colName,
						colName,
						colName,
						colName,
						colName,
						colName,
						colName,
						alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Calculate mode value.
	 * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li>SQL For Smarties, Joe Celko, Celko's Third Median</li>
	 * <li>SQL Cookbook, Anthony Molinaro, 7.9 Calculating a Mode</li>
	 * </ul>
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcModeValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.MODE;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
						"(SELECT %s " +
						"FROM %s %s " +
						"GROUP BY %s " + 
						"HAVING COUNT(*) " +
						"> ALL(SELECT COUNT(*) " +
						"FROM %s %s " +
						"GROUP BY %s) " +
						") AS %s",
						colName,
						TABLE_ALIAS1,
						whereClause,
						colName,
						TABLE_ALIAS1,
						whereClause,
						colName,
						alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Calculate the range between the maximum and minimum values.
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcRangeValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.RANGE;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
						"MAX(%s) - MIN(%s) AS %s", 
						colName,
						colName,
						alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Calculate a count of unique rows.
	 * 
	 * @param selectList SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @param colName Column to retrieve cell against
	 */
	private void calcUniqueValue(
			StringBuilder selectList,
			String whereClause,
			List<String> columnAliasList,
			ColumnHeader columnHeader)
	{
		StatCalcType statCalcType = StatCalcType.UNIQUE;
		FunctionBuilder.Functions ftn = FunctionBuilder.Functions.COUNT;

		try
		{
			if(this.statCalcMap.get(statCalcType))
			{
				if(this.isValidParameterType(statCalcType, columnHeader.getType()))
				{
					String colName = columnHeader.getName();
					String alias = String.format("%s_%s", statCalcType.name(), colName);
					if(columnAliasList != null) {columnAliasList.add(alias);}

					String spec = String.format(
							"%s(DISTINCT %s) AS %s", 
							ftn.getFunction(),
							colName,
							alias);
					selectList.append(",").append(spec);
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), null, this.toString(), e);
		}
	}

	/**
	 * Create SELECT statement.
	 * @param selectList  SELECT list
	 * @param whereClause Descriminator WHERE clause
	 * @param columnAliasList List of column aliases to use in GROUP BY clause
	 * @return Select Builder
	 */
	private SelectBuilder createSelectStatement(
			String selectList,
			String whereClause,
			List<String> columnAliasList)
	{
		SelectBuilder selectBuilder = null;
		try
		{
			StringBuilder sb = new StringBuilder();
			for(CV_String alias : this.discriminatorColumnsList)
			{
				sb.append(alias.value()).append(",");
			}
			
			String discrimSelectList = sb.toString().isEmpty() ? "" : sb.toString();

			String clause = String.format(
					"SELECT DISTINCT %s %s FROM %s", 
					discrimSelectList,
					selectList,
					TABLE_ALIAS1);

			// WHERE clause
			if(whereClause != null && !whereClause.isEmpty())
			{
				clause += whereClause;				
			}

			// GROUP BY
			sb = new StringBuilder();
			for(CV_String alias : this.discriminatorColumnsList)
			{
				sb.append(",").append(alias.value());
			}

			for(String alias : columnAliasList)
			{
				sb.append(",").append(alias);
			}

			if(!sb.toString().isEmpty())
			{
				String groupByClause = String.format(" GROUP BY %s", sb.substring(1));
				clause += groupByClause;
			}

			// Build query 
			TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
			selectBuilder = factory.selectBuilder();
			selectBuilder.tableAs(this.srcTable, TABLE_ALIAS1).spec(CV_String.valueOf(clause));
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(null, null, this.toString(), e);			
		}

		return selectBuilder;
	}

	/**
	 * Accessor to retrieve the discriminator WHERE clause.
	 * 
	 * @return Discriminator WHERE clause
	 */
	private String makeWhereClause()
	{
		String clause = 
			((this.discriminatorClause == null) || (this.discriminatorClause.value() == null) || this.discriminatorClause.isEmpty()) ? 
			"" : 
			String.format(" WHERE %s ", this.discriminatorClause);

		return clause;
	}

	/**
	 * Validate that the StatCalcType and ParameterType are cohesive for the
	 * SQL query.
	 * @param statCalcType Statistical calculation type used as key to statistical calculation map
	 * @param parameterType Table column parameter type
	 * @return
	 */
	private Boolean isValidParameterType(
			StatCalcType statCalcType,
			ParameterType parameterType)
	{
		Boolean isValid = true;
		switch(parameterType)
		{
		case decimal:
		case integer:
		case length:
			isValid = true;
			break;
		default:
			switch(statCalcType)
			{
			case MAD: 
			case MAX: 
			case MEAN: 
			case MEDIAN: 
			case MIN: 
			case RANGE: 
			case SDEV: 
			case SUM: isValid = false; break;
			default: isValid = true; break;
			}
			break;
		}
		return isValid;
	}
}
// UNCLASSIFIED
