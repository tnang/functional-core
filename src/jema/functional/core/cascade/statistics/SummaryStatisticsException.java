package jema.functional.core.cascade.statistics;

import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.table.ColumnHeader;

/**
 * The StatisticsComponentsException is a runtime exception for invalid statistical computations.
 * 
 * @author John
 */
public class SummaryStatisticsException extends RuntimeException
{
	/** Universal version ID for a Serializable class */
	private static final long serialVersionUID = 3457379089820157250L;

	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(SummaryStatisticsException.class.getName());

	/**
	 * Constructor with error message and throwable cause.
	 * 
	 * @param msg Runtime exception message
	 */
	public SummaryStatisticsException(String msg)
	{
		super(msg);
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param msg Runtime exception message
	 * @param cause Invalid functional exception
	 */
	public SummaryStatisticsException(String msg, Exception cause)
	{
		super(msg, cause);
	}

	/**
	 * Create and throw invalid statistical calculation runtime exception.
	 * 
	 * @param type Statistical calculation type
	 * @param columnHeader Source table column
	 * @param data Class string representation
	 * @param cause Invalid functional exception
	 */
	public static void throwRuntimeException(
		String type,
		ColumnHeader columnHeader,
		String data,
		Exception cause)
	{
		StringBuilder sb = new StringBuilder("\nStatistical calculation failed: ");
		if(type != null) {sb.append(String.format("Type=%s, ", type));}
		if(columnHeader != null) {sb.append(String.format("Column=%s, ", columnHeader.getName()));}
		if(cause != null) {sb.append(String.format("Error=%s, ", cause.toString()));}
		if(data != null) {sb.append(String.format("\n%s ", data));}
		SummaryStatisticsException e = 
			(cause == null) ? 
			new SummaryStatisticsException(sb.toString()) : 
			new SummaryStatisticsException(sb.toString(), cause);
		throw e;					
	}

	/**
	 * Create and throw illegal argument exception.
	 * 
	 * @param data Class string representation
	 * @param cause Invalid functional exception
	 */
	public static void throwIllegalArgumentException(
		String data,
		Exception cause)
	{
		String msg = (cause == null) ? 
			String.format("Query failed: %s", data) : 
			String.format("Query failed: %s\n%s", cause.toString(), data);
		IllegalArgumentException e = (cause == null) ? new IllegalArgumentException(msg) : new IllegalArgumentException(msg, cause);
		throw e;
	}
}
