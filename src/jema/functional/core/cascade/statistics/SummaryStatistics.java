/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.statistics;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Duration;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.CV_TemporalRange;
import jema.common.types.CV_Timestamp;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;

/**
 * Functional to calculate summary statistics against a table based on discriminator fields.
 *
 * @author CASCADE
 */
@Functional(
		creator = "CASCADE",
		name = @CAPCO("Calculate summary statistics"),
		desc = @CAPCO("Calculate summary statistics against a table based on discriminator fields."),
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Statistics"},
		uuid = "a66ce8ab-02a5-406e-b586-29828079baae",
		tags = {
				"Count", "First", "Last", "MAD", "Max", "Mean", "Median", 
				"Min", "Mode", "Range", "SDEV", "Sum", "Unique"})

public class SummaryStatistics extends SummaryStatisticsBase
{
	/** Logger */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(SummaryStatistics.class.getName());
	
	/** Column ID index */
	protected static final int COL_ID = 0;

	/** Readable source table */
	private CV_Table srcTableReadable = null;

	/** Start index of current data subset */
	private int curStartRow;

	/** Start index of next data subset */
	private int nextStartRow;
	
	/** Discriminator column index list */
	private List<Integer> discrimColIndexList = new ArrayList<Integer>();


	/**
	 * Run functional to calculate summary statistics against a table based on discriminator fields. 
	 * The result is destination table containing columns with user specified statistics.
	 */
	@SuppressWarnings("rawtypes")
	public void run() 
	{
		try 
		{
			// Validate required inputs
			this.validate();

			// Readable table
			this.srcTableReadable = this.srcTable.toReadable();

			// Setup map
			this.initMap();

			// Table headers
			TableHeader srcTableHeader = this.srcTableReadable.getHeader();

			// Table data
			List<List<CV_Super>> srcData = this.tableToList(this.srcTableReadable);	

			// Perform statistical calculations
			this.calculateStatistics(srcData, srcTableHeader);
		} 
		catch (IllegalArgumentException e) {throw e;}
		catch (RuntimeException e) {throw e;}
		catch (Exception e) {SummaryStatisticsException.throwRuntimeException(null, null, this.toString(), e);}
	}

	/**
	 * Run functional to calculate summary statistics against a table based on discriminator fields. 
	 * The result is destination table containing columns with user specified statistics.
	 * 
	 * @param srcData Source table data
	 * @param srcTableHeader Source table header
	 */
	@SuppressWarnings("rawtypes")
	private void calculateStatistics(
		List<List<CV_Super>> srcData,
		TableHeader srcTableHeader) 
	{
		try 
		{
			// List of destination rows
			List<List<CV_Super>> resultList = new ArrayList<List<CV_Super>>();
			
			// Sort discriminated data to allow traversal by discriminated fields
			List<List<CV_Super>> discriminatedSrcData =  new ArrayList<List<CV_Super>>(srcData);
			
			// Initialize discriminator
			if(this.discriminatorColumnsList != null && !this.discriminatorColumnsList.isEmpty())
			{
				for(CV_String colName : this.discriminatorColumnsList)
				{
					String name = colName.value();
					int iCol = srcTableHeader.findIndex(name);
					this.discrimColIndexList.add(Integer.valueOf(iCol+1));  // Increment due to added row_id
				}
	
				Collections.sort(discriminatedSrcData, new Comparator<Object>() 
				{
					@SuppressWarnings("unchecked")
					@Override
					public int compare(Object o1, Object o2) 
					{
						int result = 0;
						List<CV_Super> row1 = (List<CV_Super>) o1;
						List<CV_Super> row2 = (List<CV_Super>) o2;
						for(int iDiscrim = 0; iDiscrim < discrimColIndexList.size() && (result == 0); iDiscrim++)
						{
							Integer iCell = discrimColIndexList.get(iDiscrim);
							CV_Super cell1 = (CV_Super) row1.get(iCell);
							CV_Super cell2 = (CV_Super) row2.get(iCell);
							result = cell1.compareTo(cell2);								
						}
	
						// Sort by row_id
						if(result == 0)
						{
							CV_Super cell1 = (CV_Super) row1.get(COL_ID);
							CV_Super cell2 = (CV_Super) row2.get(COL_ID);
							result = cell1.compareTo(cell2);								
						}
	
						return result;
					}
				});
			}

			// Initialize discriminators
			List<List<CV_Super>> discriminatorList = this.initDiscrimList(srcTableHeader, discriminatedSrcData);			

			this.curStartRow = 0;
			this.nextStartRow = 0;
			Iterator iter = discriminatorList.iterator();

			// Resultant data
			List<CV_Super> destRow = null;

			// Destination column header list
			List<ColumnHeader> destColumnHeaderList = this.makeHeaderList(srcTableHeader);

			// Get start and stop indices of next data subset
			while(null != (destRow = this.nextSequence(discriminatedSrcData, iter)))
			{
				// Traverse data subset and calculate statistical values
				for(CV_String colName : this.statisticsColumnsList)
				{			
					int iCol = srcTableHeader.findIndex(colName.value());

					if(iCol != -1)
					{	
						// Increment to account for inserted row_id into discriminated data
						int iDiscrimCol = iCol + 1;
						
						// Sort data for MEDIAN, MODE, and UNIQUE statistical calculations
						List<List<CV_Super>> sortedData =  new ArrayList<List<CV_Super>>(srcData);
						Collections.sort(sortedData, new Comparator<Object>() 
						{
							@SuppressWarnings("unchecked")
							@Override
							public int compare(Object o1, Object o2) 
							{
								int result = 0;
								boolean isSortedByStatCalcCol = false;
								List<CV_Super> row1 = (List<CV_Super>) o1;
								List<CV_Super> row2 = (List<CV_Super>) o2;
								for(int iDiscrim = 0; iDiscrim < discrimColIndexList.size() && (result == 0); iDiscrim++)
								{
									Integer iCell = discrimColIndexList.get(iDiscrim);
									isSortedByStatCalcCol = (iCell == iDiscrimCol);
									CV_Super cell1 = (CV_Super) row1.get(iCell);
									CV_Super cell2 = (CV_Super) row2.get(iCell);
									result = cell1.compareTo(cell2);								
								}

								// Sort by row_id
								if(result == 0 && !isSortedByStatCalcCol)
								{
									CV_Super cell1 = (CV_Super) row1.get(iDiscrimCol);
									CV_Super cell2 = (CV_Super) row2.get(iDiscrimCol);
									result = cell1.compareTo(cell2);																
								}

								return result;
							}
						});

						ColumnHeader srcColumnHeader = srcTableHeader.getHeader(iCol);

						// Count number of rows in table
						this.calcCountValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Retrieve first row
						this.calcFirstValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Retrieve last row
						this.calcLastValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate maximum value
						this.calcMaxValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate minimum value
						this.calcMinValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate range of values
						this.calcRangeValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate sum value
						this.calcSumValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate mean value for MEAN, MAD, and SDEV statistical calculations
						CV_Super meanValue = this.calcMeanValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate Mean Absolute Deviation (MAD) value
						this.calcMadValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow, meanValue);

						// Calculate standard deviation (SDEV) value
						this.calcSdevValue(discriminatedSrcData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow, meanValue);
						

						// Calculate median value
						this.calcMedianValue(sortedData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Calculate mode value
						this.calcModeValue(sortedData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);

						// Count unique rows	
						this.calcUniqueValue(sortedData, srcColumnHeader, destColumnHeaderList, iDiscrimCol, destRow);
					}
				}		  
				
				resultList.add(destRow);
			}

			if(resultList.isEmpty())
			{
				String msg = String.format("Resultant table is empty: %s", this.toString());
				SummaryStatisticsException.throwRuntimeException(null, null, msg, null);				
			}
			else
			{
				String label = this.srcTableReadable.getMetadata().getLabel().getValue() + "_SummaryStatistics";
				CV_Table writableTable = ctx.createTable(label);
				TableHeader destTableHeader = new TableHeader(destColumnHeaderList);		
				writableTable.setHeader(destTableHeader);
				for(List<CV_Super> row : resultList)
				{
					writableTable.appendRow(row);
				}
				this.destTable = writableTable.toReadable();
				ctx.postProgress(-1, String.format("Performed %d statistical calculations on %d rows", statisticsColumnsList.size(), srcData.size()));
			}
		} 
		catch (IllegalArgumentException e) {throw e;}
		catch (RuntimeException e) {throw e;}
		catch (Exception e) {SummaryStatisticsException.throwRuntimeException(null, null, this.toString(), e);}
	}	

	/**
	 * Calculate the row count statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Row count statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Integer calcCountValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Integer destValue =  null;
		StatCalcType statCalcType = StatCalcType.COUNT;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);
			if((srcData != null) && !srcData.isEmpty() && doCalc)
			{
				long countValue = this.nextStartRow - this.curStartRow;
				destValue =  CV_Integer.valueOf(countValue);	
				this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);
			}	
		}
		catch(Exception e){SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);}

		return destValue;
	}

	/**
	 * Select value from the first row.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Specified value from first row
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcFirstValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.FIRST;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);
			if((srcData != null) && (doCalc != null) && doCalc)
			{
				int iRow = this.curStartRow;
				List<CV_Super> row = srcData.get(iRow);
				destValue =  row.get(iCol);
				this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
			}
		}
		catch(Exception e){SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);}

		return destValue;
	}

	/**
	 * Select value from the last row.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Specified value from last row
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcLastValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.LAST;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);
			if((srcData != null) && (doCalc != null) && doCalc)
			{
				int iRow = this.nextStartRow - 1;
				List<CV_Super> row = srcData.get(iRow);
				destValue =  row.get(iCol);
				this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
			}
		}
		catch(Exception e){SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);}

		return destValue;
	}

	/**
	 * Calculate the Mean Absolute Deviation (MAD) value.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>find the mean (average)</li>
	 * <li>find the difference between each data value and the mean</li>
	 * <li>take the absolute value of each difference</li>
	 * <li>find the mean (average) of these differences</li>
	 * </ul>
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @param meanValue Mean statistical value
	 * @return Mean Absolute Deviation (MAD) value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super calcMadValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow,
			CV_Super meanValue)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MAD;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				long size = (this.nextStartRow - this.curStartRow);
				if(size > 0)
				{
					switch(srcColumnHeader.getType())
					{
					case decimal:
					{
						double mean = ((CV_Decimal)meanValue).value();
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Decimal newValue = (CV_Decimal) row.get(iCol);
							double datum = newValue.value();
							double delta = (datum - mean);
							double absDelta = Math.abs(delta);
							sumValue += absDelta;
						}
						double mad = sumValue / size;
						destValue =  CV_Decimal.valueOf(mad);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}
					case duration:
					{
						Duration mean = ((CV_Duration)meanValue).value();
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Duration newValue = (CV_Duration) row.get(iCol);
							double datum = newValue.value().getSeconds();
							double delta = (datum - mean.getSeconds());
							double absDelta = Math.abs(delta);
							sumValue += absDelta;
						}
						double mad = sumValue / size;
						Duration duration = Duration.ofSeconds(Math.round(mad));
						destValue =  new CV_Duration(duration);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}
					case length:
					{
						double mean = ((CV_Length)meanValue).value();
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Length newValue = (CV_Length) row.get(iCol);
							double datum = newValue.value();
							double delta = (datum - mean);
							double absDelta = Math.abs(delta);
							sumValue += absDelta;
						}
						double mad = sumValue / size;
						destValue = new CV_Length(mad);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}
					case integer:
					{
						long mean = ((CV_Integer)meanValue).value();
						long sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Integer newValue = (CV_Integer) row.get(iCol);
							long datum = newValue.value();
							long delta = (datum - mean);
							long absDelta = Math.abs(delta);
							sumValue += absDelta;
						}
						long mad = sumValue / size;
						destValue = CV_Integer.valueOf(mad);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}
					case timestamp:
					{
						long mean = ((CV_Timestamp)meanValue).value().toEpochMilli();
						long sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Timestamp newValue = (CV_Timestamp) row.get(iCol);
							long datum = newValue.value().getEpochSecond();
							long delta = (datum - mean);
							long absDelta = Math.abs(delta);
							sumValue += absDelta;
						}
						double mad = sumValue / size;
						Instant tsMad = Instant.ofEpochMilli(Math.round(mad));
						destValue = new CV_Timestamp(tsMad);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}

					default:
						break;
					}					
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the maximum statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Maximum statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcMaxValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MAX;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				destValue = this.calcMinMaxValue(statCalcType, srcData, srcColumnHeader, destColumnHeaderList, iCol, destRow);
			}
		}
		catch(RuntimeException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the mean statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Mean statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcMeanValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MEAN;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc || this.statCalcMap.get(StatCalcType.MAD) || this.statCalcMap.get(StatCalcType.SDEV)))
			{
				long size = (this.nextStartRow - this.curStartRow);
				if(size > 0)
				{
					switch(srcColumnHeader.getType())
					{
					case decimal:
					{
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Decimal newValue = (CV_Decimal) row.get(iCol);
							sumValue += newValue.value();
						}

						destValue = CV_Decimal.valueOf(sumValue/size);
						if(doCalc) {this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);}			
						break;
					}
					case duration:
					{
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Duration newValue = (CV_Duration) row.get(iCol);
							sumValue += newValue.value().getSeconds();
						}
						double mean = sumValue / size;
						Duration duration = Duration.ofSeconds(Math.round(mean));
						destValue =  new CV_Duration(duration);
						if(doCalc) {this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);}			
						break;
					}
					case length:
					{
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Length newValue = (CV_Length) row.get(iCol);
							sumValue += newValue.value();
						}

						destValue = new CV_Length(sumValue/size);
						if(doCalc) {this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);}			
						break;
					}
					case integer:
					{
						Long sumValue = 0l;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							CV_Integer newValue = (CV_Integer) row.get(iCol);
							sumValue += newValue.value();
						}

						destValue = CV_Integer.valueOf(sumValue/size);
						if(doCalc) {this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);}			
						break;
					}
					case timestamp:
					{
						double sumValue = 0;
						for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
						{
							List<CV_Super> row = srcData.get(iRow);
							long ts = this.toEpochMilli(row, iCol);
							sumValue += ts;
						}

						double mean = sumValue / size;
						Instant tsMean = Instant.ofEpochMilli(Math.round(mean));
						destValue = new CV_Timestamp(tsMean);
						if(doCalc) {this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);}			
						break;
					}

					default:
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the median statistical value.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>Median is the number separating the higher half of a data sample, a population, or a probability distribution, from the lower half</li>
	 * </ul>
	 * 
	 * @param srcData Sorted source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Median statistical value
	 */
	@SuppressWarnings({ "rawtypes" })
	private CV_Super<?> calcMedianValue(
			List<List<CV_Super>> sortedData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MEDIAN;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((sortedData != null) && (doCalc != null) && doCalc)
			{
				int size = (this.nextStartRow - this.curStartRow);

				if (size % 2 != 0) 
				{
					int iRow = this.curStartRow = (size / 2);
					List<CV_Super> row = sortedData.get(iRow);
					destValue = (CV_Super) row.get(iCol);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
				}				
				else
				{
					switch(srcColumnHeader.getType())
					{
					case decimal:
					{
						int iRow1 = this.curStartRow + (size / 2 - 1);
						int iRow2 = this.curStartRow + (size / 2);
						List<CV_Super> row1 = sortedData.get(iRow1);
						List<CV_Super> row2 = sortedData.get(iRow2);
						CV_Decimal cell1 = (CV_Decimal) row1.get(iCol);
						CV_Decimal cell2 = (CV_Decimal) row2.get(iCol);
						double result = (cell1.value() + cell2.value()) / 2.0;
						destValue = CV_Decimal.valueOf(result);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
						break;
					}
					case duration:
					{
						int iRow1 = this.curStartRow + (size / 2 - 1);
						int iRow2 = this.curStartRow + (size / 2);
						List<CV_Super> row1 = sortedData.get(iRow1);
						List<CV_Super> row2 = sortedData.get(iRow2);
						CV_Duration cell1 = (CV_Duration) row1.get(iCol);
						CV_Duration cell2 = (CV_Duration) row2.get(iCol);
						Long result = (cell1.value().getSeconds() + cell2.value().getSeconds()) / 2;
						Duration duration = Duration.ofSeconds(result);
						destValue =  new CV_Duration(duration);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
						break;
					}
					case length:
					{
						int iRow1 = this.curStartRow + (size / 2 - 1);
						int iRow2 = this.curStartRow + (size / 2);
						List<CV_Super> row1 = sortedData.get(iRow1);
						List<CV_Super> row2 = sortedData.get(iRow2);
						CV_Length cell1 = (CV_Length) row1.get(iCol);
						CV_Length cell2 = (CV_Length) row2.get(iCol);
						double result = (cell1.value() + cell2.value()) / 2.0;
						destValue = new CV_Length(result);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
						break;
					}
					case integer:
					{
						int iRow1 = this.curStartRow + (size / 2 - 1);
						int iRow2 = this.curStartRow + (size / 2);
						List<CV_Super> row1 = sortedData.get(iRow1);
						List<CV_Super> row2 = sortedData.get(iRow2);
						CV_Integer cell1 = (CV_Integer) row1.get(iCol);
						CV_Integer cell2 = (CV_Integer) row2.get(iCol);
						Long result = (cell1.value() + cell2.value()) / 2;
						destValue = CV_Integer.valueOf(result);
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
						break;
					}
					case timestamp:
					{
						int iRow1 = this.curStartRow + (size / 2 - 1);
						int iRow2 = this.curStartRow + (size / 2);
						List<CV_Super> row1 = sortedData.get(iRow1);
						List<CV_Super> row2 = sortedData.get(iRow2);
						long cell1 = this.toEpochMilli(row1, iCol);
						long cell2 = this.toEpochMilli(row2, iCol);
						Long result = (cell1 + cell2) / 2;
						Instant tsMedian = Instant.ofEpochMilli(result);
						destValue = new CV_Timestamp(tsMedian); 
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
						break;
					}

					default:
						destValue = (CV_Super) null;
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);			
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the minimum statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Minimum statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcMinValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MIN;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				destValue = this.calcMinMaxValue(statCalcType, srcData, srcColumnHeader, destColumnHeaderList, iCol, destRow);
			}
		}
		catch(RuntimeException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the minimum/maximum statistical value.
	 * 
	 * @param statCalcType Statistical calculation type
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column for calculated value
	 * @param destColumnHeaderList Destination header column list to add new destination column
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Minimum statistical value
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CV_Super<?> calcMinMaxValue(
			StatCalcType statCalcType,
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		try
		{
			if(srcData != null)
			{
				int iRow = this.curStartRow;
				CV_Super curValue = srcData.get(iRow).get(iCol);
				for(;iRow < this.nextStartRow; iRow++)
				{
					CV_Super newValue = (CV_Super) srcData.get(iRow).get(iCol);
					int result = newValue.compareTo(curValue);
					if(statCalcType.equals(StatCalcType.MIN)) {curValue = (result < 0) ? newValue : curValue;}
					else {curValue = (result > 0) ? newValue : curValue;}
				}
				destValue = curValue;
				this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the mode statistical value.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>Mode is the value that appears most often in a set of data</li>
	 * <li>Only a unique mode value is calculated, no multi-mode values</li>
	 * </ul>
	 * 
	 * @param sortedData Sorted source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Mode statistical value
	 */
	@SuppressWarnings({ "rawtypes" })
	private CV_Super<?> calcModeValue(
			List<List<CV_Super>> sortedData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.MODE;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			// Traverse sorted data searching for most occurrences of a record/cell
			if((sortedData != null) && (doCalc != null) && doCalc)
			{
				long size = (this.nextStartRow - this.curStartRow);
				if(size > 0)
				{
					CV_Super<?> curValue = null;
					CV_Super<?> modeValue = null;
					CV_Super<?> prevValue = null;
					long curValueCount = 1;
					long modeValueCount = 0;
					int numMaxCounts = 0;

					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						prevValue = curValue;
						curValue = sortedData.get(iRow).get(iCol);

						// Bug in GeometryCollection comparison
						boolean isEqualValues = false;
						try {isEqualValues=curValue.equals(prevValue);}
						catch(Exception e){isEqualValues = false;}

						// Consecutive record/cell do not match
						if((prevValue == null) || !isEqualValues)
						{
							if(curValueCount > modeValueCount)
							{
								modeValue = prevValue;
								modeValueCount = curValueCount;
								numMaxCounts = 1;
							}

							// Duplicate mode values
							else if(curValueCount == modeValueCount)
							{
								numMaxCounts++;
							}

							curValueCount = 0;
						}

						curValueCount++;
					}

					// Process final value
					if(curValueCount > modeValueCount)
					{
						modeValue = curValue;
						modeValueCount = curValueCount;
						numMaxCounts = 1;
					}
					else if(curValueCount == modeValueCount)
					{
						numMaxCounts++;
					}

					// Only calculate mode for a single value (no multi-modes)
					if(numMaxCounts == 1)
					{
						destValue = modeValue;
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);	
					}
					else
					{
						destValue = null;
						this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);							
					}
				}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the range statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Range statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcRangeValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.RANGE;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				CV_Super<?>[] values = this.calcRangeValue(statCalcType, srcData, srcColumnHeader, iCol);

				switch(srcColumnHeader.getType())
				{
				case decimal:
				{
					double range = ((CV_Decimal) values[1]).value() - ((CV_Decimal) values[0]).value();
					destValue = CV_Decimal.valueOf(range);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case duration:
				{
					long range = ((CV_Duration) values[1]).value().getSeconds() - ((CV_Duration) values[0]).value().getSeconds();
					Duration duration = Duration.ofSeconds(range);
					destValue =  new CV_Duration(duration);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case length:
				{
					double range = ((CV_Length) values[1]).value() - ((CV_Length) values[0]).value();
					destValue = new CV_Length(range);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case integer:
				{
					long range = ((CV_Integer) values[1]).value() - ((CV_Integer) values[0]).value();
					destValue = CV_Integer.valueOf(range);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case timestamp:
				{
					CV_Timestamp tsMin = (CV_Timestamp) values[0];
					CV_Timestamp tsMax = (CV_Timestamp) values[1];					
					long epochMin = tsMin.value().getEpochSecond();
					long epochMax = tsMax.value().getEpochSecond();
					Instant instantMin = Instant.ofEpochSecond(epochMin);
					Instant instantMax = Instant.ofEpochSecond(epochMax);
					destValue = new CV_TemporalRange(instantMin, instantMax);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}

				default:
					break;
				}																
			}
		}
		catch(RuntimeException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the range statistical value.
	 * 
	 * @param statCalcType Statistical calculation type
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param iCol Source table column header index
	 * @return Range statistical value
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CV_Super<?>[] calcRangeValue(
			StatCalcType statCalcType,
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			int iCol)
	{
		CV_Super<?>[] values = new CV_Super<?>[2];

		try
		{
			if(srcData != null && !srcData.isEmpty())
			{
				int iRow = this.curStartRow;
				CV_Super newValue = (CV_Super) srcData.get(iRow++).get(iCol);
				CV_Super curMinValue = newValue;
				CV_Super curMaxValue = newValue;

				for(; iRow < this.nextStartRow; iRow++)
				{
					newValue = (CV_Super) srcData.get(iRow).get(iCol);
					if(newValue != null)
					{
						int result = newValue.compareTo(curMinValue);
						curMinValue = (result < 0) ? newValue : curMinValue;
						result = newValue.compareTo(curMaxValue);
						curMaxValue = (result > 0) ? newValue : curMaxValue;
					}
				}

				values[0] = curMinValue;
				values[1] = curMaxValue;
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return values;
	}

	/**
	 * Calculate the sum statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Sum statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super<?> calcSumValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Super<?> destValue =  null;
		StatCalcType statCalcType = StatCalcType.SUM;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				switch(srcColumnHeader.getType())
				{
				case decimal:
				{
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Decimal newValue = (CV_Decimal) row.get(iCol);
						sumValue += newValue.value();
					}

					destValue = CV_Decimal.valueOf(sumValue);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case duration:
				{
					long sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Duration newValue = (CV_Duration) row.get(iCol);
						sumValue += newValue.value().getSeconds();
					}
					Duration duration = Duration.ofSeconds(sumValue);
					destValue =  new CV_Duration(duration);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case length:
				{
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Length newValue = (CV_Length) row.get(iCol);
						sumValue += newValue.value();
					}

					destValue = new CV_Length(sumValue);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case integer:
				{
					long sumValue = 0l;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Integer newValue = (CV_Integer) row.get(iCol);
						sumValue += newValue.value();
					}
					destValue = CV_Integer.valueOf(sumValue);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}

				default:
					break;
				}																
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the standard deviation (SDEV) statistical value.
	 * 
	 * @param srcData Source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Standard deviation statistical value
	 */
	@SuppressWarnings("rawtypes")
	private CV_Super calcSdevValue(
			List<List<CV_Super>> srcData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow,
			CV_Super meanValue)
	{
		CV_Super<?> destValue = null;
		StatCalcType statCalcType = StatCalcType.SDEV;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((srcData != null) && (doCalc != null) && doCalc)
			{
				long size = (this.nextStartRow - this.curStartRow);
				if(size > 0)
				{
				switch(srcColumnHeader.getType())
				{
				case decimal:
				{
					// Variance
					double mean = ((CV_Decimal) meanValue).value();
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Decimal newValue = (CV_Decimal) row.get(iCol);
						double datum = newValue.value();
						double delta = (mean - datum);
						sumValue += delta * delta;
					}
					double variance = sumValue / size;

					// Standard Deviation
					double sdev = Math.sqrt(variance);
					destValue = CV_Decimal.valueOf(sdev);

					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case duration:
				{
					// Variance
					double mean = ((CV_Duration) meanValue).value().getSeconds();
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Duration newValue = (CV_Duration) row.get(iCol);
						double datum = newValue.value().getSeconds();
						double delta = (mean - datum);
						sumValue += delta * delta;
					}
					double variance = sumValue / size;

					// Standard Deviation
					double sdev = Math.sqrt(variance);
					Duration duration = Duration.ofSeconds(Math.round(sdev));
					destValue =  new CV_Duration(duration);
					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case length:
				{
					// Variance
					double mean = ((CV_Length) meanValue).value();
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Length newValue = (CV_Length) row.get(iCol);
						double datum = newValue.value();
						double delta = (mean - datum);
						sumValue += delta * delta;
					}
					double variance = sumValue / size;

					// Standard Deviation
					double sdev = Math.sqrt(variance);
					destValue = CV_Decimal.valueOf(sdev);

					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case integer:
				{
					// Variance
					double mean = ((CV_Integer) meanValue).value();
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						CV_Integer newValue = (CV_Integer) row.get(iCol);
						double datum = newValue.value();
						double delta = (mean - datum);
						sumValue += delta * delta;
					}
					double variance = sumValue / size;

					// Standard Deviation
					double sdev = Math.sqrt(variance);
					destValue = CV_Integer.valueOf(sdev);

					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}
				case timestamp:
				{
					// Variance
					double mean = ((CV_Timestamp) meanValue).value().toEpochMilli();
					double sumValue = 0;
					for(int iRow = this.curStartRow; iRow < this.nextStartRow; iRow++)
					{
						List<CV_Super> row = srcData.get(iRow);
						double datum = this.toEpochMilli(row, iCol);
						double delta = (mean - datum);
						sumValue += delta * delta;
					}
					double variance = sumValue / size;

					// Standard Deviation
					double sdev = Math.sqrt(variance);
					Instant tsSdev = Instant.ofEpochMilli(Math.round(sdev));
					destValue = (size == 0) ? null : new CV_Timestamp(tsSdev);

					this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
					break;
				}

				default:
					break;
				}																
			}
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}

		return destValue;
	}

	/**
	 * Calculate the count of unique rows.
	 * 
	 * @param sortedData Sorted source table data
	 * @param srcColumnHeader Source table column header
	 * @param destColumnHeaderList Destination table column header list
	 * @param iCol Source table column header index
	 * @param destRow Destination table row
	 * @return Count of unique rows
	 */
	@SuppressWarnings({ "rawtypes" })
	private CV_Super calcUniqueValue(
			List<List<CV_Super>> sortedData,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			int iCol,
			List<CV_Super> destRow)
	{
		CV_Integer destValue = null;
		StatCalcType statCalcType = StatCalcType.UNIQUE;

		try
		{
			Boolean doCalc = this.statCalcMap.get(statCalcType);

			if((sortedData != null) && (doCalc != null) && doCalc)
			{
				CV_Super<?> curValue = null;
				CV_Super<?> prevValue = null;
				int iRow = this.curStartRow;
				long count = 0;
				long size = (this.nextStartRow - this.curStartRow);

				if(size > 0)
				{
					List<CV_Super> row = sortedData.get(iRow++);
					curValue = row.get(iCol);
					count++; // for last value (and therefore for a single value)
					while(iRow < this.nextStartRow)
					{
						prevValue = curValue;
						row = sortedData.get(iRow++);
						curValue = row.get(iCol);

						// Bug in GeometryCollection comparison - tries to cast Point to Collection
						boolean isEqualValues = false;
						try {isEqualValues=curValue.equals(prevValue);}
						catch(Exception e){isEqualValues = false;}

						if(!isEqualValues) {count++;}
					}
				}
				destValue = CV_Integer.valueOf(count);					
				this.updateDestTable(statCalcType, srcColumnHeader, destColumnHeaderList, destRow, destValue);				
			}
		}
		catch(Exception e)
		{
			SummaryStatisticsException.throwRuntimeException(statCalcType.name(), srcColumnHeader, this.toString(), e);
		}		

		return destValue;
	}

	/**
	 * Translate a CV Table to a list of table rows.
	 * <p>
	 * <b>Notes:</b>
	 * <ul>
	 * <li>Insert row ID to allow sorting for various discriminators</li>
	 * </ul>
	 * @param table Source table
	 * @return List of table rows
	 */
	@SuppressWarnings("rawtypes")
	private List<List<CV_Super>> tableToList(CV_Table table)
	{
		List<List<CV_Super>> list = null;
		try
		{
			if(table != null)
			{
				list = new ArrayList<List<CV_Super>>();
				List<CV_Super> row = null;
				Long iRow = 0L;
				while((row = table.readRow()) != null)
				{
					row.add(COL_ID, CV_Integer.valueOf(iRow++));
					list.add(row);
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("List creation failed: \n%s\n%s", this.toString(), e.getMessage());
			throw new RuntimeException(msg);						
		}
		return list;
	}
	
	/**
	 * Initialize the destination table header list.  Add discriminator columns
	 * if they exist.
	 * @param srcTableHeader Source table header to obtain discriminator columns parameter type
	 * @return destination table header list
	 */
	private List<ColumnHeader> makeHeaderList(TableHeader srcTableHeader)
	{
		List<ColumnHeader> destColumnHeaderList = new ArrayList<ColumnHeader>();
		for(CV_String colName : this.discriminatorColumnsList)
		{
			String name = colName.value();
			ColumnHeader columnHeader = srcTableHeader.getHeader(name);
			destColumnHeaderList.add(columnHeader);
		}

        return destColumnHeaderList;
	}

	/**
	 * Make table header column containing applicable parameter type 
	 * and specified column name.
	 * 
	 * @param statCalcType Statistical calculation type
	 * @param srcColumnHeader Source table column for calculated value
	 * @param destColumnHeaderList Destination header column list to add new destination column
	 * @param destValue Calculated value
	 */
	private void makeColumnHeader(
			StatCalcType statCalcType,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			CV_Super<?> destValue)
	{
		if(srcColumnHeader != null && destColumnHeaderList != null)
		{
			ParameterType parameterType = null;
			if(destValue instanceof CV_Decimal) {parameterType = ParameterType.decimal;}
			else if(destValue instanceof CV_Duration) {parameterType = ParameterType.duration;}
			else if(destValue instanceof CV_Integer) {parameterType = ParameterType.integer;}
			else if(destValue instanceof CV_Length) {parameterType = ParameterType.length;}
			else if(destValue instanceof CV_TemporalRange) {parameterType = ParameterType.temporalRange;}
			else if(destValue instanceof CV_Timestamp) {parameterType = ParameterType.timestamp;}
			else {parameterType = srcColumnHeader.getType();}

			String colName = String.format("%s_%s", statCalcType.name(), srcColumnHeader.getName());
			ColumnHeader destColumnHeader = new ColumnHeader(colName, parameterType);
			if(!destColumnHeaderList.contains(destColumnHeader))
			{
				destColumnHeaderList.add(destColumnHeader);
			}
		}
	}

	/**
	 * Update the destination table header list and the destination row.
	 * 
	 * @param statCalcType Statistical calculation type
	 * @param srcColumnHeader Source table column for calculated value
	 * @param destColumnHeaderList Destination header column list to add new destination column
	 * @param destRow Destination table row
	 * @param destValue Calculated value
	 */
	@SuppressWarnings("rawtypes")
	private void updateDestTable(
			StatCalcType statCalcType,
			ColumnHeader srcColumnHeader,
			List<ColumnHeader> destColumnHeaderList,
			List<CV_Super> destRow,
			CV_Super<?> destValue)
	{	
		if(destRow != null) {destRow.add(destValue);}
		this.makeColumnHeader(statCalcType, srcColumnHeader, destColumnHeaderList, destValue);
	}

	/**
	 * Accessor to retrieve timestamp from row and convert to milliseconds.
	 * @param row Source table row
	 * @param iCol Column index
	 * @return Timestamp converted to milliseconds since epoch
	 */
	@SuppressWarnings("rawtypes")
	private long toEpochMilli(List<CV_Super> row, int iCol)
	{
		CV_Timestamp newValue = (CV_Timestamp) row.get(iCol);
		Instant instant = newValue.value();
		long ts = instant.toEpochMilli();
		return ts;
	}	

	/**
	 * Initialize discriminator list for traversal.
	 * @param row Current row
	 * @param iCol Column ID
	 * @return Discriminator list 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<List<CV_Super>> initDiscrimList(
			TableHeader srcTableHeader,
			List<List<CV_Super>> srcData)
	{
		List<List<CV_Super>> discriminatorList = new ArrayList<List<CV_Super>>();

		if(this.discrimColIndexList != null && !this.discrimColIndexList.isEmpty())
		{
			// Get first discrimator record
			Iterator iter = srcData.iterator();		
			if(iter.hasNext())
			{
				List<CV_Super> srcRow = (List<CV_Super>) iter.next();

				// Append all content into a row or string, then compare with next row of all content,
				// if not match, then keep as discriminator row
				List<CV_Super> discrimRow = this.makeDiscrimRow(srcRow, this.discrimColIndexList);

				if(!discrimRow.isEmpty())
				{
					// Traverse list of sorted source data and save distinct data
					while(iter.hasNext())
					{
						srcRow = (List<CV_Super>) iter.next();			

						// Append all content into a row or string, then compare with next row of all content,
						// if not match, then keep as discriminator row
						List<CV_Super> curDiscrimRow = this.makeDiscrimRow(srcRow, this.discrimColIndexList);

						if(!discrimRow.equals(curDiscrimRow))
						{
							discriminatorList.add(discrimRow);
							discrimRow = curDiscrimRow;
						}
					}

					discriminatorList.add(discrimRow);
				}
			}
		}

		return discriminatorList;
	}

	/**
	 * Make a descriminator row.
	 * @param srcRow Source data row
	 * @param colList List of discrimator column idices.
	 * @return Candidate discriminator row
	 */
	@SuppressWarnings("rawtypes")
	private List<CV_Super> makeDiscrimRow(
			List<CV_Super> srcRow,
			List<Integer> colList)
	{
		List<CV_Super> discrimRow = new ArrayList<CV_Super>();
		for(Integer key : colList)
		{
			CV_Super curCell = srcRow.get(key);
			discrimRow.add(curCell);
		}			
		return discrimRow;
	}

	/**
	 * Determine next dataset sequence.
	 * @param srcData Source data
	 * @param iter Disciminator iterator.
	 * @param destRow Destination row, put discriminators into row
	 * @return true=next sequence available, false=otherwise
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<CV_Super> nextSequence(
		List<List<CV_Super>> srcData,
		Iterator iter)
	{
		List<CV_Super> destRow = null;
		Boolean result = (iter == null) ? false : iter.hasNext();
		if(result)
		{
			List<CV_Super> curDiscrimRow = (List<CV_Super>) iter.next();
			int iRow = 0;
			for(iRow = this.nextStartRow; iRow < srcData.size(); iRow++)
			{
				List<CV_Super> srcRow = srcData.get(iRow);
				List<CV_Super> discrimRow = this.makeDiscrimRow(srcRow, this.discrimColIndexList);
				if(!curDiscrimRow.equals(discrimRow))
				{
					break;
				}
			}
			
			this.curStartRow = this.nextStartRow;
			this.nextStartRow = iRow;
			
			destRow = new ArrayList<CV_Super>();
			for(CV_Super cell : curDiscrimRow) {destRow.add(cell);}
		}
		else
		{
			result = (this.nextStartRow != srcData.size());
			if(result)
			{
				destRow = new ArrayList<CV_Super>();
				this.curStartRow = 0;
				this.nextStartRow = srcData.size();				
			}
		}

		return destRow;
	}
}
// UNCLASSIFIED
