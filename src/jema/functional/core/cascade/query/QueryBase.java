/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.query;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;

import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

/**
 * Base class to for query functionals.
 * 
 * @author CASCADE
 */
public abstract class QueryBase implements Runnable {

	/**
	 * BaseURL Property
	 */
	protected String baseURL = "";

	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static Logger LOGGER;
	
	/**
	 * Other Properties Stored in a Map
	 */
	protected Map<String,String> queryProperties;

	protected QueryBase() {
		try {
			queryProperties = new HashMap<String,String>();
			String className = this.getClass().getSimpleName() + ".json";
			if (className != null || !className.isEmpty()) {
				InputStream ios = this.getClass().getResourceAsStream(className);
				if (ios != null) {
					Map<String, String> queryProperties = new Gson().fromJson(new InputStreamReader(ios), Map.class);
					baseURL = queryProperties.get("BaseURL");
				}
			}
		} catch (Exception e) {
			baseURL = "";
		}
	}

}
