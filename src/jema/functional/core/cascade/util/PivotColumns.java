/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util;

import java.util.ArrayList;
import java.util.List;

import jema.common.types.CV_Super;
/**
 * PivotColumns:  This class is a helper class used in creating a pivot summary table for
 * the Pivot Summary Functional.   It is basically a ArrayList that is used as a grouping 
 * element for Collector.groupingBy.  Because groupingBy only takes single arguments 
 * and in a Pivot table you could pivot on multiple columns (represented by rows).
 * 
 * @author CASCADE
 */
public class PivotColumns {
		
		/**
		 * Array list of columns
		 */
		public ArrayList<CV_Super> columns;

		/**
		 * Constructs taking a list of Objects and an Array.
		 * @param pObjs Objects that you want to Pivot on
		 * @param pRows Indexes for the Object List that will be pivoted on
		 */
		public PivotColumns(List<CV_Super> pObjs, int... pRows) {
			
			columns = new ArrayList<CV_Super>();
			//  Loop through indexes array and pull out the corresponding objects then
			// slip them into the columns list
			for (int i = 0; i < pRows.length; i++) {
				columns.add(pObjs.get(pRows[i]));
			}

		}
		
		/**
		 * Basic add method
		 * @param obj
		 */
		public void addObject(CV_Super obj) {
			columns.add(obj);
		}
		
		/**
		 * Standard Hashcode method need for comparison
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((columns == null) ? 0 : columns.hashCode());
			return result;
		}
		
		/**
		 * Equals Method used for Comparison purposes.
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PivotColumns other = (PivotColumns) obj;
			if (columns == null) {
				if (other.columns != null)
					return false;
			} else if (!columns.equals(other.columns))
				return false;
			return true;
		}
		
		/**
		 * toString used for pretty printing
		 */
		public String toString() {
			String s = "";
			for (Object obj : columns) {
				s += obj + ",";
			}

			return s.substring(0, s.lastIndexOf(','));
		}
}