/*
Copyright 2010 Alexandre Gellibert

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/
LICENSE-2.0 Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an "AS IS"
BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied. See the License for the specific language governing permissions
and limitations under the License.
 */
package jema.functional.core.cascade.util.geo;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import jema.common.types.CV_Box;
import jema.common.types.CV_Point;

/**
#
# Copyright 2010 Alexandre Gellibert
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
 */


/**
 * Utils class to compute geocells.
 *
 * @author api.roman.public@gmail.com (Roman Nurik)
 * @author (java portage) Alexandre Gellibert
 *
 */
public final class GeoModel 
{
	/** Globe boundaries */
	public static final Double MIN_LONGITUDE = -180.0;
	public static final Double MAX_LONGITUDE = 180.0;
	public static final Double MIN_LATITUDE = -90.0;
	public static final Double MAX_LATITUDE = 90.0;

	/** Grid cell size for 4x4 grid */
	public static final int GEOCELL_GRID_SIZE = 4;
	
	/** Base-16 geomodel hash codes */
	private static final String GEOCELL_ALPHABET = "0123456789abcdef";

	/** Singleton instaance of this object */
	private static GeoModel instance;

	/** Longitude index */
	public static final int LON_IDX = 0;

	/** Latitude index */
	public static final int LAT_IDX = 1;

	/**
	 * Default constructor.
	 */
	protected GeoModel() {}

	/**
	 * Single interface to instantiate this object.
	 * 
	 * @return Singleton GeoModel instance
	 */
	public static GeoModel instance()
	{
		if(instance == null) {instance = new GeoModel();}
		return instance;
	}

	/**
	 * Computes the geocell containing the given point to the given resolution.
	 * 
	 * This is a simple 16-tree lookup to an arbitrary depth (resolution).
	 *
	 * @param point: The geotypes.Point to compute the cell for.
	 * @param resolution: An int indicating the resolution of the cell to compute.
	 * @return The geocell string containing the given point, of length resolution.
	 */
	public String encode(Double lon, Double lat, int resolution) 
	{
		Double north = MAX_LATITUDE;
		Double south = MIN_LATITUDE;
		Double east = MAX_LONGITUDE;
		Double west = MIN_LONGITUDE;

		StringBuilder cell = new StringBuilder();
		while(cell.length() < resolution) 
		{
			Double subcellLonSpan = (east - west) / GEOCELL_GRID_SIZE;
			Double subcellLatSpan = (north - south) / GEOCELL_GRID_SIZE;

			int ix = Math.min((int)(
					GEOCELL_GRID_SIZE * (lon - west) / (east - west)),
					GEOCELL_GRID_SIZE - 1);
			
			int iy = Math.min((int)(
					GEOCELL_GRID_SIZE * (lat - south) / (north - south)),
					GEOCELL_GRID_SIZE - 1);

			int gridIndices[] = {ix,iy};
			cell.append(subdivChar(gridIndices));

			south += subcellLatSpan * iy;
			north = south + subcellLatSpan;

			west += subcellLonSpan * ix;
			east = west + subcellLonSpan;
		}
		return cell.toString();
	}

	/**
	 * Computes the rectangular boundaries (bounding box) of the given geocell.
	 *
	 * @param cell: The geocell string whose boundaries are to be computed.
	 * @return A CV Box corresponding to the rectangular boundaries of the geocell.
	 */
	public CV_Point decode(String cell) 
	{
		CV_Point point = null;
		if(cell != null)
		{	
			Double north = MAX_LATITUDE;
			Double south = MIN_LATITUDE;
			Double east = MAX_LONGITUDE;
			Double west = MIN_LONGITUDE;
			
			CharacterIterator iter  = new StringCharacterIterator(cell);
			for(char gridCell = iter.first(); gridCell != StringCharacterIterator.DONE; gridCell = iter.next()) 
			{
				double subcellLonSpan = (east - west) / GEOCELL_GRID_SIZE;
				double subcellLatSpan = (north - south) / GEOCELL_GRID_SIZE;

				int gridIndices[] = this.subdivXY(gridCell);
				int ix = gridIndices[LON_IDX];
				int iy = gridIndices[LAT_IDX];

				south += subcellLatSpan * iy;
				north = south + subcellLatSpan * (iy + 1);

				west += subcellLonSpan * ix;
				east = west + subcellLonSpan * (ix + 1);

				north = Angle.normalizedDegreesLatitude(north);
				east = Angle.normalizedDegreesLongitude(east);
				south = Angle.normalizedDegreesLatitude(south);
				west = Angle.normalizedDegreesLongitude(west);
			}
			
			Double lat = Angle.normalizedDegreesLatitude((north-south)/2);
			Double lon = Angle.normalizedDegreesLongitude((east-west)/2);
			
			point = new CV_Point(lon, lat);
		}

		return point;
	}

	/**
	 * Computes the rectangular boundaries (bounding box) of the given geocell.
	 *
	 * @param cell: The geocell string whose boundaries are to be computed.
	 * @return A CV Box corresponding to the rectangular boundaries of the geocell.
	 */
	public CV_Box computeBox(String cell) 
	{
		CV_Box bbox = null;
		if(cell != null)
		{	
			Double north = MAX_LATITUDE;
			Double south = MIN_LATITUDE;
			Double east = MAX_LONGITUDE;
			Double west = MIN_LONGITUDE;
			
			Double northPrev = north;
			Double southPrev = south;
			Double eastPrev = east;
			Double westPrev = west;
			
			CharacterIterator iter  = new StringCharacterIterator(cell);
			for(char gridCell = iter.first(); gridCell != StringCharacterIterator.DONE; gridCell = iter.next()) 
			{
				double subcellLonSpan = (east - west) / GEOCELL_GRID_SIZE;
				double subcellLatSpan = (north - south) / GEOCELL_GRID_SIZE;

				int gridIndices[] = this.subdivXY(gridCell);
				int ix = gridIndices[LON_IDX];
				int iy = gridIndices[LAT_IDX];

				south = southPrev + subcellLatSpan * iy;
				north = south + subcellLatSpan;

				west = westPrev + subcellLonSpan * ix;
				east = west + subcellLonSpan;

				north = Angle.normalizedDegreesLatitude(north);
				east = Angle.normalizedDegreesLongitude(east);
				south = Angle.normalizedDegreesLatitude(south);
				west = Angle.normalizedDegreesLongitude(west);
				
				northPrev = north;
				southPrev = south;
				eastPrev = east;
				westPrev = west;
				
			}

			bbox = new CV_Box(west, south, east, north);
		}

		return bbox;
	}

	/**
	 * Returns the grid indices of the geocell character in the 4x4 alphabet grid.
	 * <p>
	 * <b>Notes</b>
	 * <ul>
	 * <li>This algorithm is only valid for a 4x4 grid</li>
	 * </ul>
	 * @param cellChar  Grid cell geohash character
	 * @return The (x, y) of the geocell character in the 4x4 alphabet grid.
	 */
	public int[] subdivXY(char cellChar) 
	{
		int charI = GEOCELL_ALPHABET.indexOf(cellChar);
		int ix = (charI & 4) >> 1 | (charI & 1) >> 0;
		int iy = (charI & 8) >> 2 | (charI & 2) >> 1;
		return new int[] {ix, iy};
	}

	/**
	 * Returns the geocell character in the 4x4 alphabet grid at position (x, y).
	 * <p>
	 * <b>Notes</b>
	 * <ul>
	 * <li>This algorithm is only valid for a 4x4 grid</li>
	 * </ul>
	 * @param pos Geohash character
	 * @return Geocell character in the 4x4 alphabet grid at position (x, y).
	 */
	public char subdivChar(int[] pos) 
	{
		int c = 
			(pos[LAT_IDX] & 2) << 2 |
			(pos[LON_IDX] & 2) << 1 |
			(pos[LAT_IDX] & 1) << 1 |
			(pos[LON_IDX] & 1) << 0;
		return GEOCELL_ALPHABET.charAt(c);
	}
}