/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;

import java.util.logging.Logger;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Geometry;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;

/**
 * GeoToolsUtil contains a set of methods that provide GeoTools calculations
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://docs.geotools.org/">GeoTools Documentation</a></li>
 * <li><a href="http://www.vividsolutions.com/jts/javadoc/index.html">VividSolutons API</a></li>
 * </ul>
 * 
 * @author CASCADE
 */
public class GeoToolsUtil 
{   
	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(GeoToolsUtil.class.getName());
	
	/** WGS84 URN */
	public static final String WGS84_URN = "EPSG:4326";
	
	/** GeoToolsUtil static instance */
	protected static GeoToolsUtil instance;
	
	/**
	 * Default constructor
	 */
	protected GeoToolsUtil() {}
	
	/**
	 * Singleton accessor to retrieve the instance of GeoToolsUtils.
	 * 
	 * @return instance of GeoToolsUtils.
	 */
	public static GeoToolsUtil instance()
	{
		if(instance == null) {instance = new GeoToolsUtil();}
		return instance;
	}
	
	/**
	 * Transform a CV Polygon to a CV Box.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Box. 
	 */
	public CV_Box polygonToBox(CV_Polygon cvPolygon)
	{
		CV_Box destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Convert CV WKT to GeoTools Geometry
				WKTReader reader = new WKTReader();
				Geometry geom = reader.read(cvPolygon.toString());

				// Get bounding box
				Envelope vsEnvelope = geom.getEnvelopeInternal();
				Coordinate ll = this.normalizeCoord(new Coordinate(vsEnvelope.getMinX(), vsEnvelope.getMinY()));				
				Coordinate ur = this.normalizeCoord(new Coordinate(vsEnvelope.getMaxX(), vsEnvelope.getMaxY()));				
				
				// Bounding box to Point				
				destGeom = new CV_Box(ll.x, ll.y, ur.x, ur.y);
				
//				LOGGER.log(Level.FINEST, String.format(
//					"polygonToBox: envelope=%s, WKT=%s\n", 
//					vsEnvelope.toString(), destGeom.toString()));					
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Transform a CV Polygon to a CV Point.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Point. 
	 */
	public CV_Point polygonToPoint(CV_Polygon cvPolygon)
	{
		CV_Point destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Convert CV WKT to GeoTools Geometry
				WKTReader reader = new WKTReader();
				Geometry geom = reader.read(cvPolygon.toString());

				// Get bounding box
				Envelope vsEnvelope = geom.getEnvelopeInternal();
				
				Coordinate center = this.normalizeCoord(vsEnvelope.centre());
				
				// Bounding box to Point				
				destGeom = new CV_Point(
					center.x,
					center.y);
				
//				LOGGER.log(Level.FINEST, String.format(
//					"polygonToPoint: center lat=%g lon=%g, WKT=%s\n", 
//					vsEnvelope.centre().x, vsEnvelope.centre().y, destGeom.toString()));					
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Transform a CV Polygon to a CV Circle.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV circle that contains a center point and radius. 
	 */
	public CV_Circle polygonToCircle(CV_Polygon cvPolygon)
	{
		CV_Circle destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Convert CV WKT to GeoTools Geometry
				WKTReader reader = new WKTReader();
				Geometry geom = reader.read(cvPolygon.toString());
				
				// Set CRS
				CoordinateReferenceSystem  crs = this.getCRS(cvPolygon);
				
				// Get bounding box
				Envelope vsEnvelope = geom.getEnvelopeInternal();
				Coordinate maxCoord = new Coordinate(vsEnvelope.getMaxX(), vsEnvelope.getMaxY());
				Coordinate minCoord = new Coordinate(vsEnvelope.getMinX(), vsEnvelope.getMinY());
				double maxR = JTS.orthodromicDistance(vsEnvelope.centre(), maxCoord, crs);
				double minR = JTS.orthodromicDistance(vsEnvelope.centre(), minCoord, crs);
				
				Coordinate center = this.normalizeCoord(vsEnvelope.centre());
				
				// Bounding box to Circle
				destGeom = new CV_Circle(
					center.x,
					center.y,
					maxR > minR ? maxR : minR);
				
//				LOGGER.log(Level.FINEST, String.format(
//					"polygonToCircle: center lat=%g lon=%g, maxR=%g, minR=%g, WKT=%s\n", 
//					vsEnvelope.centre().x, vsEnvelope.centre().y, maxR, minR, destGeom.toString()));					
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}

	/**
	 * Calculate a bounding box based upon circle center and radius.
	 * 
	 * @param circle CV circle containing center point and radius (meters)
	 * @return bounding box
	 */
	public CV_Box circleToBox(CV_Circle circle)
	{
		CV_Box cvBox = null;

		try 
		{
			CV_Polygon cvPolygon = circle.generatePolygon();			
			cvBox = this.polygonToBox(cvPolygon);
		} 
		catch (Exception e) 
		{
		}

		return cvBox;
	}
    
    /**
     * Accessor to retrieve the Coordinate Reference System.
     * 
     * @param cvGeom Source CV geometry with associate CRS.
     * @return Coordinate Reference System
     */
    @SuppressWarnings("rawtypes")
	protected CoordinateReferenceSystem getCRS(CV_Geometry cvGeom)
    {
    	CoordinateReferenceSystem crs = null;
    	try
    	{
    		String urn = (cvGeom == null || cvGeom.getCrs() == null) ? WGS84_URN : cvGeom.getCrs().getUrn();
    		if(urn == null || urn.isEmpty()) {urn = WGS84_URN;}
    		crs = CRS.decode(urn);
    	}
    	catch(Exception e) {throw new RuntimeException(e.toString(), e);}

    	return crs;
    }
    
    /**
     * Normalize the latitude and longitude using the NASA World Wind Angle.
     * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li><a href="http://builds.worldwind.arc.nasa.gov/worldwind-releases/1.2/docs/api/index.html">
	 * NASA World Wind API</a></li>
	 * </ul>
	 * 
     * @param coord Coordinate in degrees
     * @return normalized coordinate in degrees
     */
    protected Coordinate normalizeCoord(Coordinate coord)
    {
       	Angle lon = Angle.fromDegrees(coord.x).normalizedLongitude();
       	Angle lat = Angle.fromDegrees(coord.y).normalizedLatitude();
        Coordinate coordNormalized = new Coordinate(lon.degrees, lat.degrees);
    	return coordNormalized;
    }
}
