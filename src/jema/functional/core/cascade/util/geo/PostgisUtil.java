/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;

import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.TableFactory;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDatabaseProperties;
import jema.functional.api.ExecutionContext;

/**
 * PostgisUtil contains a set of methods that provide PostGIS functions.
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://docs.geotools.org/">GeoTools Documentation</a></li>
 * <li><a href="http://www.vividsolutions.com/jts/javadoc/index.html">VividSolutons API</a></li>
 * </ul>
 * 
 * @author CASCADE
 */
public class PostgisUtil 
{   
	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(PostgisUtil.class.getName());
	
	/** WGS84 URN */
	public static final String WGS84_URN = "EPSG:4326";
	
	/** PostgisUtil static instance */
	protected static PostgisUtil instance;

	/** SELECT statement table alias */
	protected final static String TABLE_ALIAS1 = SelectBuilder.QB_TABLE_ALIAS+"1";
	
	/** CV_Super translator */
	private CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();
	
	/**
	 * Default constructor
	 */
	protected PostgisUtil() {}
	
	/**
	 * Singleton accessor to retrieve the instance of PostgisUtils.
	 * 
	 * @return instance of PostgisUtils.
	 */
	public static PostgisUtil instance()
	{
		if(instance == null) {instance = new PostgisUtil();}
		return instance;
	}
	
	/**
	 * Transform a CV Polygon to a CV Box.
	 * 
	 * @param ctx Execution context to create temporary table
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Box. 
	 */
	@SuppressWarnings("rawtypes")
	public CV_Box polygonToBox(ExecutionContext ctx, CV_Polygon cvPolygon)
	{
		CV_Box destGeom = null;

		try
		{
			if(isActivePostgresDatabase(ctx) && (cvPolygon != null))
			{
				// Temporary source table to execute query
				CV_Table tmpTable = ctx.createTable("TempTable");
				tmpTable.close();
				
				// Bounding box query
				String selectStatement = String.format(
//						"SELECT CAST(BOX2D(ST_Shift_Longitude(ST_EXTENT(\'%s\'))) AS TEXT) AS BoundingBox", 
					"SELECT CAST(BOX2D(ST_EXTENT(\'%s\')) AS TEXT) AS BoundingBox", 
					cvPolygon.toString());
				
				// Build query 
				TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
				SelectBuilder selectBuilder = factory.selectBuilder();
				selectBuilder.tableAs(tmpTable, TABLE_ALIAS1).spec(CV_String.valueOf(selectStatement));
				CV_Table table = TableQueryExecutor.executeQuery(selectBuilder);
				
				// Get bounding box
				if(table != null)
				{
					List<CV_Super> row = table.readRow();
					CV_Super cell = row.isEmpty() ? null : row.get(0);
					CV_Box box = (CV_Box) cvDataGenerator.genSuper(ParameterType.box, cell.toString(), true);
					destGeom = box;
					table.close();
					
//					LOGGER.log(Level.FINEST, String.format(
//						"polygonToBox: WKT=%s\n", 
//						destGeom.toString()));					
				}
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Transform a CV Polygon to a CV Point.
	 * 
	 * @param ctx Execution context to create temporary table
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Point. 
	 */
	@SuppressWarnings("rawtypes")
	public CV_Point polygonToPoint(ExecutionContext ctx, CV_Polygon cvPolygon)
	{
		CV_Point destGeom = null;

		try
		{
			if(isActivePostgresDatabase(ctx) && (cvPolygon != null))
			{
				// Temporary source table to execute query
				CV_Table tmpTable = ctx.createTable("TempTable");
				tmpTable.close();
				
				// Bounding box query
				String selectStatement = String.format(
					"SELECT ST_ASTEXT(ST_CENTROID(\'%s\')) AS CenterPoint", 
					cvPolygon.toString());
				
				// Build query 
				TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
				SelectBuilder selectBuilder = factory.selectBuilder();
				selectBuilder.tableAs(tmpTable, TABLE_ALIAS1).spec(CV_String.valueOf(selectStatement));
				CV_Table table = TableQueryExecutor.executeQuery(selectBuilder);
				
				// Get bounding box
				if(table != null)
				{
					List<CV_Super> row = table.readRow();
					CV_Super cell = row.isEmpty() ? null : row.get(0);
					destGeom = (CV_Point) cvDataGenerator.genSuper(ParameterType.point, cell.toString(), true);					
					table.close();
				}
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Transform a CV Polygon to a CV Circle.
	 * 
	 * @param ctx Execution context to create temporary table
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV circle that contains a center point and radius. 
	 */
	public CV_Circle polygonToCircle(ExecutionContext ctx, CV_Polygon cvPolygon)
	{
		CV_Circle destGeom = null;

		try
		{
			if(isActivePostgresDatabase(ctx) && (cvPolygon != null))
			{
//				LOGGER.log(Level.FINEST, String.format(
//					"polygonToCircle: center lat=%g lon=%g, maxR=%g, minR=%g, WKT=%s\n", 
//					bbox.centre().x, bbox.centre().y, maxR, minR, destGeom.toString()));					
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Transform a CV Circle to a CV Box.
	 * 
	 * @param ctx Execution context to create temporary table
	 * @param cvCircle  A circle which contains a WKT string representation.
	 * @return A polygon converted to a CV Box. 
	 */
	public CV_Box circleToBox(ExecutionContext ctx, CV_Circle cvCircle)
	{
		CV_Box destGeom = null;

		try
		{
			if(isActivePostgresDatabase(ctx) && (cvCircle != null))
			{
				CV_Polygon cvPolygon = cvCircle.generatePolygon();
				destGeom = this.polygonToBox(ctx, cvPolygon);
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}
	
	/**
	 * Validate that PostgreSQL database is accessible by opening a table via
	 * the execution context which points to the specified database.
	 * @param ctx Execution context
	 * @return true=specified database in context is active, false=otherwise
	 */
	public static Boolean isActivePostgresDatabase(ExecutionContext ctx)
	{
		Boolean isValid = true;
		CV_Table tmpTable = null;
		
		try
		{
			if(ctx != null)
			{
				// Temporary table to validate db access
				tmpTable = ctx.createTable("TempTable");
				
				// PostgreSQL
				CommonVocabDatabaseProperties dbProp = tmpTable.getDatabaseProperties();
				isValid = dbProp.isPostgresql();
			}
		}
		catch(Exception e) {isValid = false;}
		finally {if(tmpTable != null) {tmpTable.close();}}

		return isValid;
	}
	
	/**
	 * Delete postgresql tables created during test runs.
	 * @param ctx Execution context
	 */
	public static void deleteTables(ExecutionContext ctx)
	{
		CV_Table tmpTable = null;
		
		try
		{
			if(ctx != null)
			{
				// Temporary table to validate db access
				tmpTable = ctx.createTable("TempTable");
				
				// PostgreSQL
				CommonVocabDatabaseProperties dbProp = tmpTable.getDatabaseProperties();
				
				if(tmpTable != null) {tmpTable.close();}
				TableFactory.deleteTables(tmpTable.getUri(), dbProp, null);
			}
		}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}
	
	/**
	 * Validate that the PostgreSQL database has been selected for the session database.
	 * @return true=PostgreSQL database has been selected for the session database, false=otherwise
	 */
	public static Boolean isPostgreSQLSelected()
	{
		String postgresValue = CommonVocabDatabaseProperties.DatabasePropertiesKey.DATABASE.getPostgresValue();
        String key = "jema.common.types.util.CommonVocabDatabaseProperties.defaultDB";
        Object value = (key == null) ? null : System.getProperty(key);
		Boolean isPostgres = (value != null)&& (value.toString().equals(postgresValue));
        return isPostgres;

	}
}
