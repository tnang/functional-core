/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vividsolutions.jts.geom.Coordinate;

import jema.common.types.CV_Box;
import jema.common.types.CV_Circle;
import jema.common.types.CV_Point;
import jema.common.types.CV_Polygon;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.functional.core.cascade.util.TableUtils;
/**
 * Class of Utilities for Geometry Functions
 * @author CASCADE
 */
public class GeometryUtil 
{
	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(GeometryUtil.class.getName());

	/** Longitude index */
	private static final int LON_IDX = 0;

	/** Latitude index */
	private static final int LAT_IDX = 1;

	/**
	 * Find the table index for the Polygon Column
	 * 
	 * @param pTable
	 * @return
	 */
	public static int findGeometryColumnPolygon(CV_Table pTable) {
		int index = -1;
		List<ColumnHeader> headerList = pTable.getHeader().asList();
		for (ColumnHeader c : headerList) {
			if (c.getType().equals(ParameterType.polygon)) {
				index = pTable.findIndex(c.getName());
			}
		}
		return index;
	}

	/**
	 * Find the table index for the Point Column
	 * 
	 * @param pTable
	 * @return
	 */
	public static int findGeometryColumnPoint(CV_Table pTable) {
		int index = -1;
		List<ColumnHeader> headerList = pTable.getHeader().asList();
		for (ColumnHeader c : headerList) {
			if (c.getType().equals(ParameterType.point)) {
				index = pTable.findIndex(c.getName());
			}
		}
		return index;
	}

	/**
	 * Find the table index for the Point Column
	 * 
	 * @param pTable
	 * @return
	 */
	public static int findGeometryColumnLineString(CV_Table pTable) {
		int index = -1;
		List<ColumnHeader> headerList = pTable.getHeader().asList();
		for (ColumnHeader c : headerList) {
			if (c.getType().equals(ParameterType.lineString)) {
				index = pTable.findIndex(c.getName());
			}
		}
		return index;
	}

	/**
	 * Compare CV_Box coordinates within a tolerance.
	 * @param a Box a
	 * @param b Box b
	 * @return true = boxes are equal within tolerance, false=otherwise
	 */
	public static boolean compareBox(CV_Box a, CV_Box b)
	{
		Double epsilon = 1.0e-3;
		boolean result = TableUtils.equalDecimal(a.getBottom(), b.getBottom(), epsilon);
		result = result && TableUtils.equalDecimal(a.getLeft(), b.getLeft(), epsilon);
		result = result && TableUtils.equalDecimal(a.getRight(), b.getRight(), epsilon);
		result = result && TableUtils.equalDecimal(a.getTop(), b.getTop(), epsilon);
		return result;
	}

	/**
	 * Calculate a bounding box based upon circle center and radius.
	 * 
	 * @param circle CV circle containing center point and radius (meters)
	 * @return bounding box
	 */
	public static CV_Box circleToBox(CV_Circle circle)
	{
		CV_Box cvBox = null;

		try 
		{	
			double xll = Integer.MAX_VALUE;
			double xur = Integer.MIN_VALUE;
			double yll = Integer.MAX_VALUE;
			double yur = Integer.MIN_VALUE;

			LatLon center = LatLon.fromDegrees(circle.getLatitude(), circle.getLongitude());
			double radius = circle.getRadiusMeters();
			double R = LatLon.earthRadiusMeters(center.latitude);
			Angle pathLength = Angle.fromRadians(radius/R);

			for(int angdeg = -180; angdeg < 180; angdeg += 90)
			{
				Angle az = Angle.fromDegrees(angdeg);
				LatLon coord = LatLon.greatCircleEndPosition(center, az, pathLength);
				xll = (xll < coord.longitude.degrees) ? xll : coord.longitude.degrees;
				xur = (xur > coord.longitude.degrees) ? xur : coord.longitude.degrees;
				yll = (yll < coord.latitude.degrees) ? yll : coord.latitude.degrees;
				yur = (yur > coord.latitude.degrees) ? yur : coord.latitude.degrees;
			}

			cvBox = new CV_Box(xll, yll, xur, yur);
		} 
		catch (Exception e) 
		{
		}

		return cvBox;
	}

	/**
	 * Transform a CV Polygon to a CV Box.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Box. 
	 */
	public static CV_Box polygonToBox(CV_Polygon cvPolygon)
	{
		CV_Box destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Populate list of LatLon
				List<LatLon> list = polygonToList(cvPolygon);

				// Region
				Sector sector = Sector.boundingSector(list);

				// Bounding box 				
				destGeom = new CV_Box(
						sector.getCorners()[Sector.CORNER_SW_IDX].longitude.degrees,
						sector.getCorners()[Sector.CORNER_SW_IDX].latitude.degrees,
						sector.getCorners()[Sector.CORNER_NE_IDX].longitude.degrees,
						sector.getCorners()[Sector.CORNER_NE_IDX].latitude.degrees);
				
//				LOGGER.log(Level.FINEST, String.format(
//					"polygonToBox: sector=%s, WKT=%s\n", 
//					sector.toString(), destGeom.toString()));					
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}

	/**
	 * Transform a CV Polygon to a CV Box.  All points in Western Hemisphere 
	 * (i.e., longitude < 0) are shifted 360 degrees for calculations.
	 * <p>
	 * <b>Notes</b>
	 * <ul>
	 * <li>If crossDateline is true, then all negative lon are shifted 360 degrees</li>
	 * </ul>
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @param crossDateline true=polygon points go from E Hemisphere to W Hemisphere across 
	 * dateline, false=polygon lines do not cross dateline
	 * @return A polygon converted to a CV Box. 
	 */
	public static CV_Box polygonToBox(CV_Polygon cvPolygon, Boolean crossDateline)
	{
		CV_Box destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				double leftLon = Double.POSITIVE_INFINITY;
				double rightLon = Double.NEGATIVE_INFINITY;
				double lowerLat = Double.POSITIVE_INFINITY;
				double upperLat = Double.NEGATIVE_INFINITY;

				List<List<List<Double>>> dataList = cvPolygon.toData();
				if(!dataList.isEmpty())
				{
					for(List<List<Double>> lsList : dataList)
					{
						for(List<Double> ptList : lsList)
						{
							double lon = ptList.get(LON_IDX);
							double lat = ptList.get(LAT_IDX);

							// Dateline
							if(crossDateline && lon < 0) {lon += 360.0;}

							// Bottom Left
							leftLon = lon < leftLon ? lon : leftLon;
							lowerLat = lat < lowerLat ? lat : lowerLat;

							// Upper Right
							rightLon = lon > rightLon ? lon : rightLon;
							upperLat = lat > upperLat ? lat : upperLat;
							
//							System.out.format(
//								"leftLon=%g, lowerLat=%g, rightLon=%g, upperLat=%g, lat=%g, lon=%g, crossDateLine=%s\n", 
//								leftLon, lowerLat, rightLon, upperLat, lat, lon, crossDateLine);
						}
					}
					
					if(leftLon > 180.0) {leftLon -= 360.0;}
					if(rightLon > 180.0) {rightLon -= 360.0;}

					destGeom = new CV_Box(leftLon, lowerLat, rightLon, upperLat);
				}
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}

	/**
	 * Transform a CV Polygon to a CV Point.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV Point. 
	 */
	public static CV_Point polygonToPoint(CV_Polygon cvPolygon)
	{
		CV_Point destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Populate list of LatLon
				List<LatLon> list = polygonToList(cvPolygon);

				// Region
				Sector sector = Sector.boundingSector(list);

				// Bounding box to Point				
				destGeom = new CV_Point(
						sector.getCentroid().longitude.degrees,
						sector.getCentroid().latitude.degrees);
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}

	/**
	 * Transform a CV Polygon to a CV Circle.
	 * 
	 * @param cvPolygon  A polygon which contains a WKT string representation.
	 * @return A polygon converted to a CV circle that contains a center point and radius. 
	 */
	public static CV_Circle polygonToCircle(CV_Polygon cvPolygon)
	{
		CV_Circle destGeom = null;

		try
		{
			if(cvPolygon != null)
			{
				// Populate list of LatLon
				List<LatLon> list = polygonToList(cvPolygon);

				// Region
				Sector sector = Sector.boundingSector(list);

				// Centroid
				LatLon center = sector.getCentroid();

				// Radius
				LatLon[] corners = sector.getCorners();

				double radius = 0;				
				for(LatLon p2 : corners)
				{
					double dist = LatLon.ellipsoidalDistance(
							center, 
							p2, 
							LatLon.EARTH_EQUATORIAL_RADIUS_METER, 
							LatLon.EARTH_POLAR_RADIUS_METER);

					radius = (radius < dist) ? dist : radius;
				}

				// Bounding box to Circle
				destGeom = new CV_Circle(
						center.longitude.normalizedLongitude().degrees,
						center.latitude.normalizedLatitude().degrees,
						radius);
			}
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return destGeom;
	}

	/**
	 * Make iterable list of latlon coordinates.
	 * @param cvPolygon CV Polygon
	 * @return iterable list of latlon coordinates
	 */
	protected static List<LatLon> polygonToList(CV_Polygon cvPolygon)
	{
		List<LatLon> latLonList = new ArrayList<LatLon>();

		// Populate list of LatLon
		if(cvPolygon != null)
		{
			List<List<List<Double>>> dataList = cvPolygon.toData();
			for(List<List<Double>> lsList : dataList)
			{
				for(List<Double> ptList : lsList)
				{
					LatLon latLon = LatLon.fromDegrees(ptList.get(LAT_IDX), ptList.get(LON_IDX));
					latLonList.add(latLon);
				}
			}
		}
		return latLonList;
	}
    
    /**
     * Normalize the latitude and longitude using the NASA World Wind Angle.
     * <p>
	 * <b>References:</b>
	 * <ul>
	 * <li><a href="http://builds.worldwind.arc.nasa.gov/worldwind-releases/1.2/docs/api/index.html">
	 * NASA World Wind API</a></li>
	 * </ul>
	 * 
     * @param coord Coordinate in degrees
     * @return normalized coordinate in degrees
     */
    public static CV_Point normalizeCoord(double x, double y)
    {
    	Coordinate coord = new Coordinate(x, y);
       	Angle lon = Angle.fromDegrees(coord.x).normalizedLongitude();
       	Angle lat = Angle.fromDegrees(coord.y).normalizedLatitude();
        Coordinate coordNormalized = new Coordinate(lon.degrees, lat.degrees);
    	return new CV_Point(coordNormalized.x, coordNormalized.y);
    }
}
