/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;

/**
 * Google GeoHash utility.
 * 
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://developer-should-know.tumblr.com/post/87283491372/geohash-encoding-and-decoding-algorithm">
 * Geohash Encoding and Decoding Algorithm</a></li>
 *
 * @author CASCADE
 */
public class GeohashBase16 extends Geohash
{
	/** Base 16 GeoHash values */
	private static final String BASE_16 = 
		"0123456789abcdef";
	
	/** Base 16 GeoHash row indices */
	private static final int[] ROW_INDICES= {
		0,1,0,1,2,3,2,3,0,1,0,1,2,3,2,3};
	
	/** Base 16 GeoHash column indices */
	private static final int[] COL_INDICES = {
		0,0,1,1,0,0,1,1,2,2,3,3,2,2,3,3};

	/** Grid cell size for 4x4 grid */
	public static final int GEOCELL_GRID_SIZE = 4;
	
	/** Singleton geohash instance */
	private static GeohashBase16 instance;

	
	/**
	 * Singleton accessor to retrieve GeoHash instance.
	 * @return Singleton geohash instance
	 */
	public static Geohash instance()
	{
		if(instance == null) {instance = new GeohashBase16();}
		return instance;
	}

    /**
     * Calculate geohash string from coordinate at specified precesion (1-12)
     * @param lat Coordinate latitude (deg)
     * @param lon Coordinate longitude (deg)
     * @param precision Geohash level (1-12)
     * @return Geohash string calculated from coordinate at specified precesion
     */
    @Override
    public String encode(double lat, double lon, int precision) 
    {
		Double north = 90.0;
		Double south = -90.0;
		Double east = 180.0;
		Double west = -180.0;

		StringBuilder cell = new StringBuilder();
		while(cell.length() < precision) 
		{
			Double subcellLonSpan = (east - west) / GEOCELL_GRID_SIZE;
			Double subcellLatSpan = (north - south) / GEOCELL_GRID_SIZE;

			int ix = Math.min((int)(
					GEOCELL_GRID_SIZE * (lon - west) / (east - west)),
					GEOCELL_GRID_SIZE - 1);
			
			int iy = Math.min((int)(
					GEOCELL_GRID_SIZE * (lat - south) / (north - south)),
					GEOCELL_GRID_SIZE - 1);

			int gridIndices[] = {ix,iy};
			cell.append(subdivChar(gridIndices));

			south += subcellLatSpan * iy;
			north = south + subcellLatSpan;

			west += subcellLonSpan * ix;
			east = west + subcellLonSpan;
		}
		return cell.toString();
    }

    /**
     * Calculate coordinate point associated with specified geohash string.
     * @param geohash Geohash string
     * @return Coordinate point associated with specified geohash string
     */
    @Override
    public double[] decode(String geohash) 
    {
		Double north = 90.0;
		Double south = -90.0;
		Double east = 180.0;
		Double west = -180.0;

        for (int iChar = 0; iChar < geohash.length(); iChar++) 
        {
        	char gridCell = geohash.charAt(iChar);
        	int iCell = BASE_16.indexOf(gridCell);
        	int iGridCol = COL_INDICES[iCell];
        	int iGridRow = ROW_INDICES[iCell];
        	
			double cellLonSpan = (east - west) / GEOCELL_GRID_SIZE;
			double cellLatSpan = (north - south) / GEOCELL_GRID_SIZE;

			south += (cellLatSpan * iGridRow);
			north = south + cellLatSpan * (iGridRow + 1);

			west += (cellLonSpan * iGridCol);
			east = west + cellLonSpan * (iGridCol + 1);

			north = Angle.normalizedDegreesLatitude(north);
			east = Angle.normalizedDegreesLongitude(east);
			south = Angle.normalizedDegreesLatitude(south);
			west = Angle.normalizedDegreesLongitude(west);
        }
		
		Double lat = Angle.normalizedDegreesLatitude((north-south)/2);
		Double lon = Angle.normalizedDegreesLongitude((east-west)/2);

        return new double[]{lat, lon};
    }

	/**
	 * Returns the geocell character in the 4x4 alphabet grid at position (x, y).
	 * <p>
	 * <b>Notes</b>
	 * <ul>
	 * <li>This algorithm is only valid for a 4x4 grid</li>
	 * </ul>
	 * @param pos Geohash character
	 * @return Geocell character in the 4x4 alphabet grid at position (x, y).
	 */
	public char subdivChar(int[] pos) 
	{
		int c = 
			(pos[LAT_IDX] & 2) << 2 |
			(pos[LON_IDX] & 2) << 1 |
			(pos[LAT_IDX] & 1) << 1 |
			(pos[LON_IDX] & 1) << 0;
		return BASE_16.charAt(c);
	}
}