/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;
/**
 * Google GeoHash utility.
 * 
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="http://developer-should-know.tumblr.com/post/87283491372/geohash-encoding-and-decoding-algorithm">
 * Geohash Encoding and Decoding Algorithm</a></li>
 *
 * @author CASCADE
 */
public class Geohash 
{
	/** Singleton geohash instance */
	private static Geohash instance;

	/** Longitude index (Reversed to lat/lon) */
	public static final int LON_IDX = 1;

	/** Latitude index (Reversed to lat/lon) */
	public static final int LAT_IDX = 0;
	
	/**
	 * Singleton accessor to retrieve GeoHash instance.
	 * @return Singleton geohash instance
	 */
	public static Geohash instance()
	{
		if(instance == null) {instance = new Geohash();}
		return instance;
	}

	/** Base 32 GeoHash values */
	private static final String BASE_32 = "0123456789bcdefghjkmnpqrstuvwxyz";

	/**
	 * 
	 * @param value
	 * @param range
	 * @return
	 */
    protected int divideRangeByValue(double value, double[] range) 
    {
        double mid = middle(range);
        if (value >= mid) {
            range[0] = mid;
            return 1;
        } else {
            range[1] = mid;
            return 0;
        }
    }

    /**
     * D
     * @param bit
     * @param range
     */
    protected void divideRangeByBit(int bit, double[] range) 
    {
        double mid = middle(range);
        if (bit > 0) {
            range[0] = mid;
        } else {
            range[1] = mid;
        }
    }

    /**
     * Calculate range midpoint.
     * @param range Endpoints of a range
     * @return Midpoint of a range
     */
    protected double middle(double[] range) {
        return (range[0] + range[1]) / 2;
    }

    /**
     * Calculate geohash string from coordinate at specified precesion (1-12)
     * @param latitude Coordinate latitude (deg)
     * @param longitude Coordinate longitude (deg)
     * @param precision Geohash level (1-12)
     * @return Geohash string calculated from coordinate at specified precesion
     */
    public String encode(double latitude, double longitude, int precision) 
    {
        double[] latRange = new double[]{-90.0, 90.0};
        double[] lonRange = new double[]{-180.0, 180.0};
        boolean isEven = true;
        int bit = 0;
        int base32CharIndex = 0;
        StringBuilder geohash = new StringBuilder();

        while (geohash.length() < precision) {
            if (isEven) {
                base32CharIndex = (base32CharIndex << 1) | divideRangeByValue(longitude, lonRange);
            } else {
                base32CharIndex = (base32CharIndex << 1) | divideRangeByValue(latitude, latRange);
            }

            isEven = !isEven;

            if (bit < 4) {
                bit++;
            } else {
                geohash.append(BASE_32.charAt(base32CharIndex));
                bit = 0;
                base32CharIndex = 0;
            }
        }

        return geohash.toString();
    }

    /**
     * Calculate coordinate point associated with specified geohash string.
     * @param geohash Geohash string
     * @return Coordinate point associated with specified geohash string
     */
    public double[] decode(String geohash) 
    {
        double[] latRange = new double[]{-90.0, 90.0};
        double[] lonRange = new double[]{-180.0, 180.0};
        boolean isEvenBit = true;

        for (int i = 0; i < geohash.length(); i++) {
            int base32CharIndex = BASE_32.indexOf(geohash.charAt(i));
            for (int j = 4; j >= 0; j--) {
                if (isEvenBit) {
                    divideRangeByBit((base32CharIndex >> j) & 1, lonRange);
                } else {
                    divideRangeByBit((base32CharIndex >> j) & 1, latRange);
                }
                isEvenBit = !isEvenBit;
            }
        }

        return new double[]{middle(latRange), middle(lonRange)};
    }}