/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util.geo;

import java.util.HashMap;

/**
 * Google Base-32 utility.
 * 
 * <p>
 * <b>References:</b>
 * <ul>
 * <li><a href="https://code.google.com/p/geospatialweb/source/browse/trunk/geohash/src/Base32.java?r=104">
 * geospatialweb Base32.java</a></li>
 *
 * @author CASCADE
 *
 */
public class Base32 
{
	public final static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p',
			'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	public final static HashMap<Character, Integer> lookup = new HashMap<Character, Integer>();
	static {
		int i = 0;
		for (char c : digits)
			lookup.put(c, i++);
	}

	/**
	 * Accessor to determine and return base-32 geohash.
	 * @param i Integer representation of coordinate point.
	 * @return base-32 geohash
	 */
	public static String base32(long i) 
	{
		char[] buf = new char[65];
		int charPos = 64;
		boolean negative = (i < 0);
		if (!negative)
			i = -i;
		while (i <= -32) {
			buf[charPos--] = digits[(int) (-(i % 32))];
			i /= 32;
		}
		buf[charPos] = digits[(int) (-i)];
		if (negative)
			buf[--charPos] = '-';
		return new String(buf, charPos, (65 - charPos));
	}
}