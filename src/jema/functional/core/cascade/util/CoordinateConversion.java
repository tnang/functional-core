/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 31, 2015 6:12:40 PM
 */
package jema.functional.core.cascade.util;

import jema.functional.core.cascade.util.avlist.AVKey;
import jema.functional.core.cascade.util.geo.Angle;
import jema.functional.core.cascade.util.geo.coords.MGRSCoord;
import jema.functional.core.cascade.util.geo.coords.UTMCoord;

/**
 * @author CASCADE
 */
public final class CoordinateConversion {

	/**
	 * Convert a Degrees, Minutes, Seconds (DMS) coordinate string to Decimal
	 * Degrees (DD).
	 *
	 * Allow N, S, E, W suffixes and signs.
	 *
	 * Examples:
	 * <ul>
	 * <li>301557.1926N</li>
	 * <li>+301557.1926</li>
	 * <li>0795856.0W</li>
	 * <li>-0795856</li>
	 * <li>115°08'11"E</li>
	 * <li>+115°08′11″</li>
	 * <li>30° 15' 57.1926" S</li>
	 * <li>-30° 15' 57.1926"</li>
	 * </ul>
	 *
	 * @param dmsString
	 *            the Degrees Minutes Seconds string to convert.
	 * @return the corresponding value in Decimal Degrees.
	 */
	public Double DMStoDD(String dmsString) {
		if (dmsString.trim().isEmpty()) {
			return null;
		} else {
			// Replace degree, min and sec signs with space
			dmsString =
					dmsString.replaceAll(
							"[\u00B0|\u0027|\u2032|\\u0022|\u2033]", " ");

			// Replace multiple spaces with single ones
			dmsString = dmsString.replaceAll("\\s+", " ");
			dmsString = dmsString.trim();

			// Check for sign prefix and suffix
			int sign = 1;
			char suffix =
					dmsString.toUpperCase().charAt(dmsString.length() - 1);
			if (!Character.isDigit(suffix)) {
				sign = (suffix == 'N' || suffix == 'E') ? 1 : -1;
				dmsString = dmsString.substring(0, dmsString.length() - 1);
				dmsString = dmsString.trim();
			}

			char prefix = dmsString.charAt(0);
			if (!Character.isDigit(prefix)) {
				sign = (prefix == '-') ? -1 : 1;
				dmsString = dmsString.substring(1, dmsString.length());
				dmsString = dmsString.trim();
			}

			// Split string into degrees, minutes, seconds and do the conversion
			double d = 0.0;
			double m = 0.0;
			double s = 0.0;

			if (dmsString.contains(" ")) {
				String[] DMS = dmsString.split(" ");

				d = Integer.parseInt(DMS[0]);

				// Check if lat / lon are in range
				if (DMS[0].length() == 3) {
					if (Math.abs(d) > 180) {
						throw new IllegalArgumentException(
								"The longitude was not within the range -180 to 180.");
					}
				} else {
					if (Math.abs(d) > 90) {
						throw new IllegalArgumentException(
								"The latitude was not within the range -90 to 90.");
					}
				}

				m = DMS.length > 1 ? Integer.parseInt(DMS[1]) : 0;
				s = DMS.length > 2 ? Double.parseDouble(DMS[2]) : 0;
			} else {
				String dms = "";
				String decSecs = "";

				if (dmsString.contains(".")) {
					String[] parts = dmsString.split("[.]");
					dms = parts[0];
					decSecs = "." + parts[1];
				} else {
					dms = dmsString;
				}

				int index = 2;
				if (dms.length() == 7) // Longitude
				{
					index = 3;
				}

				// Check if lat / lon are in range
				d = Integer.parseInt(dms.substring(0, index));
				if (index == 3 && d > 180) {
					throw new IllegalArgumentException(
							"The longitude was not within the range -180 to 180.");
				} else if (index == 2 && d > 90) {
					throw new IllegalArgumentException(
							"The latitude was not within the range -90 to 90.");
				}
				try {
					m = Integer.parseInt(dms.substring(index, index + 2));
					s = Double.parseDouble(dms.substring(index + 2) + decSecs);
				} catch (StringIndexOutOfBoundsException ex) {
					throw new IllegalArgumentException(
							"Invalid DMS string format.", ex);
				}
			}

			// Calculate decimal degree value
			if (m >= 0 && m <= 60 && s >= 0 && s <= 60) {
				return d * sign + m / 60 * sign + s / 3600 * sign;
			}

			return null;
		}
	}

	/**
	 * Convert a coordinate in Decimal Degrees (DD) to Degrees Minutes Seconds
	 * (DMS) string format.
	 *
	 * Examples:
	 * <ul>
	 * <li>301557.19N</li>
	 * <li>0795856.00W</li>
	 * </ul>
	 *
	 * @param ddCoordinate
	 *            the Decimal Degrees coordinate to convert
	 * @param isLongitude
	 *            is provided DD coordinate longitude?
	 * @return the corresponding value in Degrees Minutes Seconds
	 */
	public String DDtoDMS(double ddCoordinate, boolean isLongitude) {
		String dmsString = "";
		String direction = "";

		if (isLongitude) {
			direction = (ddCoordinate > 0) ? "E" : "W";
		} else {
			direction = (ddCoordinate > 0) ? "N" : "S";
		}
		ddCoordinate = Math.abs(ddCoordinate);

		// Check if ddCoordinate is in range
		if (isLongitude) {
			if (ddCoordinate > 180) {
				throw new IllegalArgumentException(
						"The longitude was not within the range -180 to 180.");
			}
		} else {
			if (ddCoordinate > 90) {
				throw new IllegalArgumentException(
						"The latitude was not within the range -90 to 90.");
			}
		}

		double degrees = (int) ddCoordinate;
		double decimalMinutes = 60 * Math.abs(ddCoordinate - degrees);
		double minutes = (int) decimalMinutes;
		double decimalSeconds = (decimalMinutes - minutes) * 60;

		if (isLongitude) {
			dmsString = String.format("%03d", (int) degrees);
		} else {
			dmsString = String.format("%02d", (int) degrees);
		}

		dmsString =
				dmsString +
						String.format("%1$02d%2$05.2f", (int) minutes,
								decimalSeconds) + direction;
		return dmsString;
	}

	/**
	 * Convert Decimal Latitude and Decimal Longitude to Military Grid Reference
	 * System String Examples:
	 * <ul>
	 * <li>38.155719</li>
	 * <li>-077.585600</li>
	 * </ul>
	 * 
	 * @param pLatitude
	 *            Latitude as Decimal Degrees
	 * @param pLongitude
	 *            Longitude as Decimal Degrees
	 * @return
	 */
	public String DDtoMGRS(double pLatitude, double pLongitude) {

		Angle latitude = Angle.fromDegrees(pLatitude);
		Angle longitude = Angle.fromDegrees(pLongitude);
		return MGRSCoord.fromLatLon(latitude, longitude).toString();

	}

	/**
	 * Convert Military Grid Reference System String to Decimal Latitude and
	 * Decimal Longitude Examples:
	 * <ul>
	 * <li>12SVG 00476 06553</li>
	 * </ul>
	 * 
	 * @param pMgrs
	 *            Military Grid Reference System String
	 * @return
	 */
	public double[] MGRStoDD(String pMgrs) {

		MGRSCoord coord = MGRSCoord.fromString(pMgrs);
		return new double[] { coord.getLatitude().degrees,
				coord.getLongitude().degrees };

	}
	
	/**
	 * Tests a Lat long to make sure it is valid
	 * @param pLat
	 * @param pLon
	 * @return
	 */
	public boolean isDDLatLonValid(double pLat, double pLon) {
		return ((pLat <= 90.0) && (pLat >= -90.0) && (pLon <= 180.0) && (pLon >= -180.0));
	}
        
    /**
     * Convert Decimal Latitude and Decimal Longitude to UTM String Examples:
     * <ul>
     * <li>38.155719</li>
     * <li>-077.585600</li>
     * </ul>
     *
     * @param latitude Latitude as Decimal Degrees
     * @param longitude Longitude as Decimal Degrees
     * @return
     */
    public String DDtoUTM(double latitude, double longitude) throws IllegalArgumentException {
        Angle lat = Angle.fromDegrees(latitude);
        Angle lon = Angle.fromDegrees(longitude);
        return UTMCoord.fromLatLon(lat, lon).toString();
    }
    
    /**
     * Convert Universal Transverse Mercator String to Decimal Latitude and
     * Decimal Longitude Examples:
     * <ul>
     * <li>17 S 00476 06553</li>
     * </ul>
     *
     * @param utm Universal Transverse Mercator String
     * @return
     */
    public double[] UTMToDD(String utm) throws NumberFormatException{
        String[] pieces = utm.split(" ");
        int zone = Integer.parseInt(pieces[0]);
        String latZone = pieces[1];
        String hemisphere = getHemisphere(latZone);
        double easting;
        char suffix = pieces[2].charAt(pieces[2].length() - 1);
        
        if (!Character.isDigit(suffix)) {
            easting = Double.parseDouble(pieces[2].substring(0, pieces[2].length() - 1));
        } else {
            easting = Double.parseDouble(pieces[2]);
        }
        double northing;
        suffix = pieces[3].charAt(pieces[3].length() - 1);
        if (!Character.isDigit(suffix)) {
            northing = Double.parseDouble(pieces[3].substring(0, pieces[3].length() - 1));
        } else {
            northing = Double.parseDouble(pieces[3]);
        }

        UTMCoord utmC = UTMCoord.fromUTM(zone, hemisphere, easting, northing);
        return new double[]{utmC.getLatitude().degrees, utmC.getLongitude().degrees};
    }

    /**
     * Get hemisphere from zone for UTM conversion.
     *
     * @param latZone
     * @return
     */
    private String getHemisphere(String latZone) {
        String hem = "";
        switch (latZone) {
            case "A":
            case "B":
            case "C":
            case "D":
            case "E":
            case "F":
            case "G":
            case "H":
            case "J":
            case "K":
            case "L":
            case "M":
                hem = AVKey.SOUTH;
                break;
            default:
                hem = AVKey.NORTH;
        }
        return hem;
    }
}

// UNCLASSIFIED
