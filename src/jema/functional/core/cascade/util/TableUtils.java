/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util;

import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Length;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.common.types.table.sql.DataSourceFactory;
import jema.common.types.util.CommonVocabDataGenerator;
import jema.common.types.util.CommonVocabDatabaseProperties;
import jema.functional.api.ExecutionContext;
import jema.functional.api.ExecutionContextImpl;
import jema.functional.core.cascade.util.geo.PostgisUtil;

/**
 * TableUtils:  This class is a helper class for CV_Table.  
 * 
 * @author CASCADE
 */
public class TableUtils 
{
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(TableUtils.class.getName());

	/** Data generator to generate test data */
	private static final CommonVocabDataGenerator cvDataGenerator = new CommonVocabDataGenerator();
	
	/** Error margin */
	public final static Double EPSILON = 1.0e-06;
		
	/**
	 * Given a Type return the CV_Type
	 * String -> CV_String... etc
	 * @param pType
	 * @return
	 */
	public static Class<?> getCvTypeForJavaType(Object pType) {
		
		Class<?> result = pType.getClass();
		
		if (pType instanceof String)
			result = CV_String.class;
		else if (pType instanceof Integer)
			result =  CV_Integer.class;
		else if (pType instanceof Double)
			result =  CV_Decimal.class;

		return result;

	}
	/**
	 * Given a table and an index return the type of the CV_Super, this can be used for filtering or such
	 * @param pIndex
	 * @param pInputTable
	 * @return
	 */
	public static CV_Super<?> getCV_ColumnDataType(int pIndex, CV_Table pInputTable) {
		
		CV_Super<?> resultClass = null;
		
		try {
			CV_Super<?> myType = pInputTable.stream().findFirst().get().get(pIndex);
			resultClass = myType;
		} catch (TableException e) {
			return null;
		}
		
		return resultClass;	
	}

	/**
	 * Compare input Common Vocabulary data of different types.
	 * @param col1 Common vocabulary data 1
	 * @param col2 Common vocabulary data 2
	 * @return true=data is equal, false=otherwise
	 */
	public static boolean equalColumnAlt(CV_Super<?> col1, CV_Super<?> col2)
	{
		boolean result = false;

		if(col1 instanceof CV_String && col2 instanceof CV_Integer)
		{
			CV_Integer valNum = (CV_Integer) col2;
			CV_String valStr = new CV_String(valNum.value().toString());
			result = cvDataGenerator.equalColumn(col1, valStr);
		}
		else if(col1 instanceof CV_Integer && col2 instanceof CV_String)
		{
			CV_Integer valNum = (CV_Integer) col1;
			CV_String valStr = new CV_String(valNum.value().toString());
			result = cvDataGenerator.equalColumn(valStr, col2);
		}
		else if(col1 instanceof CV_Decimal && col2 instanceof CV_Decimal)
		{
			double val1 = ((CV_Decimal) col1).value();
			double val2 = ((CV_Decimal) col2).value();
			if(col1.equals(col2)) result = true;
			else if(val1 == 0 || val2 == 0) {result = (Math.abs(val1 - val2) < EPSILON);}
			else {result = (Math.abs(val1/val2 - 1) < EPSILON);} // IBM recommended
		}
		else
		{
			result = cvDataGenerator.equalColumn(col1, col2);
		}
		return result;
	}

	/**
	 * Compare double values within tolerance.
	 * @param a value 1
	 * @param b value 2
	 * @param epsilon comparision tolerance (typically 1.0e-6)
	 * @return true=data is equal, false=otherwise
	 */
	public static boolean equalDecimal(Double a, Double b, Double epsilon)
	{
		boolean result = false;

		if(a == null && b == null) {result = true;}
		else if(a == null || b == null) {result = false;}
		else if(a.equals(b)) {result = true;}
		else if(a == 0 || b == 0) {result = (Math.abs(a - b) < epsilon);}
		else {result = (Math.abs(a/b - 1) < epsilon);} // IBM recommended
		return result;
	}

	/**
	 * Compare query results to test comparison data.
	 * @param methodName test case name
	 * @param uri Table URI
	 * @param destTable Destination table containing computed data
	 * @param testData Test comparison data
	 * @return true=destination table equals test data, false=otherwise
	 * @throws Exception Table comparison error
	 */
	@SuppressWarnings("rawtypes")
	public static boolean compareTable(
		String methodName,
		URI uri,
		CV_Table destTable,
		List<List<CV_Super>> testData) throws Exception
	{
		boolean result = (destTable != null);

		try
		{
			if((destTable != null) && (testData != null))
			{
				int testTableRowSize = testData.size();
				int destTableRowSize = destTable.size();
				result = (testTableRowSize == destTableRowSize);

				for(int iRow = 0; iRow < destTableRowSize && result; iRow++)
				{
					List<CV_Super> testRow = testData.get(iRow);
					List<CV_Super> destRow = destTable.readRow();

					int testTableColSize = testRow.size();
					int destTableColSize = destRow.size();
					result = (testTableColSize == destTableColSize);

					for(int iCol = 0; iCol < destTableColSize && result; iCol++)
					{
						CV_Super<?> testCell = testRow.get(iCol);
						CV_Super<?> destCell = destRow.get(iCol);
						if(testCell == null && destCell == null) {result = true;}
						else if(testCell == null || destCell == null) {result = false;}
						else {result = equalColumnAlt(testCell, destCell);}

						if(!result)
						{
							String msg = String.format("%s (%s) - Table compare failed: testVal=%s, destVal=%s", methodName, uri.toString(), testRow.get(iCol), destRow.get(iCol));
							LOGGER.log(Level.WARNING, msg);
						}
					}  
				}
			}
		}
		catch(Exception e)
		{
			String msg = String.format("%s (%s) - Table compare failed: %s", methodName, uri.toString(), e.getMessage());
			LOGGER.log(Level.WARNING, msg);
			throw e;
		}

		return result;
	}
	
	/**
	 * Execute SQL statement
	 * @param uri Database URI
     * @param dbProperties Database connection properties
	 * @param sql SQL statement
	 */
	public static void executeQuery(
		URI uri,
		CommonVocabDatabaseProperties dbProperties,	
		String sql)
	{
		try
		{
	        Connection connection = DataSourceFactory.getConnection(
	        	uri,
	        	dbProperties);
	        Statement statement = connection.createStatement();
	        statement.executeUpdate(sql);
	        statement.close();
	        connection.close();
		}
		catch(RuntimeException e) {throw e;}
		catch(SQLException e) {throw new RuntimeException(e.toString(), e);}
		catch(Exception e){throw new RuntimeException(e.toString(), e);}
	}

    /**
     * Print common vocabulary table.
     *
     * @param table CV table
     * @param logLevel Logger level (default FINER)
     * @param label row label
     */
 	public static void printData(CV_Table table, Level logLevel, String label) 
    {
    	TableUtils.printData(table, logLevel, label, null);
    }

    /**
     * Print common vocabulary table.
     *
     * @param table CV table
     * @param logLevel Logger level (default FINER)
     * @param label row label
     * @param maxNumRow Maximum number of rows to print, if null, then print all rows
     */
    @SuppressWarnings("rawtypes")
	public static void printData(CV_Table table, Level logLevel, String label, Integer maxNumRow) 
    {
        Level level = (logLevel == null) ? Level.FINER : logLevel;

        try 
        {
            CV_Table tableRead = (table == null) ? null : table.toReadable();
            if (tableRead != null) 
            {
                List<CV_Super> rowTable = new ArrayList<CV_Super>();
                int sizeTable = (tableRead == null) ? 0 : tableRead.size();
                String printLabel = 
                	(label == null) || label.isEmpty() ? 
                	String.format("Table [%s]", tableRead.getTableId()) : 
                	label;

                StringBuilder sb = new StringBuilder();
                sb.append("\n").append(
                	String.format("%s: Table [%s] size = %d\n", printLabel, tableRead.getTableId(), sizeTable));
                
                TableHeader tableHeader = table.getHeader();
                sb.append(String.format("%s: %s\n", printLabel, tableHeader.toString()));

                int numRow = (maxNumRow == null) ? sizeTable : (sizeTable < maxNumRow) ? sizeTable : maxNumRow;
                for (int iRow = 0; iRow < numRow; iRow++) {
                    tableRead.readRow(rowTable);
                    sb.append(String.format("%s: [%2d] [%s]\n", printLabel, iRow, rowTable));
                }

                LOGGER.log(level, sb.toString());

                if (tableRead != null) {
                    tableRead.close();
                    tableRead = null;
                }
            }
        } 
        catch (Exception e) 
        {
            String msg = String.format("Debug print error: %s", e.getMessage());
            LOGGER.log(level, msg);
        }
    }
    
    public static ExecutionContext executionContext()
    {
    	ExecutionContext ctx = null;
    	
    	// Open context associated with PostgreSQL database
    	if(PostgisUtil.isPostgreSQLSelected())
    	{
    		ctx = new ExecutionContextImpl(
    			CommonVocabDatabaseProperties.DatabasePropertiesKey.DATABASE.getPostgresValue());
    	}
    	
    	// Open context associated with HSQL database
    	else
    	{
    		ctx = new ExecutionContextImpl();				
    	}
    	return ctx;

    }

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colNameList Candidate column name
	 * @param minListSize The minimum list size (default: 2)
	 * @param sb String builder containing list of error messages
	 * @param parameterTypeList List of valid parameter types
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	public static Boolean validateColumnNameList(
			CV_Table srcTable,
			List<CV_String> colNameList,
			Integer minListSize,
			StringBuilder sb,
			ParameterType[] parameterTypeList,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		if(colNameList == null)
		{
			String msg = String.format("Column name list is null");
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(colNameList.isEmpty())
		{
			String msg = String.format("Column name list is empty");
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(colNameList.size() < minListSize)
		{
			String msg = String.format(
					"Column name list must contain 2 or more names or at least 1 name and a constant for products");
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(srcTable != null)
		{
			for(CV_String colName : colNameList)
			{
				isValid = TableUtils.validateColumnName(srcTable, colName, sb, "Source", parameterTypeList, isValid);
			}
		}

		return isValid;
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colName Candidate column name
	 * @param sb String builder containing list of error messages
	 * @param valName Input value name
	 * @param parameterTypeList List of valid parameter types
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	public static Boolean validateColumnName(
			CV_Table srcTable,
			CV_String colName,
			StringBuilder sb,
			String valName,
			ParameterType[] parameterTypeList,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		if(colName == null)
		{
			String msg = String.format("%s Column name is null", (valName == null ? "" : valName));
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(colName.isEmpty())
		{
			String msg = String.format("%s Column name is empty", (valName == null ? "" : valName));
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(srcTable != null)
		{
			TableHeader tableHeader = srcTable.getHeader();
			String name = (colName == null) ? null : colName.value();
			int iCol = (name == null) ? -1 : tableHeader.findIndex(name);

			if(iCol == -1)
			{
				String msg = String.format(
						"Invalid %s Column name (%s)", (valName == null ? "" : valName), name);
				sb.append(msg).append("\n");
				isValid = false;									
			}
			else if(parameterTypeList != null)
			{
				ColumnHeader columnHeader = tableHeader.getHeader(iCol);
				ParameterType parameterType = columnHeader.getType();
				boolean validType = false;
				StringBuilder sbParam = new StringBuilder();

				for(int iParam = 0; iParam < parameterTypeList.length && !validType; iParam++)
				{
					ParameterType type = parameterTypeList[iParam];
					validType = parameterType.equals(type);
					sbParam.append(",").append(type.name());
				}

				if(!validType)
				{
					String msg = String.format(
							"Invalid %s Column type name=%s, type=%s\nValid types are: %s", 
							(valName == null ? "" : valName), 
							name, 
							parameterType,
							sbParam.substring(1));
					sb.append(msg).append("\n");
					isValid = false;	
				}
			}
		}

		return isValid;
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colName Candidate column name
	 * @param sb String builder containing list of error messages
	 * @param valName Name of candidate column name
	 * @param parameterType Column parameter type
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	public static Boolean validateColumnName(
		CV_Table srcTable,
		CV_String colName,
		StringBuilder sb,
		String valName,
		ParameterType parameterType,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		if(colName == null)
		{
			String msg = String.format("%s Column name is null", (valName == null ? "" : valName));
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(colName.isEmpty())
		{
			String msg = String.format("%s Column name is empty", (valName == null ? "" : valName));
			sb.append(msg).append("\n");
			isValid = false;
		}

		else if(srcTable != null)
		{
			TableHeader tableHeader = srcTable.getHeader();
			String name = (colName == null) ? null : colName.value();
			int iCol = (name == null) ? -1 : tableHeader.findIndex(name);

			if(iCol == -1)
			{
				String msg = String.format(
						"Invalid %s Column name (%s)", (valName == null ? "" : valName), name);
				sb.append(msg).append("\n");
				isValid = false;									
			}
			else if(parameterType != null)
			{
				ColumnHeader columnHeader = tableHeader.getHeader(iCol);
				ParameterType type = columnHeader.getType();
				boolean validType = false;
				StringBuilder sbParam = new StringBuilder();

				validType = parameterType.equals(type);
				sbParam.append(",").append(type.name());

				if(!validType)
				{
					String msg = String.format(
							"Invalid %s Column type: name = %s, type = %s\nValid types are: %s", 
							(valName == null ? "" : valName), 
							name, 
							parameterType,
							sbParam.substring(1));
					sb.append(msg).append("\n");
					isValid = false;	
				}
			}
		}

		return isValid;
	}

	/**
	 * Validate parameter existence, parameter type, and content.
	 * 
	 * @param value Source parameter
	 * @param sb String builder containing list of error messages
	 * @param valName Input value name
	 * @param parameterType Valid parameter types
	 * @param minValue Valid minimum value
	 * @param maxValue Valid maximum value
	 * @param validValues List of valid input values
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Parameter value is valid, false=otherwise
	 */
	@SuppressWarnings("rawtypes")
	public static Boolean validateNumericContent(
		CV_Super value,
		StringBuilder sb,
		String valName,
		ParameterType parameterType,
		Double minValue,
		Double maxValue,
		Double[] validValues,
		Boolean isValidCur)
	{
		// Validate existence and parameter type
		Boolean isValid = isValidCur;

		if(value == null)
		{
			String msg = String.format(
					"%s value is null", 
					(valName == null ? "" : valName));
			sb.append(msg).append("\n");
			isValid = false;			
		}
		else
		{
			// Convert input integer to decimal for comparisons
			CV_Decimal decVal = null;			
			switch(parameterType)
			{
			case decimal:
				if(value instanceof CV_Decimal) {decVal = (CV_Decimal) value;}
				break;

			case length:
				if(value instanceof CV_Length) 
				{
					CV_Length lenVal=(CV_Length) value; 
					decVal = CV_Decimal.valueOf(lenVal.value());
				}
				
				break;

			case integer:
				if(value instanceof CV_Integer) 
				{
					CV_Integer intVal=(CV_Integer) value; 
					decVal = CV_Decimal.valueOf(intVal.value());
				}
				break;

			default:
				break;
			}

			// Validate input parameter content
			boolean isValidValue = false;
			if(decVal instanceof CV_Decimal)
			{
				Double val = decVal.value();
				if(validValues != null)
				{
					for(Double validVal : validValues)
					{
						CV_Decimal dVal = CV_Decimal.valueOf(validVal);
						if(decVal.equals(dVal)) {isValidValue = true;}
					}
				}
				else
				{
					isValidValue = true;
					if((minValue != null) && (maxValue != null))
					{
						if((val < minValue) || (val > maxValue)) {isValidValue = false;}					
					}
					else if((minValue != null) && (val < minValue)) {isValidValue = false;}
					else if((maxValue != null) && (val > maxValue)) {isValidValue = false;}
				}
			}

			// Invalid parameter type
			if(decVal == null)
			{
				String msg = String.format(
						"%s - Invalid parameter type. Valid type is: %s", 
						(valName == null ? "" : valName), 
						parameterType.toString());
				sb.append(msg).append("\n");
				isValid = false;				
			}

			// Invalid content
			if(!isValidValue)
			{
				String msg = null;
				if(validValues != null)
				{
					StringBuilder sbList = new StringBuilder();
					for(Double validVal : validValues)
					{
						String validValueStr = 
							(parameterType.equals(ParameterType.decimal)) ?
							validVal.toString() :
							String.format("%d", validVal.intValue());
							
						sbList.append(",").append(validValueStr);
					}
					msg = String.format(
						"%s (%s) must contain one of the following value: %s", 
						(valName == null ? "Value" : valName),
						value.toString(),
						sbList.substring(1));					
				}
				else
				{
					Double msgMinValue = (minValue == null) ? Integer.MIN_VALUE : minValue;
					Double msgMaxValue = (maxValue == null) ? Integer.MAX_VALUE : maxValue;
					
					String msgMinValueStr = 
						(parameterType.equals(ParameterType.decimal)) ?
						String.format("%g", msgMinValue.doubleValue()) :
						String.format("%d", msgMinValue.intValue());
							
					String msgMaxValueStr = 
						(parameterType.equals(ParameterType.decimal)) ?
						String.format("%g", msgMaxValue.doubleValue()) :
						String.format("%d", msgMaxValue.intValue());
												
					msg = String.format(
						"%s (%s) out of range. Valid range (inclusive): %s thru %s", 
						(valName == null ? "Value" : valName),
						value,
						msgMinValueStr,
						msgMaxValueStr);
				}
				sb.append(msg).append("\n");
				isValid = false;							
			}
		}

		return isValid;
	}
}
