/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.util;

import java.util.List;
import java.util.logging.Logger;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.common.types.table.query.TableQueryExecutor;
import jema.common.types.table.query.builder.SelectBuilder;
import jema.common.types.table.query.builder.TableQueryBuilderFactory;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;

/**
 * Base table functional to specify source table, and provide source column
 * validation.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Source Table</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Destination table</li>
 * </ul>
 * <p>
 * 
 * @author CASCADE
 */
public abstract class TableFunctionalBase implements Runnable 
{   
	@Context
	public ExecutionContext ctx;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(TableFunctionalBase.class.getName());

	/** SELECT statement table alias */
	protected final static String TABLE_ALIAS1 = SelectBuilder.QB_TABLE_ALIAS+"1";

	/**
	 * Run functional to perform calculations.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// Validate input parameters
			this.validate();

			// Perform calculation
			this.doCalculation();
		} 
		catch(IllegalArgumentException e) {throw e;}
		catch(RuntimeException e) {throw e;}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}
	}

	/**
	 * Create and execute SQL statement.
	 */
	protected void doCalculation()
	{
		CV_Table table = null;

		try
		{
			// Calculate columns
			String calcColumn = this.performCalculation();

			// Select list
			StringBuilder sb = new StringBuilder();
			TableHeader tableHeader = this.getSourceTable().getHeader();
			for(ColumnHeader columnHeader : tableHeader.asList())
			{
				String colName = columnHeader.getName();
				sb.append(",").append(colName);
			}
			String selectList = sb.substring(1);
			
			// Calculation list
			String calcSelectList = (calcColumn == null || calcColumn.isEmpty()) ? "" : String.format(", %s", calcColumn);

			// SELECT statement
			String selectStatement = String.format(
				"SELECT %s %s FROM %s",
				selectList,
				calcSelectList,
				TABLE_ALIAS1);

			// Build query 
			TableQueryBuilderFactory factory = new TableQueryBuilderFactory();
			SelectBuilder selectBuilder = factory.selectBuilder();
			selectBuilder.tableAs(this.getSourceTable(), TABLE_ALIAS1).spec(CV_String.valueOf(selectStatement));
			table = TableQueryExecutor.executeQuery(selectBuilder);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		this.setDestinationTable(table);
	}

	/**
	 * Validate required input parameters.
	 * 
	 * @return true=Required input parameters are valid, false=otherwise
	 */
	protected Boolean validate()
	{
		Boolean isValid = true;
		StringBuilder sb = new StringBuilder("\n");

		// Session context
		if(this.ctx == null)
		{
			sb.append("Context is null").append("\n");
			isValid = false;
		}

		// Source table
		isValid = this.validateSourceTable(this.getSourceTable(), true, sb, isValid);

		// Validate column names and associated parameter types
		isValid = this.validateInputParameters(this.getSourceTable(), sb, isValid);

		if(!isValid)
		{
			String msg = String.format("Required input parameters INVALID: %s", sb.toString());
			throw new IllegalArgumentException(msg);
		}

		return isValid;
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colNameList Candidate column name
	 * @param minListSize The minimum list size (default: 2)
	 * @param sb String builder containing list of error messages
	 * @param parameterTypeList List of valid parameter types
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	protected Boolean validateColumnNameList(
			CV_Table srcTable,
			List<CV_String> colNameList,
			Integer minListSize,
			StringBuilder sb,
			ParameterType[] parameterTypeList,
			Boolean isValidCur)
	{
		Boolean isValid = TableUtils.validateColumnNameList(
			srcTable,
			colNameList,
			minListSize,
			sb,
			parameterTypeList,
			isValidCur);

		return isValid;
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colName Candidate column name
	 * @param sb String builder containing list of error messages
	 * @param valName Input value name
	 * @param parameterTypeList List of valid parameter types
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	protected Boolean validateColumnName(
			CV_Table srcTable,
			CV_String colName,
			StringBuilder sb,
			String valName,
			ParameterType[] parameterTypeList,
			Boolean isValidCur)
	{
		Boolean isValid = TableUtils.validateColumnName(
			srcTable,
			colName,
			sb,
			valName,
			parameterTypeList,
			isValidCur);

		return isValid;
	}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param colName Candidate column name
	 * @param sb String builder containing list of error messages
	 * @param valName Name of candidate column name
	 * @param parameterType Column parameter type
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name list is valid, false=otherwise
	 */
	protected Boolean validateColumnName(
		CV_Table srcTable,
		CV_String colName,
		StringBuilder sb,
		String valName,
		ParameterType parameterType,
		Boolean isValidCur)
	{
		Boolean isValid = TableUtils.validateColumnName(
			srcTable,
			colName,
			sb,
			valName,
			parameterType,
			isValidCur);

		return isValid;
	}

	/**
	 * Validate the existence of the input source table.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param srcTableRequired true=source table is required input, false=otherwise
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Source table exists, false=otherwise
	 */
	protected Boolean validateSourceTable(
			CV_Table srcTable,
			Boolean srcTableRequired,
			StringBuilder sb,
			Boolean isValidCur)
	{
		Boolean isValid = isValidCur;

		if(srcTableRequired && (srcTable == null))
		{
			sb.append("Source table is null").append("\n");
			isValid = false;
		}

		return isValid;
	}

	/**
	 * Validate parameter existence, parameter type, and content.
	 * 
	 * @param value Source parameter
	 * @param sb String builder containing list of error messages
	 * @param valName Input value name
	 * @param parameterType Valid parameter types
	 * @param minValue Valid minimum value
	 * @param maxValue Valid maximum value
	 * @param validValues List of valid input values
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Parameter value is valid, false=otherwise
	 */
	@SuppressWarnings("rawtypes")
	protected Boolean validateNumericContent(
		CV_Super value,
		StringBuilder sb,
		String valName,
		ParameterType parameterType,
		Double minValue,
		Double maxValue,
		Double[] validValues,
		Boolean isValidCur)
	{
		// Validate existence and parameter type
		Boolean isValid = TableUtils.validateNumericContent(
			value,
			sb,
			valName,
			parameterType,
			minValue,
			maxValue,
			validValues,
			isValidCur);

		return isValid;
	}
	
	/**
	 * Update the header parameter types if necessary.
	 * 
	 * @param table Destination table
	 */
	protected void updateHeader(CV_Table table) {}

	/**
	 * Perform calculations.
	 */
	protected String performCalculation() {return null;}

	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	protected abstract Boolean validateInputParameters(
			CV_Table srcTable,
			StringBuilder sb,
			Boolean isValidCur);

	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	protected abstract void setDestinationTable(CV_Table destTable);

	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	protected abstract CV_Table getSourceTable();
}
