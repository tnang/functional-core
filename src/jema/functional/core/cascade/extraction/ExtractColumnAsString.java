/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jul 13, 2015 8:40:53 AM
 */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Compiles the column values into a single string of values."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Extract Column As String"),
        uuid = "595467ff-fbf3-4500-8f64-82ef9d7f6a4c",
        tags = {"extract", "column", "string", "table"}
)
public final class ExtractColumnAsString implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(name = @CAPCO("Table"),
            desc = @CAPCO("The table containing the column to extract."),
            required = true
    )
    public CV_Table inputTable;

    @Input(name = @CAPCO("Column Name"),
            desc = @CAPCO("Column name to extract."),
            required = true
    )
    public CV_String columnName;

    @Input(name = @CAPCO("Separator"),
            desc = @CAPCO("Separator character(s) for the output string."),
            required = true
    )
    public CV_String separator = new CV_String(",");

    @Input(name = @CAPCO("Remove Duplicates?"),
            desc = @CAPCO("Remove all duplicate items from the extracted column."),
            required = false
    )
    public CV_Boolean dedup = new CV_Boolean(false);

    @Input(name = @CAPCO("Case Sensitive?"),
            desc = @CAPCO("Ignore case when removing duplicate items."),
            required = false
    )
    public CV_Boolean caseSensitive = new CV_Boolean(true);

    @Output(name = @CAPCO("String of column values"),
            desc = @CAPCO("String containing all column values separated by the separator character.")
    )
    public CV_String result;

    @Override
    public void run() {

        try {
            int columnIndex = findColumnIndex(columnName.value());
            List<String> columnValues = new ArrayList<>();

            //For each row in the table add the elements from the column
            inputTable.stream().forEach(row -> {
                if (row.get(columnIndex) != null) {
                    columnValues.add(row.get(columnIndex).toString());
                } else {
                    columnValues.add("");
                }
            });

            String columnString;

            //Get all column values
            if (!dedup.value()) {
                columnString = columnValues.stream().collect(Collectors.joining(separator.value()));
            } //Get unique column values
            else {
                if (caseSensitive.value()) {
                    columnString = columnValues.stream().distinct().collect(Collectors.joining(separator.value()));
                } else {
                    //Remove duplicates - ignore case and order
                    Set<String> toRetain = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                    toRetain.addAll(columnValues);

                    //Remove duplicates - preserve case and order
                    Set<String> set = new LinkedHashSet<>(columnValues);

                    //Interesction - preserve order and ignore case
                    set.retainAll(new LinkedHashSet<>(toRetain));

                    columnString = set.stream().collect(Collectors.joining(separator.value()));
                }
            }

            result = new CV_String(columnString);
        } catch (Exception ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private int findColumnIndex(String column) {
        int index = inputTable.getHeader().findIndex(column);
        if (index == -1) {
            throw new RuntimeException("The column specified: " + column + " was not in the JEMA Table.");
        } else {
            return index;
        }
    }
}

//UNCLASSIFIED
