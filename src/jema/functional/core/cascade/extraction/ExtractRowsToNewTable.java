/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.List;
import jema.common.types.CV_Integer;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableException;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Extract rows in the specified index range from the original table to a new table."
                + "The result is a subtable  of the original table between the specified 'Start Index', inclusive, and 'Stop Index', inclusive  or 'Length' value (1-based)."
                + "Therefore, if a table has rows (1...10) and start=1 and stop=5 ... then rows 1,2,3,4,5 will be in the returned table and if a table has values (1...10) and start=2 and length=5 ... then rows 2,3,4,5,6 will be in the returned table."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Extract Rows To New Table"),
        uuid = "a2a4b382-e51c-4cf1-8b62-193d1d0c9a1e",
        tags = {"extract", "rows", "subset", "slice"}
)

public final class ExtractRowsToNewTable implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(name = @CAPCO("Input Table"),
            desc = @CAPCO("The input table, a subset of which will be copied to a new table."),
            required = true
    )
    public CV_Table inputTable;

    @Input(desc = @CAPCO("Start Index"),
            name = @CAPCO("The Start Index (1-based) for extracting row elements. The default is 1."),
            required = false
    )
    public CV_Integer startIndex = new CV_Integer(1);

    @Input(desc = @CAPCO("Stop Index"),
            name = @CAPCO("The Stop Index (1-based) for extracting row elements. This parameter will override the Length parameter (if provided)."),
            required = false
    )
    public CV_Integer stopIndex;

    @Input(desc = @CAPCO("Length"),
            name = @CAPCO("The number of elements you wish to extract from the Start index or up to the Stop Index. This parameter is used only if the 'Stop Index' parameter is empty"),
            required = false
    )
    public CV_Integer length;

    @Output(name = @CAPCO("Result Table"),
            desc = @CAPCO("A new table containing the copied rows specified by user parameters.")
    )
    public CV_Table outputTable;

    @Override
    public void run() {

        if (stopIndex != null && startIndex != null) {
            if (startIndex.value() > stopIndex.value()) {
                String message = "The 'Start Index' value must be less than the 'Stop Index' value to extract rows from a table";
                ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
                throw new IllegalArgumentException(message);
            }
            //for instance if a table has rows (1...10) and start=1 and stop=5 ... then rows 1,2,3,4,5 will be in the returned table
            extractRows(startIndex, stopIndex);

        } else {

            if (startIndex != null && length != null) {
                //for instance if a table has values (1...10) and start=2 and length=5 ... then rows 2,3,4,5,6 will be in the returned table
                stopIndex = new CV_Integer(startIndex.value() + (length.value() - 1));
                extractRows(startIndex, stopIndex);

            }
        }

        if (outputTable == null) {

            String message = "Either the 'Stop Index' or 'Length' must be provided to extract elements from a table";
            ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
            throw new IllegalArgumentException(message);
        }

    }

    private void extractRows(CV_Integer start, CV_Integer stop) {

        int zero_adjusted_start = (int) (start.value() - 1);
        int zero_adjusted_stop = (int) (stop.value() - 1);

        try {
            outputTable = ctx.createTable(inputTable.getMetadata().getLabel());
            List<ColumnHeader> columns = new ArrayList<>();
            inputTable.getHeader().forEach(columnheader -> {
                columns.add(columnheader);
            });
            outputTable.setHeader(new TableHeader(columns));

            //Iterate over each row in the input table
            inputTable.stream().forEach(row -> {
                try {
                    int row_number = inputTable.getCurrentRowNum();

                    //row_number is zero based
                    if (row_number >= zero_adjusted_start && row_number <= zero_adjusted_stop) {
                        //System.out.println("Added "+(row_number+1)+"   "+row.toString());
                        outputTable.appendRow(row);
                    }

                    inputTable.readRow();
                } catch (TableException ex) {
                    ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception " + ex.getMessage());
                    throw new RuntimeException(ex);
                }

            });

        } catch (Exception ex) {
            ctx.log(ExecutionContext.MsgLogLevel.INFO, "Exception " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

}//UNCLASSIFIED
