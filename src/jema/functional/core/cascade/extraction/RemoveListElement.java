/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.extraction;

import java.util.List;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Removes a specific list element from a list (1-based index)."+
                "For instance if a list has values (1...5) and the remove index is 3 ... then values 1,2,4,5 will be in the returned list"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Lists" },
        name = @CAPCO("Remove List Element"),
        uuid = "6d857a63-815c-4eb8-b7e8-d55eefedb095",
        tags = {"extract","list","elements","subset","slice"}        
)

public final class RemoveListElement implements Runnable {

    
    @Context
    public ExecutionContext ctx;
    
    @Input(name = @CAPCO("Input List"),
           desc = @CAPCO("An input list to extract the element from."),
           required = true
    )
    public List<CV_String> input;
    
    @Output(name = @CAPCO("Output List"),
            desc = @CAPCO("An ouput list with an element removed.")
    )
    public List<CV_String> result;

    @Input(desc = @CAPCO("Index"),
            name = @CAPCO("The index (1-based) position of the element to remove."),
            required = false
    )
    public CV_Integer index;
    

    @Override
    public void run() {

        try {
            // -1 to modify from 1-based index to a 0-based index
            int index_int = Integer.parseInt(index.toString()) - 1;
            input.remove(index_int);
            result = input;

        } catch (IndexOutOfBoundsException e) {
            String message = "Please ensure the input index value is within the limits of the list.";
            ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
            throw e;

        }
    }
}//UNCLASSIFIED
