/**
  * UNCLASSIFIED
  * 
  * Copyright U.S. Government, all rights reserved.
  * 
  */
package jema.functional.core.cascade.extraction;

import java.util.List;

import jema.common.types.CV_Integer;
import jema.common.types.CV_String;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;


/**
 * @author CASCADE 
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Extract elements from a list based on a combination of the three following elements. 1. Start Index 2. Stop Index 3. Length (Optional). "+
                "The result is a sublist of the original list between the specified 'Start Index', inclusive, and 'Stop Index' or 'Length' value, inclusive. "+
                "Alternatively, if only a 'Stop Index' and 'Length' values are submitted, The result is a sublist of the original list between the specified 'Stop Index', inclusive, and 'Length' value, inclusive counting backwards"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Lists" },
        name = @CAPCO("Extract List Element(s)"),
        uuid = "eaaac79c-bdd8-43a1-a390-8544361956d3",
        tags = {"extract","list","elements","subset","slice"}        
)

public final class ExtractListElements implements Runnable {

    
    @Context
    public ExecutionContext ctx;
    
    @Input(name = @CAPCO("Input List"),
           desc = @CAPCO("An input list to extract elements from."),
           required = true
    )
    public List<CV_String> input;
    
    @Output(name = @CAPCO("Output List"),
            desc = @CAPCO("An ouput list of extracted elements.")
    )
    public List<CV_String> result;

    @Input(desc = @CAPCO("Start Index"),
            name = @CAPCO("The Start Index (1-based) for extracting list elements."),
            required = false
    )
    public CV_Integer startIndex;
    
    @Input(desc = @CAPCO("Stop Index"),
            name = @CAPCO("The Stop Index (1-based) for extracting list elements."),
            required = false
    )
    public CV_Integer stopIndex;
    
    @Input(desc = @CAPCO("Length"),
            name = @CAPCO("The number of elements you wish to extract from the Start index or up to the Stop Index"),
            required = false
    )
    public CV_Integer length;
    

    
    @Override
    public void run() {
 
        try{

            if(stopIndex!=null && startIndex!=null){ 
                if(startIndex.value()>stopIndex.value()){
                    String message = "The 'Start Index' value must be less than the 'Stop Index' value to extract elements from a list";
                    ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
                    throw new IllegalArgumentException(message);
                }
                //for instance if a list has values (1...10) and start=1 and stop=5 ... then values 1,2,3,4,5 will be in the returned list
                result = input.subList((startIndex.value().intValue()-1), (stopIndex.value().intValue()));

            }else if(startIndex!=null && length!=null){
                //for instance if a list has values (1...10) and start=1 and length=5 ... then values 1,2,3,4,5 will be in the returned list
                result = input.subList((startIndex.value().intValue()-1), ((startIndex.value().intValue()-1)+ (length.value().intValue())));  

            }else if(stopIndex!=null && length!=null){
                //for instance if a list has values (1...10) and stop=10 and length=5 ... then values 6,7,8,9,10 will be in the returned list
                result = input.subList((stopIndex.value().intValue()-length.value().intValue()),stopIndex.value().intValue());  

            }else{

                String message = "Two of the following three values - 'Start Index', 'Stop Index', or 'Length' must be provided to extract elements from a list";
                ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
                throw new IllegalArgumentException(message);
            }

        }catch(IndexOutOfBoundsException e){
                String message = "Please ensure the Start, Stop, and Length boundry input values are within the confines of the list.";
                ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
                throw e;
        
        }
    }
}//UNCLASSIFIED