/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 */
package jema.functional.core.cascade.extraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Super;
import jema.common.types.CV_Table;
import jema.common.types.table.ColumnHeader;
import jema.common.types.table.TableHeader;
import jema.functional.api.CAPCO;
import jema.functional.api.Context;
import jema.functional.api.ExecutionContext;
import jema.functional.api.Functional;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.SHA1;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Collect the rows that are common between two input tables."),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Data Manipulation", "Tables"},
        name = @CAPCO("Extract Common Rows From Tables"),
        uuid = "8df62dbe-c03d-4070-a650-d3a2af827d43",
        tags = {"extract", "rows", "common", "subset"}
)

public final class ExtractCommonRowsFromTables implements Runnable {

    @Context
    public ExecutionContext ctx;

    @Input(name = @CAPCO("Input Table 1"),
            desc = @CAPCO("The first input table, a common subset of which will be copied to a new table."),
            required = true
    )
    public CV_Table inputTable1;
    
    @Input(name = @CAPCO("Input Table 2"),
            desc = @CAPCO("The second input table, a common subset of which will be copied to a new table."),
            required = true
    )
    public CV_Table inputTable2;
    
    
    @Input(name = @CAPCO("Case Sensitivity?"),
            desc = @CAPCO("If True, the case letter values of each letter/word in a row will not be ignored."
                    + "If False, case will be ignored when comparing the row values."
                    +"Default is False."))
    public CV_Boolean isCaseSensitive = new CV_Boolean(false);


    @Output(name = @CAPCO("Result Table"),
            desc = @CAPCO("A new table containing the common rows from the two input tables.")
    )
    public CV_Table outputTable;

    @Override
    public void run() {
        
        //ensure that the column headers are a match...
        List<ColumnHeader> inputTable1Columns = new ArrayList<>();
        inputTable1.getHeader().forEach(columnheader -> {
            inputTable1Columns.add(columnheader);
        });

        List<ColumnHeader> inputTable2Columns = new ArrayList<>();
        inputTable2.getHeader().forEach(columnheader -> {
            inputTable2Columns.add(columnheader);
        });
        
        if (!inputTable1Columns.equals(inputTable2Columns)) {
            String message = "Error, the input tables have different columns. The two input tables must be indentical in the column structure to compare to another. ";
            ctx.log(ExecutionContext.MsgLogLevel.INFO, message);
            throw new IllegalArgumentException(message);

        }else{
            
            try {

                Map<String, List<CV_Super>> final_row_map = new HashMap<>();
                Map<String, List<CV_Super>> table1_row_map = new HashMap<>();
                Map<String, List<CV_Super>> table2_row_map = new HashMap<>();
                
                outputTable = ctx.createTable("Common Rows Table");
                outputTable.setHeader(new TableHeader(inputTable1Columns));
                
                
                
                inputTable1.stream().forEach((List<CV_Super> current_row) -> {
                    String hashed_row = createRowHash(current_row);
                        table1_row_map.put(hashed_row, current_row);                   
                 });
                
                
                inputTable2.stream().forEach((List<CV_Super> current_row) -> {
                    String hashed_row = createRowHash(current_row);        
                        table2_row_map.put(hashed_row, current_row);
                 });
                
                
                
                for (String key : table1_row_map.keySet()) {
                   if (table2_row_map.containsKey(key)) {
                        final_row_map.put(key, table1_row_map.get(key));
                        
                    }
                }
               

                for (String val : final_row_map.keySet()) {
                    outputTable.appendRow(final_row_map.get(val));
                }
            } catch (Exception ex) {
                ctx.log(ExecutionContext.MsgLogLevel.INFO, ex.getMessage());
                throw new RuntimeException(ex);
            } 
             
        }

    }

    /**
    * Returns a String SHA1 has value of the input row
    * @param  row  value of the row to hash
    * @return  String SHA1 hashed row value
    */
    public String createRowHash(List<CV_Super> row) {
        
        StringBuilder rowhash = new StringBuilder();
        
        for(CV_Super s:row){
            if(isCaseSensitive.value()){
                rowhash.append(s.value());
            }else{
                rowhash.append(s.value().toString().toLowerCase());
            }
        }

        SHA1 md = new SHA1();
        md.reset();
        md.update(rowhash.toString().getBytes());
        md.finish();
        
        return  md.digout();
    }
    

}//UNCLASSIFIED

