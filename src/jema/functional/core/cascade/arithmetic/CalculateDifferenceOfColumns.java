/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * Functional to calculate the difference of one column from another together.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>First operand column String</li>
 * <li>Second operand column String</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended column with addition results</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li>Input columns must be decimal, integer, or length parameter type</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculate the difference of columns"),
		desc = @CAPCO("Calculate the difference of one column from another together"),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Arithmetic"},
		uuid = "d82b429f-66b2-4a31-9e9a-737eab678c3c",
		tags = {"table", "difference"}
		)
public class CalculateDifferenceOfColumns extends CalculateColumnsBase 
{
    @Input(name = @CAPCO("First operand column"),
            desc = @CAPCO("First operand column.  " +
                          "Column must be decimal, integer or duration parameter type."),
            required = true
    )
    public CV_String firstOperandColumn;
    
    @Input(name = @CAPCO("Second operand column"),
            desc = @CAPCO("Second operand column.  " +
                          "Column must be decimal, integer or duration parameter type."),
            required = true
    )
    public CV_String secondOperandColumn;

	/**
	 * Perform difference calculations on table columns and put results in destination table.
	 * @return Destination table containing calculation results.
	 */
    @Override
	protected String performCalculation()
	{
		String sumColumn = null;

		try
		{
			sumColumn = String.format("(%s - %s) AS %s", 
				this.firstOperandColumn, 
				this.secondOperandColumn,
				this.outputColumnName);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return sumColumn;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		ParameterType[] parameterTypeList = {ParameterType.decimal, ParameterType.integer, ParameterType.length};

		Boolean isValid = this.validateColumnName(
			this.srcTable, 
			this.firstOperandColumn, 
            sb, 
            "Data",
            parameterTypeList,
            isValidCur);

		isValid = this.validateColumnName(
			this.srcTable, 
			this.secondOperandColumn, 
            sb, 
            "Data",
            parameterTypeList,
            isValid);
		
		return isValid;
	}
}
