/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic;

import java.util.List;

import jema.common.types.CV_Decimal;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * Functional to calculate the result of adding one or more columns together and/or
 * adding an optional constant to that result.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>List of Strings containing column names to add</li>
 * <li>Decimal constant value (optional)</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended column with addition results</li>
 * </ul>
 * <p>
 * </b>Notes:</b>
 * <ul>
 * <li>Input columns must be decimal, integer, or length parameter type</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculate the sum of columns"),
		desc = @CAPCO("Calculate the result of adding one or more columns together and/or " +
				"adding an optional constant to that result."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Arithmetic"},
		uuid = "1484af77-ba97-4aaa-833c-21d5ca9162b1",
		tags = {"table", "sum"}
		)
public class CalculateSumOfColumns extends CalculateColumnsBase 
{
    @Input(name = @CAPCO("Column name list"),
            desc = @CAPCO("List of Strings containing column names to calculate.  " +
                          "Columns must be decimal, integer or duration parameter type."),
            required = true
    )
    public List<CV_String> columnNameList;
    
    @Input(name = @CAPCO("Decimal constant value (optional)"),
            desc = @CAPCO("Decimal constant value (optional)."),
            required = false
    )
    public CV_Decimal constantValue;

	/**
	 * Perform summation calculations on table columns and put results in destination table.
	 * @return Destination table containing calculation results.
	 */
    @Override
	protected String performCalculation()
	{
		String sumColumn = null;

		try
		{
			// Summation column
			StringBuilder sb = new StringBuilder();
			for(CV_String colName : this.columnNameList)
			{
				sb.append("+").append(colName.value());
			}

			if(this.constantValue != null)
			{
				sb.append("+").append(this.constantValue.value());    			
			}

			sumColumn = String.format("(%s) AS %s", sb.substring(1), this.outputColumnName);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return sumColumn;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		ParameterType[] parameterTypeList = {ParameterType.decimal, ParameterType.integer, ParameterType.length};

		Boolean isValid = this.validateColumnNameList(
			this.srcTable, 
			this.columnNameList, 
            this.constantValue == null ? 2 : 1, 
            sb, 
            parameterTypeList,
            isValidCur);
		return isValid;
	}
}
