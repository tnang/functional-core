/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic;

import java.util.logging.Logger;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.functional.api.CAPCO;
import jema.functional.api.Input;
import jema.functional.api.Output;
import jema.functional.core.cascade.util.TableFunctionalBase;

/**
 * Base class to perform arithmetic operation for one or more columns.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>List of Strings containing column names</li>
 * <li>Decimal constant value (optional)</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended column with calculation results</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li>Input columns must be decimal, integer, or length parameter type</li>
 * </ul>
 * 
 * @author CASCADE
 */
public abstract class CalculateColumnsBase extends TableFunctionalBase 
{
	@Input(name = @CAPCO("Source table"),
			desc = @CAPCO("Source table"),
			required = true
			)
	public CV_Table srcTable;

	@Input(name = @CAPCO("Output column name"),
			desc = @CAPCO("Output column name."),
			required = true
			)
	public CV_String outputColumnName;

	@Output(name = @CAPCO("Destination table"),
			desc = @CAPCO("Table containing appended column with calculation results."),
			required = true
			)
	public CV_Table destTable;

	/** Logger */
	protected static final Logger LOGGER = Logger.getLogger(CalculateColumnsBase.class.getName());
	
	/** Margin to zero for denominator (cot, tan) */
	public final static Double EPSILON = 1.0e-09;
	
	/**
	 * Accessor to set the destination table.
	 * @param destTable Destination table
	 */
	protected void setDestinationTable(CV_Table destTable) {this.destTable = destTable;}
	
	/**
	 * Accessor to retrieve the source table.
	 * @return Source table
	 */
	protected CV_Table getSourceTable() {return this.srcTable;}
}
