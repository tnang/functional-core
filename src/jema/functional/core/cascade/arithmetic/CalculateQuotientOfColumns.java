/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic;

import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;

/**
 * Functional to calculates the results of dividing one column by another from the input table. 
 * Outputs the results to a new column.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Numerator Column name</li>
 * <li>Denominator Column name</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended column with calculation results</li>
 * </ul>
 * <p>
 * </b>Notes:</b>
 * <ul>
 * <li>Input columns must be decimal, integer, or length parameter type</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculate the quotient of columns"),
		desc = @CAPCO("Calculate the result of adding one or more columns together and/or " +
				"adding an optional constant to that result."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Arithmetic"},
		uuid = "a1913311-6dde-4dc7-b57c-e12b09e20e2f",
		tags = {"table", "quotient"}
		)
public class CalculateQuotientOfColumns extends CalculateColumnsBase 
{   
    @Input(name = @CAPCO("Numerator column name list"),
            desc = @CAPCO("Column name to be numerator for quotient calculation.  " +
                          "Column must be decimal, integer or duration parameter type."),
            required = true
    )
    public CV_String numeratorColumnName;

    @Input(name = @CAPCO("Denominator column name list"),
            desc = @CAPCO("Column name to be denominator for quotient calculation.  " +
                          "Column must be decimal, integer or duration parameter type."),
            required = true
    )
    public CV_String denominatorColumnName;

	/**
	 * Perform quotient calculations on table columns and put results in destination table.
	 * @return Destination table containing calculation results.
	 */
    @Override
	protected String performCalculation()
	{
		String calcColumn = null;

		try
		{
			// Calculation column
			calcColumn = String.format(
				"(CASE WHEN %s=0 THEN null ELSE %s/%s END) AS %s", 
				this.denominatorColumnName, 
				this.numeratorColumnName, 
				this.denominatorColumnName, 
				this.outputColumnName);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return calcColumn;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		Boolean isValid = isValidCur;
		ParameterType[] parameterTypeList = {ParameterType.decimal, ParameterType.integer, ParameterType.length};
		
		isValid = this.validateColumnName(
			this.srcTable, 
			this.numeratorColumnName, 
			sb, 
			"Numerator",
			parameterTypeList,
			isValid);
		
		isValid = this.validateColumnName(
			this.srcTable, 
			this.denominatorColumnName, 
			sb, 
			"Denominator", 
			parameterTypeList,
			isValid);
		
		return isValid;
	}
}
