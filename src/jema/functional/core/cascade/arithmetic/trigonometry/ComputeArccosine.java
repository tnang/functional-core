/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 30, 2015 7:07:19 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the arccosine of a value"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Arccosine"),
        uuid = "e6459bcd-76fd-442f-8ebd-7e3ec9e59d14",
        tags = {"arccosine", "trigonometry"}
)
public final class ComputeArccosine implements Runnable {

    @Input(name = @CAPCO("Value"),
            desc = @CAPCO("A value, where -1 <= value <= 1"))
    public CV_Decimal value;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked arccosine is in degrees, else arccosine is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Arccosine"),
            desc = @CAPCO("Arccosine of a value"))
    public CV_Decimal result;

    @Override
    public void run() {
        double arccosine = Math.acos(value.value());

        if (isDegrees.value()) {
            result = new CV_Decimal(Math.toDegrees(arccosine));
        } else {
            result = new CV_Decimal(arccosine);
        }
    }
}

//UNCLASSIFIED
