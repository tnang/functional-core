/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 17, 2015 9:47:08 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the trigonometric cotangent of an angle"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Cotangent"),
        uuid = "f7f9e9a3-95fd-40a5-823c-6058e18fb3db",
        tags = {"cotangent", "trigonometry"}
)
public final class ComputeCotangent implements Runnable {

    @Input(name = @CAPCO("Angle"),
            desc = @CAPCO("An angle"))
    public CV_Decimal angle;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked angle is in degrees, else angle is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Cotangent"),
            desc = @CAPCO("Trigonometric cotangent"))
    public CV_Decimal result;

    @Override
    public void run() {
        if (isDegrees.value()) {
            result = new CV_Decimal(1 / Math.tan(Math.toRadians(angle.value())));
        } else {
            result = new CV_Decimal(1 / Math.tan(angle.value()));
        }
    }
}

//UNCLASSIFIED
