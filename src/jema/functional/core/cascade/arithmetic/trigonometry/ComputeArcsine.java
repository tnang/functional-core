/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 18, 2015 9:13:12 AM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the arcsine of a value"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Arcsine"),
        uuid = "88f2028a-c949-4739-ad98-e064c5750927",
        tags = {"arcsine", "trigonometry"}
)
public final class ComputeArcsine implements Runnable {

    @Input(name = @CAPCO("Value"),
            desc = @CAPCO("A value, where -1 <= value <= 1"))
    public CV_Decimal value;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked arcsine is in degrees, else arcsine is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Arcsine"),
            desc = @CAPCO("Arcsine of a value"))
    public CV_Decimal result;

    @Override
    public void run() {
        double arcsine = Math.asin(value.value());
        if (isDegrees.value()) {
            result = new CV_Decimal(Math.toDegrees(arcsine));
        } else {
            result = new CV_Decimal(arcsine);
        }
    }
}

//UNCLASSIFIED
