/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 15, 2015 10:17:00 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the trigonometric tangent of an angle"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Tangent"),
        uuid = "df8d4640-94a3-49ac-8c9b-b07e9d37ff03",
        tags = {"tangent", "trigonometry"}
)
public final class ComputeTangent implements Runnable {

    @Input(name = @CAPCO("Angle"),
            desc = @CAPCO("An angle"))
    public CV_Decimal angle;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked angle is in degrees, else angle is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Tangent"),
            desc = @CAPCO("Trigonometric tangent"))
    public CV_Decimal result;

    @Override
    public void run() {
        if (isDegrees.value()) {
            result = new CV_Decimal(Math.tan(Math.toRadians(angle.value())));
        } else {
            result = new CV_Decimal(Math.tan(angle.value()));
        }
    }
}

//UNCLASSIFIED
