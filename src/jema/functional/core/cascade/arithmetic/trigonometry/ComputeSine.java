/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * May 29, 2015 5:08:42 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the trigonometric sine of an angle"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Sine"),
        uuid = "70f6b4d7-ab79-4608-ba01-b3c7a4d1f783",
        tags = {"sine", "trigonometry"}
)
public final class ComputeSine implements Runnable {

    @Input(name = @CAPCO("Angle"),
            desc = @CAPCO("An angle"))
    public CV_Decimal angle;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked angle is in degrees, else angle is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Sine"),
            desc = @CAPCO("Trigonometric sine"))
    public CV_Decimal result;

    @Override
    public void run() {
        if (isDegrees.value()) {
            result = new CV_Decimal(Math.sin(Math.toRadians(angle.value())));
        } else {
            result = new CV_Decimal(Math.sin(angle.value()));
        }
    }
}

//UNCLASSIFIED
