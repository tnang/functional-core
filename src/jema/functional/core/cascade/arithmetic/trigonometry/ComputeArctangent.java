/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 30, 2015 9:20:36 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the arctangent of a value"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Arctangent"),
        uuid = "f6d2d74d-489a-4ef6-8ecc-dd5376c599c0",
        tags = {"arctangent", "trigonometry"}
)
public final class ComputeArctangent implements Runnable {

    @Input(name = @CAPCO("Value"),
            desc = @CAPCO("A value"))
    public CV_Decimal value;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked arctangent is in degrees, else arctangent is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Arctangent"),
            desc = @CAPCO("Arctangent of a value"))
    public CV_Decimal result;

    @Override
    public void run() {
        double arctangent = Math.atan(value.value());

        if (isDegrees.value()) {
            result = new CV_Decimal(Math.toDegrees(arctangent));
        } else {
            result = new CV_Decimal(arctangent);
        }
    }
}

//UNCLASSIFIED
