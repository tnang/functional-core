/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 *
 * Jun 10, 2015 7:41:38 PM
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.api.Output;

/**
 * @author CASCADE
 */
@Functional(
        creator = "CASCADE",
        desc = @CAPCO("Returns the trigonometric cosine of an angle"),
        lifecycleState = LifecycleState.TESTING,
        displayPath = {"Arithmetic", "Trigonometry"},
        name = @CAPCO("Compute Cosine"),
        uuid = "a78b4ce0-1db9-469b-8bc6-9ce62d395eb4",
        tags = {"cosine", "trigonometry"}
)
public final class ComputeCosine implements Runnable {

    @Input(name = @CAPCO("Angle"),
            desc = @CAPCO("An angle"))
    public CV_Decimal angle;

    @Input(name = @CAPCO("In degrees?"),
            desc = @CAPCO("If checked angle is in degrees, else angle is in radians"))
    public CV_Boolean isDegrees;

    @Output(name = @CAPCO("Cosine"),
            desc = @CAPCO("Trigonometric cosine"))
    public CV_Decimal result;

    @Override
    public void run() {
        if (isDegrees.value()) {
            result = new CV_Decimal(Math.cos(Math.toRadians(angle.value())));
        } else {
            result = new CV_Decimal(Math.cos(angle.value()));
        }
    }
}

//UNCLASSIFIED
