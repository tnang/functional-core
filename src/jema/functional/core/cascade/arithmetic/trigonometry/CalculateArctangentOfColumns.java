/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.core.cascade.arithmetic.CalculateColumnsBase;

/**
 * Functional to calculate the arctangent of a given column in the input table. 
 * Outputs the result to a new column. Ensure input column is of type Decimal or Integer.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Data column Name String</li>
 * <li>Boolean Is input in degrees</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended output column with arctangent results</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li>Input columns data type must be decimal, or integer</li>
 * <li>Input columns is an unbounded range</li>
 * <li>Output column is in the range -pi/2 to pi/2, expressed in radians or degrees</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculates the arctangent of a given column"),
		desc = @CAPCO("Calculates the arctangent of a given column in the input table. " + 
		"Outputs the result to a new column. Ensure input column is of type Decimal or Integer."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Arithmetic", "Trigonometry"},
		uuid = "cde81f31-f6c9-4bf1-ab89-047d0c6ce84d",
		tags = {"table", "arctangent", "trigonometry"}
		)
public class CalculateArctangentOfColumns extends CalculateColumnsBase 
{   
    @Input(name = @CAPCO("Data column name"),
            desc = @CAPCO("Column name to calculate arctangent computations against.  " +
                          "Column must be decimal or integer parameter type."),
            required = true
    )
    public CV_String dataColumnName;
    
    @Input(name = @CAPCO("Is output data in degrees (optional)"),
            desc = @CAPCO("Is output data in degrees, true=output data is in degrees, false or null = otherwise."),
            required = false
    )
    public CV_Boolean isOutputInDegrees;

	/**
	 * Calculate the arctangent of a given column in the input table. 
     * Outputs the result to a new column. Ensure input column is of type Decimal or Integer.
	 * @return Destination table containing calculation results.
	 */
    @Override
	protected String performCalculation()
	{
		String calcColumn = null;

		try
		{
			calcColumn = this.isOutputInDegrees.value() ?
				String.format("degrees(atan(%s)) AS %s", this.dataColumnName, this.outputColumnName) :
				String.format("atan(%s) AS %s", this.dataColumnName, this.outputColumnName);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return calcColumn;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		ParameterType[] parameterTypeList = {ParameterType.decimal, ParameterType.integer};

		Boolean isValid = this.validateColumnName(
			this.srcTable, 
			this.dataColumnName, 
            sb, 
            "Data",
            parameterTypeList,
            isValidCur);
		
		return isValid;
	}
}
