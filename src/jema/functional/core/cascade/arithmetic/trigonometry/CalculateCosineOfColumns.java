/**
 * UNCLASSIFIED
 *
 * Copyright U.S. Government, all rights reserved.
 */
package jema.functional.core.cascade.arithmetic.trigonometry;

import jema.common.types.CV_Boolean;
import jema.common.types.CV_String;
import jema.common.types.CV_Table;
import jema.common.types.ParameterType;
import jema.functional.api.CAPCO;
import jema.functional.api.Functional;
import jema.functional.api.Input;
import jema.functional.api.LifecycleState;
import jema.functional.core.cascade.arithmetic.CalculateColumnsBase;

/**
 * Functional to calculate the cosine of a given column in the input table. 
 * Outputs the result to a new column. Ensure input column is of type Decimal or Integer.
 * <p>
 * <b>Inputs</b>
 * <ul>
 * <li>Table</li>
 * <li>Data column Name String</li>
 * <li>Boolean Is input in degrees</li>
 * <li>Output column name String</li>
 * </ul>
 * <p>
 * <b>Outputs</b>
 * <ul>
 * <li>Table containing appended output column with cosine results</li>
 * </ul>
 * <p>
 * <b>Notes:</b>
 * <ul>
 * <li>Input columns must be decimal, integer, or length parameter type</li>
 * </ul>
 * 
 * @author CASCADE
 */
@Functional(
		name = @CAPCO("Calculates the cosine of a given column"),
		desc = @CAPCO("Calculates the cosine of a given column in the input table. " + 
		"Outputs the result to a new column. Ensure input column is of type Decimal or Integer."),
		creator = "CASCADE",
		lifecycleState = LifecycleState.TESTING,
		displayPath = {"Arithmetic", "Trigonometry"},
		uuid = "7176b807-af20-4e64-a5a7-bc77d3452445",
		tags = {"table", "cosine", "trigonometry"}
		)
public class CalculateCosineOfColumns extends CalculateColumnsBase 
{   
    @Input(name = @CAPCO("Data column name"),
            desc = @CAPCO("Column name to calculate cosine computations against.  " +
                          "Column must be decimal or integer parameter type."),
            required = true
    )
    public CV_String dataColumnName;
    
    @Input(name = @CAPCO("Is input data in degrees (optional)"),
            desc = @CAPCO("Is input data in degrees, true=input data is in degrees, false or null = otherwise."),
            required = false
    )
    public CV_Boolean isInputInDegrees;

	/**
	 * Calculate the cosine of a given column in the input table. 
     * Outputs the result to a new column. Ensure input column is of type Decimal or Integer.
	 * @return Destination table containing calculation results.
	 */
    @Override
	protected String performCalculation()
	{
		String calcColumn = null;

		try
		{
			// Calculation column
			calcColumn = this.isInputInDegrees.value() ?
				String.format("cos(radians(%s)) AS %s", this.dataColumnName, this.outputColumnName) :
				String.format("cos(%s) AS %s", this.dataColumnName, this.outputColumnName);
		}
		catch(Exception e) {throw new RuntimeException(e.toString(), e);}

		return calcColumn;
	}
    
	/**
	 * Validate column names existence and parameter type.
	 * 
	 * @param srcTable Source table with header to validate against
	 * @param sb String builder containing list of error messages
	 * @param isValidCur The current validation status, if false do not override.
	 * @return true=Column name(s) is valid, false=otherwise
	 */
	@Override
	protected Boolean validateInputParameters(
		CV_Table srcTable,
		StringBuilder sb,
		Boolean isValidCur)
	{
		ParameterType[] parameterTypeList = {ParameterType.decimal, ParameterType.integer};

		Boolean isValid = this.validateColumnName(
			this.srcTable, 
			this.dataColumnName, 
            sb, 
            "Data",
            parameterTypeList,
            isValidCur);
		
		return isValid;
	}
}
