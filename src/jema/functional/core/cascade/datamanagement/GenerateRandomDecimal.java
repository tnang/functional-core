/*
 *  UNCLASSIFIED
 *  
 *  Copyright U.S. Government, all rights reserved.
 *  
 *  Jul 24, 2015
 */

package jema.functional.core.cascade.datamanagement;


import java.util.Random;
import jema.common.types.CV_Boolean;
import jema.common.types.CV_Decimal;
import jema.functional.api.Functional;
import jema.functional.api.CAPCO;
import jema.functional.api.LifecycleState;
import jema.functional.api.Input;
import jema.functional.api.Output;

@Functional(
        uuid = "542ce9c5-4032-4aca-a69d-2d9c9a6d0bbc",
        name = @CAPCO("Random Decimal"),
        desc = @CAPCO("Generates a random decimal value between two values, to include those values if desired."),

        displayPath = {"Data Management", "Define Data", "Generate"},
        tags = {"math", "generate", "random", "define", "double", "decimal"},

        lifecycleState = LifecycleState.TESTING,

        creator = "cascade"

)




public class GenerateRandomDecimal implements Runnable {

    @Input(
            name = @CAPCO("Low End"),
            desc = @CAPCO("Input the low end of your range"),
            required = true
    )
    public CV_Decimal input_lowEnd; 
    
    @Input(
            name = @CAPCO("High End"),
            desc = @CAPCO("Input the high end of your range"),
            required = true
    )
    public CV_Decimal input_highEnd; 

    @Input(
            name = @CAPCO("Inclusive"),
            desc = @CAPCO("True:: min <= output <= max. False:: min < output < max."),
            required = true
    )
    public CV_Boolean input_inclusive = new CV_Boolean(true); 
    
    @Output(
            name = @CAPCO("Output"),
            desc = @CAPCO("Output")
    )
    public CV_Decimal output;   

    public static double getRandomDoubleInclusive(double min, double max) {
        Random generate = new Random(System.currentTimeMillis());
        double randomNumber;
        randomNumber = min + (max - min) * generate.nextDouble();   
        return randomNumber;
    }
    
    
    //this just generates a random double
    public static double getRandomDoubleExclusive(double min, double max) {
        Random generate = new Random(System.currentTimeMillis());
        double randomNumber;
        randomNumber = min + (max - min) * generate.nextDouble();  
        
        //checks to make sure result is not part of seeds
        while (randomNumber == min || randomNumber == max) {
            randomNumber = min + (max - min) * generate.nextDouble();
        } 
        
        return randomNumber;
        
    }
    
    public double outputDouble;
    
    @Override
    public void run() {

        double min = input_lowEnd.value();
        double max = input_highEnd.value();
        
        Boolean inclusiveCheck = input_inclusive.value();
        
        if (min>=max) {
            throw new IllegalStateException("Your high end is not larger than your low end!");
        }
        
        if (inclusiveCheck) {
            outputDouble = getRandomDoubleInclusive(min, max);
            output = new CV_Decimal(outputDouble);
        } else {
            outputDouble = getRandomDoubleExclusive(min, max);
            output = new CV_Decimal(outputDouble);
        }
        
        
    }

}

// UNCLASSIFIED