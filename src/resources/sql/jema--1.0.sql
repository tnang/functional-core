-- JEMA SQL Functions

-- Create required type
DROP TYPE IF EXISTS CAS_Grid CASCADE;
CREATE TYPE CAS_Grid AS (
  geom geometry,
  box  geometry,
  hash text
);

-- Drop function is exists
DROP FUNCTION IF EXISTS CAS_Fishnet(geometry, int4);

-- Now create the function
CREATE OR REPLACE FUNCTION CAS_Fishnet(
    srcGeom geometry,
    geoHashBase int4 DEFAULT 16,
	geoHashLevel int4 DEFAULT 4)
RETURNS SETOF CAS_Grid AS
$BODY$
DECLARE
   env geometry;
   geom geometry;
   srid  int4;
   lonMin NUMERIC;
   lonMax NUMERIC;
   latMin NUMERIC;
   latMax NUMERIC;
   lon NUMERIC;
   lat NUMERIC;
   lonStep NUMERIC;
   latStep NUMERIC;
   lonOffset NUMERIC;
   parity NUMERIC;
   bitsPerLevel NUMERIC;
   grid  CAS_Grid;
BEGIN
   IF ( srcGeom IS NULL ) THEN
      geom := ST_GeomFromText('POLYGON((-180 -90, 180 -90, 180 90, -180 90, -180 -90))');
   ELSE
      geom := srcGeom;
   END IF;
    
   srid  := ST_SRID(geom);
   env  := ST_Envelope(geom);
   
   -- Lon/Lat grid cell error
   IF ((geoHashLevel % 2) = 0) THEN   
      parity := 0;
   ELSE
      parity := 1;
   END IF;
   IF (geoHashBase = 16) THEN   
      bitsPerLevel := 4;
	  lonOffset := 0;
	  parity := -1 * parity;
   ELSE
      bitsPerLevel := 5;
	  lonOffset := 1;
   END IF;
   latStep := 180.0 / POWER(2, TRUNC((geoHashLevel*bitsPerLevel-parity)/2));
   lonStep := 180.0 / POWER(2, TRUNC(((geoHashLevel*bitsPerLevel+parity)/2)-lonOffset));
   
   -- Grid Range
   lonMin := TRUNC(ST_XMIN(env));
   lonMax := CEIL(ST_XMAX(env));
   latMin := TRUNC(ST_YMIN(env));
   latMax := CEIL(ST_YMAX(env));
   lon := lonMin + lonStep / 2;
   lat := latMin + latStep / 2;
   
   -- Create grid
   WHILE lon <= lonMax LOOP
     WHILE lat <= latMax LOOP
         grid.geom := ST_SetSRID(ST_MakePoint(lon, lat), srid);
		 grid.hash := ST_GeoHash(grid.geom, geoHashLevel);
		 grid.box := ST_Box2dFromGeoHash(grid.hash, geoHashLevel)::geometry;
		 lat := lat + latStep;
         RETURN NEXT grid;
     END LOOP;
	 lon := lon + lonStep;
     lat := latMin;
   END LOOP;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


-- 2D grid of rectangular polygons (fishnet grid)
-- Reference:
-- https://trac.osgeo.org/postgis/wiki/UsersWikiCreateFishnet
-- Input parameters:
-- nrow integer  number of rows in y-direction ($1)
-- ncol integer  number of columns in x-direction ($2)
-- xsize float8  cell size length in x-direction ($3)
-- ysize float8  cell size length in y-direction ($4)
-- x0 float8 (optional)  origin offset in x-direction; DEFAULT is 0 ($5)
-- y0 float8 (optional)  origi:n offset in y-direction; DEFAULT is 0 ($6)
-- precision (optional)  Geohash Base Encoding System (16 or 32 bit); DEFAULT is 20 ($7)

-- Output parameters:
-- row integer  row number, starting from 1 at the bottom
-- col integer  column number, starting from 1 at the left
-- geom geometry;
CREATE OR REPLACE FUNCTION ST_CreateFishnet(
nrow integer,
ncol integer,
xsize float8,
ysize float8,
x0 float8 DEFAULT 0,
y0 float8 DEFAULT 0,
geoPrec integer DEFAULT 10, 
OUT "row" integer,
OUT col integer,
OUT geom text) 
RETURNS SETOF record AS 
$$ 
SELECT 
i + 1 AS row, 
j + 1 AS col, 
ST_GeoHash(ST_Centroid(ST_Translate(cell, j * $3 + $5, i * $4 + $6)), $7) AS geom 
FROM 
generate_series(0, $1 - 1) AS i, 
generate_series(0, $2 - 1) AS j, 
(SELECT ('POLYGON((0 0, 0 '||$4||', '||$3||' '||$4||', '||$3||' 0,0 0))')::geometry AS cell) AS foo; 
$$ LANGUAGE sql IMMUTABLE STRICT; 


-- Calculates the Morton number of a cell at the given row and col[umn]  
-- Written:  D.M. Mark, Jan 1984;

-- Converted to PostgreSQL, Simon Greener, 2010;
CREATE OR REPLACE FUNCTION st_morton (p_col int8, p_row int8)
RETURNS int8
AS
$$
DECLARE
   v_row          int8 := 0;
   v_col          int8 := 0;
   v_key          int8;
   v_level        int8;
   v_left_bit     int8;
   v_right_bit    int8;
   v_quadrant     int8;
BEGIN
   v_row   := p_row;
   v_col   := p_col;
   v_key   := 0;
   v_level := 0;
   WHILE ((v_row>0) OR (v_col>0)) LOOP
     /* Split off the row (left_bit) and column (right_bit) bits and
     then combine them to form a bit-pair representing the quadrant */
     v_left_bit  := v_row % 2;
     v_right_bit := v_col % 2;
     v_quadrant  := v_right_bit + 2*v_left_bit;
     v_key       := v_key + ( v_quadrant << (2*v_level) );
     /* row, column, and level are then modified before the loop continues */
     v_row := v_row / 2;
     v_col := v_col / 2;
     v_level := v_level + 1;
   END LOOP;
   RETURN (v_key);
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;

-- Create required type
DROP TYPE IF EXISTS T_Grid CASCADE;
CREATE TYPE T_Grid AS (
  gcol  int4,
  grow  int4,
  geom geometry,
  hash text
);
-- Drop function is exists
DROP FUNCTION IF EXISTS ST_RegularGrid(geometry, NUMERIC, NUMERIC, BOOLEAN);

-- Now create the function
CREATE OR REPLACE FUNCTION ST_RegularGrid(p_geometry   geometry,
                                          p_TileSizeX  NUMERIC,
                                          p_TileSizeY  NUMERIC,
                                          p_point      BOOLEAN DEFAULT TRUE,
										  p_GeoHashLevel int4 DEFAULT 8)
RETURNS SETOF T_Grid AS
$BODY$
DECLARE
   v_mbr   geometry;
   v_srid  int4;
   v_halfX NUMERIC := p_TileSizeX / 2.0;
   v_halfY NUMERIC := p_TileSizeY / 2.0;
   v_loCol int4;
   v_hiCol int4;
   v_loRow int4;
   v_hiRow int4;
   v_grid  T_Grid;
BEGIN
   IF ( p_geometry IS NULL ) THEN
      RETURN;
   END IF;
   v_srid  := ST_SRID(p_geometry);
   v_mbr   := ST_Envelope(p_geometry);
   v_loCol := trunc((ST_XMIN(v_mbr) / p_TileSizeX)::NUMERIC );
   v_hiCol := CEIL( (ST_XMAX(v_mbr) / p_TileSizeX)::NUMERIC ) - 1;
   v_loRow := trunc((ST_YMIN(v_mbr) / p_TileSizeY)::NUMERIC );
   v_hiRow := CEIL( (ST_YMAX(v_mbr) / p_TileSizeY)::NUMERIC ) - 1;
   FOR v_col IN v_loCol..v_hiCol Loop
     FOR v_row IN v_loRow..v_hiRow Loop
         v_grid.gcol := v_col;
         v_grid.grow := v_row;
         IF ( p_point ) THEN
           v_grid.geom := ST_SetSRID(
                             ST_MakePoint((v_col * p_TileSizeX) + v_halfX,
                                          (v_row * p_TileSizeY) + v_halfY),
                             v_srid);
		   v_grid.hash := ST_GeoHash(v_grid.geom, p_GeoHashLevel);
         ELSE
           v_grid.geom := ST_SetSRID(
                             ST_MakeEnvelope((v_col * p_TileSizeX),
                                             (v_row * p_TileSizeY),
                                             (v_col * p_TileSizeX) + p_TileSizeX,
                                             (v_row * p_TileSizeY) + p_TileSizeY),
                             v_srid);
         END IF;

         RETURN NEXT v_grid;
     END Loop;
   END Loop;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;
